/*
 * @(#)IBasicService.java   1.0 2015/08/18
 */
package by.epam.lab.hryshyn.task.first.service.interfaces;

import java.io.Serializable;
import java.util.List;

import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;

/**
 * This interface defines basic methods for service layer
 * @version 1.0 18 August 2015
 * @author Pavel Hryshyn
 */
public interface IBasicService<I extends Number, E extends Serializable> {
	/**
	 * This method returns a list of all entities of a certain type that stores in database
	 * @return list of all entities of a certain type from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	List<E> findAll() throws ServiceException;
	
	/**
	 * This method returns a entity of a certain type with some id that stores in database
	 * @param id
	 * 			the param uses for searching entity in database
	 * @return entity with some id from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	E findById(I id) throws ServiceException;
	
	/**
	 * This method deletes a entity with some id from database
	 * @param id
	 * 			the param uses for deleting a entity from database
	 * @return true if a entity is deleted from database, else false
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	boolean delete(I id) throws ServiceException;
	
	/**
	 * This method adds entity to database
	 * @param entity
	 * 			the param uses for adding entity to database
	 * @return entity's id if entity is added to database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	I add(E entity) throws ServiceException;
	
	/**
	 * This method updates entity to database
	 * @param entity
	 *            the param uses for updating entity to database
	 * @return true if entity is updated to database, else false
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	boolean update(E entity) throws ServiceException;
}
