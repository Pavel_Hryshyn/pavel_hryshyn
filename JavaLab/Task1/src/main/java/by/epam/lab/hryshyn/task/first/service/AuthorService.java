/*
 * @(#)AuthorService.java   1.1 2015/08/26
 */
package by.epam.lab.hryshyn.task.first.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.IAuthorDao;
import by.epam.lab.hryshyn.task.first.database.dto.Author;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;
import by.epam.lab.hryshyn.task.first.service.interfaces.IAuthorService;

/**
 * This class implements methods for Author service
 * 
 * @version 1.1 26 August 2015
 * @author Pavel Hryshyn
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AuthorService implements IAuthorService {
	/* This object obtains logger for this class*/
	private static final Logger logger = Logger.getLogger(AuthorService.class);
	
	private IAuthorDao authorDao;
	
	@Override
	public List<Author> findAll() throws ServiceException {
		List<Author> authors = null;
		try {
			authors = authorDao.findAll();
			logger.info("List of authors is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findAll() method of AuthorService is failed", e);
		}
		return authors;
	}

	@Override
	public List<Author> findAllNonExpiredAuthors() throws ServiceException {
		List<Author> authors = null;
		try {
			authors = authorDao.findAllNonExpiredAuthors();
			logger.info("List of non-expired authors is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findAllNonExpiredAuthors() method of AuthorService is failed", e);
		}
		return authors;
	}
	
	@Override
	public Author findById(Long id) throws ServiceException {
		Author author = null;
		try {
			author = authorDao.findById(id);
			logger.info("Author is founded. Id = " + id);
		} catch (DaoException e) {
			throw new ServiceException("findById() method of AuthorService is failed", e);
		}
		return author;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean delete(Long id) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = authorDao.delete(id);
			if (isDeleted) {
				logger.info("Author is deleted successfully. Id = " + id);
			} else {
				logger.debug("Author is deleted failure. Id = " +id);
			}
		} catch (DaoException e) {
			throw new ServiceException("delete() method of AuthorService is failed", e);
		}
		return isDeleted;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long add(Author author) throws ServiceException {
		long authorId = 0;
		try {
			authorId = authorDao.add(author);
			logger.info("Author is created. Id = " + authorId);
		} catch (DaoException e) {
			throw new ServiceException("add() method of AuthorService is failed", e);
		}
		return authorId;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean update(Author author) throws ServiceException {
		boolean isUpdated = false;
		try {
			isUpdated = authorDao.update(author);
			if (isUpdated) {
				logger.info("Author is updated successfully. Id = " + author.getId());
			} else {
				logger.debug("Author is updated failure. Id = " + author.getId());
			}
		} catch (DaoException e) {
			throw new ServiceException("update() method of AuthorService is failed", e);
		}
		return isUpdated;
	}

	public IAuthorDao getAuthorDao() {
		return authorDao;
	}

	public void setAuthorDao(IAuthorDao authorDao) {
		this.authorDao = authorDao;
	}
}
