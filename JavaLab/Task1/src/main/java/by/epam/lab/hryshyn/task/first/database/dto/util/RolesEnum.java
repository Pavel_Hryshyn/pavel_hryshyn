/*
 * @(#)RolesEnum.java   1.0 2015/08/11
 */
package by.epam.lab.hryshyn.task.first.database.dto.util;

/**
 * This enum stores roles that using in news application
 * @version 1.0 11 August 2015
 * @author Pavel_Hryshyn
 */
public enum RolesEnum {
	AUTHOR, ADMIN, USER;
}
