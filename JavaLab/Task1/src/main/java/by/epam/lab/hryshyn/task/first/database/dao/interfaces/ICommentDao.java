/*
 * @(#)ICommentDao.java   1.0 2015/08/13
 */
package by.epam.lab.hryshyn.task.first.database.dao.interfaces;

import java.util.List;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dto.Comment;

/**
 * This interface defines methods that bind the Comment entities with database
 * @version 1.0 13 August 2015
 * @author Pavel Hryshyn
 */
public interface ICommentDao extends IBasicDao<Long, Comment> {
	/**
	 * This method returns all comments of some news that stores in database
	 * @param id
	 * 			the param is news id and uses for searching list of comments from database
	 * @return list of all comments of some news from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<Comment> findAllByNewsId(Long id) throws DaoException;
	
	/**
	 * This method delete all comments of some news that stores in database
	 * @param newsId
	 * 			the param is news id and uses for deleting list of comments from database
	 * @return true, if comments are deleted, else return false
	 * @throws DaoException, if arise database access error or other database errors
	 */
	boolean deleteCommentsByNewsId(Long newsId) throws DaoException;
}
