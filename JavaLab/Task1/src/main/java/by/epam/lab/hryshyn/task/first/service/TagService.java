/*
 * @(#)TagService.java   1.1 2015/08/26
 */
package by.epam.lab.hryshyn.task.first.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ITagDao;
import by.epam.lab.hryshyn.task.first.database.dto.Tag;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;
import by.epam.lab.hryshyn.task.first.service.interfaces.ITagService;

/**
 * This class implements methods for Tag service
 * @version 1.1 26 August 2015
 * @author Pavel Hryshyn
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TagService implements ITagService {
	/** This object obtains logger for this class*/
	private static final Logger logger = Logger.getLogger(TagService.class);

	private ITagDao tagDao;
	
	@Override
	public List<Tag> findAll() throws ServiceException {
		List<Tag> tags = null;
		try {
			tags = tagDao.findAll();
			logger.info("List of tags is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findAll() method of TagService is failed", e);
		}
		return tags;
	}

	@Override
	public Tag findById(Long id) throws ServiceException {
		Tag tag = null;
		try {
			tag = tagDao.findById(id);
			logger.info("Tag is founded. Id = " + id);
		} catch (DaoException e) {
			throw new ServiceException("findById() method of TagService is failed", e);
		}
		return tag;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean delete(Long id) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = tagDao.delete(id);
			if (isDeleted) {
				logger.info("Tag is deleted successfully. Id = " + id);
			} else {
				logger.debug("Tag is deleted failure. Id = " +id);
			}
		} catch (DaoException e) {
			throw new ServiceException("delete() method of TagService is failed", e);
		}
		return isDeleted;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long add(Tag tag) throws ServiceException {
		long tagId = 0;
		try {
			tagId = tagDao.add(tag);
			logger.info("Tag is created. Id = " + tagId);
		} catch (DaoException e) {
			throw new ServiceException("add() method of TagService is failed", e);
		}
		return tagId;
	}

	/**
	 * This method is unsupported
	 * @throws ServiceException, if calling this method
	 */
	@Override
	public boolean update(Tag tag) throws ServiceException {
		try {
			throw new UnsupportedOperationException();
		} catch (UnsupportedOperationException e) {
			throw new ServiceException("Update() method is unsupported", e);
		}
	}

	public ITagDao getTagDao() {
		return tagDao;
	}

	public void setTagDao(ITagDao tagDao) {
		this.tagDao = tagDao;
	}
}
