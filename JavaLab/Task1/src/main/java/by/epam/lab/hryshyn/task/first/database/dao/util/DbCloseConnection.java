/*
 * @(#)DbCloseConnection.java   1.0 2015/08/31
 */
package by.epam.lab.hryshyn.task.first.database.dao.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;

/**
 * This class contains static methods for closing connection with database
 * @version 1.0 31 August 2015
 * @author Pavel Hryshyn
 */
public class DbCloseConnection {
	
	/**
	 * This method close ResultSet
	 * @param resultSet
	 * @throws DaoException, if resultSet isn't closed				
	 */
	public static void close(ResultSet resultSet) throws DaoException{
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new DaoException("ResultSet can not be close", e);
			}
		}
	}
	
	/**
	 * This method close PreparedStatement
	 * @param stmt
	 * @throws DaoException, if stmt isn't closed				
	 */
	public static void close(PreparedStatement stmt) throws DaoException{
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				throw new DaoException("Statement can not be close", e);
			}
		}
	}
	
	/**
	 * This method close Connection
	 * @param con
	 * @param dataSource
	 * @throws DaoException, if connection isn't closed				
	 */
	public static void close(Connection con, DataSource dataSource) throws DaoException{
		if (con != null) {
			try {
				DataSourceUtils.doReleaseConnection(con, dataSource);
			} catch (SQLException e) {
				throw new DaoException("Connection can not be close", e);
			}
		}
	}
	
	/**
	 * This method close Connection
	 * @param con
	 * @param stmt
	 * @param rs
	 * @param dataSource
	 * @throws DaoException, if connection isn't closed				
	 */
	public static void close(Connection con, PreparedStatement stmt, ResultSet rs, DataSource dataSource) throws DaoException{
		close(rs);
		close(stmt);
		close(con, dataSource);
	}
}
