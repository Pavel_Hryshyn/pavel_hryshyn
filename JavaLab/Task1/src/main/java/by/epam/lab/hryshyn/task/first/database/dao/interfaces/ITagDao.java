/*
 * @(#)ITagDao.java   1.0 2015/08/12
 */
package by.epam.lab.hryshyn.task.first.database.dao.interfaces;

import by.epam.lab.hryshyn.task.first.database.dto.Tag;

/**
 * This interface defines methods that bind the Tag entities with database
 * @version 1.0 12 August 2015
 * @author Pavel Hryshyn
 */
public interface ITagDao extends IBasicDao<Long, Tag> {
}
