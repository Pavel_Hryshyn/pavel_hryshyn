/*
 * @(#)ICommentService.java   1.0 2015/08/21
 */
package by.epam.lab.hryshyn.task.first.service.interfaces;

import java.util.List;

import by.epam.lab.hryshyn.task.first.database.dto.Comment;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;

/**
 * This interface defines basic methods for Comment entity on service layer
 * @version 1.0 21 August 2015
 * @author Pavel Hryshyn
 */
public interface ICommentService extends IBasicService<Long, Comment> {
	/**
	 * This method returns all comments of some news that stores in database
	 * @param id
	 * 			the param is news id and uses for searching list of comments from database
	 * @return list of all comments of some news from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	List<Comment> findAllByNewsId(Long id) throws ServiceException;
}
