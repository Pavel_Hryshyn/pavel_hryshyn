/*
 * @(#)NewsService.java   1.1 2015/08/31
 */
package by.epam.lab.hryshyn.task.first.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ICommentDao;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.INewsDao;
import by.epam.lab.hryshyn.task.first.database.dto.News;
import by.epam.lab.hryshyn.task.first.database.dto.SearchCriteria;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;
import by.epam.lab.hryshyn.task.first.service.interfaces.INewsService;

/**
 * This class implements methods for News service
 * 
 * @version 1.1 31 August 2015
 * @author Pavel Hryshyn
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class NewsService implements INewsService {
	/** This object obtains logger for this class*/
	private static final Logger logger = Logger.getLogger(NewsService.class);
	
	private INewsDao newsDao;
	private ICommentDao commentDao;

	@Override
	public List<News> findAll() throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.findAll();
			logger.info("List of news is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findAll() method of NewsService is failed", e);
		}
		return newsList;
	}
		
	@Override
	public List<News> findAllSortingByCommentAmount() throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.findAllSortingByCommentAmount();
			logger.info("List of news is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findAllSortingByCommentAmount() method of NewsService is failed", e);
		}
		return newsList;
	}

	@Override
	public List<News> findAllWithCriteria(SearchCriteria searchCriteria)
			throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.findAllWithCriteria(searchCriteria);
			logger.info("List of news is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findAllWithCriteria() method of NewsService is failed", e);
		}
		return newsList;
	}

	@Override
	public List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc,
			int start, int end) throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, end);
			logger.info("List of news is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findNewsWithCriteriaAndPositions() method of NewsService is failed", e);
		}
		return newsList;
	}

	@Override
	public News findById(Long id) throws ServiceException {
		News news = null;
		try {
			news = newsDao.findById(id);
			logger.info("News is founded. Id = " + id);
		} catch (DaoException e) {
			throw new ServiceException("findById() method of NewsService is failed", e);
		}
		return news;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean delete(Long newsId) throws ServiceException {
		boolean isDeleted = true;
		try {
			isDeleted &= newsDao.deleteNewsWithAuthor(newsId);
			isDeleted &= newsDao.deleteNewsWithTags(newsId);
			isDeleted &= commentDao.deleteCommentsByNewsId(newsId);
			isDeleted &= newsDao.delete(newsId);
			if (isDeleted) {
				logger.info("News is deleted successfully. Id = " + newsId);
			} else {
				logger.debug("News is deleted failure. Id = " + newsId);
			}
		} catch (DaoException e) {
			throw new ServiceException("delete() method of NewsService is failed", e);
		}
		return isDeleted;
	}

	@Override
	public Long add(News news) throws ServiceException {
		try {
			throw new UnsupportedOperationException();
		} catch (UnsupportedOperationException e) {
			throw new ServiceException("Add() method is unsupported", e);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean update(News news) throws ServiceException {
		boolean isUpdated = false;
		try {
			isUpdated = newsDao.update(news);
			if (isUpdated) {
				logger.info("News is updated successfully. Id = " + news.getId());
			} else {
				logger.debug("News is updated failure. Id =" + news.getId());
			}
		} catch (DaoException e) {
			throw new ServiceException("update() method of NewsService is failed", e);
		}
		return isUpdated;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long addWithAuthorAndTags(News news, Long authorId, List<Long> tagsId)
			throws ServiceException {
		long newsId = 0;
		try {
			newsId = newsDao.add(news); 
			news.setId(newsId);
			newsDao.addNewsWithAuthor(newsId, authorId);
			newsDao.addNewsWithTags(newsId, tagsId);
			logger.info("News is created. Id = " + newsId);
		} catch (DaoException e) {
			throw new ServiceException("addWithAuthorAndTags() method of NewsService is failed", e);
		}
		return newsId;
	}
	
	@Override
	public int countAllNews() throws ServiceException {
		int amount = 0;
		try {
			amount = newsDao.countAllNews();
			logger.info("Amount of news = " + amount);
		} catch (DaoException e) {
			throw new ServiceException("countAllNews() method of NewsService is failed", e);
		}
		return amount;
	}

	@Override
	public int countAllWithCriteria(SearchCriteria searchCriteria)
			throws ServiceException {
		int amount = 0;
		try {
			amount = newsDao.countAllWithCriteria(searchCriteria);
			logger.info("Amount of news = " + amount);
		} catch (DaoException e) {
			throw new ServiceException("countAllWithCriteria() method of NewsService is failed", e);
		}
		return amount;
	}

	@Override
	public int countNewsWithCriteriaAndPositions(SearchCriteria sc, int start,
			int end) throws ServiceException {
		int amount = 0;
		try {
			amount = newsDao.countNewsWithCriteriaAndPositions(sc, start, end);
			logger.info("Amount of news = " + amount);
		} catch (DaoException e) {
			throw new ServiceException("countNewsWithCriteriaAndPositions() method of NewsService is failed", e);
		}
		return amount;
	}

	public INewsDao getNewsDao() {
		return newsDao;
	}

	public void setNewsDao(INewsDao newsDao) {
		this.newsDao = newsDao;
	}

	public ICommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}
}
