/*
 * @(#)AuthorDao.java   1.1 2015/08/24
 */
package by.epam.lab.hryshyn.task.first.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.IAuthorDao;
import by.epam.lab.hryshyn.task.first.database.dao.util.DbCloseConnection;
import by.epam.lab.hryshyn.task.first.database.dto.Author;
import by.epam.lab.hryshyn.task.first.util.ConvertToSqlDate;

/**
 * This class implements DAO pattern and contains methods that bind the Author
 * DTO with database
 * 
 * @version 1.1 24 August 2015
 * @author Pavel Hryshyn
 */
public class AuthorDao implements IAuthorDao {
	/* This constants stores SQL queries for database */
	private static final String SQL_AUTHOR_ADD = "INSERT INTO author (AUTHOR_ID_PK, AUTHOR_NAME) VALUES (author_sequence.nextval, ?)";
	private static final String SQL_AUTHOR_DELETE = "UPDATE author SET author_expired = ?  WHERE author_id_pk = ?";
	private static final String SQL_AUTHOR_UPDATE = "UPDATE author SET author_name = ?  WHERE author_id_pk = ?";
	private static final String SQL_FIND_ALL = "SELECT * FROM author ORDER BY author_id_pk ASC";
	private static final String SQL_FIND_ALL_NON_EXPIRED = "SELECT * FROM author WHERE author_expired IS NULL ORDER BY author_id_pk ASC";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM author WHERE author_id_pk = ?";

	/* This constants stores names of database column */
	private static final String AUTHOR_ID = "author_id_pk";
	private static final String AUTHOR_NAME = "author_name";
	private static final String AUTHOR_EXPIRED = "author_expired";

	/* This object is factory for connection */
	private DataSource dataSource;

	@Override
	public List<Author> findAll() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Author> authorList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL);
			resultSet = stmt.executeQuery();
			authorList = buildAuthorList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAll() method of AuthorDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return authorList;
	}

	@Override
	public List<Author> findAllNonExpiredAuthors() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Author> authorList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL_NON_EXPIRED);
			resultSet = stmt.executeQuery();
			authorList = buildAuthorList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAllNonExpiredAuthors() method of AuthorDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return authorList;
	}

	@Override
	public Author findById(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		Author author = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_ID);
			stmt.setLong(1, id);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				author = buildAuthor(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException("findById() method of AuthorDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return author;
	}

	@Override
	public boolean delete(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_AUTHOR_DELETE);
			stmt.setTimestamp(1,
					ConvertToSqlDate.convertToSqlTimestamp(new Date()));
			stmt.setLong(2, id);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("delete() method of AuthorDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isDeleted;
	}

	@Override
	public Long add(Author author) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long dbIndex = 0;
		String generatedColumns[] = {AUTHOR_ID};
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_AUTHOR_ADD, generatedColumns);
			stmt.setString(1, author.getAuthorName());
			stmt.executeUpdate();
			resultSet = stmt.getGeneratedKeys();
			if (resultSet.next()) {
				dbIndex = resultSet.getLong(1);
			} else {
				throw new DaoException("Author is not saved to database");
			}
		} catch (SQLException e) {
			throw new DaoException("add() method of AuthorDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return dbIndex;
	}

	@Override
	public boolean update(Author author) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isUpdated = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_AUTHOR_UPDATE);
			stmt.setString(1, author.getAuthorName());
			stmt.setLong(2, author.getId());
			row = stmt.executeUpdate();
			if (row != 0) {
				isUpdated = true;
			}
		} catch (SQLException e) {
			throw new DaoException("update() method of AuthorDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isUpdated;
	}

	private Author buildAuthor(ResultSet rs) throws SQLException {
		/*
		 * This method builds Author object from ResultSet object
		 */
		Author author = new Author();
		author.setId(rs.getLong(AUTHOR_ID));
		author.setAuthorName(rs.getString(AUTHOR_NAME));
		author.setExpiredDate(rs.getTimestamp(AUTHOR_EXPIRED));
		return author;
	}

	private List<Author> buildAuthorList(ResultSet rs) throws SQLException {
		/*
		 * This method builds list of Author objects from ResultSet object
		 */
		List<Author> authors = new ArrayList<Author>();
		while (rs.next()) {
			authors.add(buildAuthor(rs));
		}
		return authors;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
