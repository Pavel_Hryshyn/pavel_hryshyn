/*
 * @(#)ITagService.java   1.0 2015/08/20
 */
package by.epam.lab.hryshyn.task.first.service.interfaces;

import by.epam.lab.hryshyn.task.first.database.dto.Tag;

/**
 * This interface defines basic methods for Tag entity on service layer
 * @version 1.0 20 August 2015
 * @author Pavel Hryshyn
 */
public interface ITagService extends IBasicService<Long, Tag> {
}
