/*
 * @(#)User.java   1.0 2015/08/11
 */
package by.epam.lab.hryshyn.task.first.database.dto;

import java.io.Serializable;

/**
 * This class is data transfer object and encapsulates data for
 * transfer to database
 * @version 1.0 11 August 2015
 * @author Pavel_Hryshyn
 */
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	/* This value stores user id */
	private long id;
	
	/* This value stores user name */
	private String userName;
	
	/* This value stores login */
	private String login;
	
	/* This value stores password */
	private String password;

	public User() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + id + ", userName=" + userName + ", login=" + login + "]";
	}	
}
