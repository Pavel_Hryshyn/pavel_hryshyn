/*
 * @(#)Roles.java   1.0 2015/08/11
 */
package by.epam.lab.hryshyn.task.first.database.dto;

import java.io.Serializable;

import by.epam.lab.hryshyn.task.first.database.dto.util.RolesEnum;

/**
 * This class is data transfer object and encapsulates data for
 * transfer to database
 * @version 1.0 11 August 2015
 * @author Pavel_Hryshyn
 */
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	/* This value stores user */
	private User user;
	
	/* This value stores roles enum */
	private RolesEnum role;

	public Role() {
		super();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public RolesEnum getRole() {
		return role;
	}

	public void setRole(RolesEnum role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "RolesDto [user=" + user + ", role=" + role + "]";
	}
}
