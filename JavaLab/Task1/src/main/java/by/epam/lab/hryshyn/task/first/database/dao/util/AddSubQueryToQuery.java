/*
 * @(#)AddSubQueryToQuery.java   1.1 2015/08/20
 */
package by.epam.lab.hryshyn.task.first.database.dao.util;


/**
 * This class contains methods that adding subQuery to initialQuery 
 * @version 1.1 20 August 2015
 * @author Pavel Hryshyn
 */
public class AddSubQueryToQuery {
	/** This constant stores space character */
	private static final String SPACE = " ";
	private static final String EXC_INCORRECT_NUMBER = "Incorrect number of iteration";
	private static final String OR = "OR ";

	/**
	 * This method adds subQuery to end of query
	 * @param initialQuery
	 * 					contains initial query
	 * @param subQuery
	 * 					contains subquery that adding to end of query
	 * @return the resulting query
	 */
	public static String addSubQueryToEnd(String initialQuery, String subQuery){
		String result = initialQuery + SPACE + subQuery;
		return result;
	}
	
	/**
	 * This method adds subQuery to end of query
	 * @param initialQuery
	 * 					contains initial query
	 * @param subQuery
	 * 					contains subquery that adding to end of query
	 * @return the resulting query
	 */
	public static String addIterativeSubQueryToEnd(String initialQuery, String subQuery, int iteration){
		String result = "";
		String afterIteration = "";
		if (iteration > 0){
			for (int i=0; i<iteration; i++){
				if (i==0){
					afterIteration += (subQuery + SPACE);
				} else {
					afterIteration += (OR + subQuery + SPACE);
				}
			}
			result = addSubQueryToEnd(initialQuery, afterIteration);
		} else {
			throw new ArithmeticException(EXC_INCORRECT_NUMBER);
		}
		return result;
	}
}
