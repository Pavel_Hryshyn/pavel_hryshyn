/*
 * @(#)IAuthorService.java   1.0 2015/08/20
 */
package by.epam.lab.hryshyn.task.first.service.interfaces;

import java.util.List;

import by.epam.lab.hryshyn.task.first.database.dto.Author;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;

/**
 * This interface defines basic methods for Author Entity on service layer
 * @version 1.0 20 August 2015
 * @author Pavel Hryshyn
 */
public interface IAuthorService extends IBasicService<Long, Author> {
	/**
	 * This method returns a list of all non-expired authors that stores in database
	 * @return  a list of all non-expired authors from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	List<Author> findAllNonExpiredAuthors() throws ServiceException;
}
