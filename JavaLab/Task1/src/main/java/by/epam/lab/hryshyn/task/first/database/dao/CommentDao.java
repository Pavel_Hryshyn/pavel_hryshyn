/*
 * @(#)CommentDao.java   1.1 2015/08/24
 */
package by.epam.lab.hryshyn.task.first.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ICommentDao;
import by.epam.lab.hryshyn.task.first.database.dao.util.DbCloseConnection;
import by.epam.lab.hryshyn.task.first.database.dto.Comment;
import by.epam.lab.hryshyn.task.first.database.dto.News;
import by.epam.lab.hryshyn.task.first.util.ConvertToSqlDate;

/**
 * This class implements DAO pattern and contains methods that bind the Comment DTO 
 * with database
 * @version 1.1 24 August 2015
 * @author Pavel Hryshyn
 */
public class CommentDao implements ICommentDao {
	/* This constants stores SQL queries for database */
	private static final String SQL_COMMENT_ADD = "INSERT INTO comments (comments_id_pk, news_id_fk, comments_text, comments_creation_date) VALUES (comments_sequence.nextval, ?, ?, ?)";
	private static final String SQL_COMMENT_DELETE = "DELETE FROM comments WHERE comments_id_pk=?";
	private static final String SQL_COMMENT_DELETE_BY_NEWS_ID = "DELETE FROM comments WHERE news_id_fk=?";
	private static final String SQL_COMMENT_FIND_ALL = "SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, comments.comments_id_pk, comments.news_id_fk, comments.comments_text, comments.comments_creation_date FROM comments JOIN news ON news.news_id_pk = comments.news_id_fk ORDER BY comments.comments_creation_date DESC";
	private static final String SQL_COMMENT_FIND_BY_ID = "SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, comments.comments_id_pk, comments.news_id_fk, comments.comments_text, comments.comments_creation_date FROM comments JOIN news ON news.news_id_pk = comments.news_id_fk WHERE comments.comments_id_pk = ?";
	private static final String SQL_COMMENT_FIND_ALL_BY_NEWS_ID = "SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, comments.comments_id_pk, comments.news_id_fk, comments.comments_text, comments.comments_creation_date FROM comments JOIN news ON news.news_id_pk = comments.news_id_fk WHERE news.news_id_pk = ? ORDER BY comments.comments_creation_date DESC";
			
	/* This constants stores names of database column */
	private static final String NEWS_ID = "news_id_pk";
	private static final String NEWS_TITLE = "news_title";
	private static final String NEWS_SHORT_TEXT = "news_short_text";
	private static final String NEWS_FULL_TEXT = "news_full_text";
	private static final String NEWS_CREATION_DATE = "news_creation_date";
	private static final String NEWS_MODIFICATION_DATE = "news_modification_date";
	private static final String COMMENT_ID = "comments_id_pk";
	private static final String COMMENT_TEXT = "comments_text";
	private static final String COMMENT_CREATION_DATE = "comments_creation_date";

	/* This object is factory for connection */
	private DataSource dataSource;

	@Override
	public List<Comment> findAll() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Comment> commentList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_COMMENT_FIND_ALL);
			resultSet =  stmt.executeQuery();
			commentList = buildCommentList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAll() method of CommentDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return commentList;
	}
	
	@Override
	public List<Comment> findAllByNewsId(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Comment> commentList = new ArrayList<Comment>();
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_COMMENT_FIND_ALL_BY_NEWS_ID);
			stmt.setLong(1, id);
			resultSet = stmt.executeQuery();
			commentList = buildCommentList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAllByNewsId() method of CommentDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return commentList;
	}

	@Override
	public Comment findById(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		Comment comment = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_COMMENT_FIND_BY_ID);
			stmt.setLong(1, id);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				comment = buildComment(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException("findById() method of CommentDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return comment;
	}

	@Override
	public boolean delete(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_COMMENT_DELETE);
			stmt.setLong(1, id);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("delete() method of CommentDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isDeleted;
	}

	@Override
	public boolean deleteCommentsByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_COMMENT_DELETE_BY_NEWS_ID);
			stmt.setLong(1, newsId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("deleteCommentsByNewsId() method of CommentDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isDeleted;
	}

	@Override
	public Long add(Comment comment) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long dbIndex = 0;
		String generatedColumns[] = {COMMENT_ID};
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_COMMENT_ADD, generatedColumns);
			stmt.setLong(1, comment.getNews().getId());
			stmt.setString(2, comment.getCommentText());
			stmt.setTimestamp(3, ConvertToSqlDate.convertToSqlTimestamp(comment.getCreationDate()));
			stmt.executeUpdate();
			resultSet = stmt.getGeneratedKeys();
			if (resultSet.next()){
				dbIndex = resultSet.getLong(1);
			} else {
				throw new DaoException("Comment is not saved to database");
			}
		} catch (SQLException e) {
			throw new DaoException("add() method of CommentDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return dbIndex;
	}

	@Override
	public boolean update(Comment entity) throws DaoException {
		try {
			throw new UnsupportedOperationException();
		} catch (UnsupportedOperationException e) {
			throw new DaoException("This method is not supported", e);
		}
	}
	
	private Comment buildComment(ResultSet rs) throws SQLException{
		/*
		 * This method builds Comment object from ResultSet object
		 */
		News news = buildNews(rs);
		Comment comment = new Comment();
		comment.setId(rs.getLong(COMMENT_ID));
		comment.setNews(news);
		comment.setCommentText(rs.getString(COMMENT_TEXT));
		comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
		return comment;
	}
	
	private News buildNews(ResultSet rs) throws SQLException{
		/*
		 * This method builds Comment object from ResultSet object
		 */
		News news = new News();
		news.setId(rs.getLong(NEWS_ID));
		news.setTitle(rs.getString(NEWS_TITLE));
		news.setShortText(rs.getString(NEWS_SHORT_TEXT));
		news.setFullText(rs.getString(NEWS_FULL_TEXT));
		news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
		news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
		return news;
	}
	
	private List<Comment> buildCommentList(ResultSet rs) throws SQLException{
		/*
		 * This method builds list of Comment objects from ResultSet object
		 */
		List<Comment> comments = new ArrayList<Comment>();
		while (rs.next()){
			comments.add(buildComment(rs));
		}
		return comments;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
