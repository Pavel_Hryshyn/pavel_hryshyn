/*
 * @(#)IBasicDao.java   1.0 2015/08/11
 */
package by.epam.lab.hryshyn.task.first.database.dao.interfaces;

import java.io.Serializable;
import java.util.List;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;

/**
 * This interface defines basic methods that bind the entities with database
 * @version 1.0 11 August 2015
 * @author Pavel Hryshyn
 */
public interface IBasicDao<I extends Number, E extends Serializable> {
	/**
	 * This method returns a list of all entities of a certain type that stores in database
	 * @return list of all entities of a certain type from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<E> findAll() throws DaoException;
	
	/**
	 * This method returns a entity of a certain type with some id that stores in database
	 * @param id
	 * 			the param uses for searching entity in database
	 * @return entity with some id from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	E findById(I id) throws DaoException;
	
	/**
	 * This method deletes a entity with some id from database
	 * @param id
	 * 			the param uses for deleting a entity from database
	 * @return true if a entity is deleted from database, else false
	 * @throws DaoException, if arise database access error or other database errors
	 */
	boolean delete(I id) throws DaoException;
	
	/**
	 * This method adds entity to database
	 * @param entity
	 * 			the param uses for adding entity to database
	 * @return entity's id if entity is added to database
	 * @throws DaoException, if arise database access error or other database errors
	 * 				or entity doesn't save in database
	 */
	I add(E entity) throws DaoException;
	
	/**
	 * This method updates entity to database
	 * @param entity
	 *            the param uses for updating entity to database
	 * @return true if entity is updated to database, else false
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	boolean update(E entity) throws DaoException;
}
