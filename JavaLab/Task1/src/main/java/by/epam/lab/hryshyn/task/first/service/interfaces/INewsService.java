/*
 * @(#)INewsService.java   1.1 2015/08/30
 */
package by.epam.lab.hryshyn.task.first.service.interfaces;

import java.util.List;
import by.epam.lab.hryshyn.task.first.database.dto.News;
import by.epam.lab.hryshyn.task.first.database.dto.SearchCriteria;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;

/**
 * This interface defines basic methods for News Entity on service layer
 * @version 1.1 30 August 2015
 * @author Pavel Hryshyn
 */
public interface INewsService extends IBasicService<Long, News> {
	/**
	 * This method adds News entity to database
	 * @param news
	 * 			the param uses for adding news to database
	 * @param authorId
	 * 			the param uses for adding news to database
	 * @param tagsId
	 * 			the param uses for adding news to database
	 * @return news's id if news is added to database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	Long addWithAuthorAndTags(News news, Long authorId, List<Long> tagsId) throws ServiceException;
	
	/**
	 * This method returns amount of all news that stores in database
	 * @return amount of all news from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	int countAllNews() throws ServiceException;
	
	/**
	 * This method returns amount of all news which matching searchCriteria 
	 * that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return amount of all news which matching searchCriteria from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	int countAllWithCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * This method returns amount of news which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @return amount of all news which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	int countNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws ServiceException;
	
	/**
	 * This method returns all news sorted by amount of comments that stores in database
	 * @return list of all news sorted by amount of comments from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	List<News> findAllSortingByCommentAmount() throws ServiceException;
	
	/**
	 * This method returns list of news which matching searchCriteria
	 * that stores in database
	 * @param searchCriteria
	 * @return list of all news which matching searchCriteria from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	List<News> findAllWithCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * This method returns list of news which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of all news which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws ServiceException;
}
