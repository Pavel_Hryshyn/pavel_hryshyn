/*
 * @(#)CommentService.java   1.1 2015/08/26
 */
package by.epam.lab.hryshyn.task.first.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ICommentDao;
import by.epam.lab.hryshyn.task.first.database.dto.Comment;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;
import by.epam.lab.hryshyn.task.first.service.interfaces.ICommentService;

/**
 * This class implements methods for Comment service
 * 
 * @version 1.1 26 August 2015
 * @author Pavel Hryshyn
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CommentService implements ICommentService {
	/* This object obtains logger for this class */
	private static final Logger logger = Logger.getLogger(CommentService.class);

	private ICommentDao commentDao;

	@Override
	public List<Comment> findAll() throws ServiceException {
		List<Comment> comments = null;
		try {
			comments = commentDao.findAll();
			logger.info("List of comments is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findAll() method of CommentService is failed", e);
		}
		return comments;
	}

	@Override
	public List<Comment> findAllByNewsId(Long id) throws ServiceException {
		List<Comment> comments = null;
		try {
			comments = commentDao.findAllByNewsId(id);
			logger.info("List of comments for news is formed.");
		} catch (DaoException e) {
			throw new ServiceException("findAllByNewsId() method of CommentService is failed", e);
		}
		return comments;
	}

	@Override
	public Comment findById(Long id) throws ServiceException {
		Comment comment = null;
		try {
			comment = commentDao.findById(id);
			logger.info("Comment is founded. Id = " + id);
		} catch (DaoException e) {
			throw new ServiceException("findById() method of CommentService is failed", e);
		}
		return comment;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean delete(Long id) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = commentDao.delete(id);
			if (isDeleted) {
				logger.info("Comment is deleted successfully. Id = " + id);
			} else {
				logger.debug("Comment is deleted failure. Id = " + id);
			}
		} catch (DaoException e) {
			throw new ServiceException("delete() method of CommentService is failed", e);
		}
		return isDeleted;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long add(Comment comment) throws ServiceException {
		long commentId = 0;
		try {
			commentId = commentDao.add(comment);
			logger.info("Comment is created. Id = " + commentId);
		} catch (DaoException e) {
			throw new ServiceException("add() method of CommentService is failed", e);
		}
		return commentId;
	}

	/**
	 * This method is unsupported
	 * @throws ServiceException, if calling this method
	 */
	@Override
	public boolean update(Comment comment) throws ServiceException {
		try {
			throw new UnsupportedOperationException();
		} catch (UnsupportedOperationException e) {
			throw new ServiceException("Update() method is unsupported", e);
		}
	}

	public ICommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}
}
