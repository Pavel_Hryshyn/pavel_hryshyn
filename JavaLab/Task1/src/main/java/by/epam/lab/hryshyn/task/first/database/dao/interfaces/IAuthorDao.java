/*
 * @(#)IAuthorDao.java   1.0 2015/08/14
 */
package by.epam.lab.hryshyn.task.first.database.dao.interfaces;

import java.util.List;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dto.Author;

/**
 * This interface defines methods that bind the Author dto with database
 * @version 1.0 14 August 2015
 * @author Pavel Hryshyn
 */
public interface IAuthorDao extends IBasicDao<Long, Author> {
	/**
	 * This method returns a list of all non-expired authors that stores in database
	 * @return  a list of all non-expired authors from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<Author> findAllNonExpiredAuthors() throws DaoException;
}
