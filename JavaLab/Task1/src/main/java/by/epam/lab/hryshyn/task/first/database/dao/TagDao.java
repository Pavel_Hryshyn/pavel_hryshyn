/*
 * @(#)TagDao.java   1.1 2015/08/24
 */
package by.epam.lab.hryshyn.task.first.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ITagDao;
import by.epam.lab.hryshyn.task.first.database.dao.util.DbCloseConnection;
import by.epam.lab.hryshyn.task.first.database.dto.Tag;

/**
 * This class implements DAO pattern and contains methods that bind the Tag DTO 
 * with database
 * @version 1.1 24 August 2015
 * @author Pavel Hryshyn
 */
public class TagDao implements ITagDao {
	/* This constants stores names of database column */
	private static final String TAG_ID = "tag_id_pk";
	private static final String TAG_NAME = "tag_name";
	
	/* This constants stores SQL queries for database */
	private static final String SQL_FIND_ALL = "SELECT * FROM tag ORDER BY tag_id_pk ASC";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM tag WHERE tag.tag_id_pk = ?";
	private static final String SQL_TAG_DELETE = "DELETE FROM tag WHERE tag_id_pk=?";
	private static final String SQL_TAG_ADD = "INSERT INTO tag (tag_id_pk, tag_name) VALUES (tag_sequence.nextval, ?)";
	
	/* This object is factory for connection */
	private DataSource dataSource;

	@Override
	public List<Tag> findAll() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Tag> tagList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL);
			resultSet = stmt.executeQuery();
			tagList = buildTagList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAll() method of TagDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return tagList;
	}

	@Override
	public Tag findById(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		Tag tag = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_ID);
			stmt.setLong(1, id);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				tag = buildTag(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException("findById() method of TagDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return tag;
	}

	@Override
	public boolean delete(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_TAG_DELETE);
			stmt.setLong(1, id);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("delete() method of TagDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isDeleted;
	}

	@Override
	public Long add(Tag tag) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long dbIndex = 0;
		String generatedColumns[] = {TAG_ID};
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_TAG_ADD, generatedColumns);
			stmt.setString(1, tag.getTagName());
			stmt.executeUpdate();
			resultSet = stmt.getGeneratedKeys();
			if (resultSet.next()){
				dbIndex = resultSet.getLong(1);
			} else {
				throw new DaoException("Tag is not saved in database");
			}
		} catch (SQLException e) {
			throw new DaoException("add() method of TagDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return dbIndex;
	}

	/**
	 * This method is unsupported for Tag dto
	 * @throws DaoException, if calling this method
	 */
	@Override
	public boolean update(Tag tag) throws DaoException {
		try {
			throw new UnsupportedOperationException();
		} catch (UnsupportedOperationException e) {
			throw new DaoException("This method is not supported", e);
		}
	}
	
	private Tag buildTag(ResultSet rs) throws SQLException{
		/*
		 * This method builds Tag object from ResultSet object
		 */
		Tag tag = new Tag();
		tag.setId(rs.getLong(TAG_ID));
		tag.setTagName(rs.getString(TAG_NAME));
		return tag;
	}
	
	private List<Tag> buildTagList(ResultSet rs) throws SQLException{
		/*
		 * This method builds list of Tag objects from ResultSet object
		 */
		List<Tag> tags = new ArrayList<Tag>();
		while (rs.next()){
			tags.add(buildTag(rs));
		}
		return tags;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
