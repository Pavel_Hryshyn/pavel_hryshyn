/*
 * @(#)Author.java   1.1 2015/08/12
 */
package by.epam.lab.hryshyn.task.first.database.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is data transfer object and encapsulates data for
 * transfer to database
 * @version 1.1 12 August 2015
 * @author Pavel_Hryshyn
 */
public class Author implements Serializable {
	private static final long serialVersionUID = 1L;

	/* This value stores author id */
	private long id;
	
	/* This value stores author name */
	private String authorName;
		
	/* This value stores expired date of author */
	private Date expiredDate;

	public Author() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result
				+ ((expiredDate == null) ? 0 : expiredDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expiredDate == null) {
			if (other.expiredDate != null)
				return false;
		} else if (!expiredDate.equals(other.expiredDate))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthorDto [id=" + id + ", authorName=" + authorName + ", expiredDate=" + expiredDate + "]";
	}
}
