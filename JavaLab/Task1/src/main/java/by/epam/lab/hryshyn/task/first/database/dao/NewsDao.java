/*
 * @(#)NewsDao.java   1.5 2015/08/24
 */
package by.epam.lab.hryshyn.task.first.database.dao;

import static by.epam.lab.hryshyn.task.first.database.dao.util.AddSubQueryToQuery.addIterativeSubQueryToEnd;
import static by.epam.lab.hryshyn.task.first.database.dao.util.AddSubQueryToQuery.addSubQueryToEnd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.INewsDao;
import by.epam.lab.hryshyn.task.first.database.dao.util.DbCloseConnection;
import by.epam.lab.hryshyn.task.first.database.dto.News;
import by.epam.lab.hryshyn.task.first.database.dto.SearchCriteria;
import by.epam.lab.hryshyn.task.first.util.ConvertToSqlDate;

/**
 * This class implements DAO pattern and contains methods that bind the News DTO
 * with database
 * 
 * @version 1.5 24 August 2015
 * @author Pavel Hryshyn
 */
public class NewsDao implements INewsDao {
	/* This constants stores SQL queries for database */
	private static final String SQL_FIND_ALL = "SELECT * FROM news ORDER BY news_id_pk DESC";
	private static final String SQL_FIND_ALL_BY_COMMENT_SORT = "SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM news WHERE news_id_pk=?";
	private static final String SQL_AMOUNT_NEWS = "SELECT COUNT(*) FROM news";
	private static final String SQL_NEWS_ADD = "INSERT INTO news (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES (news_sequence.nextval, ?, ?, ?, ?, ?)";
	private static final String SQL_NEWS_TAGS_ADD = "INSERT INTO news_tag (news_id_fk, tag_id_fk) VALUES (?, ?)";
	private static final String SQL_NEWS_AUTHOR_ADD = "INSERT INTO news_author (news_id_fk, author_id_fk) VALUES (?, ?)";
	private static final String SQL_NEWS_DELETE = "DELETE FROM news WHERE news_id_pk=?";
	private static final String SQL_NEWS_TAGS_DELETE = "DELETE FROM news_tag WHERE news_id_fk=?";
	private static final String SQL_NEWS_AUTHOR_DELETE = "DELETE FROM news_author WHERE news_id_fk=?";
	private static final String SQL_NEWS_UPDATE = "UPDATE news SET news_title = ?, news_short_text = ?, news_full_text = ?, news_modification_date = ?  WHERE news_id_pk = ?";
	private static final String SQL_CRITERIA_MAIN_QUERY = "SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE ";
	private static final String SQL_CRITERIA_ORDER_BY_COMMENT_QUERY = "GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC";
	private static final String SQL_CRITERIA_AUTHOR_SUBQUERY = "news_author.author_id_fk=? AND ( ";
	private static final String SQL_CRITERIA_TAG_SUBQUERY = "news_tag.tag_id_fk=?";
	private static final String SQL_CRITERIA_WITHOUT_TAGS = "SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE news_author.author_id_fk=? GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC";
	private static final String SQL_CRITERIA_POSITION_MAIN_QUERY = "SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE";
	private static final String SQL_CRITERIA_POSITION_TAIL_SUBQUERY = "GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC) a WHERE ROWNUM <=?) WHERE rnum >=?";
	private static final String SQL_CRITERIA_POSITIONS_WITHOUT_TAGS = "SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE news_author.author_id_fk=? GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC) a WHERE ROWNUM <=?) WHERE rnum >=?";
	private static final String SQL_CRITERIA_POSITIONS_WITHOUT_AUTHOR_TAGS = "SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC) a WHERE ROWNUM <=?) WHERE rnum >=?";	
	private static final String SQL_COUNT_CRITERIA_MAIN_QUERY = "SELECT COUNT(*) FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE ";
	private static final String SQL_COUNT_CRITERIA_WITHOUT_TAGS = "SELECT COUNT(*) FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE news_author.author_id_fk=? GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC)";
	private static final String SQL_COUNT_CRITERIA_WITHOUT_AUTHOR_AND_TAGS = "SELECT COUNT(*) FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC)";
	private static final String SQL_COUNT_CRITERIA_POSITION_MAIN_QUERY = "SELECT COUNT(*) FROM (SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE";
	private static final String SQL_COUNT_CRITERIA_POSITIONS_WITHOUT_TAGS = "SELECT COUNT(*) FROM (SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE news_author.author_id_fk=? GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC) a WHERE ROWNUM <=?) WHERE rnum >=?)";
	private static final String SQL_COUNT_CRITERIA_POSITIONS_WITHOUT_AUTHOR_TAGS = "SELECT COUNT(*) FROM (SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC) a WHERE ROWNUM <=?) WHERE rnum >=?)";
	
	/* This constants stores names of database column */
	private static final String NEWS_ID = "news_id_pk";
	private static final String TITLE = "news_title";
	private static final String SHORT_TEXT = "news_short_text";
	private static final String FULL_TEXT = "news_full_text";
	private static final String CREATION_DATE = "news_creation_date";
	private static final String MODIFICATION_DATE = "news_modification_date";

	/* This constants stores bracket that using in SQL query */
	private static final String BRACKET = ") ";

	/* This object is factory for connection */
	private DataSource dataSource;

	@Override
	public List<News> findAll() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL);
			resultSet = stmt.executeQuery();
			newsList = buildNewsList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAll() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return newsList;
	}

	@Override
	public List<News> findAllSortingByCommentAmount() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL_BY_COMMENT_SORT);
			resultSet = stmt.executeQuery();
			newsList = buildNewsList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAllSortingByCommentAmount() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return newsList;
	}

	@Override
	public List<News> findAllWithCriteria(SearchCriteria searchCriteria)
			throws DaoException {
		Connection con = null;
		List<News> newsList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			if (searchCriteria.getAuthor() != null) {
				if (!searchCriteria.getTags().isEmpty()){
					newsList = buildNewsListWithCriteria(searchCriteria, con);
				} else {
					newsList = buildNewsListWithCriteriaWithoutTags(searchCriteria, con);
				}
			} else {
				if (!searchCriteria.getTags().isEmpty()){
					newsList = buildNewsListWithCriteriaWithoutAuthor(searchCriteria, con);
				} else {
					newsList = buildNewsListWithCriteriaWithoutAuthorAndTags(searchCriteria, con);
				}
			}
		} catch (SQLException e) {
			throw new DaoException("findAllWithCriteria() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(con, dataSource);
		}
		return newsList;
	}

	@Override
	public List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc, int start,
			int end) throws DaoException {
		Connection con = null;
		List<News> newsList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			if (sc.getAuthor() != null) {
				if (!sc.getTags().isEmpty()){
					newsList = buildNewsListWithCriteriaAndPositions(sc, start, end, con);
				} else {
					newsList = buildNewsListWithCriteriaAndPositionsWithoutTags(sc, start, end, con);
				}
			} else {
				if (!sc.getTags().isEmpty()){
					newsList = buildNewsListWithCriteriaAndPositionsWithoutAuthor(sc, start, end, con);
				} else {
					newsList = buildNewsListWithCriteriaAndPositionsWithoutAuthorAndTags(sc, start, end, con);
				}
			}
		} catch (SQLException e) {
			throw new DaoException("findNewsWithCriteriaAndPositions() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(con, dataSource);
		}
		return newsList;
	}

	@Override
	public News findById(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		News news = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_ID);
			stmt.setLong(1, id);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				news = buildNews(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException("findById() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return news;
	}

	@Override
	public boolean delete(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_DELETE);
			stmt.setLong(1, id);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("delete() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isDeleted;
	}

	@Override
	public boolean deleteNewsWithTags(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_TAGS_DELETE);
			stmt.setLong(1, newsId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("deleteNewsWithTags() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isDeleted;
	}

	@Override
	public boolean deleteNewsWithAuthor(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_AUTHOR_DELETE);
			stmt.setLong(1, newsId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("deleteNewsWithAuthor() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isDeleted;
	}

	@Override
	public Long add(News news) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long dbIndex = 0;
		String generatedColumns[] = { NEWS_ID };
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_ADD, generatedColumns);
			stmt.setString(1, news.getTitle());
			stmt.setString(2, news.getShortText());
			stmt.setString(3, news.getFullText());
			stmt.setTimestamp(4, ConvertToSqlDate.convertToSqlTimestamp(news
					.getCreationDate()));
			stmt.setDate(5, ConvertToSqlDate.convertToSqlDate(news
					.getModificationDate()));
			stmt.executeUpdate();
			resultSet = stmt.getGeneratedKeys();
			if (resultSet.next()) {
				dbIndex = resultSet.getLong(1);
			} else {
				throw new DaoException("News is not saved in database");
			}
		} catch (SQLException e) {
			throw new DaoException("add() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return dbIndex;
	}

	@Override
	public void addNewsWithTags(Long newsId, List<Long> tagsId)
			throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_TAGS_ADD);
			for (Long tagId : tagsId) {
				stmt.setLong(1, newsId);
				stmt.setLong(2, tagId);
				stmt.addBatch();
			}
			stmt.executeBatch();
		} catch (SQLException e) {
			throw new DaoException("addNewsWithTags() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
	}

	@Override
	public void addNewsWithAuthor(Long newsId, Long authorId)
			throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_AUTHOR_ADD);
			stmt.setLong(1, newsId);
			stmt.setLong(2, authorId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("addNewsWithAuthor() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
	}

	@Override
	public boolean update(News news) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isUpdated = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_UPDATE);
			stmt.setString(1, news.getTitle());
			stmt.setString(2, news.getShortText());
			stmt.setString(3, news.getFullText());
			stmt.setDate(4, ConvertToSqlDate.convertToSqlDate(news
					.getModificationDate()));
			stmt.setLong(5, news.getId());
			row = stmt.executeUpdate();
			if (row != 0) {
				isUpdated = true;
			}
		} catch (SQLException e) {
			throw new DaoException("update() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(stmt);
			DbCloseConnection.close(con, dataSource);
		}
		return isUpdated;
	}

	@Override
	public int countAllNews() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_AMOUNT_NEWS);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				amountOfNews = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			throw new DaoException("countAllNews() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(con, stmt, resultSet, dataSource);
		}
		return amountOfNews;
	}

	@Override
	public int countAllWithCriteria(SearchCriteria searchCriteria)
			throws DaoException {
		Connection con = null;
		int amountNews = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			if (searchCriteria.getAuthor() != null) {
				if (!searchCriteria.getTags().isEmpty()){
					amountNews = buildAmountNewsWithCriteria(searchCriteria, con);
				} else {
					amountNews = buildAmountNewsWithCriteriaWithoutTags(searchCriteria, con);
				}
			} else {
				if (!searchCriteria.getTags().isEmpty()){
					amountNews = buildAmountNewsWithCriteriaWithoutAuthor(searchCriteria, con);
				} else {
					amountNews = buildAmountNewsWithCriteriaWithoutAuthorAndTags(searchCriteria, con);
				}
			}
		} catch (SQLException e) {
			throw new DaoException("countAllWithCriteria() method of NewsDao is failed", e);
		}  finally {
			DbCloseConnection.close(con, dataSource);
		}
		return amountNews;
	}

	@Override
	public int countNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end)
			throws DaoException {
		Connection con = null;
		int amountNews = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			if (sc.getAuthor() != null) {
				if (!sc.getTags().isEmpty()){
					amountNews = buildAmountNewsWithCriteriaAndPositions(sc, start, end, con);
				} else {
					amountNews = buildAmountNewsWithCriteriaAndPositionsWithoutTags(sc, start, end, con);
				}
			} else {
				if (!sc.getTags().isEmpty()){
					amountNews = buildAmountNewsWithCriteriaAndPositionsWithoutAuthor(sc, start, end, con);
				} else {
					amountNews = buildAmountNewsWithCriteriaAndPositionsWithoutAuthorAndTags(sc, start, end, con);
				}
			}
		} catch (SQLException e) {
			throw new DaoException("countNewsWithCriteriaAndPositions() method of NewsDao is failed", e);
		} finally {
			DbCloseConnection.close(con, dataSource);
		}
		return amountNews;
	}

	private News buildNews(ResultSet rs) throws SQLException {
		/*
		 * This method builds news from ResultSet object
		 */
		News news = new News();
		news.setId(rs.getLong(NEWS_ID));
		news.setTitle(rs.getString(TITLE));
		news.setShortText(rs.getString(SHORT_TEXT));
		news.setFullText(rs.getString(FULL_TEXT));
		news.setCreationDate(rs.getTimestamp(CREATION_DATE));
		news.setModificationDate(rs.getDate(MODIFICATION_DATE));
		return news;
	}

	private List<News> buildNewsList(ResultSet rs) throws SQLException {
		/*
		 * This method builds news list from ResultSet object
		 */
		List<News> newsList = new ArrayList<News>();
		while (rs.next()) {
			newsList.add(buildNews(rs));
		}
		return newsList;
	}
	
	private List<News> buildNewsListWithCriteria(SearchCriteria searchCriteria, Connection con) throws SQLException {
		/*
		 * This method builds news list using searchCriteria from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		String mainQuery = SQL_CRITERIA_MAIN_QUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = BRACKET + SQL_CRITERIA_ORDER_BY_COMMENT_QUERY;
		int amountTags = searchCriteria.getTags().size();
		String resultingQuery = formCriteriaQuery(mainQuery, authorSubQuery, tagSubQuery, tailSubQuery, amountTags);
		ps = con.prepareStatement(resultingQuery);
		ps.setLong(1, searchCriteria.getAuthor().getId());
		for (int indexList = 0; indexList < amountTags; indexList++) {
			int indexPs = indexList + 2;
			ps.setLong(indexPs, searchCriteria.getTags().get(indexList)
					.getId());
		}
		resultSet = ps.executeQuery();
		newsList = buildNewsList(resultSet);
		return newsList;
	}
	
	private List<News> buildNewsListWithCriteriaWithoutAuthor(SearchCriteria searchCriteria, Connection con) throws SQLException {
		/*
		 * This method builds news list using searchCriteria when author is not defined from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		String mainQuery = SQL_CRITERIA_MAIN_QUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = SQL_CRITERIA_ORDER_BY_COMMENT_QUERY;
		List<News> newsList = null;
		int size = searchCriteria.getTags().size();
		String resultingQuery = formCriteriaQueryWithoutAuthor(mainQuery, tagSubQuery, tailSubQuery, size);
		ps = con.prepareStatement(resultingQuery);
		for (int indexList = 0; indexList < size; indexList++) {
			int indexPs = indexList + 1;
			ps.setLong(indexPs, searchCriteria.getTags().get(indexList)
					.getId());
		}
		resultSet = ps.executeQuery();
		newsList = buildNewsList(resultSet);
		return newsList;
	}
	
	private List<News> buildNewsListWithCriteriaWithoutTags(SearchCriteria searchCriteria, Connection con) throws SQLException {
		/*
		 * This method builds news list using searchCriteria when list of tags is not defined from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		String query = SQL_CRITERIA_WITHOUT_TAGS;
	    ps = con.prepareStatement(query);
		ps.setLong(1, searchCriteria.getAuthor().getId());
		resultSet = ps.executeQuery();
		newsList = buildNewsList(resultSet);
		return newsList;
	}
	
	private List<News> buildNewsListWithCriteriaWithoutAuthorAndTags(SearchCriteria searchCriteria, Connection con) throws SQLException {
		/*
		 * This method builds news list using searchCriteria when author and list of tags are not defined from ResultSet object
		 */
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		stmt = con.prepareStatement(SQL_FIND_ALL_BY_COMMENT_SORT);
		resultSet = stmt.executeQuery(); 
		newsList = buildNewsList(resultSet);
		return newsList;
	}
	
	private List<News> buildNewsListWithCriteriaAndPositions(SearchCriteria sc, int start, int end, Connection con) throws SQLException {
		/*
		 * This method builds news list using searchCriteria and taking position between start and end from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		String mainQuery = SQL_CRITERIA_POSITION_MAIN_QUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = BRACKET + SQL_CRITERIA_POSITION_TAIL_SUBQUERY;
		int amountTags = sc.getTags().size();
		String resultingQuery = formCriteriaQuery(mainQuery, authorSubQuery, tagSubQuery, tailSubQuery, amountTags);
		ps = con.prepareStatement(resultingQuery);
		ps.setLong(1, sc.getAuthor().getId());
		for (int indexList = 0; indexList < amountTags; indexList++) {
			int indexPs = indexList + 2;
			ps.setLong(indexPs, sc.getTags().get(indexList).getId());
		}
		ps.setInt(amountTags + 2, end);
		ps.setInt(amountTags + 3, start);
		resultSet = ps.executeQuery();
		newsList = buildNewsList(resultSet);
		return newsList;
	}
	
	private List<News> buildNewsListWithCriteriaAndPositionsWithoutAuthor(SearchCriteria sc, int start, int end, Connection con) throws SQLException {
		/*
		 * This method builds news list using searchCriteria when author is not defined and 
		 * taking position between start and end from ResultSet object
		 */
		String mainQuery = SQL_CRITERIA_POSITION_MAIN_QUERY + "(";
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = BRACKET + SQL_CRITERIA_POSITION_TAIL_SUBQUERY;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		int amountTags = sc.getTags().size();
		String resultingQuery = formCriteriaQueryWithoutAuthor(mainQuery, tagSubQuery, tailSubQuery, amountTags);
		ps = con.prepareStatement(resultingQuery);
		for (int indexList = 0; indexList < amountTags; indexList++) {
			int indexPs = indexList + 1;
			ps.setLong(indexPs, sc.getTags().get(indexList)
					.getId());
		}
		ps.setInt(amountTags + 1, end);
		ps.setInt(amountTags + 2, start);
		resultSet = ps.executeQuery();
		newsList = buildNewsList(resultSet);
		return newsList;
	}
	
	private List<News> buildNewsListWithCriteriaAndPositionsWithoutTags(SearchCriteria sc, int start, int end, Connection con) throws SQLException {
		/*
		 * This method builds news list using searchCriteria when list of tags is not defined and 
		 * taking position between start and end from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		String resultingQuery = SQL_CRITERIA_POSITIONS_WITHOUT_TAGS;
		ps = con.prepareStatement(resultingQuery);
		ps.setLong(1, sc.getAuthor().getId());
		ps.setInt(2, end);
		ps.setInt(3, start);
		resultSet = ps.executeQuery();
		newsList = buildNewsList(resultSet);
		return newsList;
	}
	
	private List<News> buildNewsListWithCriteriaAndPositionsWithoutAuthorAndTags(SearchCriteria sc, int start, int end, Connection con) throws SQLException {
		/*
		 * This method builds news list using searchCriteria when author and list of tags are not defined and 
		 * taking position between start and end from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		String resultingQuery = SQL_CRITERIA_POSITIONS_WITHOUT_AUTHOR_TAGS;
	    ps = con.prepareStatement(resultingQuery);
		ps.setInt(1, end);
		ps.setInt(2, start);
		resultSet = ps.executeQuery();
		newsList = buildNewsList(resultSet);
		return newsList;
	}
	
	private int buildAmountNewsWithCriteria(SearchCriteria searchCriteria, Connection con) throws SQLException {
		/*
		 * This method returns amount news which matching with searchCriteria from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		String mainQuery = SQL_COUNT_CRITERIA_MAIN_QUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = BRACKET + SQL_CRITERIA_ORDER_BY_COMMENT_QUERY + BRACKET;
		int amountTags = searchCriteria.getTags().size();
		String resultingQuery = formCriteriaQuery(mainQuery, authorSubQuery, tagSubQuery, tailSubQuery, amountTags);
		ps = con.prepareStatement(resultingQuery);
		ps.setLong(1, searchCriteria.getAuthor().getId());
		for (int indexList = 0; indexList < amountTags; indexList++) {
			int indexPs = indexList + 2;
			ps.setLong(indexPs, searchCriteria.getTags().get(indexList)
					.getId());
		}
		resultSet = ps.executeQuery();
		if (resultSet.next()) {
			amountOfNews = resultSet.getInt(1);
		}
		return amountOfNews;
	}
	
	private int buildAmountNewsWithCriteriaWithoutAuthor(SearchCriteria searchCriteria, Connection con) throws SQLException {
		/*
		 * This method returns amount news which matching with searchCriteria when author is not defined from ResultSet object
		 */
		String mainQuery = SQL_COUNT_CRITERIA_MAIN_QUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = SQL_CRITERIA_ORDER_BY_COMMENT_QUERY + BRACKET;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		int size = searchCriteria.getTags().size();
		String resultingQuery = formCriteriaQueryWithoutAuthor(mainQuery, tagSubQuery, tailSubQuery, size);
		ps = con.prepareStatement(resultingQuery);
		for (int indexList = 0; indexList < size; indexList++) {
			int indexPs = indexList + 1;
			ps.setLong(indexPs, searchCriteria.getTags().get(indexList)
					.getId());
		}
		resultSet = ps.executeQuery();
		if (resultSet.next()) {
			amountOfNews = resultSet.getInt(1);
		}
		return amountOfNews;
	}
	
	private int buildAmountNewsWithCriteriaWithoutTags(SearchCriteria searchCriteria, Connection con) throws SQLException {
		/*
		 * This method returns amount news which matching with searchCriteria when list of tags is not defined from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		String query = SQL_COUNT_CRITERIA_WITHOUT_TAGS;
		ps = con.prepareStatement(query);
		ps.setLong(1, searchCriteria.getAuthor().getId());
		resultSet = ps.executeQuery();
		if (resultSet.next()) {
			amountOfNews = resultSet.getInt(1);
		}
		return amountOfNews;
	}
	
	private int buildAmountNewsWithCriteriaWithoutAuthorAndTags(SearchCriteria searchCriteria, Connection con) throws SQLException {
		/*
		 * This method returns amount news which matching with searchCriteria when author and list of tags are not defined
		 *  from ResultSet object
		 */
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		stmt = con.prepareStatement(SQL_COUNT_CRITERIA_WITHOUT_AUTHOR_AND_TAGS);
		resultSet = stmt.executeQuery(); 
		if (resultSet.next()) {
			amountOfNews = resultSet.getInt(1);
		}
		return amountOfNews;
	}
	
	private int buildAmountNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end, Connection con) throws SQLException {
		/*
		 * This method  returns amount news which matching with searchCriteria and 
		 * taking position between start and end from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		String mainQuery = SQL_COUNT_CRITERIA_POSITION_MAIN_QUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = BRACKET + SQL_CRITERIA_POSITION_TAIL_SUBQUERY + BRACKET;
		int amountTags = sc.getTags().size();
		String resultingQuery = formCriteriaQuery(mainQuery, authorSubQuery, tagSubQuery, tailSubQuery, amountTags);
		ps = con.prepareStatement(resultingQuery);
		ps.setLong(1, sc.getAuthor().getId());
		for (int indexList = 0; indexList < amountTags; indexList++) {
			int indexPs = indexList + 2;
			ps.setLong(indexPs, sc.getTags().get(indexList).getId());
		}
		ps.setInt(amountTags + 2, end);
		ps.setInt(amountTags + 3, start);
		resultSet = ps.executeQuery();
		if (resultSet.next()) {
			amountOfNews = resultSet.getInt(1);
		}
		return amountOfNews;
	}
	
	private int buildAmountNewsWithCriteriaAndPositionsWithoutAuthor(SearchCriteria sc, int start, int end, Connection con) throws SQLException {
		/*
		 * This method  returns amount news which matching with searchCriteria when author is not defined and 
		 * taking position between start and end from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		String mainQuery = SQL_COUNT_CRITERIA_POSITION_MAIN_QUERY + "(";
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = BRACKET + SQL_CRITERIA_POSITION_TAIL_SUBQUERY + BRACKET;
		int amountOfNews = 0;
		int amountTags = sc.getTags().size();
		String resultingQuery = formCriteriaQueryWithoutAuthor(mainQuery, tagSubQuery, tailSubQuery, amountTags);
		ps = con.prepareStatement(resultingQuery);
		for (int indexList = 0; indexList < amountTags; indexList++) {
			int indexPs = indexList + 1;
			ps.setLong(indexPs, sc.getTags().get(indexList)
					.getId());
		}
		ps.setInt(amountTags + 1, end);
		ps.setInt(amountTags + 2, start);
		resultSet = ps.executeQuery();
		if (resultSet.next()) {
			amountOfNews = resultSet.getInt(1);
		}
		return amountOfNews;
	}
	
	private int buildAmountNewsWithCriteriaAndPositionsWithoutTags(SearchCriteria sc, int start, int end, Connection con) throws SQLException {
		/*
		 * This method  returns amount news which matching with searchCriteria when list of tags is not defined and 
		 * taking position between start and end from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		String resultingQuery = SQL_COUNT_CRITERIA_POSITIONS_WITHOUT_TAGS;
		ps = con.prepareStatement(resultingQuery);
		ps.setLong(1, sc.getAuthor().getId());
		ps.setInt(2, end);
		ps.setInt(3, start);
		resultSet = ps.executeQuery();
		if (resultSet.next()) {
			amountOfNews = resultSet.getInt(1);
		}
		return amountOfNews;
	}
	
	private int buildAmountNewsWithCriteriaAndPositionsWithoutAuthorAndTags(SearchCriteria sc, int start, int end, Connection con) throws SQLException {
		/*
		 * This method  returns amount news which matching with searchCriteria when author and list of tags are not defined and 
		 * taking position between start and end from ResultSet object
		 */
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		String resultingQuery = SQL_COUNT_CRITERIA_POSITIONS_WITHOUT_AUTHOR_TAGS;
		ps = con.prepareStatement(resultingQuery);
		ps.setInt(1, end);
		ps.setInt(2, start);
		resultSet = ps.executeQuery();
		if (resultSet.next()) {
			amountOfNews = resultSet.getInt(1);
		}
		return amountOfNews;
	}
	
	private String formCriteriaQuery(String mainQuery, String authorSubQuery, String tagSubQuery, String tailQuery, int amountTags){
		/*
		 * This method forms SQL query with using searchCriteria
		 */
		String QueryWithoutEnd = addIterativeSubQueryToEnd(addSubQueryToEnd(mainQuery, authorSubQuery), tagSubQuery, amountTags);
		String resultQuery = addSubQueryToEnd(QueryWithoutEnd, tailQuery);
		return resultQuery;
	}
	
	private String formCriteriaQueryWithoutAuthor(String mainQuery, String tagSubQuery, String tailQuery, int amountTags){
		/*
		 * This method forms SQL query with using searchCriteria when author is not set
		 */
		String QueryWithoutEnd = addIterativeSubQueryToEnd(mainQuery, tagSubQuery, amountTags);
		String resultQuery = addSubQueryToEnd(QueryWithoutEnd, tailQuery);
		return resultQuery;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
