/*
 * @(#)Comments.java   1.0 2015/08/11
 */
package by.epam.lab.hryshyn.task.first.database.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is data transfer object and encapsulates data for
 * transfer to database
 * @version 1.0 11 August 2015
 * @author Pavel_Hryshyn
 */
public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	/* This value stores comment id */
	private long id;
	
	/* This value stores news object */
	private News news;
	
	/* This value stores text of comment */
	private String commentText;
		
	/* This value stores date of creation comment */
	private Date creationDate;

	public Comment() {
		super();
	}
		
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id != other.id)
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CommentsDto [id=" + id + ", news=" + news + ", commentText=" + commentText + ", creationDate="
				+ creationDate + "]";
	}
}
