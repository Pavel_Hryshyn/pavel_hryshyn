/*
 * @(#)DaoException.java   1.0 2015/08/11
 */
package by.epam.lab.hryshyn.task.first.database.dao.exceptions;

/**
 * This class is wrapper for all DAO level exceptions  
 * @version 1.0 11 August 2015
 * @author Pavel Hryshyn
 */
public class DaoException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public DaoException(String message) {
		super(message);
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}
}
