/*
 * @(#)INewsDao.java   1.4 2015/08/24
 */
package by.epam.lab.hryshyn.task.first.database.dao.interfaces;

import java.util.List;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dto.News;
import by.epam.lab.hryshyn.task.first.database.dto.SearchCriteria;

/**
 * This interface defines methods that bind the News entities with database
 * @version 1.4 24 August 2015
 * @author Pavel Hryshyn
 */
public interface INewsDao extends IBasicDao<Long, News> {
	/**
	 * This method returns amount of all news that stores in database
	 * @return amount of all news from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	int countAllNews() throws DaoException;
	
	/**
	 * This method returns amount of all news which matching searchCriteria 
	 * that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return amount of all news which matching searchCriteria from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	int countAllWithCriteria(SearchCriteria searchCriteria) throws DaoException;
	
	/**
	 * This method returns amount of news which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @return amount of all news which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	int countNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws DaoException;
	
	/**
	 * This method returns all news sorted by amount of comments that stores in database
	 * @return list of all news sorted by amount of comments from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<News> findAllSortingByCommentAmount() throws DaoException;
	
	/**
	 * This method returns list of news which matching searchCriteria
	 * that stores in database
	 * @param searchCriteria
	 * @return list of all news which matching searchCriteria from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<News> findAllWithCriteria(SearchCriteria searchCriteria) throws DaoException;
	
	/**
	 * This method returns list of news which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of all news which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws DaoException;
	
	/**
	 * This method adds news with list of tags to database
	 * @param newsId
	 *          the param uses for adding news to database
	 * @param tagsId
	 * 			the param uses for adding list of tags to database
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	void addNewsWithTags(Long newsId, List<Long> tagsId) throws DaoException;
	
	/**
	 * This method adds news with author to database
	 * @param newsId
	 *          the param uses for adding news to database
	 * @param authorId
	 * 			the param uses for adding author to database
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	void addNewsWithAuthor(Long newsId, Long authorId) throws DaoException;
	
	/**
	 * This method deleted news with list of tags from database
	 * @param newsId
	 *          the param uses for deleting news from database
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	boolean deleteNewsWithTags(Long newsId) throws DaoException; 
	
	/**
	 * This method deleted news with author from database
	 * @param newsId
	 *          the param uses for deleting news from database
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	boolean deleteNewsWithAuthor(Long newsId) throws DaoException; 
}
