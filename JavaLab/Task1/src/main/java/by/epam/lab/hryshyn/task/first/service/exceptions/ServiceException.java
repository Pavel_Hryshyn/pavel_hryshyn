/*
 * @(#)ServiceException.java   1.0 2015/08/18
 */
package by.epam.lab.hryshyn.task.first.service.exceptions;

/**
 * This class is wrapper for all Service level exceptions  
 * @version 1.0 18 August 2015
 * @author Pavel Hryshyn
 */
public class ServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	public ServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
