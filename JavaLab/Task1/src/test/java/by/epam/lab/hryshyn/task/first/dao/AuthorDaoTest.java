package by.epam.lab.hryshyn.task.first.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.IAuthorDao;
import by.epam.lab.hryshyn.task.first.database.dto.Author;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:/test-data/author-setup.xml"})
@DatabaseTearDown(value = {"classpath:/test-data/author-setup.xml"},type = DatabaseOperation.DELETE_ALL)
public class AuthorDaoTest {

	@Autowired
	private IAuthorDao authorDao;
	
	@Test
	public void testFindAll() {
		Author expectedAuthor1 = createAuthor(1L, "author1");
		Author expectedAuthor2 = createAuthor(2L, "author2");
		Author expectedAuthor3 = createAuthor(3L, "author3");
		Author expectedAuthor4 = createAuthor(4L, "author4");
		List<Author> actualAuthors = null;
		try {
			actualAuthors = authorDao.findAll();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(4, actualAuthors.size());
		assertEqualsAuthors(expectedAuthor1, actualAuthors.get(0));
		assertEqualsAuthors(expectedAuthor2, actualAuthors.get(1));
		assertEqualsAuthors(expectedAuthor3, actualAuthors.get(2));
		assertEqualsAuthors(expectedAuthor4, actualAuthors.get(3));
	}
	
	@Test
	public void testFindAllNonExpiredAuthors() {
		Author expectedAuthor1 = createAuthor(1L, "author1");
		Author expectedAuthor2 = createAuthor(2L, "author2");
		Author expectedAuthor3 = createAuthor(3L, "author3");
		List<Author> actualAuthors = null;
		try {
			actualAuthors = authorDao.findAllNonExpiredAuthors();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(3, actualAuthors.size());
		assertEqualsAuthors(expectedAuthor1, actualAuthors.get(0));
		assertEqualsAuthors(expectedAuthor2, actualAuthors.get(1));
		assertEqualsAuthors(expectedAuthor3, actualAuthors.get(2));
	}
	
	@Test
	public void testFindById() {
		long authorId = 1L;
		Author expectedAuthor = createAuthor(authorId, "author1");
		Author actualAuthor = null;
		try {
			actualAuthor = authorDao.findById(authorId);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsAuthors(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testDelete() {
		long authorId = 1L;
		long afterDeletingId0 = 0L;
		long afterDeletingId1 = 0L;
		Author expectedAuthor1 = createAuthor(authorId, "author1");
		List<Author> actualAuthorsBeforeDelete = null;
		List<Author> actualAuthorsAfterDelete = null;
		try {
			actualAuthorsBeforeDelete = authorDao.findAllNonExpiredAuthors();
			assertEquals(3, actualAuthorsBeforeDelete.size());
			assertEqualsAuthors(expectedAuthor1, actualAuthorsBeforeDelete.get(0));
			authorDao.delete(authorId);
			actualAuthorsAfterDelete = authorDao.findAllNonExpiredAuthors();
		} catch (DaoException e) {
			fail();
		}
		afterDeletingId0 = actualAuthorsAfterDelete.get(0).getId();
		afterDeletingId1 = actualAuthorsAfterDelete.get(1).getId();
		assertEquals(2, actualAuthorsAfterDelete.size());
		assertTrue(authorId != afterDeletingId0);
		assertTrue(authorId != afterDeletingId1);
	}
	
	@Test
	public void testAdd() {
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorName("author5");
		Author actualAuthor = null;
		List<Author> actualAuthorsBeforeAdding = null;
		List<Author> actualAuthorsAfterAdding = null;
		try {
			actualAuthorsBeforeAdding = authorDao.findAll();
			assertEquals(4, actualAuthorsBeforeAdding.size());
			long actualId = authorDao.add(expectedAuthor);
			actualAuthorsAfterAdding = authorDao.findAll();
			actualAuthor = authorDao.findById(actualId);
			assertEquals(5, actualAuthorsAfterAdding.size());
			assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
		} catch (DaoException e) {
			fail();
		}
		
	}
	
	@Test
	public void testUpdate() {
		long authorId = 1L;
		Author expectedAuthor = createAuthor(authorId, "author1");
		Author actualAuthorBeforeUpdating = null;
		Author actualAuthorAfterUpdating = null;
		try {
			actualAuthorBeforeUpdating = authorDao.findById(authorId);
			assertEqualsAuthors(expectedAuthor, actualAuthorBeforeUpdating);
			expectedAuthor.setAuthorName("updatedAuthor1");
			boolean isUpdated = authorDao.update(expectedAuthor);
			assertTrue(isUpdated);
			actualAuthorAfterUpdating = authorDao.findById(authorId);
			assertEqualsAuthors(expectedAuthor, actualAuthorAfterUpdating);	
		} catch (DaoException e) {
			fail();
		}
	}
	
	private Author createAuthor(Long id, String authorName){
		Author author = new Author();
		author.setId(id);
		author.setAuthorName(authorName);
		return author;
	}
	
	private void assertEqualsAuthors(Author expectedAuthor, Author actualAuthor){
		assertEquals(expectedAuthor.getId(), actualAuthor.getId());
		assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
	}
}
