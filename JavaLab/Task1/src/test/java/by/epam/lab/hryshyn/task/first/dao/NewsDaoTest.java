package by.epam.lab.hryshyn.task.first.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.INewsDao;
import by.epam.lab.hryshyn.task.first.database.dto.Author;
import by.epam.lab.hryshyn.task.first.database.dto.News;
import by.epam.lab.hryshyn.task.first.database.dto.SearchCriteria;
import by.epam.lab.hryshyn.task.first.database.dto.Tag;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:/test-data/news-setup.xml"})
@DatabaseTearDown(value = {"classpath:/test-data/news-setup.xml"},type = DatabaseOperation.DELETE_ALL)
public class NewsDaoTest {

	@Autowired
	private INewsDao newsDao;
	
	@Test
	public void testFindAll() {
		int expectedSize = 4;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		News expectedNews3 = createNews(3L, "Title3", "ShortText3", "FullText3");
		News expectedNews4 = createNews(4L, "Title4", "ShortText4", "FullText4");
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findAll();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(3));
		assertEqualsNews(expectedNews2, actualNewsList.get(2));
		assertEqualsNews(expectedNews3, actualNewsList.get(1));
		assertEqualsNews(expectedNews4, actualNewsList.get(0));
	}
	
	@Test
	public void testFindAllSortingByCommentAmount() {
		int expectedSize = 4;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		News expectedNews3 = createNews(3L, "Title3", "ShortText3", "FullText3");
		News expectedNews4 = createNews(4L, "Title4", "ShortText4", "FullText4");
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findAllSortingByCommentAmount();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
		assertEqualsNews(expectedNews3, actualNewsList.get(3));
		assertEqualsNews(expectedNews4, actualNewsList.get(2));
	}
	
	@Test
	public void testFindAllWithCriteriaWithoutAuthorAndTags() {
		int expectedSize = 4;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		News expectedNews3 = createNews(3L, "Title3", "ShortText3", "FullText3");
		News expectedNews4 = createNews(4L, "Title4", "ShortText4", "FullText4");
		List<Tag> tags = new ArrayList<Tag>();
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findAllWithCriteria(sc);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
		assertEqualsNews(expectedNews3, actualNewsList.get(3));
		assertEqualsNews(expectedNews4, actualNewsList.get(2));
	}
	
	@Test
	public void testFindAllWithCriteriaWithoutAuthor() {
		int expectedSize = 2;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		Tag tag = createTag(1L, "tag1");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag);
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findAllWithCriteria(sc);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
	}
	
	@Test
	public void testFindAllWithCriteriaWithoutTags() {
		int expectedSize = 2;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		Author author = createAuthor(1L, "author1");
		List<Tag> tags = new ArrayList<Tag>();
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthor(author);
		sc.setTags(tags);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findAllWithCriteria(sc);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
	}
	
	@Test
	public void testFindAllWithCriteria() {
		int expectedSize = 2;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		Author author = createAuthor(1L, "author1");
		Tag tag1 = createTag(1L, "tag1");
		Tag tag3 = createTag(3L, "tag3");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag1);
		tags.add(tag3);
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthor(author);
		sc.setTags(tags);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findAllWithCriteria(sc);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
	}
	
	@Test
	public void testFindNewsWithCriteriaAndPositionsWithoutAuthorAndTags() {
		int expectedSize = 2;
		int start = 1;
		int end = 2;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		List<Tag> tags = new ArrayList<Tag>();
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
	}
	
	@Test
	public void testFindNewsWithCriteriaAndPositionsWithoutAuthor() {
		int expectedSize = 2;
		int start = 1;
		int end = 5;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		Tag tag = createTag(1L, "tag1");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag);
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
	}
	
	@Test
	public void testFindNewsWithCriteriaAndPositionsWithoutTags() {
		int expectedSize = 2;
		int start = 1;
		int end = 5;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		Author author = createAuthor(1L, "author1");
		List<Tag> tags = new ArrayList<Tag>();
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthor(author);
		sc.setTags(tags);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
	}
	
	@Test
	public void testFindNewsWithCriteriaAndPositions() {
		int expectedSize = 2;
		int start = 1;
		int end = 5;
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		Author author = createAuthor(1L, "author1");
		Tag tag1 = createTag(1L, "tag1");
		Tag tag3 = createTag(3L, "tag3");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag1);
		tags.add(tag3);
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthor(author);
		sc.setTags(tags);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualNewsList.size());
		assertEqualsNews(expectedNews1, actualNewsList.get(0));
		assertEqualsNews(expectedNews2, actualNewsList.get(1));
	}
	
	@Test
	public void testFindById() {
		News expectedNews = createNews(1L, "Title1", "ShortText1", "FullText1");
		News actualNews = null;
		try {
			actualNews = newsDao.findById(1L);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsNews(expectedNews, actualNews);
	}
	
	@Test
	public void testDelete() {
		long newsId = 4L;
		boolean isDeleted = false;
		News expectedNews = createNews(newsId, "Title4", "ShortText4", "FullText4");
		News actualNewsBeforeDeleting = null;
		News actualNewsAfterDeleting = null;
		try {
			actualNewsBeforeDeleting = newsDao.findById(newsId);
			isDeleted = newsDao.delete(newsId);
			actualNewsAfterDeleting = newsDao.findById(newsId);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsNews(expectedNews, actualNewsBeforeDeleting);
		assertTrue(isDeleted);
		assertNull(actualNewsAfterDeleting);
	}
	
	@Test
	public void testDeleteNewsWithTagsAndAuthor() {
		long newsId = 3L;
		boolean isDeleted = false;
		News expectedNews = createNews(newsId, "Title3", "ShortText3", "FullText3");
		News actualNewsBeforeDeleting = null;
		News actualNewsAfterDeleting = null;
		try {
			actualNewsBeforeDeleting = newsDao.findById(newsId);
			isDeleted = newsDao.deleteNewsWithTags(newsId);
			isDeleted &= newsDao.deleteNewsWithAuthor(newsId);
			isDeleted &= newsDao.delete(newsId);
			actualNewsAfterDeleting = newsDao.findById(newsId);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsNews(expectedNews, actualNewsBeforeDeleting);
		assertTrue(isDeleted);
		assertNull(actualNewsAfterDeleting);
	}
	
	@Test
	public void testAdd() {
		long newsId = 0;
		long actualId = 0;
		int expectedSizeBeforeAdding = 4;
		int expectedSizeAfterAdding = 5;
		News expectedNews = createNews(newsId, "Title5", "ShortText5", "FullText5");
		News actualNews = null;
		List<News> actualNewsListBeforeAdding = null;
		List<News> actualNewsListAfterAdding = null;
		try {
			actualNewsListBeforeAdding = newsDao.findAll();
			actualId = newsDao.add(expectedNews);
			expectedNews.setId(actualId);
			actualNews = newsDao.findById(actualId);
			actualNewsListAfterAdding = newsDao.findAll();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSizeBeforeAdding, actualNewsListBeforeAdding.size());
		assertEqualsNews(expectedNews, actualNews);
		assertEquals(expectedSizeAfterAdding, actualNewsListAfterAdding.size());
	}
	
	@Test
	public void testAddNewsWithTags() {
		long newsId = 4L;
		List<Long> tagsId = new ArrayList<Long>();
		tagsId.add(1L);
		boolean isDeleted = false;
		try {
			newsDao.addNewsWithTags(newsId, tagsId);
			isDeleted = newsDao.deleteNewsWithTags(newsId);
		} catch (DaoException e) {
			fail();
		}
		assertTrue(isDeleted);
	}
	
	@Test
	public void testAddNewsWithAuthor() {
		long newsId = 4L;
		long authorId = 1L;
		boolean isDeleted = false;
		try {
			newsDao.addNewsWithAuthor(newsId, authorId);
			isDeleted = newsDao.deleteNewsWithAuthor(newsId);
		} catch (DaoException e) {
			fail();
		}
		assertTrue(isDeleted);
	}
	
	@Test
	public void testUpdate() {
		long newsId = 1L;
		boolean isUpdated = false;
		News expectedNews = createNews(newsId, "Title1", "ShortText1", "FullText1");
		News actualNewsBeforeUpdating = null;
		News actualNewsAfterUpdating = null;
		try {
			actualNewsBeforeUpdating = newsDao.findById(newsId);
			assertEqualsNews(expectedNews, actualNewsBeforeUpdating);
			expectedNews.setTitle("UpdatedTitle");
			expectedNews.setShortText("UpdatedShortText");
			expectedNews.setFullText("UpdatedFullText");
			isUpdated = newsDao.update(expectedNews);
			actualNewsAfterUpdating = newsDao.findById(newsId);
			assertEqualsNews(expectedNews, actualNewsAfterUpdating);
		} catch (DaoException e) {
			fail();
		}
		assertTrue(isUpdated);
	}

	@Test
	public void testCountAllNews() {
		int expectedCount = 4;
		int actualCount = 0;
		try {
			actualCount = newsDao.countAllNews();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedCount, actualCount);
	}
	
	@Test
	public void testCountAllWithCriteriaWithoutAuthorAndTags() {
		int expectedSize = 4;
		int actualSize = 0;
		List<Tag> tags = new ArrayList<Tag>();
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		try {
			actualSize = newsDao.countAllWithCriteria(sc);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testCountAllWithCriteriaWithoutAuthor() {
		int expectedSize = 2;
		int actualSize = 0;
		List<Tag> tags = new ArrayList<Tag>();
		Tag tag = createTag(1L, "tag1");
		tags.add(tag);
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		try {
			actualSize = newsDao.countAllWithCriteria(sc);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testCountAllWithCriteriaWithoutTags() {
		int expectedSize = 2;
		int actualSize = 0;
		Author author = createAuthor(1L, "author1");
		List<Tag> tags = new ArrayList<Tag>();
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		sc.setAuthor(author);
		try {
			actualSize = newsDao.countAllWithCriteria(sc);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testCountAllWithCriteria() {
		int expectedSize = 2;
		int actualSize = 0;
		Author author = createAuthor(1L, "author1");
		List<Tag> tags = new ArrayList<Tag>();
		Tag tag = createTag(1L, "tag1");
		tags.add(tag);
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		sc.setAuthor(author);
		try {
			actualSize = newsDao.countAllWithCriteria(sc);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testCountNewsWithCriteriaAndPositionsWithoutAuthorAndTags() {
		int start = 1;
		int end = 5;
		int expectedSize = 4;
		int actualSize = 0;
		List<Tag> tags = new ArrayList<Tag>();
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		try {
			actualSize = newsDao.countNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testCountNewsWithCriteriaAndPositionsWithoutAuthor() {
		int start = 1;
		int end = 5;
		int expectedSize = 2;
		int actualSize = 0;
		List<Tag> tags = new ArrayList<Tag>();
		Tag tag = createTag(1L, "tag1");
		tags.add(tag);
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		try {
			actualSize = newsDao.countNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testCountNewsWithCriteriaAndPositionsWithoutTags() {
		int start = 1;
		int end = 5;
		int expectedSize = 2;
		int actualSize = 0;
		Author author = createAuthor(1L, "author1");
		List<Tag> tags = new ArrayList<Tag>();
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		sc.setAuthor(author);
		try {
			actualSize = newsDao.countNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testCountNewsWithCriteriaAndPositions() {
		int start = 1;
		int end = 5;
		int expectedSize = 2;
		int actualSize = 0;
		Author author = createAuthor(1L, "author1");
		List<Tag> tags = new ArrayList<Tag>();
		Tag tag = createTag(1L, "tag1");
		tags.add(tag);
		SearchCriteria sc = new SearchCriteria();
		sc.setTags(tags);
		sc.setAuthor(author);
		try {
			actualSize = newsDao.countNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualSize);
	}
	
	private News createNews(Long id, String title, String shortText, String fullText){
		News news = new News();
		news.setId(id);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		return news;
	}
	
	private Tag createTag(Long id, String tagName){
		Tag tag = new Tag();
		tag.setId(id);
		tag.setTagName(tagName);
		return tag;
	}
	
	private Author createAuthor(Long id, String authorName){
		Author author = new Author();
		author.setId(id);
		author.setAuthorName(authorName);
		return author;
	}

	private void assertEqualsNews(News expectedNews, News actualNews){
		assertEquals(expectedNews.getId(), actualNews.getId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());	
	}
}
