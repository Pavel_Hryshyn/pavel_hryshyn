package by.epam.lab.hryshyn.task.first.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ICommentDao;
import by.epam.lab.hryshyn.task.first.database.dto.Comment;
import by.epam.lab.hryshyn.task.first.database.dto.News;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:/test-data/comment-setup.xml"})
@DatabaseTearDown(value = {"classpath:/test-data/comment-setup.xml"},type = DatabaseOperation.DELETE_ALL)
public class CommentDaoTest {

	@Autowired
	private ICommentDao commentDao;
	
	@Test
	public void testFindAll() {
		int expectedSize = 3;
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News news2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		Comment expectedComment1 = createComment(1L, news1, "Comment1");
		Comment expectedComment2 = createComment(2L, news1, "Comment2");
		Comment expectedComment3 = createComment(3L, news2, "Comment3");
		List<Comment> actualComments = null;
		try {
			actualComments = commentDao.findAll();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualComments.size());
		assertEqualsComments(expectedComment1, actualComments.get(2));
		assertEqualsComments(expectedComment2, actualComments.get(1));
		assertEqualsComments(expectedComment3, actualComments.get(0));
	}
	
	@Test
	public void testFindAllByNewsId() {
		int expectedSize = 2;
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment1 = createComment(1L, news1, "Comment1");
		Comment expectedComment2 = createComment(2L, news1, "Comment2");
		List<Comment> actualComments = null;
		try {
			actualComments = commentDao.findAllByNewsId(1L);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualComments.size());
		assertEqualsComments(expectedComment1, actualComments.get(1));
		assertEqualsComments(expectedComment2, actualComments.get(0));
	}
	
	@Test
	public void testFindById() {
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment = createComment(1L, news1, "Comment1");
		Comment actualComment = null;
		try {
			actualComment = commentDao.findById(1L);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsComments(expectedComment, actualComment);
	}
	
	@Test
	public void testDelete() {
		long commentId = 1L;
		boolean isDeleted = false;
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment = createComment(commentId, news1, "Comment1");
		Comment actualCommentBeforeDeleting = null;
		Comment actualCommentAfterDeleting = null;
		try {
			actualCommentBeforeDeleting = commentDao.findById(commentId);
			isDeleted = commentDao.delete(commentId);
			actualCommentAfterDeleting = commentDao.findById(commentId);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsComments(expectedComment, actualCommentBeforeDeleting);
		assertTrue(isDeleted);
		assertNull(actualCommentAfterDeleting);
	}
	
	@Test
	public void testDeleteCommentsByNewsId() {
		int expectedSizeBeforeDeleting = 3;
		int expectedSizeAfterDeleting = 1;
		boolean isDeleted = false;
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News news2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		Comment expectedComment1 = createComment(1L, news1, "Comment1");
		Comment expectedComment2 = createComment(2L, news1, "Comment2");
		Comment expectedComment3 = createComment(3L, news2, "Comment3");
		List<Comment> actualCommentsBeforeDeleting = null;
		List<Comment> actualCommentsAfterDeleting = null;
		try {
			actualCommentsBeforeDeleting = commentDao.findAll();
			isDeleted = commentDao.deleteCommentsByNewsId(1L);
			actualCommentsAfterDeleting = commentDao.findAll();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSizeBeforeDeleting, actualCommentsBeforeDeleting.size());
		assertEqualsComments(expectedComment1, actualCommentsBeforeDeleting.get(2));
		assertEqualsComments(expectedComment2, actualCommentsBeforeDeleting.get(1));
		assertEqualsComments(expectedComment3, actualCommentsBeforeDeleting.get(0));
		assertTrue(isDeleted);
		assertEquals(expectedSizeAfterDeleting, actualCommentsAfterDeleting.size());
		assertEqualsComments(expectedComment3, actualCommentsAfterDeleting.get(0));
	}
	
	@Test
	public void testAdd() {
		int expectedSizeBeforeAdding = 3;
		int expectedSizeAfterAdding = 4;
		long actualId = 0;
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment = createComment(4L, news1, "Comment4");
		List<Comment> actualCommentsBeforeAdding = null;
		List<Comment> actualCommentsAfterAdding = null;
		Comment actualComment = null;
		try {
			actualCommentsBeforeAdding = commentDao.findAll();
			actualId = commentDao.add(expectedComment);
			expectedComment.setId(actualId);
			actualCommentsAfterAdding = commentDao.findAll();
			actualComment = commentDao.findById(actualId);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSizeBeforeAdding, actualCommentsBeforeAdding.size());
		assertEquals(expectedSizeAfterAdding, actualCommentsAfterAdding.size());
		assertEqualsComments(expectedComment, actualComment);
	}
	
	@Test(expected = DaoException.class)
	public void testUpdate() throws DaoException {
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment1 = createComment(1L, news1, "Comment1");
		commentDao.update(expectedComment1);
	}

	private Comment createComment(Long id, News news, String commentText){
		Comment сomment = new Comment();
		сomment.setId(id);
		сomment.setNews(news);
		сomment.setCommentText(commentText);
		сomment.setCreationDate(new Date());
		return сomment;
	}
	
	private News createNews(Long id, String title, String shortText, String fullText){
		News news = new News();
		news.setId(id);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		return news;
	}
	
	private void assertEqualsComments(Comment expectedComment, Comment actualComment){
		assertEquals(expectedComment.getId(), actualComment.getId());
		assertEqualsNews(expectedComment.getNews(), actualComment.getNews());
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
	}

	private void assertEqualsNews(News expectedNews, News actualNews){
		assertEquals(expectedNews.getId(), actualNews.getId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());	
	}
}
