package by.epam.lab.hryshyn.task.first.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ITagDao;
import by.epam.lab.hryshyn.task.first.database.dto.Tag;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:/test-data/tag-setup.xml"})
@DatabaseTearDown(value = {"classpath:/test-data/tag-setup.xml"},type = DatabaseOperation.DELETE_ALL)
public class TagDaoTest {

	@Autowired
	private ITagDao tagDao;
	
	@Test
	public void testFindAll() {
		int expectedSize = 3;
		Tag expectedTag1 = createTag(1L, "tag1");
		Tag expectedTag2 = createTag(2L, "tag2");
		Tag expectedTag3 = createTag(3L, "tag3");
		List<Tag> actualTags = null;
		try {
			actualTags = tagDao.findAll();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualTags.size());
		assertEqualsTags(expectedTag1, actualTags.get(0));
		assertEqualsTags(expectedTag2, actualTags.get(1));
		assertEqualsTags(expectedTag3, actualTags.get(2));
	}
	
	@Test
	public void testFindById() {
		long tagId = 1L;
		Tag expectedTag = createTag(tagId, "tag1");
		Tag actualTag = null;
		try {
			actualTag = tagDao.findById(tagId);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsTags(expectedTag, actualTag);
	}
	
	@Test
	public void testDelete() {
		long tagId = 1L;
		boolean isDeleted = false;
		Tag expectedTag = createTag(tagId, "tag1");
		Tag actualTagBeforeDeleting = null;
		Tag actualTagAfterDeleting = null;
		try {
			actualTagBeforeDeleting = tagDao.findById(tagId);
			isDeleted = tagDao.delete(tagId);
			actualTagAfterDeleting = tagDao.findById(tagId);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsTags(expectedTag, actualTagBeforeDeleting);
		assertTrue(isDeleted);
		assertNull(actualTagAfterDeleting);
	}
	
	@Test
	public void testAdd() {
		long expectedTagId = 1L;
		int expectedSizeBeforeAdding = 3;
		int expectedSizeAfterAdding = 4;
		Tag expectedTag = createTag(expectedTagId, "tag1");
		Tag actualTag = null;
		List<Tag> actualTagsBeforeAdding = null;
		List<Tag> actualTagsAfterAdding = null;
		try {
			actualTagsBeforeAdding = tagDao.findAll();
			assertEquals(expectedSizeBeforeAdding, actualTagsBeforeAdding.size());
			expectedTagId = tagDao.add(expectedTag);
			actualTagsAfterAdding = tagDao.findAll();
			assertEquals(expectedSizeAfterAdding, actualTagsAfterAdding.size());
			actualTag = tagDao.findById(expectedTagId);
			assertEquals(expectedTag.getTagName(), actualTag.getTagName());
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test(expected = DaoException.class)
	public void testUpdate() throws DaoException {
		Tag tag = new Tag();
		tagDao.update(tag);
	}
	
	private Tag createTag(Long id, String tagName){
		Tag tag = new Tag();
		tag.setId(id);
		tag.setTagName(tagName);
		return tag;
	}

	private void assertEqualsTags(Tag expectedTag, Tag actualTag){
		assertEquals(expectedTag.getId(), actualTag.getId());
		assertEquals(expectedTag.getTagName(), actualTag.getTagName());
	}
}
