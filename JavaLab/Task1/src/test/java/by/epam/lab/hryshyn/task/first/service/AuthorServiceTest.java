package by.epam.lab.hryshyn.task.first.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.IAuthorDao;
import by.epam.lab.hryshyn.task.first.database.dto.Author;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
public class AuthorServiceTest {

	@Mock
	private IAuthorDao authorDao;
	
	@InjectMocks
	private AuthorService authorService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAll() throws DaoException, ServiceException {
		Author author1 = createAuthor(1L, "author1");
		Author author2 = createAuthor(2L, "author2");
		Author author3 = createAuthor(3L, "author3");
		List<Author> expectedAuthors = new ArrayList<Author>();
		expectedAuthors.add(author1);
		expectedAuthors.add(author2);
		expectedAuthors.add(author3);
		when(authorDao.findAll()).thenReturn(expectedAuthors);
		List<Author> actualAuthors = authorService.findAll();
		verify(authorDao).findAll();
		assertEquals(3, actualAuthors.size());
		assertEqualsAuthors(expectedAuthors.get(0), actualAuthors.get(0));
		assertEqualsAuthors(expectedAuthors.get(1), actualAuthors.get(1));
		assertEqualsAuthors(expectedAuthors.get(2), actualAuthors.get(2));
		verifyNoMoreInteractions(authorDao);
	}
	
	@Test
	public void testFindAllNonExpiredAuthors() throws DaoException, ServiceException {
		Author author1 = createAuthor(1L, "author1");
		Author author2 = createAuthor(2L, "author2");
		Author author3 = createAuthor(3L, "author3");
		List<Author> expectedAuthors = new ArrayList<Author>();
		expectedAuthors.add(author1);
		expectedAuthors.add(author2);
		expectedAuthors.add(author3);
		when(authorDao.findAllNonExpiredAuthors()).thenReturn(expectedAuthors);
		List<Author> actualAuthors = authorService.findAllNonExpiredAuthors();
		verify(authorDao).findAllNonExpiredAuthors();
		assertEquals(3, actualAuthors.size());
		assertEqualsAuthors(expectedAuthors.get(0), actualAuthors.get(0));
		assertEqualsAuthors(expectedAuthors.get(1), actualAuthors.get(1));
		assertEqualsAuthors(expectedAuthors.get(2), actualAuthors.get(2));
		verifyNoMoreInteractions(authorDao);
	}
	
	@Test
	public void testFindById() throws DaoException, ServiceException {
		Author expectedAuthor = createAuthor(1L, "author");
		when(authorDao.findById(1L)).thenReturn(expectedAuthor);
		Author actualAuthor = authorService.findById(1L);
		verify(authorDao).findById(1L);
		assertEqualsAuthors(expectedAuthor, actualAuthor);
		verifyNoMoreInteractions(authorDao);
	}
	
	@Test
	public void testDelete() throws DaoException, ServiceException {
		long id = 1L;
		when(authorDao.delete(id)).thenReturn(true);
		boolean isDeleted = authorService.delete(id);
		verify(authorDao).delete(id);
		assertTrue(isDeleted);
		verifyNoMoreInteractions(authorDao);
	}
	
	@Test
	public void testAdd() throws DaoException, ServiceException {
		long expectedId = 1L;
		Author author = createAuthor(expectedId, "author");
		when(authorDao.add(author)).thenReturn(expectedId);
		long actualId = authorService.add(author);
		verify(authorDao).add(author);
		assertEquals(expectedId, actualId);
		verifyNoMoreInteractions(authorDao);
	}
	
	@Test
	public void testUpdate() throws DaoException, ServiceException {
		Author expectedAuthor = createAuthor(1L, "author");
		expectedAuthor.setAuthorName("updatedAuthor");
		when(authorDao.update(expectedAuthor)).thenReturn(true);
		boolean isUpdated = authorService.update(expectedAuthor);
		assertTrue(isUpdated);
		verify(authorDao).update(expectedAuthor);
		verifyNoMoreInteractions(authorDao);
	}
	
	private Author createAuthor(Long id, String authorName){
		Author author = new Author();
		author.setId(id);
		author.setAuthorName(authorName);
		return author;
	}
	
	private void assertEqualsAuthors(Author expectedAuthor, Author actualAuthor){
		assertEquals(expectedAuthor.getId(), actualAuthor.getId());
		assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
	}
}
