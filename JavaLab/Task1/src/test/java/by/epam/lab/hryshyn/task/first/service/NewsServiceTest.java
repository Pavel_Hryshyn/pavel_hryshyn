package by.epam.lab.hryshyn.task.first.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.lab.hryshyn.task.first.database.dao.CommentDao;
import by.epam.lab.hryshyn.task.first.database.dao.NewsDao;
import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dto.News;
import by.epam.lab.hryshyn.task.first.database.dto.SearchCriteria;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;

@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {

	@Mock
	private NewsDao newsDao;
	
	@Mock
	private CommentDao commentDao;
	
	@InjectMocks
	private NewsService newsService;
		
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAll() throws DaoException, ServiceException{
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News news2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		News news3 = createNews(3L, "Title3", "ShortText3", "FullText3");
		List<News> newsList = new ArrayList<News>();
		newsList.add(news1);
		newsList.add(news2);
		newsList.add(news3);
		when(newsDao.findAll()).thenReturn(newsList);
		List<News> actualNews = newsService.findAll();
		verify(newsDao).findAll();
		assertEquals(3, actualNews.size());
		assertEqualsNews(news1, actualNews.get(0));
		assertEqualsNews(news2, actualNews.get(1));
		assertEqualsNews(news3, actualNews.get(2));
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testFindAllSortingByCommentAmount() throws ServiceException, DaoException{
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News news2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		News news3 = createNews(3L, "Title3", "ShortText3", "FullText3");
		List<News> newsList = new ArrayList<News>();
		newsList.add(news1);
		newsList.add(news2);
		newsList.add(news3);
		when(newsDao.findAllSortingByCommentAmount()).thenReturn(newsList);
		List<News> actualNews = newsService.findAllSortingByCommentAmount();
		verify(newsDao).findAllSortingByCommentAmount();
		assertEquals(3, actualNews.size());
		assertEqualsNews(news1, actualNews.get(0));
		assertEqualsNews(news2, actualNews.get(1));
		assertEqualsNews(news3, actualNews.get(2));
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testFindAllWithCriteria() throws DaoException, ServiceException{
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		News expectedNews3 = createNews(3L, "Title3", "ShortText3", "FullText3");
		List<News> newsList = new ArrayList<News>();
		newsList.add(expectedNews1);
		newsList.add(expectedNews2);
		newsList.add(expectedNews3);
		SearchCriteria searchCriteria = new SearchCriteria();
		when(newsDao.findAllWithCriteria(searchCriteria)).thenReturn(newsList);
		List<News> actualNews = newsService.findAllWithCriteria(searchCriteria);
		verify(newsDao).findAllWithCriteria(searchCriteria);
		assertEquals(3, actualNews.size());
		assertEqualsNews(expectedNews1, actualNews.get(0));
		assertEqualsNews(expectedNews2, actualNews.get(1));
		assertEqualsNews(expectedNews3, actualNews.get(2));
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testFindNewsWithCriteriaAndPositions() throws DaoException, ServiceException{
		News expectedNews1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News expectedNews2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		News expectedNews3 = createNews(3L, "Title3", "ShortText3", "FullText3");
		List<News> newsList = new ArrayList<News>();
		newsList.add(expectedNews1);
		newsList.add(expectedNews2);
		newsList.add(expectedNews3);
		SearchCriteria searchCriteria = new SearchCriteria();
		int start = 1;
		int end = 4;
		when(newsDao.findNewsWithCriteriaAndPositions(searchCriteria, start, end)).thenReturn(newsList);
		List<News> actualNews = newsService.findNewsWithCriteriaAndPositions(searchCriteria, start, end);
		verify(newsDao).findNewsWithCriteriaAndPositions(searchCriteria, start, end);
		assertEquals(3, actualNews.size());
		assertEqualsNews(expectedNews1, actualNews.get(0));
		assertEqualsNews(expectedNews2, actualNews.get(1));
		assertEqualsNews(expectedNews3, actualNews.get(2));
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testFindById() throws DaoException, ServiceException{
		News expectedNews = createNews(1L, "Title", "ShortText", "FullText");
		when(newsDao.findById(1L)).thenReturn(expectedNews);
		News actualNews = newsService.findById(1L);
		verify(newsDao).findById(1L);
		assertEqualsNews(expectedNews, actualNews);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testDelete() throws DaoException, ServiceException{
		long newsId = 1L;
		when(newsDao.delete(newsId)).thenReturn(true);
		when(newsDao.deleteNewsWithAuthor(newsId)).thenReturn(true);
		when(newsDao.deleteNewsWithTags(newsId)).thenReturn(true);
		when(commentDao.deleteCommentsByNewsId(newsId)).thenReturn(true);
		boolean isDeleted = newsService.delete(newsId);
		verify(newsDao).delete(newsId);
		verify(newsDao).deleteNewsWithAuthor(newsId);
		verify(newsDao).deleteNewsWithTags(newsId);
		verify(commentDao).deleteCommentsByNewsId(newsId);
		assertTrue(isDeleted);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test(expected = ServiceException.class)
	public void testAdd() throws DaoException, ServiceException{
		Exception e = new Exception();
		News news = createNews(1L, "Title1", "ShortText1", "FullText1");
		doThrow(new DaoException("DaoException", e)).when(newsDao).update(news);
		newsService.update(news);
		verify(newsDao).update(news);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testAddWithAuthorAndTags() throws DaoException, ServiceException{
		long expectedNewsId = 1L;
		long authorId = 1L;
		News news = createNews(expectedNewsId, "Title", "ShortText", "FullText");
		List<Long> tagsId = new ArrayList<Long>();
		when(newsDao.add(news)).thenReturn(expectedNewsId);
		long actualNewsId = newsService.addWithAuthorAndTags(news, authorId, tagsId);
		verify(newsDao).add(news);
		verify(newsDao).addNewsWithAuthor(expectedNewsId, authorId);
		verify(newsDao).addNewsWithTags(expectedNewsId, tagsId);
		assertEquals(expectedNewsId, actualNewsId);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testUpdate() throws DaoException, ServiceException{
		News expectedNews = createNews(1L, "Title", "ShortText", "FullText");
		when(newsDao.update(expectedNews)).thenReturn(true);
		boolean isUpdated = newsService.update(expectedNews);
		assertTrue(isUpdated);
		verify(newsDao).update(expectedNews);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testCountAllNews() throws DaoException, ServiceException{
		int expectedCount = 3;
		when(newsDao.countAllNews()).thenReturn(expectedCount);
		int actualCount = newsService.countAllNews();
		assertEquals(expectedCount, actualCount);
		verify(newsDao).countAllNews();
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testCountAllWithCriteria() throws DaoException, ServiceException{
		int expectedCount = 3;
		SearchCriteria searchCriteria = new SearchCriteria();
		when(newsDao.countAllWithCriteria(searchCriteria)).thenReturn(expectedCount);
		int actualCount = newsService.countAllWithCriteria(searchCriteria);
		assertEquals(expectedCount, actualCount);
		verify(newsDao).countAllWithCriteria(searchCriteria);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testCountNewsWithCriteriaAndPositions() throws DaoException, ServiceException{
		int expectedCount = 3;
		int start = 0;
		int end =3;
		SearchCriteria searchCriteria = new SearchCriteria();
		when(newsDao.countNewsWithCriteriaAndPositions(searchCriteria, start, end)).thenReturn(expectedCount);
		int actualCount = newsService.countNewsWithCriteriaAndPositions(searchCriteria, start, end);
		assertEquals(expectedCount, actualCount);
		verify(newsDao).countNewsWithCriteriaAndPositions(searchCriteria, start, end);
		verifyNoMoreInteractions(newsDao);
	}
	
	
	private News createNews(Long id, String title, String shortText, String fullText){
		News news = new News();
		news.setId(id);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		return news;
	}
	
	private void assertEqualsNews(News expectedNews, News actualNews){
		assertEquals(expectedNews.getId(), actualNews.getId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
		assertEquals(expectedNews.getCreationDate(), actualNews.getCreationDate());
	}	
}
