package by.epam.lab.hryshyn.task.first.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ITagDao;
import by.epam.lab.hryshyn.task.first.database.dto.Tag;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;

@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest {

	@Mock
	private ITagDao tagDao;
	
	@InjectMocks
	private TagService tagService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAll() throws DaoException, ServiceException {
		List<Tag> expectedTags = new ArrayList<Tag>();
		expectedTags.add(createTag(1L, "Tag1"));
		expectedTags.add(createTag(2L, "Tag3"));
		expectedTags.add(createTag(3L, "Tag3"));
		when(tagDao.findAll()).thenReturn(expectedTags);
		List<Tag> actualTags = tagService.findAll();
		verify(tagDao).findAll();
		assertEquals(3, actualTags.size());
		assertEqualsTag(expectedTags.get(0), actualTags.get(0));
		assertEqualsTag(expectedTags.get(1), actualTags.get(1));
		assertEqualsTag(expectedTags.get(2), actualTags.get(2));
		verifyNoMoreInteractions(tagDao);
	}
	
	@Test
	public void testFindById() throws DaoException, ServiceException {
		Tag expectedTag = createTag(1L, "Tag1");
		when(tagDao.findById(1L)).thenReturn(expectedTag);
		Tag actualTag = tagService.findById(1L);
		verify(tagDao).findById(1L);
		assertEqualsTag(expectedTag, actualTag);
		verifyNoMoreInteractions(tagDao);
	}
	
	@Test
	public void testDelete() throws DaoException, ServiceException {
		long id = 1L;
		when(tagDao.delete(id)).thenReturn(true);
		boolean isDeleted = tagService.delete(id);
		verify(tagDao).delete(id);
		assertTrue(isDeleted);
		verifyNoMoreInteractions(tagDao);
	}
	
	@Test
	public void testAdd() throws DaoException, ServiceException {
		Tag tag = createTag(1L, "Tag1");
		when(tagDao.add(tag)).thenReturn(1L);
		long id = tagService.add(tag);
		verify(tagDao).add(tag);
		assertEquals(1L, id);
		verifyNoMoreInteractions(tagDao);
	}
	
	
	@Test(expected = ServiceException.class)
	public void testUpdate() throws DaoException, ServiceException {
		Exception e = new Exception();
		Tag tag = createTag(1L, "Tag1");
		doThrow(new DaoException("DaoException", e)).when(tagDao).update(tag);
		tagService.update(tag);
		verify(tagDao).update(tag);
		verifyNoMoreInteractions(tagDao);
	}
	
	private Tag createTag(Long id, String tagName){
		Tag tag = new Tag();
		tag.setId(id);
		tag.setTagName(tagName);
		return tag;
	}
	
	private void assertEqualsTag(Tag expectedTag, Tag actualTag){
		assertEquals(expectedTag.getId(), actualTag.getId());
		assertEquals(expectedTag.getTagName(), actualTag.getTagName());
	}
}
