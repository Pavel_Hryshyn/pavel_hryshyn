package by.epam.lab.hryshyn.task.first.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.lab.hryshyn.task.first.database.dao.exceptions.DaoException;
import by.epam.lab.hryshyn.task.first.database.dao.interfaces.ICommentDao;
import by.epam.lab.hryshyn.task.first.database.dto.Comment;
import by.epam.lab.hryshyn.task.first.database.dto.News;
import by.epam.lab.hryshyn.task.first.service.exceptions.ServiceException;

@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest {

	@Mock
	private ICommentDao commentDao;
	
	@InjectMocks
	private CommentService commentService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAll() throws DaoException, ServiceException {
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		News news2 = createNews(2L, "Title2", "ShortText2", "FullText2");
		News news3 = createNews(3L, "Title3", "ShortText3", "FullText3");
		Comment expectedComment1 = createComment(1L, news1, "Comment1");
		Comment expectedComment2 = createComment(2L, news2, "Comment2");
		Comment expectedComment3 = createComment(3L, news3, "Comment3");
		List<Comment> expectedComments = new ArrayList<Comment>();
		expectedComments.add(expectedComment1);
		expectedComments.add(expectedComment2);
		expectedComments.add(expectedComment3);
		when(commentDao.findAll()).thenReturn(expectedComments);
		List<Comment> actualComments = commentService.findAll();
		verify(commentDao).findAll();
		assertEquals(3, actualComments.size());
		assertEqualsComments(expectedComments.get(0), actualComments.get(0));
		assertEqualsComments(expectedComments.get(1), actualComments.get(1));
		assertEqualsComments(expectedComments.get(2), actualComments.get(2));
		verifyNoMoreInteractions(commentDao);
	}
	
	@Test
	public void testFindAllByNewsId() throws DaoException, ServiceException {
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment1 = createComment(1L, news1, "Comment1");
		Comment expectedComment2 = createComment(2L, news1, "Comment2");
		Comment expectedComment3 = createComment(3L, news1, "Comment3");
		List<Comment> expectedComments = new ArrayList<Comment>();
		expectedComments.add(expectedComment1);
		expectedComments.add(expectedComment2);
		expectedComments.add(expectedComment3);
		when(commentDao.findAllByNewsId(1L)).thenReturn(expectedComments);
		List<Comment> actualComments = commentService.findAllByNewsId(1L);
		verify(commentDao).findAllByNewsId(1L);
		assertEquals(3, actualComments.size());
		assertEqualsComments(expectedComments.get(0), actualComments.get(0));
		assertEqualsComments(expectedComments.get(1), actualComments.get(1));
		assertEqualsComments(expectedComments.get(2), actualComments.get(2));
		verifyNoMoreInteractions(commentDao);
	}
	
	@Test
	public void testFindById() throws DaoException, ServiceException {
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment = createComment(1L, news1, "Comment1");
		when(commentDao.findById(1L)).thenReturn(expectedComment);
		Comment actualComment = commentService.findById(1L);
		verify(commentDao).findById(1L);
		assertEqualsComments(expectedComment, actualComment);
		verifyNoMoreInteractions(commentDao);
	}
	
	@Test
	public void testDelete() throws DaoException, ServiceException {
		long id = 1L;
		when(commentDao.delete(id)).thenReturn(true);
		boolean isDeleted = commentService.delete(id);
		verify(commentDao).delete(id);
		assertTrue(isDeleted);
		verifyNoMoreInteractions(commentDao);
	}
	
	@Test
	public void testAdd() throws DaoException, ServiceException {
		long expectedId = 1L;
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment = createComment(1L, news1, "Comment1");
		when(commentDao.add(expectedComment)).thenReturn(expectedId);
		long actualId = commentService.add(expectedComment);
		verify(commentDao).add(expectedComment);
		assertEquals(expectedId, actualId);
		verifyNoMoreInteractions(commentDao);
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdate() throws DaoException, ServiceException {
		Exception e = new Exception();
		News news1 = createNews(1L, "Title1", "ShortText1", "FullText1");
		Comment expectedComment = createComment(1L, news1, "Comment1");
		doThrow(new DaoException("DaoException", e)).when(commentDao).update(expectedComment);
		commentService.update(expectedComment);
		verify(commentDao).update(expectedComment);
		verifyNoMoreInteractions(commentDao);
	}
	
	private Comment createComment(Long id, News news, String commentText){
		Comment сomment = new Comment();
		сomment.setId(id);
		сomment.setNews(news);
		сomment.setCommentText(commentText);
		сomment.setCreationDate(new Date());
		return сomment;
	}
	
	private News createNews(Long id, String title, String shortText, String fullText){
		News news = new News();
		news.setId(id);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		return news;
	}
	
	private void assertEqualsComments(Comment expectedComment, Comment actualComment){
		assertEquals(expectedComment.getId(), actualComment.getId());
		assertEqualsNews(expectedComment.getNews(), actualComment.getNews());
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		assertEquals(expectedComment.getCreationDate(), actualComment.getCreationDate());
	}

	private void assertEqualsNews(News expectedNews, News actualNews){
		assertEquals(expectedNews.getId(), actualNews.getId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
		assertEquals(expectedNews.getCreationDate(), actualNews.getCreationDate());		
	}
}
