/*
 * @(#)NewsBeanImpl.java   1.0 2016/03/24
 */
package com.epam.newsportal.beans.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.epam.newsportal.beans.INewsBean;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.INewsService;

/**
 * This class implements methods INewsBean and represents facade for News service
 * @version 1.0 24 March 2016
 * @author Pavel_Hryshyn
 */
@ManagedBean(name = "newsBean")
@SessionScoped
public class NewsBeanImpl implements INewsBean {

	@EJB
	private INewsService newsService;
	
	@Override
	public List<News> findBetweenPositions(int start, int end) throws BeansException {
		List<News> newsList = null;
		try {
			newsList = newsService.findBetweenPositions(start, end);
		} catch (ServiceException e) {
			throw new BeansException("findBetweenPositions() method of NewsBean is failed", e);
		}
		return newsList;
	}

	@Override
	public List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws BeansException {
		List<News> newsList = null;
		try {
			newsList = newsService.findNewsWithCriteriaAndPositions(sc, start, end);
		} catch (ServiceException e) {
			throw new BeansException("findNewsWithCriteriaAndPositions() method of NewsBean is failed", e);
		}
		return newsList;
	}

	@Override
	public News findById(Long newsId) throws BeansException {
		News news = null;
		try {
			news = newsService.findById(newsId);
		} catch (ServiceException e) {
			throw new BeansException("findById() method of NewsBean is failed", e);
		}
		return news;
	}

	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria sc) throws BeansException {
		Long nextId = null;
		try {
			nextId = newsService.getNextNewsId(newsId, sc);
		} catch (ServiceException e) {
			throw new BeansException("getNextNewsId() method of NewsBean is failed", e);
		}
		return nextId;
	}

	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria sc) throws BeansException {
		Long previousId = null;
		try {
			previousId = newsService.getPreviousNewsId(newsId, sc);
		} catch (ServiceException e) {
			throw new BeansException("getPreviousNewsId() method of NewsBean is failed", e);
		}
		return previousId;
	}

	@Override
	public long countAllNews() throws BeansException {
		long amount = 0L;
		try {
			amount = newsService.countAllNews();
		} catch (ServiceException e) {
			throw new BeansException("countAllNews() method of NewsBean is failed", e);
		}
		return amount;
	}

	@Override
	public long countNewsWithCriteria(SearchCriteria searchCriteria) throws BeansException {
		long amount = 0L;
		try {
			amount = newsService.countNewsWithCriteria(searchCriteria);
		} catch (ServiceException e) {
			throw new BeansException("countNewsWithCriteria() method of NewsBean is failed", e);
		}
		return amount;
	}

	@Override
	public Long create(News news) throws BeansException {
		Long id = null;
		try {
			id = newsService.create(news);
		} catch (ServiceException e) {
			throw new BeansException("create() method of NewsBean is failed", e);
		}
		return id;
	}

	@Override
	public boolean update(News news) throws BeansException {
		boolean isUpdated = false;
		try {
			isUpdated = newsService.update(news);
		} catch (ServiceException e) {
			throw new BeansException("update() method of NewsBean is failed", e);
		}
		return isUpdated;
	}

	@Override
	public boolean delete(Long newsId) throws BeansException {
		boolean isDeleted = false;
		try {
			isDeleted = newsService.delete(newsId);
		} catch (ServiceException e) {
			throw new BeansException("delete() method of NewsBean is failed", e);
		}
		return isDeleted;
	}

	public INewsService getNewsService() {
		return newsService;
	}

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}
}
