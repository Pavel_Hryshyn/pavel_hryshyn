/*
 * @(#)NewsBeanImpl.java   1.0 2016/03/25
 */
package com.epam.newsportal.beans.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.epam.newsportal.beans.IAuthorBean;
import com.epam.newsportal.beans.IBeanManager;
import com.epam.newsportal.beans.ICommentBean;
import com.epam.newsportal.beans.INewsBean;
import com.epam.newsportal.beans.ITagBean;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.vo.NewsComplexVO;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.IServiceManager;

/**
 * This class implements methods IBeanManager and represents facade for  IServiceManager
 * @version 1.0 25 March 2016
 * @author Pavel_Hryshyn
 */
@ManagedBean(name = "beanManager")
@SessionScoped
public class BeanManagerImpl implements IBeanManager {
	
	@EJB
	private IServiceManager serviceManager;
	
	@EJB
	private IAuthorBean authorBean;
	
	@EJB
	private ITagBean tagBean;
	
	@EJB
	private ICommentBean commentBean;
	
	@EJB
	private INewsBean newsBean;

	@Override
	public NewsComplexVO findByNewsId(Long newsId) throws BeansException {
		NewsComplexVO newsVO = null;
		try {
			newsVO = serviceManager.findByNewsId(newsId);
		} catch (ServiceException e) {
			throw new BeansException("findByNewsId() method of BeanManager is failed", e);
		}
		return newsVO;
	}

	@Override
	public List<NewsComplexVO> findNewsComplexWithCriteriaAndPositions(SearchCriteria sc, int start, int end)
			throws BeansException {
		List<NewsComplexVO> complexList = null;
		try {
			complexList = serviceManager.findNewsComplexWithCriteriaAndPositions(sc, start, end);
		} catch (ServiceException e) {
			throw new BeansException("findNewsComplexWithCriteriaAndPositions() method of BeanManager is failed", e);
		}
		return complexList;
	}

	@Override
	public IAuthorBean getAuthorBean() {
		return authorBean;
	}

	@Override
	public ICommentBean getCommentBean() {
		return commentBean;
	}

	@Override
	public INewsBean getNewsBean() {
		return newsBean;
	}

	@Override
	public ITagBean getTagBean() {
		return tagBean;
	}

	public void setAuthorBean(IAuthorBean authorBean) {
		this.authorBean = authorBean;
	}

	public void setTagBean(ITagBean tagBean) {
		this.tagBean = tagBean;
	}

	public void setCommentBean(ICommentBean commentBean) {
		this.commentBean = commentBean;
	}

	public void setNewsBean(INewsBean newsBean) {
		this.newsBean = newsBean;
	}

	public IServiceManager getServiceManager() {
		return serviceManager;
	}

	public void setServiceManager(IServiceManager serviceManager) {
		this.serviceManager = serviceManager;
	}
}
