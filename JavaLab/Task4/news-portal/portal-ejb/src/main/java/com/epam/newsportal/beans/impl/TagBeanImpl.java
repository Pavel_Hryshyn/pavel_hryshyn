/*
 * @(#)TagBeanImpl.java   1.0 2016/03/24
 */
package com.epam.newsportal.beans.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.epam.newsportal.beans.ITagBean;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.ITagService;

/**
 * This class implements methods ITagBean and represents facade for Tag service
 * @version 1.0 24 March 2016
 * @author Pavel_Hryshyn
 */
@ManagedBean(name = "tagBean")
@SessionScoped
public class TagBeanImpl implements ITagBean {

	@EJB
	private ITagService tagService;
	
	@Override
	public List<Tag> findAll() throws BeansException {
		List<Tag> tags = null;
		try {
			tags = tagService.findAll();
		} catch (ServiceException e) {
			throw new BeansException("findAll() method of TagBean is failed", e);
		}
		return tags;
	}

	@Override
	public List<Tag> findByNewsId(Long newsId) throws BeansException {
		List<Tag> tags = null;
		try {
			tags = tagService.findByNewsId(newsId);
		} catch (ServiceException e) {
			throw new BeansException("findByNewsId() method of TagBean is failed", e);
		}
		return tags;
	}

	@Override
	public Tag findById(Long tagId) throws BeansException {
		Tag tag = null;
		try {
			tag = tagService.findById(tagId);
		} catch (ServiceException e) {
			throw new BeansException("findById() method of TagBean is failed", e);
		}
		return tag;
	}

	@Override
	public Long create(Tag tag) throws BeansException {
		Long id = null;
		try {
			id = tagService.create(tag);
		} catch (ServiceException e) {
			throw new BeansException("create() method of TagBean is failed", e);
		}
		return id;
	}

	@Override
	public boolean update(Tag tag) throws BeansException {
		boolean isUpdated = false;
		try {
			isUpdated = tagService.update(tag);
		} catch (ServiceException e) {
			throw new BeansException("update() method of TagBean is failed", e);
		}
		return isUpdated;
	}

	@Override
	public boolean delete(Long tagId) throws BeansException {
		boolean isDeleted = false;
		try {
			isDeleted = tagService.delete(tagId);
		} catch (ServiceException e) {
			throw new BeansException("delete() method of TagBean is failed", e);
		}
		return isDeleted;
	}

	public ITagService getTagService() {
		return tagService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}
}
