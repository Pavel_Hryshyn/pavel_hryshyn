/*
 * @(#)IAuthorBean.java   1.0 2016/03/22
 */
package com.epam.newsportal.beans;

import java.util.List;

import com.epam.newsportal.entity.Author;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.service.IAuthorService;

/**
 * This interface defines basic methods for Author Bean and 
 * represents facade for AuthorService
 * @version 1.0 22 March 2016
 * @author Pavel_Hryshyn
 */
public interface IAuthorBean {
	/**
	 * This method returns a list of all authors that stores in database
	 * @return list of all authors from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	List<Author> findAll() throws BeansException;
	
	/**
	 * This method returns a list of all non-expired authors that stores in database
	 * @return  a list of all non-expired authors from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	List<Author> findAllNonExpiredAuthors() throws BeansException;
	
	/**
	 * This method returns author with some id that stores in database
	 * @param authorId
	 * 			the param uses for searching author in database
	 * @return author with some id from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	Author findById(Long authorId) throws BeansException;
	
	/**
	 * This method returns author for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching author in database
	 * @return author for some newsId from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	Author findByNewsId(Long newsId) throws BeansException;
	
	/**
	 * This method adds author to database
	 * @param author
	 * 			the param uses for adding author to database
	 * @return author's id
	 * @throws BeansException, if arise ejb layer errors
	 */
	Long create(Author author) throws BeansException;
	
	/**
	 * This method updates author to database
	 * @param author
	 *            the param uses for updating author to database
	 * @return true if author is updated to database, else false
	 * @throws BeansException, if arise ejb layer errors
	 */
	boolean update(Author author) throws BeansException;
	
	/**
	 * This method sets a author expired 
	 * @param authorId
	 * 			the param uses for expired author
	 * @return true if a author is expired, else false
	 * @throws BeansException, if arise ejb layer errors
	 */
	boolean expire(Long authorId) throws BeansException;
	
	IAuthorService getAuthorService();
	
	void setAuthorService(IAuthorService authorService);
}
