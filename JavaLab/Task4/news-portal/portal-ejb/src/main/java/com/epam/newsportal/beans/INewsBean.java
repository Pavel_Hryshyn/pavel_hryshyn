/*
 * @(#)INewsBean.java   1.0 2016/03/24
 */
package com.epam.newsportal.beans;

import java.util.List;

import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.service.INewsService;

/**
 * This interface defines basic methods for News Bean and 
 * represents facade for NewsService
 * @version 1.0 24 March 2016
 * @author Pavel_Hryshyn
 */
public interface INewsBean {
	/**
	 * This method returns a list of all news that stores in database
	 * @return list of all news from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	List<News> findBetweenPositions(int start, int end) throws BeansException;
	
	/**
	 * This method returns list of news which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of all news which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws BeansException;
	
	
	/**
	 * This method returns news with some id that stores in database
	 * @param newsId
	 * 			the param uses for searching news in database
	 * @return news with some id from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	News findById(Long newsId) throws BeansException;
	
	/**
	 * This method returns next newsId from list of news formed by SearchCriteris
	 * @param newsId
	 * 			the param uses for getting next newsId in database
	 * @param sc
	 * 			the param uses for formed list of news
	 * @return next newsId
	 * @throws BeansException, if arise ejb layer errors
	 */
	Long getNextNewsId(Long newsId, SearchCriteria sc) throws BeansException;
	
	/**
	 * This method returns previous newsId from list of news formed by SearchCriteris
	 * @param newsId
	 * 			the param uses for getting previous newsId in database
	 * @param sc
	 * 			the param uses for formed list of news
	 * @return previous newsId
	 * @throws BeansException, if arise ejb layer errors
	 */
	Long getPreviousNewsId(Long newsId, SearchCriteria sc) throws BeansException;
	
	/**
	 * This method returns amount of all news that stores in database
	 * @return amount of all news from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	long countAllNews() throws BeansException;
	
	/**
	 * This method returns amount of all news which matching searchCriteria 
	 * that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return amount of all news which matching searchCriteria from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	long countNewsWithCriteria(SearchCriteria searchCriteria) throws BeansException;
	
	/**
	 * This method adds News entity to database
	 * @param news
	 * 			the param uses for adding news to database
	 * @return news's id if news is added to database
	 * @throws BeansException, if arise ejb layer errors
	 */
	Long create(News news) throws BeansException;
	
	/**
	 * This method adds News entity to database
	 * @param news
	 * 			the param uses for adding news to database
	 * @return news's id if news is added to database
	 * @throws BeansException, if arise ejb layer errors
	 */
	boolean update(News news) throws BeansException;
	
	/**
	 * This method deletes a news with some newsId from database
	 * @param newsId
	 * 			the param uses for deleting a news from database
	 * @return true if a news is deleted from database, else false
	 * @throws BeansException, if arise ejb layer errors
	 */
	boolean delete(Long newsId) throws BeansException;
	
	INewsService getNewsService();
	
	void setNewsService(INewsService newsService);
}
