/*
 * @(#)ITagBean.java   1.0 2016/03/22
 */
package com.epam.newsportal.beans;

import java.util.List;

import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.service.ITagService;

/**
 * This interface defines basic methods for Tag Bean and 
 * represents facade for TagService
 * @version 1.0 22 March 2016
 * @author Pavel_Hryshyn
 */
public interface ITagBean {
	/**
	 * This method returns a list of all tags that stores in database
	 * @return list of all tags from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	List<Tag> findAll() throws BeansException;
	
	/**
	 * This method returns a list of tags for news with some newsId that stores in database
	 * @param newsId
	 * @return list of tags for news with some newsId from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	List<Tag> findByNewsId(Long newsId) throws BeansException;
	
	/**
	 * This method tag with some tagId that stores in database
	 * @param tagId
	 * @return tag with some tagId with some newsId from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	Tag findById(Long tagId) throws BeansException;
	
	/**
	 * This method adds tag to database
	 * @param tag
	 * 			the param uses for adding tag to database
	 * @return tagId
	 * @throws BeansException, if arise ejb layer errors
	 */
	Long create(Tag tag) throws BeansException;
	
	/**
	 * This method updates tag to database
	 * @param tag
	 *            the param uses for updating tag to database
	 * @return true if tag is updated to database, else false
	 * @throws BeansException, if arise ejb layer errors
	 */
	boolean update(Tag tag) throws BeansException;
	
	/**
	 * This method deletes a tag from database
	 * @param tagId
	 * 			the param uses for deleting tag
	 * @return true if a tag is deleted, else false
	 * @throws BeansException, if arise ejb layer errors
	 */
	boolean delete(Long tagId) throws BeansException;
	
	ITagService getTagService();
	
	void setTagService(ITagService tagService);
}
