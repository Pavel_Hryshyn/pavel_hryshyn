/*
 * @(#)ICommentBean.java   1.0 2016/03/22
 */
package com.epam.newsportal.beans;

import java.util.List;

import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.service.ICommentService;

/**
 * This interface defines basic methods for Author Bean and 
 * represents facade for AuthorService
 * @version 1.0 22 March 2016
 * @author Pavel_Hryshyn
 */
public interface ICommentBean {
	/**
	 * This method returns list of comments for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching list of comments in database
	 * @return list of comments for some newsId from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	List<Comment> findByNewsId(Long newsId) throws BeansException;
	
	/**
	 * This method adds comment to database
	 * @param comment
	 * 			the param uses for adding comment to database
	 * @return comment's id
	 * @throws BeansException, if arise ejb layer errors
	 */
	Long create(Comment comment) throws BeansException;
	
	/**
	 * This method delete comment with some id that stores in database
	 * @param id
	 * 			the param uses for deleting comment from database
	 * @return true, if comment are deleted, else return false
	 * @throws BeansException, if arise ejb layer errors
	 */
	boolean delete(Long id) throws BeansException;
	
	/**
	 * This method get comments amount for news from database
	 * @param newsId
	 * @return comments amount
	 * @throws BeansException, if arise ejb layer errors
	 */
	boolean deleteCommentsByNewsId(Long newsId) throws BeansException;
	
	ICommentService getCommentService();
	
	void setCommentService(ICommentService commentService);
}
