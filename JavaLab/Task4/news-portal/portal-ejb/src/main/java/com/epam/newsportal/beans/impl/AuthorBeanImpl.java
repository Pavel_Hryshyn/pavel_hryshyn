/*
 * @(#)AuthorBeanImpl.java   1.0 2016/03/22
 */
package com.epam.newsportal.beans.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.epam.newsportal.beans.IAuthorBean;
import com.epam.newsportal.entity.Author;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.IAuthorService;

/**
 * This class implements methods IAuthorBean and represents facade for Author service
 * @version 1.0 22 March 2016
 * @author Pavel_Hryshyn
 */
@ManagedBean(name = "authorBean")
@SessionScoped
public class AuthorBeanImpl implements IAuthorBean {

	@EJB
	private IAuthorService authorService;
	
	@Override
	public List<Author> findAll() throws BeansException {
		List<Author> authors = null;
		try {
			authors = authorService.findAll();
		} catch (ServiceException e) {
			throw new BeansException("findAll() method of AuthorBean is failed", e);
		}
		return authors;
	}

	@Override
	public List<Author> findAllNonExpiredAuthors() throws BeansException {
		List<Author> authors = null;
		try {
			authors = authorService.findAllNonExpiredAuthors();
		} catch (ServiceException e) {
			throw new BeansException("findAllNonExpiredAuthors() method of AuthorBean is failed", e);
		}
		return authors;
	}

	@Override
	public Author findById(Long authorId) throws BeansException {
		Author author = null;
		try {
			author = authorService.findById(authorId);
		} catch (ServiceException e) {
			throw new BeansException("findById() method of AuthorBean is failed", e);
		}
		return author;
	}

	@Override
	public Author findByNewsId(Long newsId) throws BeansException {
		Author author = null;
		try {
			author = authorService.findByNewsId(newsId);
		} catch (ServiceException e) {
			throw new BeansException("findByNewsId() method of AuthorBean is failed", e);
		}
		return author;
	}

	@Override
	public Long create(Author author) throws BeansException {
		Long id = null;
		try {
			id = authorService.create(author);
		} catch (ServiceException e) {
			throw new BeansException("create() method of AuthorBean is failed", e);
		}
		return id;
	}

	@Override
	public boolean update(Author author) throws BeansException {
		boolean isUpdated = false;
		try {
			isUpdated = authorService.update(author);
		} catch (ServiceException e) {
			throw new BeansException("update() method of AuthorBean is failed", e);
		}
		return isUpdated;
	}

	@Override
	public boolean expire(Long authorId) throws BeansException {
		boolean isExpired = false;
		try {
			isExpired = authorService.expire(authorId);
		} catch (ServiceException e) {
			throw new BeansException("expire() method of AuthorBean is failed", e);
		}
		return isExpired;
	}

	public IAuthorService getAuthorService() {
		return authorService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}
}
