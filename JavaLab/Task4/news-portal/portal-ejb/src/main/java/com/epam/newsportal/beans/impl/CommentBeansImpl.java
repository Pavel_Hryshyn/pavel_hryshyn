/*
 * @(#)CommentBeansImpl.java   1.0 2016/03/22
 */
package com.epam.newsportal.beans.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.epam.newsportal.beans.ICommentBean;
import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.exception.BeansException;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.ICommentService;

/**
 * This class implements methods ICommentBean and represents facade for Comment service
 * @version 1.0 22 March 2016
 * @author Pavel_Hryshyn
 */
@ManagedBean(name = "commentBean")
@SessionScoped
public class CommentBeansImpl implements ICommentBean {

	@EJB
	private ICommentService commentService;
	
	@Override
	public List<Comment> findByNewsId(Long newsId) throws BeansException {
		List<Comment> comments = null;
		try {
			comments = commentService.findByNewsId(newsId);
		} catch (ServiceException e) {
			throw new BeansException("findByNewsId() method of ICommentBean is failed", e);
		}
		return comments;
	}

	@Override
	public Long create(Comment comment) throws BeansException {
		Long id = null;
		try {
			id = commentService.create(comment);
		} catch (ServiceException e) {
			throw new BeansException("create() method of ICommentBean is failed", e);
		}
		return id;
	}

	@Override
	public boolean delete(Long id) throws BeansException {
		boolean isDeleted = false;
		try {
			isDeleted = commentService.delete(id);
		} catch (ServiceException e) {
			throw new BeansException("delete() method of ICommentBean is failed", e);
		}
		return isDeleted;
	}

	@Override
	public boolean deleteCommentsByNewsId(Long newsId) throws BeansException {
		boolean isDeleted = false;
		try {
			isDeleted = commentService.deleteCommentsByNewsId(newsId);
		} catch (ServiceException e) {
			throw new BeansException("deleteCommentsByNewsId() method of ICommentBean is failed", e);
		}
		return isDeleted;
	}

	public ICommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}
}
