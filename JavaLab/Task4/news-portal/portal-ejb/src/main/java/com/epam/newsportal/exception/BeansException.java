/*
 * @(#)BeansException.java   1.0 2016/03/22
 */
package com.epam.newsportal.exception;

/**
 * This class is wrapper for all EJB layer exceptions  
 * @version 1.0 22 March 2016
 * @author Pavel_Hryshyn
 */
public class BeansException extends Exception {
	private static final long serialVersionUID = 1L;

	public BeansException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public BeansException(String arg0) {
		super(arg0);
	}
}
