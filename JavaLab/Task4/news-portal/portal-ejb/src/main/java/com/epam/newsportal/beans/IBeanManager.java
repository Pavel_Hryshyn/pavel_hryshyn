/*
 * @(#)INewsBean.java   1.0 2016/03/24
 */
package com.epam.newsportal.beans;

import java.util.List;

import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.vo.NewsComplexVO;
import com.epam.newsportal.exception.BeansException;



/**
 * This interface defines basic methods for Manager Bean and 
 * represents facade for ManagerService
 * @version 1.0 24 March 2016
 * @author Pavel_Hryshyn
 */
public interface IBeanManager {
	/**
	 * This method returns newsComplex for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching list of comments in database
	 * @return newsComplex for some newsId from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	NewsComplexVO findByNewsId(Long newsId) throws BeansException;
	
	/**
	 * This method returns list of newsComplex which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of all newsComplex which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws BeansException, if arise ejb layer errors
	 */
	List<NewsComplexVO> findNewsComplexWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws BeansException;
	
	IAuthorBean getAuthorBean();
	
	ICommentBean getCommentBean();
	
	INewsBean getNewsBean();
	
	ITagBean getTagBean();
}
