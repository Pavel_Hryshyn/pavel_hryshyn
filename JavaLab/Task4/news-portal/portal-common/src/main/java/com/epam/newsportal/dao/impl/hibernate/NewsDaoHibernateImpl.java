/*
 * @(#)TagDaoHibernateImpl.java   1.0 2015/01/13
 */
package com.epam.newsportal.dao.impl.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import com.epam.newsportal.dao.INewsDao;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.utils.QueryFormer;

public class NewsDaoHibernateImpl implements INewsDao {
	
	private static final String ID = "id";
	private static final String AUTHOR = "author";
	private static final String AUTHORS = "authors";
	private static final String AUTHOR_ID = "author.id";
	private static final String TAG = "tags";
	private static final String TAGS = "tag";
	private static final String TAG_ID = "tag.id";
	
	private static final String HQL_AMOUNT_NEWS = "SELECT COUNT(*) FROM News";
	private static final String HQL_DELETE = "delete from News where id = :id";
	
	private static final String MODIFICATION_DATE = "modificationDate";
	private static final String SQL_GET_NEXT_NEWS_MAIN_SUBQUERY = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LEAD(news_id_pk, 1) over(order by news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_modification_date FROM news LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE";
	private static final String SQL_GET_NEXT_NEWS_WITHOUT_AUTHOR_TAG = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LEAD(news_id_pk, 1) over(order by news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news ORDER BY news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_GET_NEXT_NEWS_WITHOUT_TAGS = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LEAD(news_id_pk, 1) over(order by news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk WHERE news_author.author_id_fk=? ORDER BY news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_GET_NEXT_PREVIOUS_NEWS_TAIL_SUBQUERY = "GROUP BY news.news_id_pk, news.news_modification_date ORDER BY news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_GET_PREVIOUS_NEWS_MAIN_SUBQUERY = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LAG(news_id_pk, 1) over(order by news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_modification_date FROM news LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE";
	private static final String SQL_GET_PREVIOUS_NEWS_WITHOUT_AUTHOR_TAG = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LAG(news_id_pk, 1) over(order by news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news ORDER BY news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_GET_PREVIOUS_NEWS_WITHOUT_TAGS = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LAG(news_id_pk, 1) over(order by news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk WHERE news_author.author_id_fk=? ORDER BY news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_CRITERIA_AUTHOR_SUBQUERY = "news_author.author_id_fk=? AND ( ";
	private static final String SQL_CRITERIA_TAG_SUBQUERY = "news_tag.tag_id_fk=?";
	
	
	/* This constants stores bracket that using in SQL query */
	private static final String BRACKET = ") ";
	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<News> findBetweenPositions(int start, int perPage)
			throws DaoException {
		List<News> newsList = new ArrayList<News>();
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(News.class).addOrder(Order.desc(MODIFICATION_DATE));
			criteria.setFirstResult(start);
			criteria.setMaxResults(perPage);
			newsList = (List<News>)criteria.list(); 
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting list of news", e);
		} 
		return newsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc,
			int start, int perPage) throws DaoException {
		List<News> newsList = new ArrayList<News>();
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			DetachedCriteria detachedCriteria = createDetachedCriteria(sc);
			Criteria criteria = session.createCriteria(News.class);
			criteria.addOrder(Order.desc(MODIFICATION_DATE));
			criteria.setFirstResult(start);
			criteria.setMaxResults(perPage);
			criteria.add(Subqueries.propertyIn(ID, detachedCriteria));
			newsList = (List<News>)criteria.list(); 
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting list of news", e);
		} 
		return newsList;
	}

	@Override
	public News findById(Long newsId) throws DaoException {
		News news = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			news = (News) session.createCriteria(News.class)
					.add(Restrictions.eq(ID, newsId)).uniqueResult();
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting news by Id", e);
		}
		return news;
	}

	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria sc)
			throws DaoException {
		String mainSubQuery = SQL_GET_NEXT_NEWS_MAIN_SUBQUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = SQL_GET_NEXT_PREVIOUS_NEWS_TAIL_SUBQUERY;
		long nextNewsId = 0L;
		Session session = null;
		SQLQuery query = null;
		try {
			session = sessionFactory.getCurrentSession();
			if (sc.getAuthor() !=null){
				if (sc.getTags() != null){
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteria(mainSubQuery,
							authorSubQuery, tagSubQuery, tailSubQuery,
							amountTags);
					query = session.createSQLQuery(resultingQuery);
					query.setParameter(0, sc.getAuthor().getId());
					for (int indexList = 0; indexList < amountTags; indexList++){
						int indexPs = indexList + 1;
						query.setParameter(indexPs, sc.getTags().get(indexList).getId());
					}
					query.setParameter(amountTags + 1, newsId);
				} else{
					String resultingQuery = SQL_GET_NEXT_NEWS_WITHOUT_TAGS;
					query = session.createSQLQuery(resultingQuery);
					query.setParameter(0, sc.getAuthor().getId());
					query.setParameter(1, newsId);
				}
			} else {
				if (sc.getTags() != null){
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteriaWithoutAuthor(
							mainSubQuery, tagSubQuery, tailSubQuery, amountTags);
					query = session.createSQLQuery(resultingQuery);
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList;
						query.setParameter(indexPs, sc.getTags().get(indexList).getId());
					}
					query.setParameter(amountTags, newsId);
				} else{
					query = session.createSQLQuery(SQL_GET_NEXT_NEWS_WITHOUT_AUTHOR_TAG);
					query.setLong(0, newsId);
				}
			}
			if (query.uniqueResult()!= null) {
				nextNewsId = ((BigDecimal)query.uniqueResult()).longValue();
			}
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting news by Id", e);
		}
		return nextNewsId;
	}

	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria sc)
			throws DaoException {
		String mainSubQuery = SQL_GET_PREVIOUS_NEWS_MAIN_SUBQUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = SQL_GET_NEXT_PREVIOUS_NEWS_TAIL_SUBQUERY;
		Long previousNewsId = 0L;
		Session session = null;
		SQLQuery query = null;
		try {
			session = sessionFactory.getCurrentSession();
			if (sc.getAuthor() !=null){
				if (sc.getTags() != null){
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteria(mainSubQuery,
							authorSubQuery, tagSubQuery, tailSubQuery,
							amountTags);
					query = session.createSQLQuery(resultingQuery);
					query.setParameter(0, sc.getAuthor().getId());
					for (int indexList = 0; indexList < amountTags; indexList++){
						int indexPs = indexList + 1;
						query.setParameter(indexPs, sc.getTags().get(indexList).getId());
					}
					query.setParameter(amountTags + 1, newsId);
				} else{
					String resultingQuery = SQL_GET_PREVIOUS_NEWS_WITHOUT_TAGS;
					query = session.createSQLQuery(resultingQuery);
					query.setParameter(0, sc.getAuthor().getId());
					query.setParameter(1, newsId);
				}
			} else {
				if (sc.getTags() != null){
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteriaWithoutAuthor(
							mainSubQuery, tagSubQuery, tailSubQuery, amountTags);
					query = session.createSQLQuery(resultingQuery);
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList;
						query.setParameter(indexPs, sc.getTags().get(indexList).getId());
					}
					query.setParameter(amountTags, newsId);
				} else{
					query = session.createSQLQuery(SQL_GET_PREVIOUS_NEWS_WITHOUT_AUTHOR_TAG);
					query.setParameter(0, newsId);
				}
			}
			if (query.uniqueResult() != null) {
				previousNewsId = ((BigDecimal)query.uniqueResult()).longValue();
			}
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting news by Id", e);
		}
		return previousNewsId;
	}

	@Override
	public long countAllNews() throws DaoException {
		long amount = 0;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			amount = ( (Long) session.createQuery(HQL_AMOUNT_NEWS).iterate().next() ).longValue();
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting amount of news by Id", e);
		} 
		return amount;
	}

	@Override
	public long countNewsWithCriteria(SearchCriteria searchCriteria)
			throws DaoException {
		Long amount = 0L;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			DetachedCriteria detachedCriteria = createDetachedCriteria(searchCriteria);
			Criteria criteria = session.createCriteria(News.class);
			criteria.addOrder(Order.desc(MODIFICATION_DATE));
			criteria.add(Subqueries.propertyIn("id", detachedCriteria));
			criteria.setProjection(Projections.rowCount());
			amount = (Long) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting amount of news by search criteria", e);
		} 
		return amount;
	}

	@Override
	public Long create(News news) throws DaoException {
		Long id = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			id = (long) session.save(news);
		} catch (HibernateException e) {
			throw new DaoException("Error occured during saving news", e);
		} 
		return id;
	}

	@Override
	public void addNewsWithTags(Long newsId, List<Long> tagsId)
			throws DaoException {
		try {
			throw new UnsupportedOperationException("This method is unsupported");
		} catch (UnsupportedOperationException e) {
			throw new DaoException("This method is unsupported", e);
		}
	}

	@Override
	public void addNewsWithAuthor(Long newsId, Long authorId)
			throws DaoException {
		try {
			throw new UnsupportedOperationException("This method is unsupported");
		} catch (UnsupportedOperationException e) {
			throw new DaoException("This method is unsupported", e);
		}
	}

	@Override
	public boolean update(News news) throws DaoException {
		boolean isUpdated = false;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.update(news);
			isUpdated = true;
		} catch (HibernateException e) {
			throw new DaoException("Error occured during updating news", e);
		} 
		return isUpdated;
	}

	@Override
	public boolean delete(Long newsId) throws DaoException {
		boolean isDeleted = false;
		int row = 0;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(HQL_DELETE);
			query.setParameter(ID, newsId);
			row = query.executeUpdate();
			if (row > 0) {
				isDeleted = true;
			}
		} catch (HibernateException e) {
			throw new DaoException("Error occured during deleting news", e);
		} 
		return isDeleted;
	}

	@Override
	public boolean deleteTagsForNews(Long newsId) throws DaoException {
		throw new DaoException("This method is unsupported");
	}

	@Override
	public boolean deleteAuthorForNews(Long newsId) throws DaoException {
		throw new DaoException("This method is unsupported");
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	private DetachedCriteria createDetachedCriteria(SearchCriteria sc) {
		DetachedCriteria criteria = DetachedCriteria.forClass(News.class);
		if (sc != null) {
			if (sc.getAuthor() != null){
				criteria.createAlias(AUTHORS, AUTHOR);
				criteria.add(Restrictions.eq(AUTHOR_ID, sc.getAuthor().getId()));
			}
			if (sc.getTags()!=null && !sc.getTags().isEmpty()){
				List<Long> tagIds = new ArrayList<Long>();
				for (Tag tag: sc.getTags()){
					tagIds.add(tag.getId());
				}
				criteria.createAlias(TAGS, TAG);
				criteria.add(Restrictions.in(TAG_ID, tagIds));
			}
		}
		  criteria.setProjection(Projections.distinct(Projections.property("id"))); 
		return criteria;
	}
	
	private String buildQueryForCriteria(String mainSubQuery,
			String authorSubQuery, String tagSubQuery, String tailSubQuery,
			int amountTags) {
		/*
		 * This method builds SQL query for search criteria with sets author and
		 * tags
		 */
		String resultingQuery = null;
		String tempQueryWithoutEnd = QueryFormer.addIterativeSubQueryToEnd(
				QueryFormer.addSubQueryToEnd(mainSubQuery, authorSubQuery),
				tagSubQuery, amountTags);
		tailSubQuery = QueryFormer.addSubQueryToEnd(BRACKET, tailSubQuery);
		resultingQuery = QueryFormer.addSubQueryToEnd(tempQueryWithoutEnd,
				tailSubQuery);
		return resultingQuery;
	}
	
	private String buildQueryForCriteriaWithoutAuthor(String mainSubQuery,
			String tagSubQuery, String tailSubQuery, int amountTags) {
		/*
		 * This method builds SQL query for search criteria with sets tags
		 */
		String resultingQuery = null;
		String tempQueryWithoutEnd = QueryFormer.addIterativeSubQueryToEnd(
				mainSubQuery, tagSubQuery, amountTags);
		resultingQuery = QueryFormer.addSubQueryToEnd(tempQueryWithoutEnd,
				tailSubQuery);
		return resultingQuery;
	}
}
