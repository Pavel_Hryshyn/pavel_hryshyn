/*
 * @(#)RolesEnum.java   1.0 2016/01/12
 */
package com.epam.newsportal.entity;

/**
 * This enum stores roles that using in application
 * @version 1.0 12 January 2016
 * @author Pavel_Hryshyn
 */
public enum RolesEnum {
	ROLE_AUTHOR, ROLE_ADMIN, ROLE_USER;
}
