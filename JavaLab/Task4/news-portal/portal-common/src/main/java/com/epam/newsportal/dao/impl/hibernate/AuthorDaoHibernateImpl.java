package com.epam.newsportal.dao.impl.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.epam.newsportal.dao.IAuthorDao;
import com.epam.newsportal.entity.Author;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.exception.DaoException;

public class AuthorDaoHibernateImpl implements IAuthorDao {
	private static final String ID = "id";
	private static final String EXPIRED_DATE = "expiredDate";
	private static final String HQL_EXPIRE = "update Author set expiredDate = :expiredDate where id = :id";
	
	private SessionFactory sessionFactory;

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Author> findAll() throws DaoException {
		List<Author> authors = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			authors = (List<Author>) session.createCriteria(Author.class)
					.addOrder(Order.asc(ID)).list();
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting author list",
					e);
		} 
		return authors;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Author> findAllNonExpiredAuthors() throws DaoException {
		List<Author> authors = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			authors = (List<Author>) session.createCriteria(Author.class)
					.add(Restrictions.isNull(EXPIRED_DATE)).addOrder(Order.asc(ID)).list();
		} catch (HibernateException e) {
			throw new DaoException(
					"Error occured during getting list of nonexpired authors", e);
		} 
		return authors;
	}


	@Override
	public Author findById(Long authorId) throws DaoException {
		Author author = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			author = (Author) session.createCriteria(Author.class)
					.add(Restrictions.eq(ID, authorId)).uniqueResult();
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting author by Id", e);
		} 
		return author;
	}

	@Override
	public Author findByNewsId(Long newsId) throws DaoException {
		Author author = null;
		News news = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			news = (News)session.createCriteria(News.class).add(Restrictions.eq(ID, newsId)).uniqueResult();
			if (news!=null){
				news.getAuthors().size();
				author = news.getAuthors().get(0);
			}
		} catch (HibernateException e) {
			throw new DaoException("Author can't be founded", e);
		} 
		return author;
	}

	@Override
	public Long create(Author author) throws DaoException {
		Long id = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			id = (long) session.save(author);
			tx.commit();
		} catch (HibernateException e) {
			throw new DaoException("Error occured during saving author", e);
		} 
		return id;
	}

	@Override
	public boolean update(Author author) throws DaoException {
		boolean isUpdated = false;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			session.update(author);
			tx.commit();
			isUpdated = true;
		} catch (HibernateException e) {
			throw new DaoException("Error occured during updating author", e);
		} 
		return isUpdated;
	}

	@Override
	public boolean expire(Long authorId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery(HQL_EXPIRE);
			query.setParameter(EXPIRED_DATE, new Date());
			query.setParameter(ID, authorId);
			query.executeUpdate();
			tx.commit();
		} catch (HibernateException e) {
			throw new DaoException("Author can't be expired", e);
		} 
		return true;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
