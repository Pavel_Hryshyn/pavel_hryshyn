/*
 * @(#)DaoException.java   1.0 2015/12/23
 */
package com.epam.newsportal.exception;

/**
 * This class is wrapper for all DAO level exceptions  
 * @version 1.0 23 December 2015
 * @author Pavel_Hryshyn
 */
public class DaoException extends Exception {
	private static final long serialVersionUID = 1L;

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}
}
