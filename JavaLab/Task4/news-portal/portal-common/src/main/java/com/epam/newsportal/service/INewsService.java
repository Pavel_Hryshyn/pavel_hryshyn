/*
 * @(#)INewsService.java   1.0 2016/02/08
 */
package com.epam.newsportal.service;

import java.util.List;

import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.exception.ServiceException;

/**
 * This interface defines basic methods for News Entity on service layer
 * @version 1.0 08 February 2016
 * @author Pavel_Hryshyn
 */
public interface INewsService {
	/**
	 * This method returns a list of all news that stores in database
	 * @return list of all news from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<News> findBetweenPositions(int start, int end) throws ServiceException;
	
	/**
	 * This method returns list of news which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of all news which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws ServiceException;
	
	
	/**
	 * This method returns news with some id that stores in database
	 * @param newsId
	 * 			the param uses for searching news in database
	 * @return news with some id from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	News findById(Long newsId) throws ServiceException;
	
	/**
	 * This method returns next newsId from list of news formed by SearchCriteris
	 * @param newsId
	 * 			the param uses for getting next newsId in database
	 * @param sc
	 * 			the param uses for formed list of news
	 * @return next newsId
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	Long getNextNewsId(Long newsId, SearchCriteria sc) throws ServiceException;
	
	/**
	 * This method returns previous newsId from list of news formed by SearchCriteris
	 * @param newsId
	 * 			the param uses for getting previous newsId in database
	 * @param sc
	 * 			the param uses for formed list of news
	 * @return previous newsId
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	Long getPreviousNewsId(Long newsId, SearchCriteria sc) throws ServiceException;
	
	/**
	 * This method returns amount of all news that stores in database
	 * @return amount of all news from database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	long countAllNews() throws ServiceException;
	
	/**
	 * This method returns amount of all news which matching searchCriteria 
	 * that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return amount of all news which matching searchCriteria from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	long countNewsWithCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * This method adds News entity to database
	 * @param news
	 * 			the param uses for adding news to database
	 * @return news's id if news is added to database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	Long create(News news) throws ServiceException;
	
	/**
	 * This method adds News entity to database
	 * @param news
	 * 			the param uses for adding news to database
	 * @return news's id if news is added to database
	 * @throws ServiceException, if arise any service layer exceptions
	 */
	boolean update(News news) throws ServiceException;
	
	/**
	 * This method deletes a news with some newsId from database
	 * @param newsId
	 * 			the param uses for deleting a news from database
	 * @return true if a news is deleted from database, else false
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	boolean delete(Long newsId) throws ServiceException;
}
