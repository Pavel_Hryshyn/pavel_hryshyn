package com.epam.newsportal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.dao.ICommentDao;
import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.ICommentService;

@Service("commentService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CommentServiceImpl implements ICommentService {

	@Autowired
	private ICommentDao commentDao;
	
	@Override
	public List<Comment> findByNewsId(Long newsId) throws ServiceException {
		List<Comment> comments = null;
		try {
			comments = commentDao.findByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("findAll() method of CommentService is failed", e);
		}
		return comments;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long create(Comment comment) throws ServiceException {
		long commentId = 0;
		try {
			commentId = commentDao.create(comment);
		} catch (DaoException e) {
			throw new ServiceException("create() method of CommentService is failed", e);
		}
		return commentId;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean delete(Long id) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = commentDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException("delete() method of CommentService is failed", e);
		}
		return isDeleted;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean deleteCommentsByNewsId(Long newsId) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = commentDao.deleteCommentsByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("delete() method of CommentService is failed", e);
		}
		return isDeleted;
	}
}
