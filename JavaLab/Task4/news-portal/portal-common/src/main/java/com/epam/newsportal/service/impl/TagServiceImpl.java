/*
 * @(#)TagServiceImpl.java   1.0 2016/02/08
 */
package com.epam.newsportal.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.dao.ITagDao;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.ITagService;

/**
 * This class implements methods for Tag service
 * @version 1.0 08 February 2016
 * @author Pavel_Hryshyn
 */
@Service("tagService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TagServiceImpl implements ITagService {

	@Autowired
	private ITagDao tagDao;
	
	@Override
	public List<Tag> findAll() throws ServiceException {
		List<Tag> tags = null;
		try {
			tags = tagDao.findAll();
		} catch (DaoException e) {
			throw new ServiceException("findAll() method of TagService is failed", e);
		}
		return tags;
	}

	@Override
	public List<Tag> findByNewsId(Long newsId) throws ServiceException {
		List<Tag> tags = null;
		try {
			tags = tagDao.findByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("findByNewsId() method of TagService is failed", e);
		}
		return tags;
	}

	@Override
	public Tag findById(Long tagId) throws ServiceException {
		Tag tag = null;
		try {
			tag = tagDao.findById(tagId);
		} catch (DaoException e) {
			throw new ServiceException("findById() method of TagService is failed", e);
		}
		return tag;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long create(Tag tag) throws ServiceException {
		long tagId = 0;
		try {
			tagId = tagDao.create(tag);
		} catch (DaoException e) {
			throw new ServiceException("create() method of TagService is failed", e);
		}
		return tagId;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean update(Tag tag) throws ServiceException {
		boolean isUpdated = false;
		try {			
			isUpdated = tagDao.update(tag);
		} catch (DaoException e) {
			throw new ServiceException("update() method of TagService is failed", e);
		}
		return isUpdated;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean delete(Long tagId) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted &= tagDao.delete(tagId);
		} catch (DaoException e) {
			throw new ServiceException("delete() method of TagService is failed", e);
		}
		return isDeleted;
	}

}
