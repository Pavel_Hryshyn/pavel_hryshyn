/*
 * @(#)Comment.java   1.0 2016/01/12
 */
package com.epam.newsportal.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This class is entity that defines the parameters of Comment
 * @version 1.0 12 January 2016
 * @author Pavel_Hryshyn
 */
@Entity
@Table(name="COMMENTS")
public class Comment {
	/* This value stores comment id */
	@Id
	@Column(name = "comments_id_pk", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_seq")
	@SequenceGenerator(name = "comment_seq", sequenceName = "COMMENTS_SEQUENCE", allocationSize = 1)
	private long id;
	
	/* This value stores news object */
	@ManyToOne
	@JoinColumn(name = "news_id_fk")
	private News news;
	
	/* This value stores text of comment */
	@NotNull(message="Not null")
	@Size(min=5, max=100)
	@Column(name = "comments_text", nullable = false)
	private String commentText;
		
	/* This value stores date of creation comment */
	@Column(name = "comments_creation_date", nullable = false)
	private Date creationDate;

	public Comment() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id != other.id)
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", news=" + news + ", commentText="
				+ commentText + ", creationDate=" + creationDate + "]";
	}	
}
