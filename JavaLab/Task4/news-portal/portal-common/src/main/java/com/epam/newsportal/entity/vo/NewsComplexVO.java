/*
 * @(#)NewsComplexVO.java   1.0 2016/02/08
 */
package com.epam.newsportal.entity.vo;

import java.util.List;

import com.epam.newsportal.entity.Author;
import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.Tag;

/**
 * This class is VO object that contains News, Comments, Author and Tags entities
 * @version 1.0 08 February 2016
 * @author Pavel_Hryshyn
 */
public class NewsComplexVO {
		
	/* This value stores News entity */
	private News news;
	
	/* This value stores comment list */
	private List<Comment> comments;

	public NewsComplexVO() {
		super();
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsComplexVO other = (NewsComplexVO) obj;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsComplexVO [news=" + news + ", comments=" + comments + "]";
	}
}
