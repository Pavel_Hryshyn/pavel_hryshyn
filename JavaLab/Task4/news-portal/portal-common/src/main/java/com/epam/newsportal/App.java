package com.epam.newsportal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsportal.dao.IAuthorDao;
import com.epam.newsportal.dao.ICommentDao;
import com.epam.newsportal.dao.INewsDao;
import com.epam.newsportal.dao.ITagDao;
import com.epam.newsportal.entity.Author;
import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.entity.vo.NewsComplexVO;
import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.IServiceManager;

public class App {

	public static void main(String[] args) {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring/hibernateBeans.xml");
		IAuthorDao authorDao = (IAuthorDao) ac.getBean("authorDao");
		ICommentDao commentDao = (ICommentDao) ac.getBean("commentDao");
		ITagDao tagDao = (ITagDao)ac.getBean("tagDao");
		INewsDao newsDao = (INewsDao)ac.getBean("newsDao");
		IServiceManager serviceManager = (IServiceManager)ac.getBean("serviceManager");
	
		try {
			NewsComplexVO newsComplex = serviceManager.findByNewsId(11414L);
			System.out.println(newsComplex);
			
//			List<Comment> comments = commentDao.findByNewsId(11414L);
//			for (Comment comment: comments) {
//				System.out.println(comment);
//			}
//			System.out.println(comments.size());
//			newsDao.findBetweenPositions(10302, 10580);
//			
//			News news = newsDao.findById(11414L);
//			System.out.println("--------------------------"+news);
//			news.setFullText("Updated full Text 11414");
//			News news1 = news;
//			news1.setShortText("kldfsjkllsjdklfjs");
//			News news2 = news;
//			news2.setFullText("dfsljfdjlfdjkjskldjdfsjldfjdsjkldfj");
//			News news3 = news;
//			news3.setTitle("d,sfjsdkjfsdfkjs");
//			newsDao.update(news);
//			news = newsDao.findById(11414L);
//			System.out.println("--------------------------"+news);
			
//			Author author = authorDao.findById(1L);
//			Author newAuthor = new Author();
//			newAuthor.setAuthorName("new");
//			authorDao.create(newAuthor);
//			System.out.println(author);
//			Tag tag = new Tag();
//			tag.setTagName("newTag");
//			tag = tagDao.findById(1L);
//			tagDao.create(tag);
//			SearchCriteria sc = new SearchCriteria();
//			sc.setTags(new ArrayList<Tag>());
//			sc.getTags().add(tag);
//			sc.getTags().add(tagDao.findById(2L));
//			sc.setAuthor(author);
			
//			System.out.println(sc.getAuthor());
//			System.out.println("--------------------------"+newsDao.findNewsWithCriteriaAndPositions(sc, 1, 10));
//			System.out.println(commentDao.deleteCommentsByNewsId(1L));
			
//			Author author = authorDao.findByNewsId(1000000L);
//			System.out.println("++++++++++++++++++++++++++++++" + author);
			
//			System.out.println("===============" + tagDao.findByNewsId(11414L));
//			List<Author> authors = authorDao.findAll();
//			for (Author author: authors) {
//				System.out.println(author);
//			}
//			Author author = authorDao.findById(110L);
//			System.out.println("=====================" + author);
//			
//			Author savingAuthor = new Author();
//			savingAuthor.setId(1L);
//			savingAuthor.setAuthorName("Hibernate");
//			System.out.println("===================== ID = " + authorDao.create(savingAuthor));
//			savingAuthor.setId(190L);
//			savingAuthor.setAuthorName("Hibernate111111111");
//			System.out.println("++++++++++++++++++++++++++++"+authorDao.update(savingAuthor));
//			System.out.println("===================== ID = " + authorDao.findById(1740000000000000000L));
////			authorDao.expire(8600L);
//			List<Author> nonExpiredAuthors = authorDao.findAllNonExpiredAuthors();
//			for (Author author1: nonExpiredAuthors) {
//				System.out.println(author1);
//			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
