package com.epam.newsportal.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.epam.newsportal.dao.ICommentDao;
import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.exception.DaoException;

public class CommentDaoHibernateImpl implements ICommentDao {
	
	private static final String CREATION_DATE = "creationDate";

	private static final String ID = "id";
	private static final String HQL_DELETE = "delete from Comment where id = :id";
	private static final String HQL_DELETE_BY_NEWS = "delete from Comment where news.id = :id";
	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> findByNewsId(Long newsId) throws DaoException {
		List<Comment> comments = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Comment.class).addOrder(Order.desc(CREATION_DATE)).createCriteria("news").add(Restrictions.eq(ID, newsId));
			comments = (List<Comment>)criteria.list(); 
		} catch (HibernateException e) {
			throw new DaoException("List of comments can not be formed", e);
		} 
		return comments;
	}

	@Override
	public Long create(Comment comment) throws DaoException {
		Long id = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			id = (long) session.save(comment);
			tx.commit();
		} catch (HibernateException e) {
			throw new DaoException("Error occured during saving comment", e);
		} 
		return id;
	}

	@Override
	public boolean delete(Long id) throws DaoException {
		boolean isDeleted = false;
		int row = 0;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery(HQL_DELETE);
			query.setParameter(ID, id);
			row = query.executeUpdate();
			tx.commit();
			if (row > 0) {
				isDeleted = true;
			}
		} catch (HibernateException e) {
			throw new DaoException("Error occured during deleting comment", e);
		} 
		return isDeleted;
	}

	@Override
	public boolean deleteCommentsByNewsId(Long newsId) throws DaoException {
		boolean isDeleted = false;
		Session session = null;
		int row = 0;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery(HQL_DELETE_BY_NEWS);
			query.setParameter(ID, newsId);
			row = query.executeUpdate();
			tx.commit();
			if (row >0){
				isDeleted = true;
			}
		} catch (HibernateException e) {
			throw new DaoException("Error occured during deleting comment", e);
		} 
		return isDeleted;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
