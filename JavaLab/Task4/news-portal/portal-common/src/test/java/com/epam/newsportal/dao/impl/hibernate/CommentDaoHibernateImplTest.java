package com.epam.newsportal.dao.impl.hibernate;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsportal.dao.ICommentDao;
import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/hibernateBeans-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:/test-data/comment-setup.xml"})
@DatabaseTearDown(value = {"classpath:/test-data/comment-setup.xml"},type = DatabaseOperation.DELETE_ALL)
@Ignore
public class CommentDaoHibernateImplTest {

	@Autowired
	ICommentDao commentDao;
	
	@Test
	public void testFindByNewsId(){
		int expectedSize = 2;
		long newsId = 1L;
		News news1 = createNews(newsId, "Title1", "ShortText1", "FullText1");
		Comment expectedComment1 = createComment(1L, news1, "Comment1");
		Comment expectedComment2 = createComment(2L, news1, "Comment2");
		List<Comment> actualComments = null;
		try {
			actualComments = commentDao.findByNewsId(newsId);
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualComments.size());
		assertEqualsComments(expectedComment1, actualComments.get(1));
		assertEqualsComments(expectedComment2, actualComments.get(0));
	}
	
	@Test
	public void testCreate(){
		int expectedSizeBeforeAdding = 2;
		int expectedSizeAfterAdding = 3;
		long actualId = 0;
		long newsId = 1L;
		News news1 = createNews(newsId, "Title1", "ShortText1", "FullText1");
		Comment expectedComment = createComment(4L, news1, "Comment4");
		List<Comment> actualCommentsBeforeAdding = null;
		List<Comment> actualCommentsAfterAdding = null;
		Comment actualComment = null;
		try {
			actualCommentsBeforeAdding = commentDao.findByNewsId(newsId);
			assertEquals(expectedSizeBeforeAdding, actualCommentsBeforeAdding.size());
			actualId = commentDao.create(expectedComment);
			expectedComment.setId(actualId);
			actualCommentsAfterAdding = commentDao.findByNewsId(newsId);
			actualComment = actualCommentsAfterAdding.get(0);
			assertEquals(expectedSizeAfterAdding, actualCommentsAfterAdding.size());
			assertEqualsComments(expectedComment, actualComment);
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testDelete(){
		long commentId = 1L;
		long nonExistingCommentId = 100L;
		long newsId = 1L;
		int initialSize = 2;
		int afterDeletingSize = 1;
		boolean isDeleted = false;
		News news1 = createNews(newsId, "Title1", "ShortText1", "FullText1");
		Comment expectedComment = createComment(commentId, news1, "Comment1");
		Comment actualCommentBeforeDeleting = null;
		List<Comment> initialList = null;
		List<Comment> afterDeletingList = null;
		try {
			initialList = commentDao.findByNewsId(newsId);
			assertEquals(initialSize, initialList.size());
			actualCommentBeforeDeleting = initialList.get(initialSize-1);
			assertEqualsComments(expectedComment, actualCommentBeforeDeleting);
			isDeleted = commentDao.delete(commentId);
			assertTrue(isDeleted);
			afterDeletingList = commentDao.findByNewsId(newsId);
			assertEquals(afterDeletingSize, afterDeletingList.size());
			assertNotEqualsComments(expectedComment, afterDeletingList.get(afterDeletingSize-1));
			assertFalse(commentDao.delete(nonExistingCommentId));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testDeleteCommentsByNewsId(){
		long newsId = 1L;
		int initialSize = 2;
		boolean isDeleted = false;
		List<Comment> initialList = null;
		List<Comment> afterDeletingList = null;
		try {
			initialList = commentDao.findByNewsId(newsId);
			assertEquals(initialSize, initialList.size());
			isDeleted = commentDao.deleteCommentsByNewsId(newsId);
			assertTrue(isDeleted);
			afterDeletingList = commentDao.findByNewsId(newsId);
			assertTrue(afterDeletingList.isEmpty());
			assertFalse(commentDao.deleteCommentsByNewsId(newsId));
		} catch (DaoException e) {
			fail();
		}
	}
	
	private Comment createComment(Long id, News news, String commentText){
		Comment сomment = new Comment();
		сomment.setId(id);
		сomment.setNews(news);
		сomment.setCommentText(commentText);
		сomment.setCreationDate(new Date());
		return сomment;
	}
	
	private News createNews(Long id, String title, String shortText, String fullText){
		News news = new News();
		news.setId(id);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		return news;
	}
	
	private void assertEqualsComments(Comment expectedComment, Comment actualComment){
		assertEquals(expectedComment.getId(), actualComment.getId());
		assertEqualsNews(expectedComment.getNews(), actualComment.getNews());
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
	}

	private void assertEqualsNews(News expectedNews, News actualNews){
		assertEquals(expectedNews.getId(), actualNews.getId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());	
	}
	
	private void assertNotEqualsComments(Comment expectedComment, Comment actualComment){
		assertNotEquals(expectedComment.getId(), actualComment.getId());
		assertEqualsNews(expectedComment.getNews(), actualComment.getNews());
		assertNotEquals(expectedComment.getCommentText(), actualComment.getCommentText());
	}
}
