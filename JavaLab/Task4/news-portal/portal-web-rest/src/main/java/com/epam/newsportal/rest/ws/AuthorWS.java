/*
 * @(#)AuthorWS.java   1.0 2016/03/25
 */
package com.epam.newsportal.rest.ws;

import javax.ws.rs.Path;

/**
 * This class is RESTful web service for Author entity
 * @version 1.0 25 March 2016
 * @author Pavel_Hryshyn
 */
@Path("/authors")
public class AuthorWS {

}
