-- -----------------------------------------------------
-- Table `HRYSHYN`.`NEWS`
-- -----------------------------------------------------
CREATE TABLE HRYSHYN.NEWS 
(
  news_id_pk NUMBER(20) NOT NULL  
, news_title NVARCHAR2(30) NOT NULL 
, news_short_text NVARCHAR2(100) NOT NULL 
, news_full_text NVARCHAR2(2000) NOT NULL 
, news_creation_date TIMESTAMP NOT NULL 
, news_modification_date DATE NOT NULL 
, CONSTRAINT pk_news PRIMARY KEY 
  (
    news_id_pk 
  )
  ENABLE 
);

-- -----------------------------------------------------
-- Table `HRYSHYN`.`TAG`
-- -----------------------------------------------------
CREATE TABLE HRYSHYN.TAG 
(
  tag_id_pk NUMBER(20) NOT NULL 
, tag_name NVARCHAR2(30) NOT NULL 
, CONSTRAINT pk_tag PRIMARY KEY 
  (
    tag_id_pk 
  )
  ENABLE 
);

-- -----------------------------------------------------
-- Table `HRYSHYN`.`NEWS_TAG`
-- -----------------------------------------------------
CREATE TABLE HRYSHYN.NEWS_TAG 
(
  news_id_fk NUMBER(20) NOT NULL 
, tag_id_fk NUMBER(20) NOT NULL
, CONSTRAINT fk_news_tag_news_id FOREIGN KEY (news_id_fk) REFERENCES HRYSHYN.NEWS(news_id_pk)
, CONSTRAINT fk_news_tag_tag_id FOREIGN KEY (tag_id_fk) REFERENCES HRYSHYN.TAG(tag_id_pk)
);

-- -----------------------------------------------------
-- Table `HRYSHYN`.`AUTHOR`
-- -----------------------------------------------------
CREATE TABLE HRYSHYN.AUTHOR 
(
  author_id_pk NUMBER(20) NOT NULL 
, author_name NVARCHAR2(30) NOT NULL 
, author_expired TIMESTAMP
, CONSTRAINT pk_author PRIMARY KEY 
  (
    author_id_pk 
  )
  ENABLE 
);

-- -----------------------------------------------------
-- Table `HRYSHYN`.`NEWS_AUTHOR`
-- -----------------------------------------------------
CREATE TABLE HRYSHYN.NEWS_AUTHOR 
(
  news_id_fk NUMBER(20) NOT NULL 
, author_id_fk NUMBER(20) NOT NULL
, CONSTRAINT fk_news_author_news_id FOREIGN KEY (news_id_fk) REFERENCES HRYSHYN.NEWS(news_id_pk)
, CONSTRAINT fk_news_author_author_id FOREIGN KEY (author_id_fk) REFERENCES HRYSHYN.AUTHOR(author_id_pk)
);

-- -----------------------------------------------------
-- Table `HRYSHYN`.`COMMENTS`
-- -----------------------------------------------------
CREATE TABLE HRYSHYN.COMMENTS 
(
  comments_id_pk NUMBER(20) NOT NULL 
, news_id_fk NUMBER(20) NOT NULL
, comments_text NVARCHAR2(100) NOT NULL  
, comments_creation_date TIMESTAMP NOT NULL 
, CONSTRAINT pk_comments PRIMARY KEY (comments_id_pk) ENABLE 
, CONSTRAINT fk_comments_news_id FOREIGN KEY (news_id_fk) REFERENCES HRYSHYN.NEWS(news_id_pk)
);

 -- -----------------------------------------------------
-- Table `HRYSHYN`.`USERS`
-- -----------------------------------------------------
CREATE TABLE HRYSHYN.USERS
(
  user_id_pk NUMBER(20) NOT NULL 
, user_name NVARCHAR2(50) NOT NULL 
, user_login NVARCHAR2(30) NOT NULL
, user_password NVARCHAR2(32) NOT NULL
, CONSTRAINT pk_users PRIMARY KEY 
  (
    user_id_pk 
  )
  ENABLE 
);

-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `HRYSHYN`.`ROLES`
-- -----------------------------------------------------
CREATE TABLE HRYSHYN.ROLES 
(
  user_id_fk NUMBER(20) NOT NULL 
, role_name VARCHAR2(50) NOT NULL
, CONSTRAINT fk_roles_user_id FOREIGN KEY (user_id_fk) REFERENCES HRYSHYN.USERS(user_id_pk)
);

-- -----------------------------------------------------

-- -----------------------------------------------------
-- Sequence `HRYSHYN`.`AUTHOR_SEQUENCE`
-- -----------------------------------------------------
CREATE SEQUENCE  "HRYSHYN"."AUTHOR_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999 INCREMENT BY 1 START WITH 10 CACHE 20 NOORDER  NOCYCLE ;

-- -----------------------------------------------------

-- -----------------------------------------------------
-- Sequence `HRYSHYN`.`COMMENTS_SEQUENCE`
-- -----------------------------------------------------
CREATE SEQUENCE  "HRYSHYN"."COMMENTS_SEQUENCE"  MINVALUE 1 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 16 CACHE 20 NOORDER  NOCYCLE ;

-- -----------------------------------------------------

-- -----------------------------------------------------
-- Sequence `HRYSHYN`.`NEWS_SEQUENCE`
-- -----------------------------------------------------
CREATE SEQUENCE  "HRYSHYN"."NEWS_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999 INCREMENT BY 1 START WITH 33 CACHE 20 NOORDER  NOCYCLE ;

-- -----------------------------------------------------

-- -----------------------------------------------------
-- Sequence `HRYSHYN`.`TAG_SEQUENCE`
-- ----------------------------------------------------- 
CREATE SEQUENCE  "HRYSHYN"."TAG_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;

-- -----------------------------------------------------

-- -----------------------------------------------------
-- Sequence `HRYSHYN`.`USERS_SEQUENCE`
-- -----------------------------------------------------
   CREATE SEQUENCE  "HRYSHYN"."USERS_SEQUENCE"  MINVALUE 1 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

-- -----------------------------------------------------  