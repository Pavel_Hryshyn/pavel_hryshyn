-- -----------------------------------------------------
-- Table `HRYSHYN`.`NEWS`. INITIAL DATA
-- -----------------------------------------------------
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('1', '�������-����, 1-�� ���.', '��������� ������. 1-� ���. ���������� ���� � ������ � ����� �������� � ������ �����', '� ������ ���� ���������� �������-���� ���������� ������� ������ ���������� (1:0), � ������ ��������� ����� � ������� (2:2). � ����������� �������� ������� ����� ���� (0:2), � � ����������� ���������� ���� ������� � ����� ���������.', TO_TIMESTAMP('2015-08-09 21:39:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 08:39:56', 'YYYY-MM-DD HH24:MI:SS'))
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('2', '�������-����, ���������.', '���������� ���� ����� ����������� ����� � ��������', '���������� ���� ����� ����������� �������� ����������� ����� � ���������� �������, �������� Marca.
�������� ������� ���������� ������ � ������� ��� �� ����� 4-�� ����������������� ������ ���� ��������� ������ �������, ������ ���������� �� �������� ��������� ������ �� ���������� ������.
����� ��������� ������� � ������ � ����� ���������� ���������� ���.
� �� �� ����� ������� ������ ����������� ���� ������ �� �����, ����� 28-������ ������� ������� ������� � ������� ����������� � ����������� ������ �� ������� ������ �� ����� ������, ��������� � ��� ���������.', TO_TIMESTAMP('2015-08-10 11:00:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:00:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('3', '"�����" �� ������� �� ����', '������� ������ "�����" ���� ������� ������, ��� � ����� ��� ������� ��-�� ����� ����� ����', '������� ������ "�����" ���� ������� ����������, ��� � ����� ��� ������� � ����� � ������ ����� ���� � "�������". ��������, ��� �� ��� ����� �������� ��������� ������ ��������. � ������ �� ���� ���������� ������ ������ "������" �� ��� �������� �������� � ����, ��������� �� ��� ��������� ������� ��� ������ ���� ������. 
� �� �� ������� �� �����, � �������� Daily Express ��������. � �������� �� � ���, ��� ��� ��� �� ������� � � ��� ���� ��� ���������� ���������. �������� � ���, ��� ��������� ����� �������. ���� � ����� � ����������� �������. �� ��������� ���������, ��������� ��������� �������, � �� 31 �������.', TO_TIMESTAMP('2015-08-10 11:10:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:21:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('4', '������� �������� �� "�����"', '����������� ��������� ������� "�����"', '����������� ��������� � ������� ������ �������� �������� "�����", �������� Bild. �������� ��������� � ����������, ��� ���������� ���� ������� ������� ������ ������� "�������" � ����� 1/32 ������ ����� ��������. �������������, ������������ �� ����� ���������, ������� ��������� �������� ��������, ����� ������� ��� � �������� ���� �������. �������� �������� �� ���������, ����������� ������� �� ��� ������ � ������������ �������� �� ����.', TO_TIMESTAMP('2015-08-10 11:11:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:23:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('5', '�������� ����� �������������', '"���������" ���� �� ����� ��������� ����� ��-�� ������� �������.', '����������� "���������" ������ ����������� � ���������� ����������� ����� � "��������� �������", ���������� sport.es. �� ���������� ���������, �������� ����� ��������� ������� �������� �������, � �������� ��������������� ������. ����� ����� ��������, ��� ��-�� ������� �������� ��������� ���������� ������� � ������. 
���������, ��� �������� ����� ��������� "������� ��������" � 30 ��������� ����. � �������� ������ 28-������ ������� ������ � ������� 35 ������, ����� ����� ����� � ������ ����� �������������� �������.', TO_TIMESTAMP('2015-08-10 11:12:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:23:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('6', '"��������" ������� ����������', '"��������" ������� ���������� ����������', '"��������" � ����� �� ���������� ���������� ������� "�������" � 1:0. ������������ ��� ������� ����� ���������� ���������� ����� ��������. 
"�������" � "��������" � 0:1 (0:0) 
���: ��������, 53 (0:1).', TO_TIMESTAMP('2015-08-10 11:14:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:25:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('7', '�������� ���������� � ��������', '������. �������� �������� ���������� �� 21 ����� � �������� ���', '� ����������� �������� ��� �� 10 ������� �������� �������� ���������� �� 2 ������ � ������������� �� 21 �����. ������ ������� ��������� �� ����������. 
������� ��� �� 10 �������: 
1 (1). ������ ������� (���) � 12721 
2 (2). ����� �������� (������) � 6490 
3 (3). ������ ����� (�������) � 5151 
4 (4). ������ ������� (�����) � 5000 
5 (5). ������� �������� (�����) � 4910... 
21. (19). �������� �������� (��������) � 2061... 
77 (78). ����� ��������� (��������) � 759... 
122 (121). ���������� �������� (��������) � 476...', TO_TIMESTAMP('2015-08-10 11:25:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:25:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('8', '��������� �� ������� � �������', '������. ����� ��������� ����� � �������� ����� ������� � �������', '��������� ����� ��������� (78-� ����� �������� ���) ����� � �������� ����� ������� � ������� (�������� ���� � 2 377 305 ��������). � �������� ����� ������������ ���� ���������������� ���������� �������������� ������� ���� ������ (172-� ������� ����) � 6:0, 2:6, 6:3.', TO_TIMESTAMP('2015-08-10 11:26:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:27:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('9', '����� - ������ "�������"?', '������. ������� ����� ����� ���������� "�������"', '������ ������������ ������� ������� � "�����" ������� ����� ����� ���� �������� ������� �������� "�������", �������� Equipe. � ��������� ����� 43-������ ���������� ����������� ��������� ������� ��������� � "��������". ������� ����� �������� � ���, ��� ������� ������ ���������� ������� ������� ������� � ������� �������� ���� ���� ����� ��������� �� "����" (0:1) � ����� 1-�� ���� ���������� �������. ����������� ����������� �������� ������� ������� �������� ������ ��������� ������� ����� �����. �� ���� ���������� ������������� ��������� ������� ����� ���������� ���� ����� � �������� ���������.', TO_TIMESTAMP('2015-08-10 11:28:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:29:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('10', '"�������" �������� "���� ����"', '������. ����� ������: �� ���� ������������� � � ������, � � �����', '������� ������ "��������" ����� ������ ��������� ������� � ��������� � ����� 1-�� ���� ���������� ������ �� "���� ����" (0:2). 
"�� �� ���� ����������� �� � ������, �� � �����. � ����, ��� ��� ���� ����� ���� �������. ���� �� �� ������ �������� ����, ���� ������, ��� �� ������� ������ ��� ������. 
�� ����� ��� � ���������� ���������. "���� ���" ��� ����� ���, ��� ������� ��� ��������� ������. �������� ������ � �� �������. �� ������ ����� �� ���� ���������� ������. 
�� ������ ������������� �������� �� �������� ����� ����. ������� ������ ������ ���������� � ���. ����� �����, ���� �� �� ����� ������� ����� ��������, � ��������� ��������. ������ ����������� ������� �������� � ��� �������� ���������. 
��� ������� ���� �������� ���������, ��� ������� ����������� ��� ������", � ������ ������.', TO_TIMESTAMP('2015-08-10 09:28:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:29:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('11', '���� ��������� �����������', '������. "����" ��������� ��������� ���������� �����������', '�������� ��������� ���������� ��������� ������� � "����", �������� ����������� ���� �����. ����� �������� ������������� ������� ������, ������ ������� ����� ��������� � ������� "������ ��������". 
�����-2014/15 �� ������ � ������� "�����������" (38 ���, 1 ��� � 7 �������) � "����������" (5 ���).', TO_TIMESTAMP('2015-08-10 11:38:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 11:39:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HRYSHYN"."NEWS" (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES ('12', '�������� �������', '������. "�����" � "���� �������" ������������ � ��������� ������� �� 15 ��������� ������', '���������� "������" ���� ������� ������ ��������� ������� � "���� ��������". 
�� ���������� Sky Sports, ����� ����������� �������� �������� ������� ��������� �� 15 ��������� ������. ������� 25-������ ��������� ����� �������������� �� �������� ����� "���� ��������" ������ "��������� ����". 
���������� ���� ����, ����� ������ ������� ���������� �� ������, ����� ���� ������� ����� ����������� ����������.', TO_TIMESTAMP('2015-08-10 12:40:35.681000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-10 12:49:56', 'YYYY-MM-DD HH24:MI:SS'));

-- -----------------------------------------------------
-- Table `HRYSHYN`.`TAG`. INITIAL DATA
-- -----------------------------------------------------
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('1', '������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('2', '������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('3', '������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('4', '���������� ������� ����');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('5', '������ ����');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('6', '��������� �������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('7', '����� �');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('8', '���� 1');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('9', '���������� ������� ����');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('10', '���');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('11', '���');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('12', '�������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('13', '������ ����');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('14', '���������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('15', '������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('16', '����������� ����');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('17', '���������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('18', '��������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('19', '�������');
INSERT INTO "HRYSHYN"."TAG" (TAG_ID_PK, TAG_NAME) VALUES ('20', '����');

-- -----------------------------------------------------
-- Table `HRYSHYN`.`NEWS_TAG`. INITIAL DATA
-- -----------------------------------------------------
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('1', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('1', '4');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('2', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('2', '4');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('2', '14');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('3', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('3', '4');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('3', '14');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('4', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('4', '5');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('5', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('5', '6');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('5', '14');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('6', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('7', '3');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('8', '3');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('9', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('9', '8');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('10', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('10', '4');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('11', '2');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('11', '14');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('12', '1');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('12', '9');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('12', '4');
INSERT INTO "HRYSHYN"."NEWS_TAG" (NEWS_ID_FK, TAG_ID_FK) VALUES ('12', '14');

-- -----------------------------------------------------
-- Table `HRYSHYN`.`COMMENTS`. INITIAL DATA
-- -----------------------------------------------------
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('1', '1', '���!!! �� - �����!!!', TO_TIMESTAMP('2015-08-10 12:06:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('2', '1', '������� ������� ����� ������� �����������, �� ������ ���� ���������� �����������.', TO_TIMESTAMP('2015-08-10 12:16:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('3', '5', '��� �� ��� �����? ��� ����� �������� ������� ��� ����� �������.', TO_TIMESTAMP('2015-08-10 12:06:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('4', '3', '������ ���� - �� ����������. ��� ��� ������� �� ��� ��� ��������', TO_TIMESTAMP('2015-08-10 12:26:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('5', '3', '��������, ��� ������ ����� �������.', TO_TIMESTAMP('2015-08-10 12:27:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('6', '2', '�� ������ ���� �����, ��� ����� ����', TO_TIMESTAMP('2015-08-10 12:45:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('7', '6', '�������� � �������!!!!', TO_TIMESTAMP('2015-08-10 12:47:25.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('8', '9', '� ��� ��� ����� ���������!', TO_TIMESTAMP('2015-08-10 13:06:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('9', '9', '����� ��������� ���������� ��� ����� ���������� � ������� ��������', TO_TIMESTAMP('2015-08-10 13:08:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('10', '9', '����, ����� ������� ���� � ������� � ����� � ����, ��� ���� ���������.', TO_TIMESTAMP('2015-08-10 13:10:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('11', '10', '���-�� ������ �� ���������� ����.', TO_TIMESTAMP('2015-08-10 13:11:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('12', '10', '��, ��� ������ ��� :(', TO_TIMESTAMP('2015-08-10 13:15:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('13', '10', '������, �� �� ���� ��� �������. ������� - �������!', TO_TIMESTAMP('2015-08-10 13:16:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "HRYSHYN"."COMMENTS" (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES ('14', '10', '������� �������� :) ������ ��� ������ ����������', TO_TIMESTAMP('2015-08-10 13:17:45.842000000', 'YYYY-MM-DD HH24:MI:SS.FF'));

-- -----------------------------------------------------
-- Table `HRYSHYN`.`AUTHOR`. INITIAL DATA
-- -----------------------------------------------------
INSERT INTO "HRYSHYN"."AUTHOR" (AUTHOR_ID_PK, AUTHOR_NAME) VALUES ('1', 'santamarius');
INSERT INTO "HRYSHYN"."AUTHOR" (AUTHOR_ID_PK, AUTHOR_NAME) VALUES ('2', '����� ������');
INSERT INTO "HRYSHYN"."AUTHOR" (AUTHOR_ID_PK, AUTHOR_NAME) VALUES ('3', '������� �����');
INSERT INTO "HRYSHYN"."AUTHOR" (AUTHOR_ID_PK, AUTHOR_NAME) VALUES ('4', '��������');
INSERT INTO "HRYSHYN"."AUTHOR" (AUTHOR_ID_PK, AUTHOR_NAME) VALUES ('5', '���� ����������');
INSERT INTO "HRYSHYN"."AUTHOR" (AUTHOR_ID_PK, AUTHOR_NAME) VALUES ('6', '������ ������');
INSERT INTO "HRYSHYN"."AUTHOR" (AUTHOR_ID_PK, AUTHOR_NAME) VALUES ('7', '����� ��������');
INSERT INTO "HRYSHYN"."AUTHOR" (AUTHOR_ID_PK, AUTHOR_NAME) VALUES ('8', '�����');


-- -----------------------------------------------------
-- Table `HRYSHYN`.`NEWS_AUTHOR`. INITIAL DATA
-- -----------------------------------------------------
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('1', '1');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('2', '1');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('3', '4');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('4', '3');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('5', '2');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('6', '5');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('7', '6');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('8', '6');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('9', '1');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('10', '2');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('11', '7');
INSERT INTO "HRYSHYN"."NEWS_AUTHOR" (NEWS_ID_FK, AUTHOR_ID_FK) VALUES ('12', '8');


-- -----------------------------------------------------
-- Table `HRYSHYN`.`USERS`. INITIAL DATA
-- -----------------------------------------------------
INSERT INTO "HRYSHYN"."USERS" (USER_ID_PK, USER_NAME, USER_LOGIN, USER_PASSWORD) VALUES ('1', 'Pavel_Hryshyn', 'admin', '21232f297a57a5a743894a0e4a801fc3');
INSERT INTO "HRYSHYN"."USERS" (USER_ID_PK, USER_NAME, USER_LOGIN, USER_PASSWORD) VALUES ('2', 'Santamarius', 'santamarius', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO "HRYSHYN"."USERS" (USER_ID_PK, USER_NAME, USER_LOGIN, USER_PASSWORD) VALUES ('3', '��������', '��������', '21232f297a57a5a743894a0e4a801fc3');

-- -----------------------------------------------------
-- Table `HRYSHYN`.`ROLES`. INITIAL DATA
-- -----------------------------------------------------
INSERT INTO "HRYSHYN"."ROLES" (USER_ID_FK, ROLE_NAME) VALUES ('1', 'ROLE_ADMIN');
INSERT INTO "HRYSHYN"."ROLES" (USER_ID_FK, ROLE_NAME) VALUES ('2', 'ROLE_ADMIN');
INSERT INTO "HRYSHYN"."ROLES" (USER_ID_FK, ROLE_NAME) VALUES ('3', 'ROLE_ADMIN');