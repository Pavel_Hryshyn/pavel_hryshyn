/*
 * @(#)IAuthorDao.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.dao;

import java.util.List;

import com.epam.newsmanagent.entity.Author;
import com.epam.newsmanagent.exception.DaoException;

/**
 * This interface defines methods that bind the Author entity with database
 * @version 1.0 04 September 2015
 * @author Pavel Hryshyn
 */
public interface IAuthorDao {

	/**
	 * This method returns a list of all authors that stores in database
	 * @return list of all authors from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<Author> findAll() throws DaoException;
	
	/**
	 * This method returns a list of all non-expired authors that stores in database
	 * @return  a list of all non-expired authors from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<Author> findAllNonExpiredAuthors() throws DaoException;
	
	/**
	 * This method returns author with some id that stores in database
	 * @param authorId
	 * 			the param uses for searching author in database
	 * @return author with some id from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	Author findById(Long authorId) throws DaoException;
	
	/**
	 * This method returns author for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching author in database
	 * @return author for some newsId from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	Author findByNewsId(Long newsId) throws DaoException;
	
	/**
	 * This method adds author to database
	 * @param author
	 * 			the param uses for adding author to database
	 * @return author's id
	 * @throws DaoException, if arise database access error or other database errors
	 * 				or author doesn't save in database
	 */
	Long create(Author author) throws DaoException;
	
	/**
	 * This method updates author to database
	 * @param author
	 *            the param uses for updating author to database
	 * @return true if author is updated to database, else false
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	boolean update(Author author) throws DaoException;
	
	/**
	 * This method sets a author expired 
	 * @param authorId
	 * 			the param uses for expired author
	 * @return true if a author is expired, else false
	 * @throws DaoException, if arise database access error or other database errors
	 */
	boolean expire(Long authorId) throws DaoException;
}
