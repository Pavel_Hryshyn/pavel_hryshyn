/*
 * @(#)CommentServiceImpl.java   1.0 2015/09/08
 */
package com.epam.newsmanagent.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagent.dao.ICommentDao;
import com.epam.newsmanagent.entity.Comment;
import com.epam.newsmanagent.exception.DaoException;
import com.epam.newsmanagent.exception.ServiceException;
import com.epam.newsmanagent.service.ICommentService;

/**
 * This class implements methods for Comment service
 * @version 1.0 08 September 2015
 * @author Pavel_Hryshyn
 */
@Service("commentService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CommentServiceImpl implements ICommentService {
		
	@Autowired
	private ICommentDao commentDao;

	@Override
	public List<Comment> findByNewsId(Long newsId) throws ServiceException {
		List<Comment> comments = null;
		try {
			comments = commentDao.findByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("findAll() method of CommentService is failed", e);
		}
		return comments;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long create(Comment comment) throws ServiceException {
		long commentId = 0;
		try {
			commentId = commentDao.create(comment);
		} catch (DaoException e) {
			throw new ServiceException("create() method of CommentService is failed", e);
		}
		return commentId;
	}
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean delete(Long id) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = commentDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException("delete() method of CommentService is failed", e);
		}
		return isDeleted;
	}

	@Override
	public Integer countCommentsByNewsId(Long newsId) throws ServiceException {
		int amount = 0;
		try {
			amount = commentDao.countCommentsByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("countCommentsByNewsId() method of CommentService is failed", e);
		}
		return amount;
	}

	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}
}
