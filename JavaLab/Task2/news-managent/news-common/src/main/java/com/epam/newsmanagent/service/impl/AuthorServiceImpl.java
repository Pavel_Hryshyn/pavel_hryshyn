/*
 * @(#)AuthorServiceImpl.java   1.0 2015/09/07
 */
package com.epam.newsmanagent.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagent.dao.IAuthorDao;
import com.epam.newsmanagent.entity.Author;
import com.epam.newsmanagent.exception.DaoException;
import com.epam.newsmanagent.exception.ServiceException;
import com.epam.newsmanagent.service.IAuthorService;

/**
 * This class implements methods for Author service
 * @version 1.0 07 September 2015
 * @author Pavel_Hryshyn
 */
@Service("authorService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AuthorServiceImpl implements IAuthorService {
		
	@Autowired
	private IAuthorDao authorDao;
	
	@Override
	public List<Author> findAll() throws ServiceException {
		List<Author> authors = null;
		try {
			authors = authorDao.findAll();
		} catch (DaoException e) {
			throw new ServiceException("findAll() method of AuthorService is failed", e);
		}
		return authors;
	}

	@Override
	public List<Author> findAllNonExpiredAuthors() throws ServiceException {
		List<Author> authors = null;
		try {
			authors = authorDao.findAllNonExpiredAuthors();
		} catch (DaoException e) {
			throw new ServiceException("findAllNonExpiredAuthors() method of AuthorService is failed", e);
		}
		return authors;
	}

	@Override
	public Author findById(Long authorId) throws ServiceException {
		Author author = null;
		try {
			author = authorDao.findById(authorId);
		} catch (DaoException e) {
			throw new ServiceException("findById() method of AuthorService is failed", e);
		}
		return author;
	}

	@Override
	public Author findByNewsId(Long newsId) throws ServiceException {
		Author author = null;
		try {
			author = authorDao.findByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("findByNewsId() method of AuthorService is failed", e);
		}
		return author;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long create(Author author) throws ServiceException {
		long authorId = 0;
		try {
			authorId = authorDao.create(author);
		} catch (DaoException e) {
			throw new ServiceException("create() method of AuthorService is failed", e);
		}
		return authorId;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean update(Author author) throws ServiceException {
		boolean isUpdated = false;
		try {
			isUpdated = authorDao.update(author);
		} catch (DaoException e) {
			throw new ServiceException("update() method of AuthorService is failed", e);
		}
		return isUpdated;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean expire(Long authorId) throws ServiceException {
		boolean isExpired = false;
		try {
			isExpired = authorDao.expire(authorId);
		} catch (DaoException e) {
			throw new ServiceException("expire() method of AuthorService is failed", e);
		}
		return isExpired;
	}

	public IAuthorDao getAuthorDao() {
		return authorDao;
	}

	public void setAuthorDao(IAuthorDao authorDao) {
		this.authorDao = authorDao;
	}
}
