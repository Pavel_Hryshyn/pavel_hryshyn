/*
 * @(#)CommentDaoImpl.java   1.1 2015/09/18
 */
package com.epam.newsmanagent.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagent.dao.ICommentDao;
import com.epam.newsmanagent.entity.Comment;
import com.epam.newsmanagent.entity.News;
import com.epam.newsmanagent.exception.DaoException;
import com.epam.newsmanagent.utils.DbConnectionManager;
import com.epam.newsmanagent.utils.SqlDateConvertor;

/**
 * This class implements DAO pattern and contains methods that bind the Comment
 * entity with database
 * 
 * @version 1.1 18 September 2015
 * @author Pavel Hryshyn
 */
@Repository("commentDao")
public class CommentDaoImpl implements ICommentDao {
	/* This constants stores SQL queries for database */
	private static final String SQL_FIND_BY_NEWS_ID = "SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, comments.comments_id_pk, comments.news_id_fk, comments.comments_text, comments.comments_creation_date FROM comments JOIN news ON news.news_id_pk = comments.news_id_fk WHERE news.news_id_pk = ? ORDER BY comments.comments_creation_date DESC";
	private static final String SQL_COMMENT_ADD = "INSERT INTO comments (COMMENTS_ID_PK, NEWS_ID_FK, COMMENTS_TEXT, COMMENTS_CREATION_DATE) VALUES (comments_sequence.nextval, ?, ?, ?)";
	private static final String SQL_COUNT_BY_NEWS_ID = "SELECT COUNT(*) FROM comments WHERE news_id_fk=?";
	private static final String SQL_DELETE = "DELETE FROM comments WHERE comments_id_pk=?";
	private static final String SQL_DELETE_BY_NEWS_ID = "DELETE FROM comments WHERE news_id_fk=?";

	/* This constants stores names of database column */
	private static final String NEWS_ID = "news_id_pk";
	private static final String NEWS_TITLE = "news_title";
	private static final String NEWS_SHORT_TEXT = "news_short_text";
	private static final String NEWS_FULL_TEXT = "news_full_text";
	private static final String NEWS_CREATION_DATE = "news_creation_date";
	private static final String NEWS_MODIFICATION_DATE = "news_modification_date";
	private static final String COMMENT_ID = "comments_id_pk";
	private static final String COMMENT_TEXT = "comments_text";
	private static final String COMMENT_CREATION_DATE = "comments_creation_date";

	/* This object is factory for connection */
	@Autowired
	private DataSource dataSource;

	@Override
	public List<Comment> findByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Comment> comments = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_NEWS_ID);
			stmt.setLong(1, newsId);
			resultSet = stmt.executeQuery();
			comments = buildCommentList(resultSet);
		} catch (SQLException e) {
			throw new DaoException(
					"findByNewsId() request of CommentDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return comments;
	}

	@Override
	public Long create(Comment comment) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long commentId = 0;
		String generatedColumns[] = { COMMENT_ID };
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_COMMENT_ADD, generatedColumns);
			stmt.setLong(1, comment.getNews().getId());
			stmt.setString(2, comment.getCommentText());
			stmt.setTimestamp(3, SqlDateConvertor.convertToSqlTimestamp(comment
					.getCreationDate()));
			stmt.executeUpdate();
			resultSet = stmt.getGeneratedKeys();
			if (resultSet.next()) {
				commentId = resultSet.getLong(1);
			} else {
				throw new DaoException("Comment is not created");
			}
		} catch (SQLException e) {
			throw new DaoException("create() request of CommentDao is failed",
					e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return commentId;
	}

	@Override
	public Integer countCommentsByNewsId(Long newsId) throws DaoException {
		int amount = 0;
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_COUNT_BY_NEWS_ID);
			stmt.setLong(1, newsId);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				amount = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			throw new DaoException(
					"countCommentsByNewsId() request of CommentDao is failed",
					e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return amount;
	}

	@Override
	public boolean delete(Long id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_DELETE);
			stmt.setLong(1, id);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("delete() request of CommentDao is failed",
					e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return isDeleted;
	}

	@Override
	public boolean deleteCommentsByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_DELETE_BY_NEWS_ID);
			stmt.setLong(1, newsId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException(
					"deleteCommentsByNewsId() request of CommentDao is failed",
					e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return isDeleted;
	}

	private Comment buildComment(ResultSet rs) throws SQLException {
		/*
		 * This method builds Comment object from ResultSet object
		 */
		News news = buildNews(rs);
		Comment comment = new Comment();
		comment.setId(rs.getLong(COMMENT_ID));
		comment.setNews(news);
		comment.setCommentText(rs.getString(COMMENT_TEXT));
		comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
		return comment;
	}

	private News buildNews(ResultSet rs) throws SQLException {
		/*
		 * This method builds Comment object from ResultSet object
		 */
		News news = new News();
		news.setId(rs.getLong(NEWS_ID));
		news.setTitle(rs.getString(NEWS_TITLE));
		news.setShortText(rs.getString(NEWS_SHORT_TEXT));
		news.setFullText(rs.getString(NEWS_FULL_TEXT));
		news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
		news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
		return news;
	}

	private List<Comment> buildCommentList(ResultSet rs) throws SQLException {
		/*
		 * This method builds list of Comment objects from ResultSet object
		 */
		List<Comment> comments = new ArrayList<Comment>();
		while (rs.next()) {
			comments.add(buildComment(rs));
		}
		return comments;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
