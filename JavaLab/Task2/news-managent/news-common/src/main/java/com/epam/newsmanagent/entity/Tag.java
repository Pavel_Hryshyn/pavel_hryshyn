/*
 * @(#)Tag.java   1.0 2015/09/03
 */
package com.epam.newsmanagent.entity;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * This class is entity that defines the parameters of Tag
 * @version 1.0 03 September 2015
 * @author Pavel_Hryshyn
 */
public class Tag implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/* This value stores tag id */
	private long id;
	
	/* This value stores tag name */
	@Size(min=1, max=30)
	private String tagName;

	public Tag() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (id != other.id)
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", tagName=" + tagName + "]";
	}
}
