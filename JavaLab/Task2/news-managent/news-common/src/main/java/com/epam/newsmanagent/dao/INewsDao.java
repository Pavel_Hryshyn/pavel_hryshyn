/*
 * @(#)INewsDao.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.dao;

import java.util.List;

import com.epam.newsmanagent.entity.News;
import com.epam.newsmanagent.entity.SearchCriteria;
import com.epam.newsmanagent.exception.DaoException;

/**
 * This interface defines methods that bind the News entity with database
 * @version 1.0 04 September 2015
 * @author Pavel Hryshyn
 */
public interface INewsDao {
	/**
	 * This method returns a list of all news that stores in database
	 * @param start
	 * 			is the start position in news list
 	 * @param end
 	 * 			is the end position in news list
	 * @return list of all news from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<News> findBetweenPositions(int start, int end) throws DaoException;
	
	/**
	 * This method returns list of news which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of all news which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws DaoException;
	
	/**
	 * This method returns news with some id that stores in database
	 * @param newsId
	 * 			the param uses for searching news in database
	 * @return news with some id from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	News findById(Long newsId) throws DaoException;
	
	/**
	 * This method returns next newsId from list of news formed by SearchCriteris
	 * @param newsId
	 * 			the param uses for getting next newsId in database
	 * @param sc
	 * 			the param uses for formed list of news
	 * @return next newsId
	 * @throws DaoException, if arise database access error or other database errors
	 */
	Long getNextNewsId(Long newsId, SearchCriteria sc) throws DaoException;
	
	/**
	 * This method returns previous newsId from list of news formed by SearchCriteris
	 * @param newsId
	 * 			the param uses for getting previous newsId in database
	 * @param sc
	 * 			the param uses for formed list of news
	 * @return previous newsId
	 * @throws DaoException, if arise database access error or other database errors
	 */
	Long getPreviousNewsId(Long newsId, SearchCriteria sc) throws DaoException;
	
	/**
	 * This method returns amount of all news that stores in database
	 * @return amount of all news from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	int countAllNews() throws DaoException;
	
	/**
	 * This method returns amount of all news which matching searchCriteria 
	 * that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return amount of all news which matching searchCriteria from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	int countNewsWithCriteria(SearchCriteria searchCriteria) throws DaoException;
	
	/**
	 * This method adds news to database
	 * @param news
	 * 			the param uses for adding news to database
	 * @return news's id
	 * @throws DaoException, if arise database access error or other database errors
	 * 				or news doesn't save in database
	 */
	Long create(News news) throws DaoException;
	
	/**
	 * This method adds news with list of tags to database
	 * @param newsId
	 *          the param uses for adding news to database
	 * @param tagsId
	 * 			the param uses for adding list of tags to database
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	void addNewsWithTags(Long newsId, List<Long> tagsId) throws DaoException;
	
	/**
	 * This method adds news with author to database
	 * @param newsId
	 *          the param uses for adding news to database
	 * @param authorId
	 * 			the param uses for adding author to database
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	void addNewsWithAuthor(Long newsId, Long authorId) throws DaoException;
	
	/**
	 * This method updates news to database
	 * @param news
	 *            the param uses for updating news to database
	 * @return true if news is updated to database, else false
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	boolean update(News news) throws DaoException;
	
	/**
	 * This method deletes a news with some newsId from database
	 * @param newsId
	 * 			the param uses for deleting a news from database
	 * @return true if a news is deleted from database, else false
	 * @throws DaoException, if arise database access error or other database errors
	 */
	boolean delete(Long newsId) throws DaoException;
	
	/**
	 * This method deleted news with list of tags from database
	 * @param newsId
	 *          the param uses for deleting news from database
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	boolean deleteTagsForNews(Long newsId) throws DaoException; 
	
	/**
	 * This method deleted news with author from database
	 * @param newsId
	 *          the param uses for deleting news from database
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	boolean deleteAuthorForNews(Long newsId) throws DaoException; 
	
}
