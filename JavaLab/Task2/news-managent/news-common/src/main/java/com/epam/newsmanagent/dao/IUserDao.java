/*
 * @(#)IUserDao.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.dao;

/**
 * This interface defines methods that bind the User entity with database
 * @version 1.0 04 September 2015
 * @author Pavel Hryshyn
 */
public interface IUserDao {

}
