/*
 * @(#)NewsDaoImpl.java   1.0 2015/09/09
 */
package com.epam.newsmanagent.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagent.dao.INewsDao;
import com.epam.newsmanagent.entity.News;
import com.epam.newsmanagent.entity.SearchCriteria;
import com.epam.newsmanagent.exception.DaoException;
import com.epam.newsmanagent.utils.DbConnectionManager;
import com.epam.newsmanagent.utils.QueryFormer;
import com.epam.newsmanagent.utils.SqlDateConvertor;

/**
 * This class implements DAO pattern and contains methods that bind the News
 * entity with database
 * 
 * @version 1.0 04 September 2015
 * @author Pavel Hryshyn
 */
@Repository("newsDao")
public class NewsDaoImpl implements INewsDao {
	/* This constants stores SQL queries for database */
	private static final String SQL_FIND_ALL_POSITIONS = "SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC) a WHERE ROWNUM <=?) WHERE rnum >=?";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM news WHERE news_id_pk=?";
	private static final String SQL_NEWS_ADD = "INSERT INTO news (NEWS_ID_PK, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES (news_sequence.nextval, ?, ?, ?, ?, ?)";
	private static final String SQL_NEWS_TAGS_ADD = "INSERT INTO news_tag (news_id_fk, tag_id_fk) VALUES (?, ?)";
	private static final String SQL_NEWS_AUTHOR_ADD = "INSERT INTO news_author (news_id_fk, author_id_fk) VALUES (?, ?)";
	private static final String SQL_NEWS_DELETE = "DELETE FROM news WHERE news_id_pk=?";
	private static final String SQL_NEWS_UPDATE = "UPDATE news SET news_title = ?, news_short_text = ?, news_full_text = ?, news_modification_date = ?  WHERE news_id_pk = ?";
	private static final String SQL_NEWS_TAGS_DELETE = "DELETE FROM news_tag WHERE news_id_fk=?";
	private static final String SQL_NEWS_AUTHOR_DELETE = "DELETE FROM news_author WHERE news_id_fk=?";
	private static final String SQL_AMOUNT_NEWS = "SELECT COUNT(*) FROM news";
	private static final String SQL_CRITERIA_AUTHOR_SUBQUERY = "news_author.author_id_fk=? AND ( ";
	private static final String SQL_CRITERIA_TAG_SUBQUERY = "news_tag.tag_id_fk=?";
	private static final String SQL_CRITERIA_POSITION_MAIN_SUBQUERY = "SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE";
	private static final String SQL_CRITERIA_POSITION_TAIL_SUBQUERY = "GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC) a WHERE ROWNUM <=?) WHERE rnum >=?";
	private static final String SQL_CRITERIA_POSITIONS_WITHOUT_TAGS = "SELECT * from (SELECT  a.*, ROWNUM rnum FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE news_author.author_id_fk=? GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC) a WHERE ROWNUM <=?) WHERE rnum >=?";
	private static final String SQL_COUNT_CRITERIA_MAIN_QUERY = "SELECT COUNT(*) FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE ";
	private static final String SQL_COUNT_CRITERIA_WITHOUT_TAGS = "SELECT COUNT(*) FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE news_author.author_id_fk=? GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC)";
	private static final String SQL_COUNT_CRITERIA_WITHOUT_AUTHOR_AND_TAGS = "SELECT COUNT(*) FROM (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC)";
	private static final String SQL_COUNT_CRITERIA_TAIL_SUBQUERY = "GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC";
	private static final String SQL_GET_NEXT_NEWS_MAIN_SUBQUERY = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LEAD(news_id_pk, 1) over(order by am DESC, news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, COUNT(comments.news_id_fk) as am FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE";
	private static final String SQL_GET_NEXT_NEWS_WITHOUT_AUTHOR_TAG = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LEAD(news_id_pk, 1) over(order by am DESC, news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, COUNT(comments.news_id_fk) as am FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_GET_NEXT_NEWS_WITHOUT_TAGS = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LEAD(news_id_pk, 1) over(order by am DESC, news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, COUNT(comments.news_id_fk) as am FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE news_author.author_id_fk=? GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_GET_NEXT_PREVIOUS_NEWS_TAIL_SUBQUERY = "GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_GET_PREVIOUS_NEWS_MAIN_SUBQUERY = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LAG(news_id_pk, 1) over(order by am DESC, news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, COUNT(comments.news_id_fk) as am FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE";
	private static final String SQL_GET_PREVIOUS_NEWS_WITHOUT_AUTHOR_TAG = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LAG(news_id_pk, 1) over(order by am DESC, news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, COUNT(comments.news_id_fk) as am FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC)) where news_id_pk = ?";
	private static final String SQL_GET_PREVIOUS_NEWS_WITHOUT_TAGS = "SELECT next_news_id from (SELECT news_id_pk, news_modification_date,  LAG(news_id_pk, 1) over(order by am DESC, news_MODIFICATION_DATE DESC) as next_news_id from (SELECT news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date, COUNT(comments.news_id_fk) as am FROM news LEFT JOIN comments ON news.news_id_pk = comments.news_id_fk LEFT JOIN news_author ON news.news_id_pk = news_author.news_id_fk LEFT JOIN news_tag ON news.news_id_pk = news_tag.news_id_fk WHERE news_author.author_id_fk=? GROUP BY news.news_id_pk, news.news_title, news.news_short_text, news.news_full_text, news.news_creation_date, news.news_modification_date ORDER BY COUNT(comments.news_id_fk) DESC, news.news_modification_date DESC)) where news_id_pk = ?";

	/* This constants stores names of database column */
	private static final String NEWS_ID = "news_id_pk";
	private static final String TITLE = "news_title";
	private static final String SHORT_TEXT = "news_short_text";
	private static final String FULL_TEXT = "news_full_text";
	private static final String CREATION_DATE = "news_creation_date";
	private static final String MODIFICATION_DATE = "news_modification_date";

	/* This constants stores bracket that using in SQL query */
	private static final String BRACKET = ") ";

	/* This object is factory for connection */
	@Autowired
	private DataSource dataSource;

	@Override
	public List<News> findBetweenPositions(int start, int end)
			throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL_POSITIONS);
			stmt.setInt(1, end);
			stmt.setInt(2, start);
			resultSet = stmt.executeQuery();
			newsList = buildNewsList(resultSet);
		} catch (SQLException e) {
			throw new DaoException(
					"findBetweenPositions() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return newsList;
	}

	@Override
	public List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc,
			int start, int end) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<News> newsList = null;
		String mainSubQuery = SQL_CRITERIA_POSITION_MAIN_SUBQUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = SQL_CRITERIA_POSITION_TAIL_SUBQUERY;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			if (sc.getAuthor() != null) {
				if (sc.getTags() != null) {
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteria(mainSubQuery,
							authorSubQuery, tagSubQuery, tailSubQuery,
							amountTags);
					stmt = con.prepareStatement(resultingQuery);
					stmt.setLong(1, sc.getAuthor().getId());
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList + 2;
						stmt.setLong(indexPs, sc.getTags().get(indexList)
								.getId());
					}
					stmt.setInt(amountTags + 2, end);
					stmt.setInt(amountTags + 3, start);
					resultSet = stmt.executeQuery();
					newsList = buildNewsList(resultSet);
				} else {
					String resultingQuery = SQL_CRITERIA_POSITIONS_WITHOUT_TAGS;
					stmt = con.prepareStatement(resultingQuery);
					stmt.setLong(1, sc.getAuthor().getId());
					stmt.setInt(2, end);
					stmt.setInt(3, start);
					resultSet = stmt.executeQuery();
					newsList = buildNewsList(resultSet);
				}
			} else {
				if (sc.getTags() != null) {
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteriaWithoutAuthor(
							mainSubQuery, tagSubQuery, tailSubQuery, amountTags);
					stmt = con.prepareStatement(resultingQuery);
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList + 1;
						stmt.setLong(indexPs, sc.getTags().get(indexList)
								.getId());
					}
					stmt.setInt(amountTags + 1, end);
					stmt.setInt(amountTags + 2, start);
					resultSet = stmt.executeQuery();
					newsList = buildNewsList(resultSet);
				} else {
					stmt = con.prepareStatement(SQL_FIND_ALL_POSITIONS);
					stmt.setInt(1, end);
					stmt.setInt(2, start);
					resultSet = stmt.executeQuery();
					newsList = buildNewsList(resultSet);
				}
			}
		} catch (SQLException e) {
			throw new DaoException(
					"findNewsWithCriteriaAndPositions() request of NewsDao is failed",
					e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return newsList;
	}

	@Override
	public News findById(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		News news = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_ID);
			stmt.setLong(1, newsId);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				news = buildNews(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException("findById() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return news;
	}

	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria sc)
			throws DaoException {
		String mainSubQuery = SQL_GET_NEXT_NEWS_MAIN_SUBQUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = SQL_GET_NEXT_PREVIOUS_NEWS_TAIL_SUBQUERY;
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long nextId = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			if (sc.getAuthor() != null) {
				if (sc.getTags() != null) {
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteria(mainSubQuery,
							authorSubQuery, tagSubQuery, tailSubQuery,
							amountTags);
					stmt = con.prepareStatement(resultingQuery);
					stmt.setLong(1, sc.getAuthor().getId());
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList + 2;
						stmt.setLong(indexPs, sc.getTags().get(indexList)
								.getId());
					}
					stmt.setLong(amountTags + 2, newsId);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						nextId = resultSet.getInt(1);
					}
				} else {
					String resultingQuery = SQL_GET_NEXT_NEWS_WITHOUT_TAGS;
					stmt = con.prepareStatement(resultingQuery);
					stmt.setLong(1, sc.getAuthor().getId());
					stmt.setLong(2, newsId);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						nextId = resultSet.getInt(1);
					}
				}
			} else {
				if (sc.getTags() != null) {
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteriaWithoutAuthor(
							mainSubQuery, tagSubQuery, tailSubQuery, amountTags);
					stmt = con.prepareStatement(resultingQuery);
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList + 1;
						stmt.setLong(indexPs, sc.getTags().get(indexList)
								.getId());
					}
					stmt.setLong(amountTags + 1, newsId);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						nextId = resultSet.getInt(1);
					}
				} else {
					stmt = con
							.prepareStatement(SQL_GET_NEXT_NEWS_WITHOUT_AUTHOR_TAG);
					stmt.setLong(1, newsId);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						nextId = resultSet.getLong(1);
					}
				}
			}
		} catch (SQLException e) {
			throw new DaoException(
					"getNextNewsId() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return nextId;
	}

	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria sc)
			throws DaoException {
		String mainSubQuery = SQL_GET_PREVIOUS_NEWS_MAIN_SUBQUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = SQL_GET_NEXT_PREVIOUS_NEWS_TAIL_SUBQUERY;
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long previousId = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			if (sc.getAuthor() != null) {
				if (sc.getTags() != null) {
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteria(mainSubQuery,
							authorSubQuery, tagSubQuery, tailSubQuery,
							amountTags);
					stmt = con.prepareStatement(resultingQuery);
					stmt.setLong(1, sc.getAuthor().getId());
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList + 2;
						stmt.setLong(indexPs, sc.getTags().get(indexList)
								.getId());
					}
					stmt.setLong(amountTags + 2, newsId);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						previousId = resultSet.getInt(1);
					}
				} else {
					String resultingQuery = SQL_GET_PREVIOUS_NEWS_WITHOUT_TAGS;
					stmt = con.prepareStatement(resultingQuery);
					stmt.setLong(1, sc.getAuthor().getId());
					stmt.setLong(2, newsId);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						previousId = resultSet.getInt(1);
					}
				}
			} else {
				if (sc.getTags() != null) {
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteriaWithoutAuthor(
							mainSubQuery, tagSubQuery, tailSubQuery, amountTags);
					stmt = con.prepareStatement(resultingQuery);
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList + 1;
						stmt.setLong(indexPs, sc.getTags().get(indexList)
								.getId());
					}
					stmt.setLong(amountTags + 1, newsId);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						previousId = resultSet.getInt(1);
					}
				} else {
					stmt = con
							.prepareStatement(SQL_GET_PREVIOUS_NEWS_WITHOUT_AUTHOR_TAG);
					stmt.setLong(1, newsId);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						previousId = resultSet.getLong(1);
					}
				}
			}
		} catch (SQLException e) {
			throw new DaoException(
					"getPreviousNewsId() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return previousId;
	}

	@Override
	public int countAllNews() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		int amountOfNews = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_AMOUNT_NEWS);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				amountOfNews = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			throw new DaoException(
					"countAllNews() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return amountOfNews;
	}

	@Override
	public int countNewsWithCriteria(SearchCriteria sc) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		int amountNews = 0;
		String mainSubQuery = SQL_COUNT_CRITERIA_MAIN_QUERY;
		String authorSubQuery = SQL_CRITERIA_AUTHOR_SUBQUERY;
		String tagSubQuery = SQL_CRITERIA_TAG_SUBQUERY;
		String tailSubQuery = SQL_COUNT_CRITERIA_TAIL_SUBQUERY + BRACKET;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			if (sc.getAuthor() != null) {
				if (sc.getTags() != null) {
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteria(mainSubQuery,
							authorSubQuery, tagSubQuery, tailSubQuery,
							amountTags);
					stmt = con.prepareStatement(resultingQuery);
					stmt.setLong(1, sc.getAuthor().getId());
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList + 2;
						stmt.setLong(indexPs, sc.getTags().get(indexList)
								.getId());
					}
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						amountNews = resultSet.getInt(1);
					}
				} else {
					String query = SQL_COUNT_CRITERIA_WITHOUT_TAGS;
					stmt = con.prepareStatement(query);
					stmt.setLong(1, sc.getAuthor().getId());
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						amountNews = resultSet.getInt(1);
					}
				}
			} else {
				if (sc.getTags() != null) {
					int amountTags = sc.getTags().size();
					String resultingQuery = buildQueryForCriteriaWithoutAuthor(
							mainSubQuery, tagSubQuery, tailSubQuery, amountTags);
					stmt = con.prepareStatement(resultingQuery);
					for (int indexList = 0; indexList < amountTags; indexList++) {
						int indexPs = indexList + 1;
						stmt.setLong(indexPs, sc.getTags().get(indexList)
								.getId());
					}
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						amountNews = resultSet.getInt(1);
					}
				} else {
					stmt = con
							.prepareStatement(SQL_COUNT_CRITERIA_WITHOUT_AUTHOR_AND_TAGS);
					resultSet = stmt.executeQuery();
					if (resultSet.next()) {
						amountNews = resultSet.getInt(1);
					}
				}
			}
		} catch (SQLException e) {
			throw new DaoException(
					"countNewsWithCriteria() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return amountNews;
	}

	@Override
	public Long create(News news) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long newsId = 0;
		String generatedColumns[] = { NEWS_ID };
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_ADD, generatedColumns);
			stmt.setString(1, news.getTitle());
			stmt.setString(2, news.getShortText());
			stmt.setString(3, news.getFullText());
			stmt.setTimestamp(4, SqlDateConvertor.convertToSqlTimestamp(news
					.getCreationDate()));
			stmt.setDate(5, SqlDateConvertor.convertToSqlDate(news
					.getModificationDate()));
			stmt.executeUpdate();
			resultSet = stmt.getGeneratedKeys();
			if (resultSet.next()) {
				newsId = resultSet.getLong(1);
			} else {
				throw new DaoException("News is not created");
			}
		} catch (SQLException e) {
			throw new DaoException("create() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return newsId;
	}

	@Override
	public void addNewsWithTags(Long newsId, List<Long> tagsId)
			throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_TAGS_ADD);
			for (Long tagId : tagsId) {
				stmt.setLong(1, newsId);
				stmt.setLong(2, tagId);
				stmt.addBatch();
			}
			stmt.executeBatch();
		} catch (SQLException e) {
			throw new DaoException(
					"addNewsWithTags() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
	}

	@Override
	public void addNewsWithAuthor(Long newsId, Long authorId)
			throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_AUTHOR_ADD);
			stmt.setLong(1, newsId);
			stmt.setLong(2, authorId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(
					"addNewsWithAuthor() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
	}

	@Override
	public boolean update(News news) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		boolean isUpdated = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_UPDATE);
			stmt.setString(1, news.getTitle());
			stmt.setString(2, news.getShortText());
			stmt.setString(3, news.getFullText());
			stmt.setDate(4, SqlDateConvertor.convertToSqlDate(news
					.getModificationDate()));
			stmt.setLong(5, news.getId());
			row = stmt.executeUpdate();
			if (row != 0) {
				isUpdated = true;
			}
		} catch (SQLException e) {
			throw new DaoException("update() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return isUpdated;
	}

	@Override
	public boolean delete(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_DELETE);
			stmt.setLong(1, newsId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("delete() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return isDeleted;
	}

	@Override
	public boolean deleteTagsForNews(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_TAGS_DELETE);
			stmt.setLong(1, newsId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException(
					"deleteTagsForNews() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return isDeleted;
	}

	@Override
	public boolean deleteAuthorForNews(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_NEWS_AUTHOR_DELETE);
			stmt.setLong(1, newsId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException(
					"deleteAuthorForNews() request of NewsDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return isDeleted;
	}

	private News buildNews(ResultSet rs) throws SQLException {
		/*
		 * This method builds news from ResultSet object
		 */
		News news = new News();
		news.setId(rs.getLong(NEWS_ID));
		news.setTitle(rs.getString(TITLE));
		news.setShortText(rs.getString(SHORT_TEXT));
		news.setFullText(rs.getString(FULL_TEXT));
		news.setCreationDate(rs.getTimestamp(CREATION_DATE));
		news.setModificationDate(rs.getDate(MODIFICATION_DATE));
		return news;
	}

	private List<News> buildNewsList(ResultSet rs) throws SQLException {
		/*
		 * This method builds news list from ResultSet object
		 */
		List<News> newsList = new ArrayList<News>();
		while (rs.next()) {
			newsList.add(buildNews(rs));
		}
		return newsList;
	}

	private String buildQueryForCriteria(String mainSubQuery,
			String authorSubQuery, String tagSubQuery, String tailSubQuery,
			int amountTags) {
		/*
		 * This method builds SQL query for search criteria with sets author and
		 * tags
		 */
		String resultingQuery = null;
		String tempQueryWithoutEnd = QueryFormer.addIterativeSubQueryToEnd(
				QueryFormer.addSubQueryToEnd(mainSubQuery, authorSubQuery),
				tagSubQuery, amountTags);
		tailSubQuery = QueryFormer.addSubQueryToEnd(BRACKET, tailSubQuery);
		resultingQuery = QueryFormer.addSubQueryToEnd(tempQueryWithoutEnd,
				tailSubQuery);
		return resultingQuery;
	}

	private String buildQueryForCriteriaWithoutAuthor(String mainSubQuery,
			String tagSubQuery, String tailSubQuery, int amountTags) {
		/*
		 * This method builds SQL query for search criteria with sets tags
		 */
		String resultingQuery = null;
		String tempQueryWithoutEnd = QueryFormer.addIterativeSubQueryToEnd(
				mainSubQuery, tagSubQuery, amountTags);
		resultingQuery = QueryFormer.addSubQueryToEnd(tempQueryWithoutEnd,
				tailSubQuery);
		return resultingQuery;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
