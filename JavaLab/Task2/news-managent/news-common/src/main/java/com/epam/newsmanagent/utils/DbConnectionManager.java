/*
 * @(#)DbConnectionManager.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import com.epam.newsmanagent.exception.DaoException;

/**
 * This class contains static methods for managing database connections
 * @version 1.0 04 September 2015
 * @author Pavel_Hryshyn
 */
public class DbConnectionManager {
	/* This constants stores exception messages */
	private static final String EXC_CON_CLOSE = "Error of closing connection";
	private static final String EXC_RESULT_SET_CLOSE = "Error of closing resultSet";
	private static final String EXC_STATEMENT_CLOSE = "Error of closing statement";

	/**
	 * This method close ResultSet
	 * @param resultSet
	 * @throws DaoException, if resultSet isn't closed				
	 */
	public static void close(ResultSet resultSet) throws DaoException{
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new DaoException(EXC_RESULT_SET_CLOSE, e);
			}
		}
	}
	
	/**
	 * This method close PreparedStatement
	 * @param stmt
	 * @throws DaoException, if stmt isn't closed				
	 */
	public static void close(PreparedStatement stmt) throws DaoException{
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				throw new DaoException(EXC_STATEMENT_CLOSE, e);
			}
		}
	}
	
	/**
	 * This method close Connection
	 * @param con
	 * @param dataSource
	 * @throws DaoException, if connection isn't closed				
	 */
	public static void close(Connection con, DataSource dataSource) throws DaoException{
		if (con != null) {
			try {
				DataSourceUtils.doReleaseConnection(con, dataSource);
			} catch (SQLException e) {
				throw new DaoException(EXC_CON_CLOSE, e);
			}
		}
	}
	
	/**
	 * This method close Connection
	 * @param con
	 * @param stmt
	 * @param rs
	 * @param dataSource
	 * @throws DaoException, if connection isn't closed				
	 */
	public static void close(Connection con, PreparedStatement stmt, ResultSet rs, DataSource dataSource) throws DaoException{
		close(rs);
		close(stmt);
		close(con, dataSource);
	}
}
