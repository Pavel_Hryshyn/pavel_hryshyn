/*
 * @(#)IAuthorService.java   1.0 2015/11/26
 */
package com.epam.newsmanagent.service;


import java.util.List;

import com.epam.newsmanagent.entity.SearchCriteria;
import com.epam.newsmanagent.entity.vo.NewsComplex;
import com.epam.newsmanagent.exception.ServiceException;

/**
 * This interface defines basic methods for ServiceManager on service layer
 * @version 1.0 26 November 2015
 * @author Pavel_Hryshyn
 */
public interface IServiceManager {
	/**
	 * This method returns newsComplex for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching list of comments in database
	 * @return newsComplex for some newsId from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	NewsComplex findByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * This method returns list of newsComplex which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of all newsComplex which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<NewsComplex> findNewsComplexWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws ServiceException;
	
	IAuthorService getAuthorService();
	
	void setAuthorService(IAuthorService authorService);
	
	ICommentService getCommentService();
	
	void setCommentService(ICommentService commentService);
	
	public INewsService getNewsService();
	
	public void setNewsService(INewsService newsService); 

	public ITagService getTagService(); 

	public void setTagService(ITagService tagService); 	
}
