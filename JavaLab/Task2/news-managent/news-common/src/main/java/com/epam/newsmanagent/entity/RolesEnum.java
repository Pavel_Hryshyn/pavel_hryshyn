/*
 * @(#)RolesEnum.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.entity;

/**
 * This enum stores roles that using in application
 * @version 1.0 04 September 2015
 * @author Pavel_Hryshyn
 */
public enum RolesEnum {
	AUTHOR, ADMIN, USER;
}
