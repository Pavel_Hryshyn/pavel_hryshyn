/*
 * @(#)Author.java   1.0 2015/11/26
 */
package com.epam.newsmanagent.entity.vo;

import java.io.Serializable;
import java.util.List;

import com.epam.newsmanagent.entity.Author;
import com.epam.newsmanagent.entity.Comment;
import com.epam.newsmanagent.entity.News;
import com.epam.newsmanagent.entity.Tag;

/**
 * This class is VO object that contains News, Comments, Author and Tags entities
 * @version 1.0 26 November 2015
 * @author Pavel_Hryshyn
 */
public class NewsComplex implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/* This value stores News entity */
	private News news;
	
	/* This value stores author entity */
	private Author author;
	
	/* This value stores tag list */
	private List<Tag> tags;
	
	/* This value stores comment list */
	private List<Comment> comments;

	public NewsComplex() {
		super();
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsComplex other = (NewsComplex) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsComplex [news=" + news + ", author=" + author + ", tags="
				+ tags + ", comments=" + comments + "]";
	}
}
