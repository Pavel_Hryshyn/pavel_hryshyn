/*
 * @(#)ITagDao.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.dao;

import java.util.List;

import com.epam.newsmanagent.entity.Tag;
import com.epam.newsmanagent.exception.DaoException;

/**
 * This interface defines methods that bind the Tag entity with database
 * @version 1.0 04 September 2015
 * @author Pavel Hryshyn
 */
public interface ITagDao {
	/**
	 * This method returns a list of all tags that stores in database
	 * @return list of all tags from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<Tag> findAll() throws DaoException;
	
	/**
	 * This method returns a list of tags for news with some newsId that stores in database
	 * @param newsId
	 * @return list of tags for news with some newsId from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<Tag> findByNewsId(Long newsId) throws DaoException;
	
	/**
	 * This method tag with some tagId that stores in database
	 * @param tagId
	 * @return tag with some tagId with some newsId from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	Tag findById(Long tagId) throws DaoException;
	
	/**
	 * This method adds tag to database
	 * @param tag
	 * 			the param uses for adding tag to database
	 * @return tagId
	 * @throws DaoException, if arise database access error or other database errors
	 * 				or author doesn't save in database
	 */
	Long create(Tag tag) throws DaoException;
	
	/**
	 * This method updates tag to database
	 * @param tag
	 *            the param uses for updating tag to database
	 * @return true if tag is updated to database, else false
	 * @throws DaoException,
	 *             if arise database access error or other database errors
	 */
	boolean update(Tag tag) throws DaoException;
	
	/**
	 * This method deletes a tag from database
	 * @param tagId
	 * 			the param uses for deleting tag
	 * @return true if a tag is deleted, else false
	 * @throws DaoException, if arise database access error or other database errors
	 */
	boolean delete(Long tagId) throws DaoException;
	
	/**
	 * This method deletes a tag from news database
	 * @param tagId
	 * 			the param uses for deleting tag
	 * @return true if a tag is deleted, else false
	 * @throws DaoException, if arise database access error or other database errors
	 */
	boolean deleteTagFromNews(Long tagId) throws DaoException;
}
