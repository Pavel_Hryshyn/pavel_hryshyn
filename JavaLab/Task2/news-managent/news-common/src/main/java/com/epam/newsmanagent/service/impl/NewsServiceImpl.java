/*
 * @(#)NewsServiceImpl.java   1.0 2015/09/08
 */
package com.epam.newsmanagent.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagent.dao.ICommentDao;
import com.epam.newsmanagent.dao.INewsDao;
import com.epam.newsmanagent.entity.News;
import com.epam.newsmanagent.entity.SearchCriteria;
import com.epam.newsmanagent.exception.DaoException;
import com.epam.newsmanagent.exception.ServiceException;
import com.epam.newsmanagent.service.INewsService;

/**
 * This class implements methods for News service
 * 
 * @version 1.0 08 September 2015
 * @author Pavel_Hryshyn
 */
@Service("newsService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class NewsServiceImpl implements INewsService {
	
	@Autowired
	private INewsDao newsDao;

	@Autowired
	private ICommentDao commentDao;

	@Override
	public List<News> findBetweenPositions(int start, int end)
			throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.findBetweenPositions(start, end);
		} catch (DaoException e) {
			throw new ServiceException("findBetweenPositions() method of NewsService is failed", e);
		}
		return newsList;
	}

	@Override
	public List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc,
			int start, int end) throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			throw new ServiceException("findNewsWithCriteriaAndPositions() method of NewsService is failed", e);
		}
		return newsList;
	}

	@Override
	public News findById(Long newsId) throws ServiceException {
		News news = null;
		try {
			news = newsDao.findById(newsId);
		} catch (DaoException e) {
			throw new ServiceException("findById() method of NewsService is failed", e);
		}
		return news;
	}

	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria sc)
			throws ServiceException {
		long nextNewsId = 0;
		try {
			nextNewsId = newsDao.getNextNewsId(newsId, sc);
		} catch (DaoException e) {
			throw new ServiceException("getNextNewsId() method of NewsService is failed", e);
		}
		return nextNewsId;
	}

	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria sc)
			throws ServiceException {
		long previousNewsId = 0;
		try {
			previousNewsId = newsDao.getPreviousNewsId(newsId, sc);
		} catch (DaoException e) {
			throw new ServiceException("getPreviousNewsId() method of NewsService is failed", e);
		}
		return previousNewsId;
	}

	@Override
	public int countAllNews() throws ServiceException {
		int amount = 0;
		try {
			amount = newsDao.countAllNews();
		} catch (DaoException e) {
			throw new ServiceException("countAllNews() method of NewsService is failed", e);
		}
		return amount;
	}

	@Override
	public int countNewsWithCriteria(SearchCriteria searchCriteria)
			throws ServiceException {
		int amount = 0;
		try {
			amount = newsDao.countNewsWithCriteria(searchCriteria);
		} catch (DaoException e) {
			throw new ServiceException("countNewsWithCriteria() method of NewsService is failed", e);
		}
		return amount;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public Long addWithAuthorAndTags(News news, Long authorId, List<Long> tagsId)
			throws ServiceException {
		long newsId = 0;
		try {
			newsId = newsDao.create(news);
			newsDao.addNewsWithAuthor(newsId, authorId);
			if (tagsId != null) {
				newsDao.addNewsWithTags(newsId, tagsId);
			}
		} catch (DaoException e) {
			throw new ServiceException("addWithAuthorAndTags() method of NewsService is failed", e);
		}
		return newsId;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean updateWithAuthorAndTags(News news, Long authorId,
			List<Long> tagsId) throws ServiceException {
		boolean isUpdated = false;
		try {
			isUpdated = newsDao.update(news);
			if (isUpdated) {
				newsDao.deleteAuthorForNews(news.getId());
				newsDao.deleteTagsForNews(news.getId());
				newsDao.addNewsWithAuthor(news.getId(), authorId);
				if (tagsId != null) {
					newsDao.addNewsWithTags(news.getId(), tagsId);
				}
			}
		} catch (DaoException e) {
			throw new ServiceException("updateWithAuthorAndTags() method of NewsService is failed", e);
		}
		return isUpdated;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = DaoException.class)
	public boolean delete(Long newsId) throws ServiceException {
		boolean isDeleted = true;
		try {
			isDeleted &= newsDao.deleteAuthorForNews(newsId);
			isDeleted &= newsDao.deleteTagsForNews(newsId);
			isDeleted &= commentDao.deleteCommentsByNewsId(newsId);
			isDeleted &= newsDao.delete(newsId);
		} catch (DaoException e) {
			throw new ServiceException("delete() method of NewsService is failed", e);
		}
		return isDeleted;
	}

	public void setNewsDao(INewsDao newsDao) {
		this.newsDao = newsDao;
	}

	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}
}
