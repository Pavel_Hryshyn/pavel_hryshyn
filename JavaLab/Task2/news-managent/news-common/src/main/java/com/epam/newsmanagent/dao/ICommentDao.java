/*
 * @(#)ICommentDao.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.dao;

import java.util.List;

import com.epam.newsmanagent.entity.Comment;
import com.epam.newsmanagent.exception.DaoException;

/**
 * This interface defines methods that bind the Comment entity with database
 * @version 1.0 04 September 2015
 * @author Pavel Hryshyn
 */
public interface ICommentDao {
	/**
	 * This method returns list of comments for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching list of comments in database
	 * @return list of comments for some newsId from database
	 * @throws DaoException, if arise database access error or other database errors
	 */
	List<Comment> findByNewsId(Long newsId) throws DaoException;
	
	/**
	 * This method adds comment to database
	 * @param comment
	 * 			the param uses for adding comment to database
	 * @return comment's id
	 * @throws DaoException, if arise database access error or other database errors
	 * 				or comment doesn't save in database
	 */
	Long create(Comment comment) throws DaoException;
	
	/**
	 * This method get comments amount for news from database
	 * @param newsId
	 * @return comments amount
	 * @throws DaoException, if arise database access error or other database errors
	 */
	Integer countCommentsByNewsId(Long newsId) throws DaoException;
	
	/**
	 * This method delete comment with some id that stores in database
	 * @param id
	 * 			the param uses for deleting comment from database
	 * @return true, if comment are deleted, else return false
	 * @throws DaoException, if arise database access error or other database errors
	 */
	boolean delete(Long id) throws DaoException;
	
	/**
	 * This method delete all comments of some news that stores in database
	 * @param newsId
	 * 			the param is news id and uses for deleting list of comments from database
	 * @return true, if comments are deleted, else return false
	 * @throws DaoException, if arise database access error or other database errors
	 */
	boolean deleteCommentsByNewsId(Long newsId) throws DaoException;
}
