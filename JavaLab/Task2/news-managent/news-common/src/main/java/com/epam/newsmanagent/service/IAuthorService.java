/*
 * @(#)IAuthorService.java   1.0 2015/09/07
 */
package com.epam.newsmanagent.service;

import java.util.List;

import com.epam.newsmanagent.entity.Author;
import com.epam.newsmanagent.exception.ServiceException;

/**
 * This interface defines basic methods for Author Entity on service layer
 * @version 1.0 07 September 2015
 * @author Pavel_Hryshyn
 */
public interface IAuthorService {
	/**
	 * This method returns a list of all authors that stores in database
	 * @return list of all authors from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<Author> findAll() throws ServiceException;
	
	/**
	 * This method returns a list of all non-expired authors that stores in database
	 * @return  a list of all non-expired authors from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<Author> findAllNonExpiredAuthors() throws ServiceException;
	
	/**
	 * This method returns author with some id that stores in database
	 * @param authorId
	 * 			the param uses for searching author in database
	 * @return author with some id from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	Author findById(Long authorId) throws ServiceException;
	
	/**
	 * This method returns author for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching author in database
	 * @return author for some newsId from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	Author findByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * This method adds author to database
	 * @param author
	 * 			the param uses for adding author to database
	 * @return author's id
	 * @throws ServiceException, if arise database access error or other database errors
	 * 				or author doesn't save in database
	 */
	Long create(Author author) throws ServiceException;
	
	/**
	 * This method updates author to database
	 * @param author
	 *            the param uses for updating author to database
	 * @return true if author is updated to database, else false
	 * @throws ServiceException,
	 *             if arise database access error or other database errors
	 */
	boolean update(Author author) throws ServiceException;
	
	/**
	 * This method sets a author expired 
	 * @param authorId
	 * 			the param uses for expired author
	 * @return true if a author is expired, else false
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	boolean expire(Long authorId) throws ServiceException;
}
