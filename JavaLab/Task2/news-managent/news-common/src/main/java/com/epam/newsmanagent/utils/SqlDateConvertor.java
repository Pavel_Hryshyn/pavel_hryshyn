/*
 * @(#)ConvertToSQLDate.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.utils;

import java.util.Date;

/**
 * This class contains method to convert java.util.Date to java.sql.Date
 * @version 1.0 04 September 2015
 * @author Pavel_Hryshyn
 */
public class SqlDateConvertor {
	/**
	 * This method converts java.util.Date to java.sql.Date
	 * @param date
	 * 			the param is java.util.Date
	 * @return java.sql.Date
	 */
	public static java.sql.Date convertToSqlDate(Date date){
		java.sql.Date sqlDate = null;
		if (date != null){
			sqlDate = new java.sql.Date(date.getTime());
		}
		return sqlDate;
	}
	
	/**
	 * This method converts java.util.Date to java.sql.Timestamp
	 * @param date
	 * 			the param is java.util.Date
	 * @return java.sql.Timestamp
	 */
	public static java.sql.Timestamp convertToSqlTimestamp(Date date){
		java.sql.Timestamp sqlTimeStamp = null;
		if (date != null){
			sqlTimeStamp = new java.sql.Timestamp(date.getTime());
		}
		return sqlTimeStamp;
	}
}
