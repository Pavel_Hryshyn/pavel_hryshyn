/*
 * @(#)AuthorDaoImpl.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagent.dao.IAuthorDao;
import com.epam.newsmanagent.entity.Author;
import com.epam.newsmanagent.exception.DaoException;
import com.epam.newsmanagent.utils.DbConnectionManager;
import com.epam.newsmanagent.utils.SqlDateConvertor;


/**
 * This class implements DAO pattern and contains methods that bind the Author
 * entity with database
 * 
 * @version 1.0 04 September 2015
 * @author Pavel Hryshyn
 */
@Repository("authorDao")
public class AuthorDaoImpl implements IAuthorDao {
	/* This constants stores SQL queries for database */
	private static final String SQL_AUTHOR_ADD = "INSERT INTO author (AUTHOR_ID_PK, AUTHOR_NAME) VALUES (author_sequence.nextval, ?)";
	private static final String SQL_AUTHOR_EXPIRE = "UPDATE author SET author_expired = ?  WHERE author_id_pk = ?";
	private static final String SQL_AUTHOR_UPDATE = "UPDATE author SET author_name = ?  WHERE author_id_pk = ?";
	private static final String SQL_FIND_ALL = "SELECT * FROM author ORDER BY author_id_pk ASC";
	private static final String SQL_FIND_ALL_NON_EXPIRED = "SELECT * FROM author WHERE author_expired IS NULL ORDER BY author_id_pk ASC";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM author WHERE author_id_pk = ?";
	private static final String SQL_FIND_BY_NEWS_ID = "SELECT * FROM author JOIN news_author ON author.author_id_pk=news_author.author_id_fk WHERE news_author.news_id_fk = ?";

	/* This constants stores names of database column */
	private static final String AUTHOR_ID = "author_id_pk";
	private static final String AUTHOR_NAME = "author_name";
	private static final String AUTHOR_EXPIRED = "author_expired";

	/* This object is factory for connection */
	@Autowired
	private DataSource dataSource;

	@Override
	public List<Author> findAll() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Author> authorList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL);
			resultSet = stmt.executeQuery();
			authorList = buildAuthorList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAll() request of AuthorDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return authorList;
	}

	@Override
	public List<Author> findAllNonExpiredAuthors() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Author> authorList = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL_NON_EXPIRED);
			resultSet = stmt.executeQuery();
			authorList = buildAuthorList(resultSet);
		} catch (SQLException e) {
			throw new DaoException(
					"findAllNonExpiredAuthors() request of AuthorDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return authorList;
	}

	@Override
	public Author findById(Long authorId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		Author author = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_ID);
			stmt.setLong(1, authorId);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				author = buildAuthor(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException("findById() request of AuthorDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return author;
	}

	@Override
	public Author findByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		Author author = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_NEWS_ID);
			stmt.setLong(1, newsId);
			resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				author = buildAuthor(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException("findByNewsId() request of AuthorDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return author;
	}

	@Override
	public Long create(Author author) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long authorId = 0;
		String generatedColumns[] = { AUTHOR_ID };
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_AUTHOR_ADD,	generatedColumns);
			stmt.setString(1, author.getAuthorName());
			stmt.executeUpdate();
			resultSet = stmt.getGeneratedKeys();
			if (resultSet.next()) {
				authorId = resultSet.getLong(1);
			} else {
				throw new DaoException("Author is not created");
			}
		} catch (SQLException e) {
			throw new DaoException("create() request of AuthorDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return authorId;
	}

	@Override
	public boolean update(Author author) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isUpdated = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_AUTHOR_UPDATE);
			stmt.setString(1, author.getAuthorName());
			stmt.setLong(2, author.getId());
			row = stmt.executeUpdate();
			if (row != 0) {
				isUpdated = true;
			}
		} catch (SQLException e) {
			throw new DaoException("update() request of AuthorDao is failed", e);
		} finally {
			DbConnectionManager.close(stmt);
			DbConnectionManager.close(con, dataSource);
		}
		return isUpdated;
	}

	@Override
	public boolean expire(Long authorId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isExpired = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_AUTHOR_EXPIRE);
			stmt.setTimestamp(1,
					SqlDateConvertor.convertToSqlTimestamp(new Date()));
			stmt.setLong(2, authorId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isExpired = true;
			}
		} catch (SQLException e) {
			throw new DaoException("expire() request of AuthorDao is failed", e);
		} finally {
			DbConnectionManager.close(stmt);
			DbConnectionManager.close(con, dataSource);
		}
		return isExpired;
	}

	private Author buildAuthor(ResultSet rs) throws SQLException {
		/*
		 * This method builds Author object from ResultSet object
		 */
		Author author = new Author();
		author.setId(rs.getLong(AUTHOR_ID));
		author.setAuthorName(rs.getString(AUTHOR_NAME));
		author.setExpiredDate(rs.getTimestamp(AUTHOR_EXPIRED));
		return author;
	}

	private List<Author> buildAuthorList(ResultSet rs) throws SQLException {
		/*
		 * This method builds list of Author objects from ResultSet object
		 */
		List<Author> authors = new ArrayList<Author>();
		while (rs.next()) {
			authors.add(buildAuthor(rs));
		}
		return authors;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
