/*
 * @(#)DaoException.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.exception;

/**
 * This class is wrapper for all DAO level exceptions  
 * @version 1.0 04 September 2015
 * @author Pavel_Hryshyn
 */
public class DaoException extends Exception {
	private static final long serialVersionUID = 1L;

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}
}
