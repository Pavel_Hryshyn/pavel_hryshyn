/*
 * @(#)ServiceException.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.exception;

/**
 * This class is wrapper for all Service level exceptions  
 * @version 1.0 04 September 2015
 * @author Pavel_Hryshyn
 */
public class ServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(String message) {
		super(message);
	}	
}
