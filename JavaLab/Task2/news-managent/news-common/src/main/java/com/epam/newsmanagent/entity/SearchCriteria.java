/*
 * @(#)User.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.entity;

import java.io.Serializable;
import java.util.List;

/**
 * This class uses for setting of search criteria
 * @version 1.0 04 September 2015
 * @author Pavel_Hryshyn
 */
public class SearchCriteria implements Serializable{
	private static final long serialVersionUID = 1L;

	/* This value stores Author object */
	private Author author;
	
	/* This value stores list of tags */
	private List<Tag> tags;

	public SearchCriteria() {
		super();
	}

	public SearchCriteria(Author author, List<Tag> tags) {
		super();
		this.author = author;
		this.tags = tags;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SeacrchCriteria [author=" + author + ", tags=" + tags + "]";
	}
}
