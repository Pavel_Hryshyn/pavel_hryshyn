/*
 * @(#)Role.java   1.0 2015/09/04
 */
package com.epam.newsmanagent.entity;

import java.io.Serializable;

/**
 * This class is entity that defines the parameters of Role
 * @version 1.0 04 September 2015
 * @author Pavel_Hryshyn
 */
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	/* This value stores user */
	private User user;
	
	/* This value stores roles enum */
	private RolesEnum role;

	public Role() {
		super();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public RolesEnum getRole() {
		return role;
	}

	public void setRole(RolesEnum role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (role != other.role)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Role [user=" + user + ", role=" + role + "]";
	}
}
