/*
 * @(#)ServiceManagerImpl.java   1.0 2015/09/08
 */
package com.epam.newsmanagent.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagent.entity.News;
import com.epam.newsmanagent.entity.SearchCriteria;
import com.epam.newsmanagent.entity.vo.NewsComplex;
import com.epam.newsmanagent.exception.ServiceException;
import com.epam.newsmanagent.service.IAuthorService;
import com.epam.newsmanagent.service.ICommentService;
import com.epam.newsmanagent.service.INewsService;
import com.epam.newsmanagent.service.IServiceManager;
import com.epam.newsmanagent.service.ITagService;

/**
 * This class is manager for all service classes
 * @version 1.0 08 September 2015
 * @author Pavel_Hryshyn
 */
@Component("serviceManager")
public class ServiceManagerImpl implements IServiceManager {

	@Autowired
	private IAuthorService authorService;
	
	@Autowired
	private ICommentService commentService;
	
	@Autowired
	private INewsService newsService;
	
	@Autowired
	private ITagService tagService;
	
	@Override
	public NewsComplex findByNewsId(Long newsId) throws ServiceException {
		NewsComplex newsComplex = null;
		News news = newsService.findById(newsId);
		if (news != null)	{
			newsComplex = new NewsComplex();
			newsComplex.setNews(news);
			newsComplex.setAuthor(authorService.findByNewsId(newsId));
			newsComplex.setTags(tagService.findByNewsId(newsId));
			newsComplex.setComments(commentService.findByNewsId(newsId));
		}
		return newsComplex;
	}
		
	@Override
	public List<NewsComplex> findNewsComplexWithCriteriaAndPositions(
			SearchCriteria sc, int start, int end) throws ServiceException {
		List<NewsComplex> newsComplexList = new ArrayList<NewsComplex>();
		List<News> newsList = newsService.findNewsWithCriteriaAndPositions(sc, start, end);
		for (News news: newsList){
			NewsComplex newsComplex = findByNewsId(news.getId());
			newsComplexList.add(newsComplex);
		}
		return newsComplexList;
	}

	@Override
	public IAuthorService getAuthorService() {
		return authorService;
	}

	@Override
	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	@Override
	public ICommentService getCommentService() {
		return commentService;
	}

	@Override
	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	@Override
	public INewsService getNewsService() {
		return newsService;
	}

	@Override
	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	@Override
	public ITagService getTagService() {
		return tagService;
	}

	@Override
	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}
}
