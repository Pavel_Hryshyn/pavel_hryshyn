/*
 * @(#)TagDaoImpl.java   1.0 2015/09/08
 */
package com.epam.newsmanagent.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagent.dao.ITagDao;
import com.epam.newsmanagent.entity.Tag;
import com.epam.newsmanagent.exception.DaoException;
import com.epam.newsmanagent.utils.DbConnectionManager;

@Repository("tagDao")
public class TagDaoImpl implements ITagDao {
	/* This constants stores SQL queries for database */
	private static final String SQL_FIND_ALL = "SELECT * FROM tag ORDER BY tag_id_pk ASC";
	private static final String SQL_FIND_BY_NEWS_ID = "SELECT tag.tag_id_pk, tag.tag_name FROM tag JOIN news_tag ON tag.tag_id_pk=news_tag.tag_id_fk WHERE news_tag.news_id_fk=?";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM tag WHERE tag.tag_id_pk = ?";
	private static final String SQL_TAG_ADD = "INSERT INTO tag (tag_id_pk, tag_name) VALUES (tag_sequence.nextval, ?)";
	private static final String SQL_TAG_DELETE = "DELETE FROM tag WHERE tag_id_pk=?";
	private static final String SQL_TAG_DELETE_FROM_NEWS = "DELETE FROM news_tag WHERE tag_id_fk=?";
	private static final String SQL_TAG_UPDATE = "UPDATE tag SET tag_name = ? WHERE tag_id_pk = ?";
	
	/* This constants stores names of database column */
	private static final String TAG_ID = "tag_id_pk";
	private static final String TAG_NAME = "tag_name";
	
	/* This object is factory for connection */
	@Autowired
	private DataSource dataSource;
	
	@Override
	public List<Tag> findAll() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Tag> tags = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_ALL);
			resultSet = stmt.executeQuery();
			tags = buildTagList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findAll() request of TagDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return tags;
	}
	
	

	@Override
	public List<Tag> findByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<Tag> tags = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_NEWS_ID);
			stmt.setLong(1, newsId);
			resultSet = stmt.executeQuery();
			tags = buildTagList(resultSet);
		} catch (SQLException e) {
			throw new DaoException("findByNewsId() request of TagDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return tags;
	}

	@Override
	public Tag findById(Long tagId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		Tag tag = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_FIND_BY_ID);
			stmt.setLong(1, tagId);
			resultSet = stmt.executeQuery();
			if (resultSet.next()){
				tag = buildTag(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException("findById() request of TagDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return tag;
	}


	@Override
	public Long create(Tag tag) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		long tagId = 0;
		String generatedColumns[] = {TAG_ID};
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_TAG_ADD, generatedColumns);
			stmt.setString(1, tag.getTagName());
			stmt.executeUpdate();
			resultSet = stmt.getGeneratedKeys();
			if (resultSet.next()) {
				tagId = resultSet.getLong(1);
			} else {
				throw new DaoException("Tag is not created");
			}
		} catch (SQLException e) {
			throw new DaoException("create() request of TagDao is failed", e);
		} finally {
			DbConnectionManager.close(con, stmt, resultSet, dataSource);
		}
		return tagId;
	}



	@Override
	public boolean update(Tag tag) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isUpdated = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_TAG_UPDATE);
			stmt.setString(1, tag.getTagName());
			stmt.setLong(2, tag.getId());
			row = stmt.executeUpdate();
			if (row != 0) {
				isUpdated = true;
			}
		} catch (SQLException e) {
			throw new DaoException("update() request of TagDao is failed", e);
		} finally {
			DbConnectionManager.close(stmt);
			DbConnectionManager.close(con, dataSource);
		}
		return isUpdated;
	}

	@Override
	public boolean delete(Long tagId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_TAG_DELETE);
			stmt.setLong(1, tagId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("delete() request of TagDao is failed", e);
		} finally {
			DbConnectionManager.close(stmt);
			DbConnectionManager.close(con, dataSource);
		}
		return isDeleted;
	}
	
	@Override
	public boolean deleteTagFromNews(Long tagId) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		boolean isDeleted = false;
		int row = 0;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			stmt = con.prepareStatement(SQL_TAG_DELETE_FROM_NEWS);
			stmt.setLong(1, tagId);
			row = stmt.executeUpdate();
			if (row != 0) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			throw new DaoException("deleteTagFromNews() request of TagDao is failed", e);
		} finally {
			DbConnectionManager.close(stmt);
			DbConnectionManager.close(con, dataSource);
		}
		return isDeleted;
	}

	private Tag buildTag(ResultSet rs) throws SQLException{
		/*
		 * This method builds Tag object from ResultSet object
		 */
		Tag tag = new Tag();
		tag.setId(rs.getLong(TAG_ID));
		tag.setTagName(rs.getString(TAG_NAME));
		return tag;
	}

	private List<Tag> buildTagList(ResultSet rs) throws SQLException{
		/*
		 * This method builds list of Tag objects from ResultSet object
		 */
		List<Tag> tags = new ArrayList<Tag>();
		while (rs.next()){
			tags.add(buildTag(rs));
		}
		return tags;
	}
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
