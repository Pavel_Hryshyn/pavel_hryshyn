<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="language">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	
	<div class="header-main">		
				News portal - Administration
	</div>		
				
				<sec:authorize access="isAuthenticated()">
					<c:url var="logoutUrl" value="/logout" />
					<form class="logout" action="${logoutUrl}" method="post">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="form-group">
							<spring:message code="header.msg.hello" text="Hello, " />
							<sec:authentication property="principal.username" />
							<input type="submit" class="btn btn-warning btn-sm"
								value="Log out" />
						</div>
					</form>
				</sec:authorize>
	
		<div class="lang">
					<a class="btn btn-default btn-sm" href="?language=en">EN</a> <a
						class="btn btn-default btn-sm" href="?language=ru">RU</a>
		</div>		
			
		
	
</body>
</html>