<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="language">
<head>
<meta charset="utf-8">


<title></title>
</head>
<body>
	<div class = "row">
		<div class="btn-back">
	<spring:url value="/newslist" var="newsListViewUrl" />
	<a class="btn btn-link" href="${newsListViewUrl}" ><spring:message
			code="body.news.view.home" text="Home" /></a>
			</div>
	</div>
	
	<div class="news-block-single">
				<div class="row">
					<div class = "news-title">
						<b><c:out value="${newsComplex.news.title}" /></b>
					</div>
				
					<div class="news-author">
						<em><c:out value="by (${newsComplex.author.authorName})" /></em>
					</div>
					<div class="news-date">
						<fmt:formatDate pattern="dd/MM/yyyy"
							value="${newsComplex.news.modificationDate}" />
					</div>
				</div>
				<br>
				<div class="news-short-text">
					<em><c:out value="${newsComplex.news.shortText}" /></em>
				</div>
				<div class="news-full-text">
						<c:out value="${newsComplex.news.fullText}" />
				</div>

			<div class="comment-block">
				<c:forEach items="${newsComplex.comments}" var="comment">
					<div class="comment">
						<div class="comment-date">
							<fmt:formatDate pattern="dd/MM/yyyy"
								value="${comment.creationDate}" />
						</div>
					
					
						<div class="comment-text">
							<div align="right">
								<spring:url value="/comment/delete" var="commentDeleteUrl" />
								<form:form method="POST" action="${commentDeleteUrl}">
									<input type="hidden" name="commentId" value="${comment.id}">
									<input type="hidden" name="newsId" value="${newsComplex.news.id}">
									<input class="btn btn-warning" type="submit" value="x" />
								</form:form>
							</div>
							<div>
								<p class="text-justify">
									<c:out value="${comment.commentText}" />
								</p>
							</div>
						</div>
					
					</div>
				</c:forEach>
				
				<spring:message code="Size.comment.commentText"
									text="Comment must be between 5 and 100 characters" var="badComment"/>
						<spring:url value="/comment/add" var="commentAddUrl" />
						<form:form class="create-comment-block" method="POST"
							action="${commentAddUrl}" modelAttribute="comment" onsubmit="return validate_comment('${badComment}');">
							<div class="bad-message">
								<form:errors path="commentText" />
							</div>
							<div>
								<input type="hidden" name="newsId" value="${newsComplex.news.id}" />
								<form:textarea class="create-comment"  rows="5" path="commentText" />
							</div>
							<div align="right">
									<input class="btn btn-primary" type="submit"
										value="<spring:message code="button.news.view.comment.post"
									text="Post comment" />" />
							</div>							
						</form:form>
						</div>
					</div>
			

			<div class="navigation-block">
				<div class="previous">
					<c:if test="${previousNews ne 0}">
						<spring:url value="/news/${previousNews}"
							var="previousNewsViewUrl" />
						<a class="btn btn-default" href="${previousNewsViewUrl}"
							role="button"><spring:message
								code="button.news.view.previous" text="PREVIOUS" /></a>
					</c:if>
				</div>

				<div class="next">
					<c:if test="${nextNews ne 0}">
						<spring:url value="/news/${nextNews}" var="newsNewsViewUrl" />
						<a class="btn btn-default" href="${newsNewsViewUrl}" role="button"><spring:message
								code="button.news.view.next" text="NEXT" /></a>
					</c:if>
				</div>
			</div>
	
</body>
</html>