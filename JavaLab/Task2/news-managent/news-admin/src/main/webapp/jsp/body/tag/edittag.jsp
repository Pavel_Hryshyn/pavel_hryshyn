<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>


	<div class="edit-container">
		<c:forEach items="${tags}" var="tag">
			<div class="area-edit">
					<spring:url value="/tags/update" var="tagUpdateUrl" />
					
					<form:form id="updateForm${tag.id}"
						modelAttribute="tagForUpdating" action="${tagUpdateUrl}"
						method="POST" class="edit-form">
						<c:if test="${errorTagId eq tag.id}">
							<div class="bad-message">
								<form:errors path="tagName"
										cssStyle="error" />
							</div>
						</c:if>
						<label class="edit-label" for="tagName"><spring:message
									code="body.tag.edit.msg.tag.name" text="Tag: " /></label> <input
								class="edit-input-short-1" id="text${tag.id}" name="tagName"
								value='<c:out value="${tag.tagName}"/>' disabled />
							<form:hidden path="id" value="${tag.id}" />
						
						<div class="btn-group">
							<div id="update${tag.id}" hidden="true">
								<input class="btn btn-primary" type="submit"
									value="<spring:message code="button.tag.update" text="Update" />" />
							</div>
							<a id="edit${tag.id}" class="btn btn-default"
								onclick="switchEnableInput(${tag.id})"><spring:message
									code="button.tag.edit" text="Edit" /></a>
						</div>
					</form:form>
					
					<div class="btn-group" >
					<div id="delete${tag.id}" hidden="true">
						<spring:url value="/tags/delete/${tag.id}" var="tagDeleteUrl" />
						<form action="${tagDeleteUrl}" method="POST" >
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> <input class="btn btn-warning"
								type="submit"
								value="<spring:message code="button.tag.delete" text="Delete" />" />
						</form>
					</div>
					</div>
				
				<div class="btn-group" >
					<div id="cancel${tag.id}" hidden="true">
						<a class="btn btn-default" onclick="switchDisableInput(${tag.id})"
							hidden="true"><spring:message code="button.tag.cancel"
								text="Cancel" /></a><br>
					</div>
				</div>
				
			</div>
		</c:forEach>
		</div>
		
		<div class="save-container">
			<spring:message code="Size.tagForSaving.tagName"
									text="Tag must be between 1 and 30 characters" var="badTag"/>
			<spring:url value="/tags/save" var="tagSaveUrl" />
			<form:form method="POST"
				action="${tagSaveUrl}" modelAttribute="tagForSaving" onsubmit="return validate_tag('${badTag}');">
				<div class="bad-message" align="center">
					<form:errors path="tagName" />
				</div>
					<label class="edit-label-middle"
						for="tagName"><spring:message
							code="body.tag.edit.msg.tag.add" text="Add Tag: "></spring:message></label>	
						<form:input path="tagName" class="edit-input-short-1" id="tagName" />
					<input class="btn btn-primary" type="submit"
						value="<spring:message code="button.tag.save" text="Save" />" />	
			</form:form>
		</div>
	
</body>
</html>