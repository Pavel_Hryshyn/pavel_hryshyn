<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<div class="menu-content">
		<div class="menu-link">
			<spring:url value="/newslist" var="newsListViewUrl" />
			<a class="btn btn-link" href="${newsListViewUrl}"> <span
				class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> <spring:message
					code="menu.news.list" text="News list" /></a>
		</div>
		<div class="menu-link">
			<spring:url value="/news/new" var="newsAddViewUrl" />
			<a class="btn btn-link" href="${newsAddViewUrl}"> <span
				class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> <spring:message
					code="menu.news.add" text="Add News" /></a>
		</div>
		<div class="menu-link">
			<spring:url value="/authors" var="authorEditViewUrl" />
			<a class="btn btn-link" href="${authorEditViewUrl}"> <span
				class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> <spring:message
					code="menu.authors.add" /></a>
		</div>
		<div class="menu-link">
			<spring:url value="/tags" var="tagEditViewUrl" />
			<a class="btn btn-link" href="${tagEditViewUrl}"> <span
				class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> <spring:message
					code="menu.tags.add" /></a>
		</div>
	</div>
</body>
</html>