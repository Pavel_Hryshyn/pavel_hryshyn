<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>
	<spring:message code="Size.news.title" text="Title must be between 1 and 30 characters" var="badTitle" />
	<spring:message code="Size.news.shortText" text="Brief must be between 1 and 100 characters" var="badShort" />
	<spring:message code="Size.news.fullText" text="News text must be between 1 and 2000 characters" var="badFull" />
	<spring:message code="DateTimeFormat.news.creationDate" text="Date must have dd/MM/yyyy format" var="badDate" />
	<spring:url value="/news/edit/${news.id}" var="newsEditUrl" />
	
	<form:form modelAttribute="news" action="${newsEditUrl}" method="POST"
		onsubmit="return validate_news('${badTitle}', '${badShort}', '${badFull}', '${badDate}');">
		<form:input path="id" value="${news.id}" type="hidden"/>
		<div class="bad-message">
			<form:errors path="title" />
		</div>
		<div class="row-edit">
			<label class="edit-news-label" for="news_title"> 
				<spring:message code="body.news.edit.msg.title" text="Title: " />
			</label>
			<form:input class="edit-input" path="title" id="news_title" value='${news.title}' />
		</div>
		<c:if test="${creationDate}">
			<div class="bad-message">
				<spring:message code="DateTimeFormat.news.creationDate" text="Date must have dd/MM/yyyy format " />
			</div>
		</c:if>
		<div class="row-edit">
			<label class="edit-news-label" for="news_date">
				<spring:message code="body.news.edit.msg.date" text="Date: " />
			</label>
			<fmt:formatDate value="${news.creationDate}" pattern="dd/MM/yyyy" var="formattedDate" />
			<c:if test="${news.id eq 0}">
				<form:input class="edit-input" type="text" path="creationDate" id="news_date" value='${formattedDate}' />
			</c:if>
			<c:if test="${news.id ne 0}">
				<form:input class="edit-input" type="text" path="creationDate" id="news_date" value="${formattedDate}" readonly="true" />
			</c:if>
		</div>
		<div class="bad-message">
			<form:errors path="shortText" />
		</div>
		<div class="news-short-text">
			<label class="edit-news-label" for="news_short_text">
				<spring:message	code="body.news.edit.msg.brief" text="Brief: " />
			</label>
			<form:textarea class="edit-input-short" path="shortText" id="news_short_text" value="${news.shortText}" rows="3" />
		</div>
		<div class="bad-message">
			<form:errors path="fullText" />
		</div>
		<div class="news-full-text">
			<label class="edit-news-label" for="news_full_text">
				<spring:message	code="body.news.edit.msg.content" text="Content: " />
			</label>
			<form:textarea class="edit-input-full" path="fullText" id="news_full_text" value="${news.fullText}" rows="5" />
		</div>
		<div class="row">
			<div class="filter-form">
				<select class="select select-middle" name="authorIdForNews">
					<c:forEach items="${authors}" var="author">
						<c:if test="${selectedAuthorId eq author.id}">
							<option value="${author.id}" selected>
								<c:out value="${author.authorName}" />
						</option>
						</c:if>
						<c:if test="${selectedAuthorId ne author.id}">
							<option value="${author.id}">
								<c:out value="${author.authorName}" />
							</option>
						</c:if>
					</c:forEach>
				</select> 
				<select id="multi-dropbox" multiple="multiple"
					class="select select-middle" name="tagsIdForNews">
					<c:forEach items="${tags}" var="tag">
						<c:choose>
							<c:when test="${not empty selectedTagsId[tag.id]}">
								<option value="${tag.id}" selected>
									<c:out value="${tag.tagName}" />
								</option>
							</c:when>
							<c:otherwise>
								<option value="${tag.id}">
									<c:out value="${tag.tagName}" />
								</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="save-news-btn">
			<input class="btn btn-info" type="submit"
				value="<spring:message code="button.news.edit.save" text="Save" />" />
		</div>
	</form:form>
	
	<script type="text/javascript">
		$("#multi-dropbox").dropdownchecklist(
			{
				emptyText : "<spring:message	code="body.newslist.choose.tags" text="Choose tags" />",
				width : 180
			});
	</script>
</body>
</html>