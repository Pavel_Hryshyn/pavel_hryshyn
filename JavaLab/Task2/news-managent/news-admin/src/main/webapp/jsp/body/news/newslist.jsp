<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="language">
<head>
<title></title>
<meta charset="utf-8">
</head>
<body>
	<spring:message code="Filter.empty" text="Please, choose filter parameters" var="badFilter" />
	
	<div class="row">
		<form:form class="filter-form" method="POST" action="filter" name="filter"
					onsubmit="return validate_filter('${badFilter}');">
			<select class="select select-middle" name="authorIdFromFilter">
				<option value="-1">
					<spring:message code="body.newslist.choose.author" text="Choose authors" />
				</option>
				<c:forEach items="${authors}" var="author">
					<c:if test="${searchCriteria.author.id eq author.id}">
						<option selected value="${author.id}">
							<c:out value="${author.authorName}" />
						</option>
					</c:if>
					<c:if test="${searchCriteria.author.id ne author.id}">
						<option value="${author.id}">
							<c:out value="${author.authorName}" />
						</option>
					</c:if>
				</c:forEach>
			</select>
			<select id="multi-dropbox" multiple="multiple"
				name="tagsIdFromFilter" class="select select-middle">
				<c:forEach items="${tags}" var="tag">
					<c:choose>
						<c:when test="${not empty selectedTagsId[tag.id]}">
							<option value="${tag.id}" selected>
								<c:out value="${tag.tagName}" />
							</option>
						</c:when>
						<c:otherwise>
							<option value="${tag.id}">
								<c:out value="${tag.tagName}" />
							</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			

			<script type="text/javascript">
				$("#multi-dropbox")
						.dropdownchecklist(
								{
									emptyText : "<spring:message	code="body.newslist.choose.tags" text="Choose tags" />",
									width : 180
								});
			</script>

			<input class="btn btn-info" type="submit"
				value="<spring:message	code="button.newslist.search" text="Search" />" />
		</form:form>
		
		<form:form method="POST" action="reset" class="reset-form">
			<input class="btn btn-warning" type="submit"
				value="<spring:message	code="button.newslist.reset" text="Reset" />" />
		</form:form>
	</div>

	<c:if test="${emptyNewsList eq true}">
		<div class="alert alert-info" role="alert">
			<spring:message code="body.newslist.msg.empty"
				text="News list is empty" />
		</div>
	</c:if>

	<c:if test="${emptyNewsList ne true}">
		<form:form action="delete" method="POST">
			<c:forEach items="${newsComplexList}" var="newsComplex">
				<div class="news-block">
					<div class="row">
						<div class="news-title">
							<spring:url value="/news/${newsComplex.news.id}" var="newsViewUrl" />
							<a href="${newsViewUrl}"><c:out value="${newsComplex.news.title}" /></a>
						</div>
						<div class="news-author">
							<em>
								<c:out value="(by ${newsComplex.author.authorName})" />
							</em>
						</div>
						<div class="news-date">
							<ins>
								<c:out value="${newsComplex.news.modificationDate}" />
							</ins>
						</div>
					</div>
					<div class="news-short-text">
						<c:out value="${newsComplex.news.shortText}" />
					</div>
					<br>
					<div class="row">
						<div class="news-tags">
							<p>
								<small> 
									<c:forEach items="${newsComplex.tags}" var="tag">
										<mark>
											<c:out value="${tag.tagName}, " />
										</mark>
									</c:forEach>
								</small>
							</p>
						</div>
						<div class="news-comment-amount">
							<p>
								<em>
									<spring:message code="body.newslist.msg.comments" text="Comments" /> 
									<c:out value="(${fn:length(newsComplex.comments)})" />
								</em>
							</p>
						</div>
						<div class="news-checkbox">
							<input type="checkbox" value="${newsComplex.news.id}" name="newsIdForDeleting">
						</div>
					</div>
					<div class="row-edit" align="right">
						<spring:url value="/news/edit/${newsComplex.news.id}" var="newsEditUrl" />
						<a class="btn" href="${newsEditUrl}">
							<spring:message	code="body.newslist.msg.edit" text="Edit" />
						</a>
					</div>
				</div>
			</c:forEach>
			<div class="row" align="right">
				<div class="news-delete-btn">
					<input class="btn btn-warning" type="submit"
						value="<spring:message code="button.newslist.delete" text="Delete" />" />
				</div>
			</div>
		</form:form>
	</c:if>

	<div class="pagination">
		<c:forEach var="page" begin="1" end="${pageNumber}" step="1">
			<c:choose>
				<c:when test="${currentPage eq page}">
					<div class="page active">
						<form action="newslist" method="GET">
							<input type="hidden" name="page" value="${page}"> 
							<input class="btn-page active" type="submit" value="${page}" />
						</form>
					</div>
				</c:when>
				<c:otherwise>
					<div class="page">
						<form action="newslist" method="GET">
							<input type="hidden" name="page" value="${page}"> 
							<input class="btn-page" type="submit" value="${page}" />
						</form>
					</div>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</div>
</body>
</html>