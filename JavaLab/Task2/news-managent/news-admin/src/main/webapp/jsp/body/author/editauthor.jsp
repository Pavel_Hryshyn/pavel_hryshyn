<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>

	<div class="edit-container">
		<c:forEach items="${authors}" var="author">
			<div class="area-edit">
				<spring:url value="/authors/update" var="authorUpdateUrl" />
				<form:form class="edit-form" id="updateForm${author.id}"
					modelAttribute="authorForUpdating" action="${authorUpdateUrl}"
					method="POST">
					<c:if test="${errorAuthorId eq author.id}">
						<div class="bad-message">
							<form:errors path="authorName" cssStyle="error" />
						</div>
					</c:if>
					<label class="edit-label" for="authorName">
						<spring:message code="body.author.edit.msg.author.name" text="Author: " />
					</label>
					<input class="edit-input-short-1" id="text${author.id}"
						name="authorName" value='<c:out value="${author.authorName}"/>'
						disabled />
					<form:hidden path="id" value="${author.id}" />

					<div class="btn-group">
						<div id="update${author.id}" hidden="true">
							<input class="btn btn-primary" type="submit"
								value="<spring:message code="button.author.update" text="Update" />" />
						</div>
						<a id="edit${author.id}" class="btn btn-default" onclick="switchEnableInput(${author.id})">
							<spring:message code="button.author.edit" text="Edit" />
						</a>
					</div>
				</form:form>

				<div class="btn-group">
					<div id="delete${author.id}" hidden="true">
						<spring:url value="authors/expire/${author.id}" var="authorExpireUrl" />
						<form action="${authorExpireUrl}" method="POST">
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
							<input class="btn btn-warning" type="submit"
								value="<spring:message code="button.author.expire" text="Expire" />" />
						</form>
					</div>
				</div>

				<div class="btn-group">
					<div id="cancel${author.id}" hidden="true">
						<a class="btn btn-default" onclick="switchDisableInput(${author.id})" hidden="true">
							<spring:message code="button.author.cancel" text="Cancel" />
						</a>
					</div>
				</div>
			</div>
		</c:forEach>

		<div class="save-container">
			<spring:message code="Size.authorForSaving.authorName" text="Author must be between 1 and 30 characters" var="badAuthor" />
			<spring:url value="/authors/save" var="authorSaveUrl" />
			<form:form class="form-horizontal" method="POST"
				action="${authorSaveUrl}" modelAttribute="authorForSaving"
				onsubmit="return validate_author('${badAuthor}');">
				<div class="bad-message" align="center">
					<form:errors path="authorName" />
				</div>
				<label class="edit-label-middle" for="authorName">
					<spring:message	code="body.author.edit.msg.author.add" text="Add Author: "></spring:message>
				</label>
				<form:input path="authorName" class="edit-input-short-1" id="author" />
				<input class="btn btn-primary" type="submit"
					value="<spring:message code="button.author.save" text="Save" />" />
			</form:form>
		</div>
	</div>
</body>
</html>