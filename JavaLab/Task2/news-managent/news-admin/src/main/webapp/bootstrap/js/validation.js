function validate_news(badTitle, badShort, badFull, badDate) {
	if (document.getElementById("news_title").value.length < 1
			|| document.getElementById("news_title").value.length > 30) {
		alert(badTitle);
		return false;
	}
	// regular expression to match required date format
	re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
	if (document.getElementById("news_date").value.length <1 && !document.getElementById("news_date").value.match(re)) {
		alert(badDate);
		return false;
	}
	if (document.getElementById("news_short_text").value.length < 1
			|| document.getElementById("news_short_text").value.length > 100) {
		alert(badShort);
		return false;
	}
	if (document.getElementById("news_full_text").value.length < 1
			|| document.getElementById("news_full_text").value.length > 2000) {
		alert(badFull);
		return false;
	}
	return true;
}

function validate_author(badAuthor) {
	if (document.getElementById("author").value.length < 1
			|| document.getElementById("author").value.length > 30) {
		alert(badAuthor);
		return false;
	}
	return true;
}

function validate_comment(badComment) {
	if (document.getElementById("commentText").value.length < 5
			|| document.getElementById("commentText").value.length > 100) {
		alert(badComment);
		return false;
	}
	return true;
}

function validate_tag(badTag) {
	if (document.getElementById("tagName").value.length < 1
			|| document.getElementById("tagName").value.length > 30) {
		alert(badTag);
		return false;
	}
	return true;
}

function validate_login_password(badLogin, badPassword) {
	if (document.getElementById("login").value.length < 1
			|| document.getElementById("login").value.length > 30) {
		alert(badLogin);
		return false;
	}
	if (document.getElementById("password").value.length < 1
			|| document.getElementById("password").value.length > 30) {
		alert(badPassword);
		return false;
	}
	return true;
}

function validate_filter(badFilter) {
	if (document.filter.authorIdFromFilter.value == '-1'
			&& document.filter.tagsIdFromFilter.value <= 0) {
		alert(badFilter);
		return false;
	}
	return true;
}