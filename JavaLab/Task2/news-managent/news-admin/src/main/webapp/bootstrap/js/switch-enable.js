function switchEnableInput(inputId) {
    document.getElementById("text"+inputId).disabled = false;
    document.getElementById("edit"+inputId).style.display = "none";
    document.getElementById("update"+inputId).hidden = false;
    document.getElementById("delete"+inputId).hidden = false;
    document.getElementById("cancel"+inputId).hidden = false;
}

function switchDisableInput(inputId) {
    document.getElementById("text"+inputId).disabled = true;
    document.getElementById("edit"+inputId).style.display = "inline";
    document.getElementById("update"+inputId).hidden = true;
    document.getElementById("delete"+inputId).hidden = true;
    document.getElementById("cancel"+inputId).hidden = true;
}