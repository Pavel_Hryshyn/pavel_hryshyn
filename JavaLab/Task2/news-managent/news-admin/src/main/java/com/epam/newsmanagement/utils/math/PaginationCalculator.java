/*
 * @(#)PaginationCalculator.java   1.0 2015/09/17
 */
package com.epam.newsmanagement.utils.math;

/**
 * This class contains methods for list pagination
 * 
 * @version 1.0 17 September 2015
 * @author Pavel_Hryshyn
 */
public class PaginationCalculator {
	
	/**
	 * This methods calculates the start position of the list element on the
	 * page
	 * 
	 * @param currentPage
	 *            is the current page number
	 * @param elementPerPage
	 *            is amount of element per page
	 * @return the start position of the list element
	 */
	public static int calculateStartPosition(int currentPage, int elementPerPage) {
		int start = (currentPage - 1) * elementPerPage + 1;
		return start;
	}

	/**
	 * This methods calculates the end position of the list element on the page
	 * 
	 * @param currentPage
	 *            is the current page number
	 * @param elementPerPage
	 *            is amount of element per page
	 * @return the end position of the list element
	 */
	public static int calculateEndPosition(int currentPage, int elementPerPage) {
		int end = currentPage * elementPerPage;
		return end;
	}

	/**
	 * This methods calculates number of pages
	 * 
	 * @param countElements
	 *            is the amount of elements in the list
	 * @param elementPerPage
	 *            is amount of element per page
	 * @return number of pages
	 */
	public static int calculateNumberOfPages(int countElements,
			int elementPerPage) {
		int numberOfPages = (int) Math.ceil(countElements * 1.0
				/ elementPerPage);
		return numberOfPages;
	}
}
