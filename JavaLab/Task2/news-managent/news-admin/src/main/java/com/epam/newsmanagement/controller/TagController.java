/*
 * @(#)TagController.java   1.1 2015/10/13
 */
package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagent.entity.Tag;
import com.epam.newsmanagent.exception.ServiceException;
import com.epam.newsmanagent.service.ITagService;

/**
 * This class is responsible for processing all tags requests and building
 * appropriate model and passes it to the view for rendering.
 * 
 * @version 1.1 13 October 2015
 * @author Pavel_Hryshyn
 */
@Controller
@RequestMapping("tags")
public class TagController {
	
	/* This constant stores the logger instance */
	private static final Logger logger = Logger.getLogger(TagController.class);

	/* This constant stores the path mapping URIs */	
	private static final String PATH_REDIRECT_TAGS = "redirect:/tags";
	
	/* This constant stores the model attribute names */
	private static final String ATR_TAGS = "tags";
	private static final String ATR_TAG_ID = "id";
	private static final String ATR_ERROR_TAG_ID = "errorTagId";
	private static final String ATR_TAG_FOR_UPDATING = "tagForUpdating";
	private static final String ATR_TAG_FOR_SAVING = "tagForSaving";

	/* This constant stores the view names */
	private static final String VIEW_EDIT_TAG = "editTag";
	private static final String VIEW_ERROR = "error";

	@Autowired
	ITagService tagService;

	/**
	 * This method processing GET request to "tags" path mapping URIs, sets all
	 * model attributes and returns "edit tags" view
	 * 
	 * @param model
	 * @return "edit tags" view
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getAddNewsForm(Model model) {
		List<Tag> tags = null;
		try {
			tags = tagService.findAll();
		} catch (ServiceException e) {
			logger.error("Can't formed tags list", e);
			return VIEW_ERROR;
		}
		if (!model.containsAttribute(ATR_TAG_FOR_UPDATING)) {
			model.addAttribute(ATR_TAG_FOR_UPDATING, new Tag());
		}
		if (!model.containsAttribute(ATR_TAG_FOR_SAVING)) {
			model.addAttribute(ATR_TAG_FOR_SAVING, new Tag());
		}
		model.addAttribute(ATR_TAGS, tags);
		return VIEW_EDIT_TAG;
	}

	/**
	 * This method processes POST request to "/save" path mapping URIs, saves
	 * tag to database and redirects to "tags" path mapping URIs
	 * 
	 * @param tag
	 * @param result
	 *            provides the tag validation
	 * @param attr
	 *            flashes the model attributes for a redirect scenario
	 * @param session
	 * @return redirect to "tags" path mapping URIs
	 */
	@RequestMapping(value = { "/save" }, method = RequestMethod.POST)
	public String saveTags(@Valid @ModelAttribute(ATR_TAG_FOR_SAVING) Tag tag,
			BindingResult result, RedirectAttributes attr, HttpSession session) {
		String redirectPage = PATH_REDIRECT_TAGS;
		long tagId = 0L;
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.tagForSaving",
					result);
			attr.addFlashAttribute(ATR_TAG_FOR_SAVING, tag);
			return redirectPage;
		}
		try {
			tagId = tagService.create(tag);
			logger.info("Tag is saved succesfully. Id = " + tagId);
		} catch (ServiceException e) {
			logger.error("Tag is not saved. The reason is: ", e);
			return VIEW_ERROR;
		}
		return redirectPage;
	}

	/**
	 * This method processes POST request to "tags/update" path mapping URIs,
	 * updates tags to database and redirects to "tags" path mapping URIs
	 * 
	 * @param tag
	 * @param result
	 *            provides the tag validation
	 * @param attr
	 *            flashes the model attributes for a redirect scenario
	 * @param session
	 * @return redirect to "tags" path mapping URIs
	 */
	@RequestMapping(value = { "/update" }, method = RequestMethod.POST)
	public String updateTags(
			@Valid @ModelAttribute(ATR_TAG_FOR_UPDATING) Tag tag,
			BindingResult result, RedirectAttributes attr, HttpSession session) {
		String redirectPage = PATH_REDIRECT_TAGS;
		boolean isUpdated = false;
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.tagForUpdating",
					result);
			attr.addFlashAttribute(ATR_TAG_FOR_UPDATING, tag);
			attr.addFlashAttribute(ATR_ERROR_TAG_ID, tag.getId());
			return redirectPage;
		}
		try {
			isUpdated = tagService.update(tag);
			logger.info("Tag is updated. Status = " + isUpdated);
		} catch (ServiceException e) {
			logger.error("Tag is not updated. The reason is: ", e);
			return VIEW_ERROR;
		}
		return redirectPage;
	}

	/**
	 * This method processes POST request to "/delete/{id}" path mapping URIs,
	 * deletes tag from database and redirects to "tags" path mapping URIs
	 * 
	 * @param tagId
	 * @return redirect to "tags" path mapping URIs
	 */
	@RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.POST)
	public String expireAuthor(@PathVariable(ATR_TAG_ID) Long tagId) {
		String redirectPage = PATH_REDIRECT_TAGS;
		boolean isDeleted = false;
		try {
			isDeleted = tagService.delete(tagId);
			logger.info("Author is expired. Status = " + isDeleted);
		} catch (ServiceException e) {
			logger.error("Tag is not deleted. The reason is: ", e);
			return VIEW_ERROR;
		}
		return redirectPage;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}
}
