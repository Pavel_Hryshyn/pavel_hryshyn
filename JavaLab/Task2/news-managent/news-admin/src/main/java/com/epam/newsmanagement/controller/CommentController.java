/*
 * @(#)CommentController.java   1.1 2015/10/12
 */
package com.epam.newsmanagement.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagent.entity.Comment;
import com.epam.newsmanagent.exception.ServiceException;
import com.epam.newsmanagent.service.ICommentService;
import com.epam.newsmanagent.service.INewsService;

/**
 * This class is responsible for processing all comments requests and building
 * appropriate model and passes it to the view for rendering.
 * 
 * @version 1.1 12 October 2015
 * @author Pavel_Hryshyn
 */
@Controller
@RequestMapping("comment")
public class CommentController {
	
	/* This constant stores the logger instance */
	private static final Logger logger = Logger
			.getLogger(CommentController.class);

	/* This constant stores the path mapping URIs */
	private static final String PATH_REDIRECT_NEWS = "redirect:/news/";

	/* This constant stores the model attribute names */
	private static final String ATR_COMMENT = "comment";
	private static final String ATR_COMMENT_ID = "commentId";
	private static final String ATR_NEWS_ID = "newsId";

	/* This constant stores the view names */
	private static final String VIEW_ERROR = "error";

	@Autowired
	private ICommentService commentService;

	@Autowired
	private INewsService newsService;

	/**
	 * This method processes POST request to "comment/delete" path mapping URIs,
	 * deletes comments from database and redirects to "/news/{id}" path mapping
	 * URIs
	 * 
	 * @param commentId
	 * @param newsId
	 * @return redirect to "/news/{id}" path mapping URIs
	 */
	@RequestMapping(value = { "/delete" }, method = RequestMethod.POST)
	public String delete(@RequestParam(value = ATR_COMMENT_ID) Long commentId,
			@RequestParam(value = ATR_NEWS_ID) Long newsId) {
		String redirectPage = PATH_REDIRECT_NEWS + newsId;
		boolean isDeleted = false;
		try {
			isDeleted = commentService.delete(commentId);
			logger.info("Comment is deleted. Status = " + isDeleted);
		} catch (ServiceException e) {
			logger.error("Comment is not deleted. The reason is: ", e);
			return VIEW_ERROR;
		}
		return redirectPage;
	}

	/**
	 * This method processes POST request to "comment/add" path mapping URIs,
	 * saves comment to database and redirects to "/news/{id}" path mapping URIs
	 * 
	 * @param comment
	 * @param result
	 *            provides the author validation
	 * @param attr
	 *            flashes the model attributes for a redirect scenario
	 * @param session
	 * @param newsId
	 * @return redirect to "/news/{id}" path mapping URIs
	 */
	@RequestMapping(value = { "/add" }, method = RequestMethod.POST)
	public String postComment(
			@Valid @ModelAttribute(ATR_COMMENT) Comment comment,
			BindingResult bindingResult, RedirectAttributes attr,
			HttpSession session, @RequestParam(ATR_NEWS_ID) Long newsId) {
		String redirectPage = PATH_REDIRECT_NEWS + newsId;
		Long commentId = 0L;
		if (bindingResult.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.comment",
					bindingResult);
			attr.addFlashAttribute(ATR_COMMENT, comment);
			return redirectPage;
		}
		try {
			comment.setNews(newsService.findById(newsId));
			comment.setCreationDate(new Date());
			commentId = commentService.create(comment);
			logger.info("Comment is saved succesfully. Id = " + commentId);
		} catch (ServiceException e) {
			logger.error("Comment is not saved. The reason is: ", e);
			return VIEW_ERROR;
		}
		return redirectPage;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}
}
