<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n/messages" scope="application" />

<html lang="language">
<head>
<meta charset="utf-8">
<title><fmt:message key="jsp.title.news.view" /></title>

<link href="<c:url value="/bootstrap/css/style.css"/>" rel="stylesheet">
</head>
<body>
	<script src="${pageContext.request.contextPath}/bootstrap/js/validation.js"></script>

	<div class="panel">
		<div class="panel-header">
			<jsp:include page="/jsp/bars/header.jsp" />
			<div class="row">
				<div class="col-offset-9 col">
					<form method="GET" action="news?id=${news.id}">
						<input type="hidden" name="command" value="GET_NEWS_BY_ID" /> 
						<input type="hidden" name="newsId" value="${newsComplex.news.id}"> 
						<select	class="select" id="language" name="language" onchange="submit()">
							<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
							<option value="ru" ${language == 'ru' ? 'selected' : ''}>Russian</option>
						</select>
					</form>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-1">
					<form method="GET" action="newslist">
						<input type="hidden" name="command" value="NEWS_LIST" /> 
						<input type="hidden" name="page" value="1"> 
						<input type="submit" class="btn btn-info" value="<fmt:message key="jsp.button.back"/>" />
					</form>
				</div>
			</div>
			<div class="news-block-single">
				<div class="news-title">
					<b><c:out value="${newsComplex.news.title}" /></b>
				</div>
				<div class="news-author">
					<em><c:out value="by (${newsComplex.author.authorName})" /></em>
				</div>
				<div class="news-date">
					<fmt:formatDate pattern="dd/MM/yyyy" value="${newsComplex.news.modificationDate}" />
				</div>
				<div class="news-short-text">
					<em><c:out value="${newsComplex.news.shortText}" /></em>
				</div>
				<div class="news-full-text">
					<p><c:out value="${newsComplex.news.fullText}" /></p>
				</div>
			</div>

			<div class="comment-block">
				<c:forEach items="${newsComplex.comments}" var="comment">
					<div class="comment">
						<div class="comment-date">
							<fmt:formatDate pattern="dd/MM/yyyy" value="${comment.creationDate}" />
						</div>
						<div class="comment-text">
							<p><c:out value="${comment.commentText}" /></p>
						</div>
					</div>
				</c:forEach>

				<div class="create-comment-block">
					<fmt:message key="jsp.msg.comment.empty" var="badComment" />
					<form method="POST" action="news?id=${newsComplex.news.id}" name="postComment"
						onsubmit="return validate_comment('${badComment}');">
						<c:if test="${param.error eq true}">
							<div class="create-comment-bad">
								<fmt:message key="jsp.msg.comment.empty" />
							</div>
						</c:if>
						<textarea id="commentText" class="create-comment" rows="3"
							name="commentText"></textarea>
						<div class="row">
							<div class="col-offset-6">
								<input type="hidden" name="command" value="POST_COMMENT" /> 
								<input type="hidden" name="newsId" value="${newsComplex.news.id}"> 
								<input type="submit" class="btn btn-primary"
									value="<fmt:message key="jsp.button.comment.post"/>" />
							</div>
						</div>
					</form>
				</div>

			</div>
			<div class="navigation-block">
				<div class="previous">
					<c:if test="${previousNews ne 0}">
						<form method="GET" action="news?id=${previousNews}">
							<input type="hidden" name="command" value="GET_NEWS_BY_ID" /> 
							<input type="hidden" name="newsId" value="${previousNews}"> 
							<input type="submit" class="btn btn-default"
								value="<fmt:message key="jsp.button.news.previous"/>" />
						</form>
					</c:if>
				</div>

				<c:if test="${nextNews ne 0}">
					<div class="next">
						<form method="GET" action="news?id=${nextNews}">
							<input type="hidden" name="command" value="GET_NEWS_BY_ID" /> 
							<input type="hidden" name="newsId" value="${nextNews}"> 
							<input type="submit" class="btn btn-default"
								value="<fmt:message key="jsp.button.news.next"/>" />
						</form>
					</div>
				</c:if>
			</div>
		</div>

		<div class="panel-footer"><jsp:include
				page="/jsp/bars/footer.jsp" />
		</div>

	</div>
</body>
</html>