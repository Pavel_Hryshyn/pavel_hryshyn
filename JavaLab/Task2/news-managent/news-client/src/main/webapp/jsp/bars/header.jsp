<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n/messages" scope="application" />
<html lang="language">
<head>
<link href="<c:url value="/bootstrap/css/style.css"></c:url>" rel="stylesheet">
<meta charset="utf-8">
<title></title>

</head>
<body>
	<div class="row">
		<div class="col">
			<h1>News portal</h1>
		</div>
	</div>
</body>
</html>