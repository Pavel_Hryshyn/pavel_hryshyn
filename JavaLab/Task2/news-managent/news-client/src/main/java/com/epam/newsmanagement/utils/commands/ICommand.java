/*
 * @(#)ICommand.java   1.0 2015/09/07
 */
package com.epam.newsmanagement.utils.commands;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagent.exception.ServiceException;

/**
 * This interface is implemented by all Command classes and holds method of
 * executing command
 * 
 * @version 1.0 07 September 2015
 * @author Pavel_Hryshyn
 */
public interface ICommand {
	/**
	 * This methods executes command
	 * 
	 * @param request
	 *            provides request information for HTTP servlets
	 * @throws ServiceException
	 * @return String specifying the pathname to the jsp page
	 */
	String execute(HttpServletRequest request) throws ServiceException;
}
