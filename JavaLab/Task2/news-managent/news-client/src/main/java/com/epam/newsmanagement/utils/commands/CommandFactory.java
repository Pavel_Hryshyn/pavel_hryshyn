/*
 * @(#)CommandFactory.java   1.0 2015/09/07
 */
package com.epam.newsmanagement.utils.commands;

import org.apache.log4j.Logger;

/**
 * This class creates commands objects
 * 
 * @version 1.0 07 September 2015
 * @author Pavel_Hryshyn
 */
public class CommandFactory {
	/* This object obtains logger for this class */
	private static final Logger logger = Logger.getLogger(CommandFactory.class);

	private static final CommandFactory INSTANCE = new CommandFactory();
	
	private CommandFactory(){}
	
	/**
	 * This method creates command
	 * 
	 * @param commandName
	 * @throws IllegalArgumentException
	 *             , if the command is not registered in {@link CommandEnum}
	 * @return command
	 */
	public ICommand getCommand(String commandName) {
		ICommand command = null;
		CommandEnum commandEnum = null;
		try {
			commandEnum = CommandEnum.valueOf(commandName.toUpperCase());
			command = commandEnum.getCommand();
		} catch (IllegalArgumentException e) {
			logger.error("Command is not found", e);
			commandEnum = CommandEnum.ERROR_404;
			command = commandEnum.getCommand();
			return command;
		}
		return command;
	}
	
	/**
	 * This methods return instance of {@link CommandFactory}
	 * 
	 * return INSTANCE
	 */
	public static CommandFactory getInstance() {
		return INSTANCE;
	}
}
