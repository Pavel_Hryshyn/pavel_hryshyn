/*
 * @(#)GetNewsByIdCommand.java   1.0 2015/09/10
 */
package com.epam.newsmanagement.utils.commands.impl.crud;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.utils.commands.ICommand;
import com.epam.newsmanagement.utils.loaders.BeanLoader;
import com.epam.newsmanagent.entity.Author;
import com.epam.newsmanagent.entity.SearchCriteria;
import com.epam.newsmanagent.entity.Tag;
import com.epam.newsmanagent.entity.vo.NewsComplex;
import com.epam.newsmanagent.exception.ServiceException;
import com.epam.newsmanagent.service.INewsService;
import com.epam.newsmanagent.service.IServiceManager;

/**
 * This class implements command pattern and gets view of news with some newsId
 * 
 * @version 1.0 10 September 2015
 * @author Pavel_Hryshyn
 */
public class GetNewsByIdCommand implements ICommand {
	
	/* This constants store pathname to jsp pages */
	private static final String PATH_NEWS = "/jsp/news/singlenews.jsp";
	private static final String PAGE_NOT_FOUND = "/jsp/error/error404.jsp";

	/* This constants store names of request parameters */
	private static final String PARAM_NEWS_ID = "newsId";
	private static final String PARAM_NEWS_COMPLEX = "newsComplex";
	private static final String PARAM_NEXT_NEWS = "nextNews";
	private static final String PARAM_PREVIOUS_NEWS = "previousNews";
	private static final String SESSION_AUTHOR = "sessionAuthor";
	private static final String SESSION_TAGS = "sessionTags";

	private IServiceManager serviceManager;
	private INewsService newsService;

	public GetNewsByIdCommand() {
		super();
		this.serviceManager = BeanLoader.getInstance().getServiceManager();
		this.newsService = serviceManager.getNewsService();
	}

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = null;
		NewsComplex newsComplex = null;
		SearchCriteria sc = buildSearchCriteriaFromRequest(request);
		Long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
		newsComplex = serviceManager.findByNewsId(newsId);
		if (newsComplex == null) {
			return PAGE_NOT_FOUND;
		}
		Long previousNews = newsService.getPreviousNewsId(newsId, sc);
		Long nextNews = newsService.getNextNewsId(newsId, sc);
		request.setAttribute(PARAM_NEWS_COMPLEX, newsComplex);	
		request.setAttribute(PARAM_NEXT_NEWS, nextNews);
		request.setAttribute(PARAM_PREVIOUS_NEWS, previousNews);
		page = PATH_NEWS;
		return page;
	}

	@SuppressWarnings("unchecked")
	private SearchCriteria buildSearchCriteriaFromRequest(
			HttpServletRequest request) {
		/*
		 * This method builds SearchCriteria object from request
		 */
		SearchCriteria searchCriteria = new SearchCriteria();
		Author authorFromSession = (Author) request.getSession().getAttribute(
				SESSION_AUTHOR);
		List<Tag> tagsFromSession = (List<Tag>) request.getSession()
				.getAttribute(SESSION_TAGS);
		searchCriteria.setAuthor(authorFromSession);
		searchCriteria.setTags(tagsFromSession);
		return searchCriteria;
	}
}
