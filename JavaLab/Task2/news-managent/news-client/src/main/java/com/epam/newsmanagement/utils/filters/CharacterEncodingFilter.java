/*
 * @(#)CharacterEncodingFilter.java   1.0 2015/09/07
 */
package com.epam.newsmanagement.utils.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This class sets request and response encoding in UTF-8
 * 
 * @version 1.0 07 September 2015
 * @author Pavel_Hryshyn
 */
public class CharacterEncodingFilter implements Filter {
	
	/* This constants store reference to encoding in web.xml */
	private static final String ENCODING = "encoding";

	/* This fields store encoding parameter */
	private String code;

	/**
	 * This method resets encoding when disconnecting the filter
	 */
	@Override
	public void destroy() {
		code = null;
	}

	/**
	 * This method sets encoding for request and response
	 * 
	 * @param request
	 *            provides request information for HTTP servlets
	 * @param response
	 *            provides response information to client
	 * @param chain
	 *            gives a view into the invocation chain of a filtered request
	 *            for a resource.
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse responce,
			FilterChain chain) throws IOException, ServletException {
		String requestEncoding = request.getCharacterEncoding();
		if (code != null && !code.equalsIgnoreCase(requestEncoding)) {
			request.setCharacterEncoding(code);
			responce.setCharacterEncoding(code);
		}
		chain.doFilter(request, responce);
	}

	/**
	 * This method sets encoding when connecting the filter
	 * 
	 * @param filterConfig
	 *            is filter configuration object
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		code = filterConfig.getInitParameter(ENCODING);
	}
}
