/*
 * @(#)CommandEnum.java   1.0 2015/09/07
 */
package com.epam.newsmanagement.utils.commands;

import com.epam.newsmanagement.utils.commands.impl.crud.FilterCommand;
import com.epam.newsmanagement.utils.commands.impl.crud.GetNewsByIdCommand;
import com.epam.newsmanagement.utils.commands.impl.crud.NewsListCommand;
import com.epam.newsmanagement.utils.commands.impl.crud.PageNotFoundCommand;
import com.epam.newsmanagement.utils.commands.impl.crud.PostCommentCommand;
import com.epam.newsmanagement.utils.commands.impl.crud.ResetFilterCommand;
import com.epam.newsmanagement.utils.commands.impl.crud.ReturnErrorPageCommand;

/**
 * This enumeration stores set of commands for web application
 * 
 * @version 1.0 07 September 2015
 * @author Pavel_Hryshyn
 */
public enum CommandEnum {
	NEWS_LIST {
		{
			this.command = new NewsListCommand();
		}
	},
	GET_NEWS_BY_ID {
		{
			this.command = new GetNewsByIdCommand();
		}
	},
	FILTER {
		{
			this.command = new FilterCommand();
		}
	},
	RESET_FILTER {
		{
			this.command = new ResetFilterCommand();
		}
	},
	ERROR {
		{
			this.command = new ReturnErrorPageCommand();
		}
	},
	ERROR_404 {
		{
			this.command = new PageNotFoundCommand();
		}
	},
	POST_COMMENT {
		{
			this.command = new PostCommentCommand();
		}
	};

	protected ICommand command;

	public ICommand getCommand() {
		return command;
	}
}
