/*
 * @(#)BeanLoader.java   1.0 2015/09/10
 */
package com.epam.newsmanagement.utils.loaders;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagent.service.impl.ServiceManagerImpl;

/**
 * This class contains methods for lifecycles managing of spring context
 * 
 * @version 1.0 10 September 2015
 * @author Pavel_Hryshyn
 */
public class BeanLoader {
	
	private final static BeanLoader INSTANCE = new BeanLoader();

	private ClassPathXmlApplicationContext ctx;
	
	private ServiceManagerImpl serviceManager;

	private BeanLoader() {

	}

	/**
	 * This methods creates beans from application context
	 */
	public void initialize() {
		ctx = new ClassPathXmlApplicationContext("spring/newscommonContext.xml");
		serviceManager = (ServiceManagerImpl) ctx.getBean("serviceManager");
	}

	/**
	 * This method closes spring application context, destroying all beans in
	 * its bean factory
	 */
	public void destroy() {
		ctx.close();
	}

	/**
	 * This method gets instance of BeanLoader
	 * 
	 * @return instance of BeanLoader
	 */
	public static BeanLoader getInstance() {
		return INSTANCE;
	}

	public ServiceManagerImpl getServiceManager() {
		return serviceManager;
	}

	public void setServiceManager(ServiceManagerImpl serviceManager) {
		this.serviceManager = serviceManager;
	}
}
