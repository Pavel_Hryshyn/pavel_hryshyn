package by.hryshyn.chat;

import java.util.Scanner;

import by.hryshyn.chat.client.Client;
import by.hryshyn.chat.server.Server;

public class App {
	private static char SERVER = 's';
	private static char CLIENT = 'c';
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		System.out.println("��������� ��������� � ������ ������� ��� �������? (S(erver) / C(lient))");
		while (true) {
			int port = 0;
			char answer = Character.toLowerCase(in.nextLine().charAt(0));
			switch (answer) {
			case 's':
				System.out.println("Enter PORT:");
				port = Integer.parseInt(in.nextLine());
				new Server(port).start();
				break;
			case 'c':
				System.out.println("Enter PORT:");
				port = Integer.parseInt(in.nextLine());
				new Client(port).start();
				break;	
			default:
				System.out.println("Incorrect input. Please repeat");
				break;
			}
			
		}
	}

}
