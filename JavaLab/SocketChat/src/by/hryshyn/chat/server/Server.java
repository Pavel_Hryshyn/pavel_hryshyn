package by.hryshyn.chat.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Server {
	private final static String MSG_END = "quit";

	private final int PORT;
	private List<Connection> connectionList = Collections
			.synchronizedList(new ArrayList<Connection>());

	public Server(int port) {
		super();
		PORT = port;
	}

	public void start() {
		ServerSocket server = null;
		Socket socket = null;
		try {
			server = new ServerSocket(PORT);
			while (true) {
				socket = server.accept();
				Connection con = new Connection(socket);
				connectionList.add(con);
				new Thread(con).start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(server);
		}
	}

	public void closeAll(ServerSocket server) {
		try {
			server.close();
			synchronized (connectionList) {
				Iterator<Connection> iter = connectionList.iterator();
				while (iter.hasNext()) {
					((Connection) iter.next()).close();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private class Connection implements Runnable {
		private Socket socket;
		private BufferedReader in;
		private PrintWriter out;

		public Connection(Socket socket) {
			super();
			this.socket = socket;
		}

		@Override
		public void run() {
			String name = "";
			String str = "";

			try {
				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);
				name = in.readLine();

				while (true) {
					str = in.readLine();
					if (str.equals(MSG_END))
						break;

					synchronized (connectionList) {
						Iterator<Connection> iter = connectionList.iterator();
						while (iter.hasNext()) {
							((Connection) iter.next()).out.println(name + ": "
									+ str);
						}
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				close();
			}
		}

		public void close() {
			try {
				in.close();
				out.close();
				socket.close();
				connectionList.remove(this);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
