package by.hryshyn.chat.client;

import java.io.BufferedReader;
import java.io.IOException;

public class ClientSender implements Runnable {
	
	private boolean isStopped;
	private BufferedReader in;
		
	public ClientSender(BufferedReader in) {
		super();
		this.in = in;
	}

	@Override
	public void run() {
		while (!isStopped) {
			String str = "";
			try {
				str = in.readLine();
				System.out.println(str);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public void stopSender() {
		isStopped = true;
	}
}
