package by.hryshyn.chat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {
	
	private final int PORT;
	
	private final static String MSG_END = "quit"; 
	
	
	
	public Client(int port) {
		super();
		PORT = port;
	}

	public void start(){
		Scanner scanner = new Scanner(System.in);
		String serverIp = enterServerIP(scanner);
		String nickname = "";
		String str = "";
		Socket socket = null;
		BufferedReader in = null;
		PrintWriter out = null;
		
		try {
			socket = new Socket(serverIp, PORT);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			
			System.out.println("Enter your nickname: ");
			nickname = scanner.nextLine();
			out.println(nickname);
			
			ClientSender sender = new ClientSender(in);
			new Thread(sender).start();
			
			while (!str.equalsIgnoreCase(MSG_END)) {
				str = scanner.nextLine();
				out.println(str);
			}
			sender.stopSender();
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
		}
	}
	
	public String enterServerIP(Scanner scan) {
		String ip = "";
		System.out.println("Enter server IP in format xxx.xxx.xxx.xxx");
		ip = scan.nextLine();
		return ip;
	}
	
	public void close(BufferedReader in, PrintWriter out, Socket socket){
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
