package com.epam.newsportal.dao.impl.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsportal.dao.ITagDao;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/hibernateBeans-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:/test-data/tag-setup.xml"})
@DatabaseTearDown(value = {"classpath:/test-data/tag-setup.xml"},type = DatabaseOperation.DELETE_ALL)
@Ignore
public class TagDaoHibernateImplTest {

	@Autowired
	ITagDao tagDao;
	
	@Test
	public void testFindAll() {
		int expectedSize = 3;
		Tag expectedTag1 = createTag(1L, "tag1");
		Tag expectedTag2 = createTag(2L, "tag2");
		Tag expectedTag3 = createTag(3L, "tag3");
		List<Tag> actualTags = null;
		try {
			actualTags = tagDao.findAll();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(expectedSize, actualTags.size());
		assertEqualsTags(expectedTag1, actualTags.get(0));
		assertEqualsTags(expectedTag2, actualTags.get(1));
		assertEqualsTags(expectedTag3, actualTags.get(2));
	}

	@Test
	public void testFindByNewsId() {
		long newsId = 1L;
		long nonExistingNews = 5L;
		List<Tag> expectedTags = new ArrayList<Tag>();
		List<Tag> actualTags = null;
		expectedTags.add(createTag(1L, "tag1"));
		expectedTags.add(createTag(2L, "tag2"));
		try {
			actualTags = tagDao.findByNewsId(newsId);
			assertEquals(expectedTags, actualTags);
			assertNull(tagDao.findByNewsId(nonExistingNews));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testFindById() {
		long tagId = 1L;
		Tag expectedTag = createTag(tagId, "tag1");
		Tag actualTag = null;
		try {
			actualTag = tagDao.findById(tagId);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsTags(expectedTag, actualTag);
	}
	
	@Test
	public void testCreate() {
		long expectedTagId = 1L;
		int expectedSizeBeforeAdding = 3;
		int expectedSizeAfterAdding = 4;
		Tag expectedTag = createTag(expectedTagId, "tag1");
		Tag actualTag = null;
		List<Tag> actualTagsBeforeAdding = null;
		List<Tag> actualTagsAfterAdding = null;
		try {
			actualTagsBeforeAdding = tagDao.findAll();
			assertEquals(expectedSizeBeforeAdding, actualTagsBeforeAdding.size());
			expectedTagId = tagDao.create(expectedTag);
			actualTagsAfterAdding = tagDao.findAll();
			assertEquals(expectedSizeAfterAdding, actualTagsAfterAdding.size());
			actualTag = tagDao.findById(expectedTagId);
			assertEquals(expectedTag.getTagName(), actualTag.getTagName());
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testUpdate() {
		long tagId = 1L;
		Tag expectedTag = createTag(tagId, "tag1");
		Tag initialTag = null;
		Tag updatedTag = null;
		try {
			initialTag = tagDao.findById(tagId);
			assertEqualsTags(expectedTag, initialTag);
			expectedTag.setTagName("updatedTag");
			boolean isUpdated = tagDao.update(expectedTag);
			assertTrue(isUpdated);
			updatedTag = tagDao.findById(tagId);
			assertEqualsTags(expectedTag, updatedTag);
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test(expected = DaoException.class)
	public void testUpdateThrowDaoException() throws DaoException {
		Tag tag = createTag(100L, "tagName");
		tagDao.update(tag);
	}
	
	@Test
	public void testDelete() {
		long tagId = 1L;
		long nonExistingId = 100L;
		boolean isDeleted = false;
		Tag expectedTag = createTag(tagId, "tag1");
		Tag actualTagBeforeDeleting = null;
		Tag actualTagAfterDeleting = null;
		try {
			actualTagBeforeDeleting = tagDao.findById(tagId);
			assertEqualsTags(expectedTag, actualTagBeforeDeleting);
			isDeleted = tagDao.delete(tagId);
			assertTrue(isDeleted);
			actualTagAfterDeleting = tagDao.findById(tagId);
			assertNull(actualTagAfterDeleting);
			isDeleted = tagDao.delete(nonExistingId);
			assertFalse(isDeleted);
		} catch (DaoException e) {
			fail();
		}
	}
	
	private Tag createTag(Long id, String tagName) {
		Tag tag = new Tag();
		tag.setId(id);
		tag.setTagName(tagName);
		return tag;
	}
	
	private void assertEqualsTags(Tag expectedTag, Tag actualTag){
		assertEquals(expectedTag.getId(), actualTag.getId());
		assertEquals(expectedTag.getTagName(), actualTag.getTagName());
	}
}
