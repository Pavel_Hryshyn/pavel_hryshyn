package com.epam.newsportal.dao.impl.hibernate;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsportal.dao.INewsDao;
import com.epam.newsportal.entity.Author;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/hibernateBeans-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:/test-data/news-setup.xml"})
@DatabaseTearDown(value = {"classpath:/test-data/news-setup.xml"},type = DatabaseOperation.DELETE_ALL)
@Ignore
public class NewsDaoHibernateImplTest {

	@Autowired
	INewsDao newsDao;
	
	@Test
	public void testFindBetweenPositions() {
		int start = 1;
		int perPage = 3;
		int size = 3;
		List<News> expectedNewsList = new ArrayList<News>();
		expectedNewsList.add(createNews(3L, "Title3", "ShortText3", "FullText3"));
		expectedNewsList.add(createNews(2L, "Title2", "ShortText2", "FullText2"));
		expectedNewsList.add(createNews(1L, "Title1", "ShortText1", "FullText1"));
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findBetweenPositions(start, perPage);
			assertEquals(size, actualNewsList.size());
			assertEqualsNews(expectedNewsList.get(0), actualNewsList.get(0));
			assertEqualsNews(expectedNewsList.get(1), actualNewsList.get(1));
			assertEqualsNews(expectedNewsList.get(2), actualNewsList.get(2));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testSeachCriteriaIsNull() {
		int start = 1;
		int perPage = 3;
		int size = 3;
		List<News> expectedNewsList = new ArrayList<News>();
		expectedNewsList.add(createNews(3L, "Title3", "ShortText3", "FullText3"));
		expectedNewsList.add(createNews(2L, "Title2", "ShortText2", "FullText2"));
		expectedNewsList.add(createNews(1L, "Title1", "ShortText1", "FullText1"));
		List<News> actualNewsList = null;
		SearchCriteria sc = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, perPage);
			assertEquals(size, actualNewsList.size());
			assertEqualsNews(expectedNewsList.get(0), actualNewsList.get(0));
			assertEqualsNews(expectedNewsList.get(1), actualNewsList.get(1));
			assertEqualsNews(expectedNewsList.get(2), actualNewsList.get(2));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testSeachCriteriaWithoutAuthorAndTags() {
		int start = 1;
		int perPage = 3;
		int size = 3;
		List<News> expectedNewsList = new ArrayList<News>();
		expectedNewsList.add(createNews(3L, "Title3", "ShortText3", "FullText3"));
		expectedNewsList.add(createNews(2L, "Title2", "ShortText2", "FullText2"));
		expectedNewsList.add(createNews(1L, "Title1", "ShortText1", "FullText1"));
		List<News> actualNewsList = null;
		SearchCriteria sc = new SearchCriteria();
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, perPage);
			assertEquals(size, actualNewsList.size());
			assertEqualsNews(expectedNewsList.get(0), actualNewsList.get(0));
			assertEqualsNews(expectedNewsList.get(1), actualNewsList.get(1));
			assertEqualsNews(expectedNewsList.get(2), actualNewsList.get(2));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testSearchCriteriaWithAuthorWithoutNews() {
		int start = 0;
		int perPage = 3;
		int size = 0;
		Author author = createAuthor(3L, "author3");
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthor(author);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, perPage);
			assertEquals(size, actualNewsList.size());
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testSearchCriteriaWithAuthor() {
		int start = 0;
		int perPage = 3;
		int size = 2;
		List<News> expectedNewsList = new ArrayList<News>();
		expectedNewsList.add(createNews(2L, "Title2", "ShortText2", "FullText2"));
		expectedNewsList.add(createNews(1L, "Title1", "ShortText1", "FullText1"));
		Author author = createAuthor(1L, "author1");
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthor(author);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, perPage);
			assertEquals(size, actualNewsList.size());
			assertEqualsNews(expectedNewsList.get(0), actualNewsList.get(0));
			assertEqualsNews(expectedNewsList.get(1), actualNewsList.get(1));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testSearchCriteriaWithTagWithoutNews() {
		int start = 0;
		int perPage = 3;
		int size = 0;
		SearchCriteria sc = new SearchCriteria();
		Tag tag = createTag(4L, "tag4");
		sc.setTags(new ArrayList<Tag>());
		sc.getTags().add(tag);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, perPage);
			assertEquals(size, actualNewsList.size());
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testSearchCriteriaWithOnePlusOneTag() {
		int start = 0;
		int perPage = 3;
		int size = 1;
		List<News> expectedNewsList = new ArrayList<News>();
		expectedNewsList.add(createNews(3L, "Title3", "ShortText3", "FullText3"));
		SearchCriteria sc = new SearchCriteria();
		Tag tag4 = createTag(4L, "tag4");
		Tag tag3 = createTag(3L, "tag3");
		sc.setTags(new ArrayList<Tag>());
		sc.getTags().add(tag4);
		sc.getTags().add(tag3);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, perPage);
			assertEquals(size, actualNewsList.size());
			assertEqualsNews(expectedNewsList.get(0), actualNewsList.get(0));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testSearchCriteriaWithTwoTags() {
		int start = 0;
		int perPage = 3;
		int size = 2;
		List<News> expectedNewsList = new ArrayList<News>();
		expectedNewsList.add(createNews(3L, "Title3", "ShortText3", "FullText3"));
		expectedNewsList.add(createNews(1L, "Title1", "ShortText1", "FullText1"));
		SearchCriteria sc = new SearchCriteria();
		Tag tag4 = createTag(2L, "tag2");
		Tag tag3 = createTag(3L, "tag3");
		sc.setTags(new ArrayList<Tag>());
		sc.getTags().add(tag4);
		sc.getTags().add(tag3);
		List<News> actualNewsList = null;
		try {
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, perPage);
			assertEquals(size, actualNewsList.size());
			assertEqualsNews(expectedNewsList.get(0), actualNewsList.get(0));
			assertEqualsNews(expectedNewsList.get(1), actualNewsList.get(1));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testSearchCriteriaWithAuthorAndTags() {
		int start = 0;
		int perPage = 3;
		int size = 1;
		List<News> expectedNewsList = new ArrayList<News>();
		expectedNewsList.add(createNews(3L, "Title3", "ShortText3", "FullText3"));
		SearchCriteria sc = new SearchCriteria();
	//	Tag tag2 = createTag(2L, "tag2");
		Tag tag3 = createTag(3L, "tag3");
		Author author = createAuthor(2L, "author2");
		sc.setTags(new ArrayList<Tag>());
	//	sc.getTags().add(tag2);
		sc.getTags().add(tag3);
		sc.setAuthor(author);
		List<News> actualNewsList = null;
		try {
			System.out.println(sc);
			actualNewsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, perPage);
			System.out.println(actualNewsList);
			assertEquals(size, actualNewsList.size());
			assertEqualsNews(expectedNewsList.get(0), actualNewsList.get(0));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testFindById() {
		News expectedNews = createNews(1L, "Title1", "ShortText1", "FullText1");
		News actualNews = null;
		try {
			actualNews = newsDao.findById(1L);
			assertEqualsNews(expectedNews, actualNews);
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testGetNextNewsId() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testGetPreviousNewsId() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testCountAllNews() {
		long expectedAmount = 4L;
		long actualAmount = 0;
		try {
			actualAmount = newsDao.countAllNews();
			assertEquals(expectedAmount, actualAmount);
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testCountNewsWithCriteria() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testCreate() {
		long newsId = 0L;
		Tag tag1 = createTag(1L, "tag1");
		Tag tag2 = createTag(2L, "tag2");
		Author author = createAuthor(1L, "author1");
		News expectedNews = createNews(5L, "Title5", "shortText5", "fullText5");
		expectedNews.getAuthors().add(author);
		expectedNews.getTags().add(tag1);
		expectedNews.getTags().add(tag2);
		News actualNews = null;
		try {
			newsId = newsDao.create(expectedNews);
			expectedNews.setId(newsId);
			actualNews = newsDao.findById(newsId);
			assertEqualsNews(expectedNews, actualNews);
//			assertEquals(expectedNews.getAuthors().get(0), actualNews.getAuthors().get(0));
//			assertEquals(expectedNews.getTags().get(0), actualNews.getTags().get(0));	
//			assertEquals(expectedNews.getTags().get(1), actualNews.getTags().get(1));	
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testAddNewsWithTags() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testAddNewsWithAuthor() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}
	
	@Test(expected = DaoException.class)
	public void testUpdateWithDaoException() throws DaoException {
		News news = newsDao.findById(1L);
		news.setShortText("updated");
		newsDao.update(news);
		news.setShortText("updated1");
		news.setVersion(1);
		newsDao.update(news);
	}
	
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}
	
	
	@Test
	public void testDeleteTagsForNews() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testDeleteAuthorForNews() {
		fail("Not yet implemented");
	}

	private News createNews(Long id, String title, String shortText, String fullText){
		News news = new News();
		news.setId(id);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		return news;
	}
	
	private Tag createTag(Long id, String tagName){
		Tag tag = new Tag();
		tag.setId(id);
		tag.setTagName(tagName);
		return tag;
	}
	
	private Author createAuthor(Long id, String authorName){
		Author author = new Author();
		author.setId(id);
		author.setAuthorName(authorName);
		return author;
	}

	private void assertEqualsNews(News expectedNews, News actualNews){
		assertEquals(expectedNews.getId(), actualNews.getId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());	
	}
}
