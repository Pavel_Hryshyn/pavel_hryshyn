package com.epam.newsportal.dao.impl.hibernate;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsportal.dao.IAuthorDao;
import com.epam.newsportal.entity.Author;
import com.epam.newsportal.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/hibernateBeans-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:/test-data/author-setup.xml"})
@DatabaseTearDown(value = {"classpath:/test-data/author-setup.xml"},type = DatabaseOperation.DELETE_ALL)
@Ignore
public class AuthorDaoHibernateImplTest {

	@Autowired
	private IAuthorDao authorDao;
	
	@Test
	public void testFindAll(){
		Author expectedAuthor1 = createAuthor(1L, "author1");
		Author expectedAuthor2 = createAuthor(2L, "author2");
		Author expectedAuthor3 = createAuthor(3L, "author3");
		Author expectedAuthor4 = createAuthor(4L, "author4");
		List<Author> actualAuthors = null;
		try {
			actualAuthors = authorDao.findAll();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(4, actualAuthors.size());
		assertEqualsAuthors(expectedAuthor1, actualAuthors.get(0));
		assertEqualsAuthors(expectedAuthor2, actualAuthors.get(1));
		assertEqualsAuthors(expectedAuthor3, actualAuthors.get(2));
		assertEqualsAuthors(expectedAuthor4, actualAuthors.get(3));
	}
		
	@Test
	public void testFindAllNonExpiredAuthors() {
		Author expectedAuthor1 = createAuthor(1L, "author1");
		Author expectedAuthor2 = createAuthor(2L, "author2");
		Author expectedAuthor3 = createAuthor(3L, "author3");
		List<Author> actualAuthors = null;
		try {
			actualAuthors = authorDao.findAllNonExpiredAuthors();
		} catch (DaoException e) {
			fail();
		}
		assertEquals(3, actualAuthors.size());
		assertEqualsAuthors(expectedAuthor1, actualAuthors.get(0));
		assertEqualsAuthors(expectedAuthor2, actualAuthors.get(1));
		assertEqualsAuthors(expectedAuthor3, actualAuthors.get(2));
	}
	
	@Test
	public void testFindById() {
		long authorId = 1L;
		Author expectedAuthor = createAuthor(authorId, "author1");
		Author actualAuthor = null;
		try {
			actualAuthor = authorDao.findById(authorId);
		} catch (DaoException e) {
			fail();
		}
		assertEqualsAuthors(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testFindByNewsId(){
		long newsId = 1L;
		long nonExistingNews = 5L;
		Author expectedAuthor = createAuthor(1L, "author1");
		Author actualAuthor = null;
		try {
			actualAuthor = authorDao.findByNewsId(newsId);
			assertEqualsAuthors(expectedAuthor, actualAuthor);
			assertNull(authorDao.findByNewsId(nonExistingNews));
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testCreate(){
		Author actualAuthor = null;
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorName("author5");
		List<Author> actualAuthorsBeforeAdding = null;
		List<Author> actualAuthorsAfterAdding = null;
		try {
			actualAuthorsBeforeAdding = authorDao.findAll();
			assertEquals(4, actualAuthorsBeforeAdding.size());
			long actualId = authorDao.create(expectedAuthor);
			actualAuthorsAfterAdding = authorDao.findAll();
			actualAuthor = authorDao.findById(actualId);
			assertEquals(5, actualAuthorsAfterAdding.size());
			assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test
	public void testUpdate(){
		long authorId = 1L;
		Author expectedAuthor = createAuthor(authorId, "author1");
		Author actualAuthorBeforeUpdating = null;
		Author actualAuthorAfterUpdating = null;
		try {
			actualAuthorBeforeUpdating = authorDao.findById(authorId);
			assertEqualsAuthors(expectedAuthor, actualAuthorBeforeUpdating);
			expectedAuthor.setAuthorName("updatedAuthor1");
			boolean isUpdated = authorDao.update(expectedAuthor);
			assertTrue(isUpdated);
			actualAuthorAfterUpdating = authorDao.findById(authorId);
			assertEqualsAuthors(expectedAuthor, actualAuthorAfterUpdating);	
		} catch (DaoException e) {
			fail();
		}
	}
	
	@Test(expected = DaoException.class)
	public void testUpdateWithDaoException() throws DaoException {
		Author author = createAuthor(10L, "author");
		authorDao.update(author);
	}
	
	@Test
	public void testExpire(){
		long authorId = 1L;
		long afterExpiringId0 = 0L;
		long afterExpiringId1 = 0L;
		Author expectedAuthor1 = createAuthor(authorId, "author1");
		List<Author> actualAuthorsBeforeExpiring = null;
		List<Author> actualAuthorsAfterExpiring = null;
		try {
			actualAuthorsBeforeExpiring = authorDao.findAllNonExpiredAuthors();
			assertEquals(3, actualAuthorsBeforeExpiring.size());
			assertEqualsAuthors(expectedAuthor1, actualAuthorsBeforeExpiring.get(0));
			authorDao.expire(authorId);
			actualAuthorsAfterExpiring = authorDao.findAllNonExpiredAuthors();
		} catch (DaoException e) {
			fail();
		}
		afterExpiringId0 = actualAuthorsAfterExpiring.get(0).getId();
		afterExpiringId1 = actualAuthorsAfterExpiring.get(1).getId();
		assertEquals(2, actualAuthorsAfterExpiring.size());
		assertTrue(authorId != afterExpiringId0);
		assertTrue(authorId != afterExpiringId1);
	}
	
	private Author createAuthor(Long id, String authorName){
		Author author = new Author();
		author.setId(id);
		author.setAuthorName(authorName);
		return author;
	}
	
	private void assertEqualsAuthors(Author expectedAuthor, Author actualAuthor){
		assertEquals(expectedAuthor.getId(), actualAuthor.getId());
		assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
	}

}
