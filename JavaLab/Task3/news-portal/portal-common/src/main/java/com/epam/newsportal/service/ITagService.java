/*
 * @(#)ITagService.java   1.0 2016/02/08
 */
package com.epam.newsportal.service;

import java.util.List;

import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.ServiceException;

/**
 * This interface defines basic methods for Tag Entity on service layer
 * @version 1.0 08 February 2016
 * @author Pavel_Hryshyn
 */
public interface ITagService {
	/**
	 * This method returns a list of all tags that stores in database
	 * @return list of all tags from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<Tag> findAll() throws ServiceException;
	
	/**
	 * This method returns a list of tags for news with some newsId that stores in database
	 * @param newsId
	 * @return list of tags for news with some newsId from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<Tag> findByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * This method tag with some tagId that stores in database
	 * @param tagId
	 * @return tag with some tagId with some newsId from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	Tag findById(Long tagId) throws ServiceException;
	
	/**
	 * This method adds tag to database
	 * @param tag
	 * 			the param uses for adding tag to database
	 * @return tagId
	 * @throws ServiceException, if arise database access error or other database errors
	 * 				or author doesn't save in database
	 */
	Long create(Tag tag) throws ServiceException;
	
	/**
	 * This method updates tag to database
	 * @param tag
	 *            the param uses for updating tag to database
	 * @return true if tag is updated to database, else false
	 * @throws ServiceException,
	 *             if arise database access error or other database errors
	 */
	boolean update(Tag tag) throws ServiceException;
	
	/**
	 * This method deletes a tag from database
	 * @param tagId
	 * 			the param uses for deleting tag
	 * @return true if a tag is deleted, else false
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	boolean delete(Long tagId) throws ServiceException;
}
