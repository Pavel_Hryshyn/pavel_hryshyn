/*
 * @(#)DaoException.java   1.0 2016/02/08
 */
package com.epam.newsportal.exception;

/**
 * This class is wrapper for all Service level exceptions  
 * @version 1.0 08 February 2016
 * @author Pavel_Hryshyn
 */
public class ServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(String message) {
		super(message);
	}
}
