/*
 * @(#)IServiceManager.java   1.0 2016/02/08
 */
package com.epam.newsportal.service;

import java.util.List;

import com.epam.newsportal.service.IAuthorService;
import com.epam.newsportal.service.ICommentService;
import com.epam.newsportal.service.INewsService;
import com.epam.newsportal.service.ITagService;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.vo.NewsComplexVO;
import com.epam.newsportal.exception.ServiceException;

/**
 * This interface defines basic methods for ServiceManager on service layer
 * @version 1.0 08 February 2016
 * @author Pavel_Hryshyn
 */
public interface IServiceManager {
	/**
	 * This method returns newsComplex for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching list of comments in database
	 * @return newsComplex for some newsId from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	NewsComplexVO findByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * This method returns list of newsComplex which matching searchCriteria and positions 
	 * between start and end that stores in database
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of all newsComplex which matching searchCriteria and positions 
	 * 	between start and end from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<NewsComplexVO> findNewsComplexWithCriteriaAndPositions(SearchCriteria sc, int start, int end) throws ServiceException;
	
	IAuthorService getAuthorService();
	
	ICommentService getCommentService();
	
	INewsService getNewsService();
	
	ITagService getTagService();
}
