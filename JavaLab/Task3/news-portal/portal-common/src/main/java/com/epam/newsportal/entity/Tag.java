/*
 * @(#)Tag.java   1.0 2016/01/12
 */
package com.epam.newsportal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * This class is entity that defines the parameters of Tag
 * @version 1.0 12 January 2016
 * @author Pavel_Hryshyn
 */
@Entity
@Table(name = "TAG")
public class Tag implements Serializable {
	private static final long serialVersionUID = 1L;

	/* This value stores tag id */
	@Id
	@Column(name = "tag_id_pk", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tag_seq")
	@SequenceGenerator(name = "tag_seq", sequenceName = "TAG_SEQUENCE", allocationSize = 1)
	private long id;
	
	/* This value stores tag name */
	@Size(min=1, max=30)
	@Column(name = "tag_name", nullable = false)
	private String tagName;
	
	@ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private List<News> newsList = new ArrayList<News>();

	public Tag() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public List<News> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (id != other.id)
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", tagName=" + tagName + "]";
	}	
}
