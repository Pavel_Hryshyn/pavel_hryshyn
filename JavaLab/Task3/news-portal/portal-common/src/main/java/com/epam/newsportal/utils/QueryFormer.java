/*
 * @(#)QueryFormer.java   1.0 2016/02/23
 */
package com.epam.newsportal.utils;

/**
 * This class contains static methods for forming query from several subQueries
 * @version 1.0 23 February 2016
 * @author Pavel_Hryshyn
 */
public class QueryFormer {
	/* This constant stores space character */
	private static final String SPACE = " ";
	private static final String EXC_INCORRECT_NUMBER = "Incorrect number of iteration";
	private static final String OR = "OR ";

	/**
	 * This method adds subQuery to end of query
	 * @param initialQuery
	 * 					contains initial query
	 * @param subQuery
	 * 					contains subquery that adding to end of query
	 * @return the resulting query
	 */
	public static String addSubQueryToEnd(String initialQuery, String subQuery){
		String result = initialQuery + SPACE + subQuery;
		return result;
	}
	
	/**
	 * This method adds subQuery to end of query
	 * @param initialQuery
	 * 					contains initial query
	 * @param subQuery
	 * 					contains subquery that adding to end of query
	 * @return the resulting query
	 */
	public static String addIterativeSubQueryToEnd(String initialQuery, String subQuery, int iteration){
		String result = "";
		String afterIteration = "";
		if (iteration > 0){
			for (int i=0; i<iteration; i++){
				if (i==0){
					afterIteration += (subQuery + SPACE);
				} else {
					afterIteration += (OR + subQuery + SPACE);
				}
			}
			result = addSubQueryToEnd(initialQuery, afterIteration);
		} else {
			throw new ArithmeticException(EXC_INCORRECT_NUMBER);
		}
		return result;
	}
}
