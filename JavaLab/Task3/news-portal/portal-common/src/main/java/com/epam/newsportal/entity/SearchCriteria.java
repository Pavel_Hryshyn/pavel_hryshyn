/*
 * @(#)SearchCriteria.java   1.0 2016/01/12
 */
package com.epam.newsportal.entity;

import java.util.List;

/**
 * This class uses for setting of search criteria
 * @version 1.0 12 January 2016
 * @author Pavel_Hryshyn
 */
public class SearchCriteria {
	/* This value stores Author */
	private Author author;
	
	/* This value stores list of tags */
	private List<Tag> tags;

	public SearchCriteria() {
		super();
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SearchCriteria [author=" + author + ", tags=" + tags + "]";
	}
}
