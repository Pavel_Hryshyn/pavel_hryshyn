/*
 * @(#)Role.java   1.0 2016/01/12
 */
package com.epam.newsportal.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class is entity that defines the parameters of Role
 * @version 1.0 12 January 2016
 * @author Pavel_Hryshyn
 */
@Embeddable
@Table(name = "ROLES")
public class Role {
	/* This value stores user */
	@ManyToOne
	@JoinColumn(name = "user_id_fk")
	private User user;
	
	/* This value stores roles enum */
	@Column(name = "role_name", nullable = false)
	private RolesEnum role;

	public Role() {
		super();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public RolesEnum getRole() {
		return role;
	}

	public void setRole(RolesEnum role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (role != other.role)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Role [user=" + user + ", role=" + role + "]";
	}
}
