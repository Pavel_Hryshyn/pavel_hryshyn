/*
 * @(#)TagDaoHibernateImpl.java   1.0 2015/01/12
 */
package com.epam.newsportal.dao.impl.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.epam.newsportal.dao.ITagDao;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.DaoException;

public class TagDaoHibernateImpl implements ITagDao {
	private static final String ID = "id";
	private static final String HQL_DELETE = "delete from Tag where id = :id";
	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> findAll() throws DaoException {
		List<Tag> tags = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			tags = (List<Tag>)session.createCriteria(Tag.class).addOrder(Order.asc(ID)).list(); 
		} catch (HibernateException e) {
			throw new DaoException("List of tag can not be formed", e);
		}
		return tags;
	}

	@Override
	public List<Tag> findByNewsId(Long newsId) throws DaoException {
		List<Tag> tags = null;
		News news = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			news = (News)session.createCriteria(News.class).add(Restrictions.eq(ID, newsId)).uniqueResult();
			if (news != null) {
				news.getTags().size();
				tags = news.getTags();
			} 
		} catch (HibernateException e) {
			throw new DaoException("List of tag can not be formed", e);
		} 
		return tags;
	}

	@Override
	public Tag findById(Long tagId) throws DaoException {
		Tag tag = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			tag = (Tag)session.createCriteria(Tag.class).add(Restrictions.eq(ID, tagId)).uniqueResult();; 
		} catch (HibernateException e) {
			throw new DaoException("Error occured during getting tag by Id", e);
		} 
		return tag;
	}

	@Override
	public Long create(Tag tag) throws DaoException {
		Long id = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			id = (long) session.save(tag);
			tx.commit();
		} catch (HibernateException e) {
			throw new DaoException("Error occured during saving tag", e);
		} 
		return id;
	}

	@Override
	public boolean update(Tag tag) throws DaoException {
		boolean isUpdated = false;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			session.update(tag);
			tx.commit();
			isUpdated = true;
		} catch (HibernateException e) {
			throw new DaoException("Error occured during updating tag", e);
		} 
		return isUpdated;
	}

	@Override
	public boolean delete(Long tagId) throws DaoException {
		boolean isDeleted = false;
		int row = 0;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery(HQL_DELETE);
			query.setParameter(ID, tagId);
			row = query.executeUpdate();
			tx.commit();
			if (row >0) {
				isDeleted = true;
			}
		} catch (HibernateException e) {
			throw new DaoException("Error occured during deleting tag", e);
		} 
		return isDeleted;
	}

	@Override
	public boolean deleteTagFromNews(Long tagId) throws DaoException {
		throw new DaoException("This method isn't implemented for Hibernate");
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
