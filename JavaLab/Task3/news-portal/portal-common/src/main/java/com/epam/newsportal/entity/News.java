/*
 * @(#)News.java   1.0 2016/01/12
 */
package com.epam.newsportal.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * This class is entity that defines the parameters of News
 * 
 * @version 1.0 12 January 2016
 * @author Pavel_Hryshyn
 */
@Entity
@Table(name = "NEWS")
public class News {
	/* This value stores news id */
	@Id
	@Column(name = "news_id_pk", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "news_seq")
	@SequenceGenerator(name = "news_seq", sequenceName = "NEWS_SEQUENCE", allocationSize = 1)
	private long id;

	/* This value stores news title */
	@Size(min = 1, max = 30)
	@Column(name = "news_title", nullable = false)
	private String title;

	/* This value stores short text of news */
	@Size(min = 1, max = 100)
	@Column(name = "news_short_text", nullable = false)
	private String shortText;

	/* This value stores full text of news */
	@Size(min = 1, max = 2000)
	@Column(name = "news_full_text", nullable = false)
	private String fullText;

	/* This value stores date of creation news */
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull
	@Column(name = "news_creation_date", nullable = false)
	private Date creationDate;

	/* This value stores date of modification news */
	@Column(name = "news_modification_date", nullable = false)
	private Date modificationDate;

	/* This value stores news version */
	@Version
	@Column(name = "news_version", nullable = false)
	private long version;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "NEWS_TAG", joinColumns = { 
			@JoinColumn(name = "news_id_fk", nullable = false, referencedColumnName = "news_id_pk") },
			inverseJoinColumns = { @JoinColumn(name = "tag_id_fk", nullable = false, 
			referencedColumnName = "tag_id_pk") })
	private List<Tag> tags = new ArrayList<Tag>();
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "NEWS_AUTHOR", joinColumns = { 
			@JoinColumn(name = "news_id_fk", nullable = false, referencedColumnName = "news_id_pk") },
			inverseJoinColumns = { @JoinColumn(name = "author_id_fk", nullable = false, 
			referencedColumnName = "author_id_pk") })
	private List<Author> authors = new ArrayList<Author>();

	public News() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + (int) (version ^ (version >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id != other.id)
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (version != other.version)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", shortText="
				+ shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ ", version=" + version + ", tags=" + tags + ", authors="
				+ authors + "]";
	}
}
