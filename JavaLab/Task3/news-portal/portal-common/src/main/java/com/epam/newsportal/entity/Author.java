/*
 * @(#)Author.java   1.0 2015/12/23
 */
package com.epam.newsportal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * This class is entity that defines the parameters of Author
 * @version 1.0 23 December 2015
 * @author Pavel_Hryshyn
 */
@Entity
@Table(name = "AUTHOR")
public class Author implements Serializable {
	private static final long serialVersionUID = 1L;

	/* This value stores author id */
	@Id
	@Column(name = "author_id_pk", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_seq")
	@SequenceGenerator(name = "author_seq", sequenceName = "AUTHOR_SEQUENCE", allocationSize = 1)
	private long id;
	
	/* This value stores author name */
	@Size(min=1, max=30)
	@Column(name = "author_name", nullable = false)
	private String authorName;
		
	/* This value stores expired date of author */
	@Column(name = "author_expired")
	private Date expiredDate;

	@ManyToMany(mappedBy = "authors", fetch = FetchType.LAZY)
	private List<News> newsList = new ArrayList<News>();
	
	public Author() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", authorName=" + authorName + "]";
	}
}
