/*
 * @(#)ServiceManagerImpl.java   1.0 2016/02/08
 */
package com.epam.newsportal.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.vo.NewsComplexVO;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.IAuthorService;
import com.epam.newsportal.service.ICommentService;
import com.epam.newsportal.service.INewsService;
import com.epam.newsportal.service.IServiceManager;
import com.epam.newsportal.service.ITagService;


/**
 * This class is manager for all service classes
 * @version 1.0 09 February 2016
 * @author Pavel_Hryshyn
 */
@Service("serviceManager")
@Transactional(propagation = Propagation.REQUIRED, readOnly = true, rollbackFor=ServiceException.class)
public class ServiceManagerImpl implements IServiceManager {

	@Autowired
	private INewsService newsService;
	
	@Autowired
	private ICommentService commentService;
	
	@Autowired
	private IAuthorService authorService;
	
	@Autowired
	private ITagService tagService;
	
	@Override
	public NewsComplexVO findByNewsId(Long newsId) throws ServiceException {
		NewsComplexVO newsComplex = null;
		News news = newsService.findById(newsId);
		if (news != null )	{
			newsComplex = new NewsComplexVO();
			Hibernate.initialize(news.getAuthors());
			Hibernate.initialize(news.getTags());
			newsComplex.setNews(news);
			newsComplex.setComments(commentService.findByNewsId(newsId));
		}
		return newsComplex;
	}

	@Override
	public List<NewsComplexVO> findNewsComplexWithCriteriaAndPositions(
			SearchCriteria sc, int start, int end) throws ServiceException {
		List<NewsComplexVO> newsComplexList = null;
		List<News> newsList = newsService.findNewsWithCriteriaAndPositions(sc, start, end);
		if (newsList != null && !newsList.isEmpty() )	{
			newsComplexList = new ArrayList<NewsComplexVO>();
			for (News news: newsList){
				Hibernate.initialize(news.getAuthors());
				Hibernate.initialize(news.getTags());
				NewsComplexVO newsComplex = new NewsComplexVO();
				newsComplex.setNews(news);
				newsComplex.setComments(commentService.findByNewsId(news.getId()));
			}
		}
		return newsComplexList;
	}

	@Override
	public IAuthorService getAuthorService() {
		return authorService;
	}

	@Override
	public ICommentService getCommentService() {
		return commentService;
	}

	@Override
	public INewsService getNewsService() {
		return newsService;
	}

	@Override
	public ITagService getTagService() {
		return tagService;
	}
}
