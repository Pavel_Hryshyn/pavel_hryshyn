/*
 * @(#)ICommentService.java   1.0 2016/02/08
 */
package com.epam.newsportal.service;

import java.util.List;

import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.exception.ServiceException;


/**
 * This interface defines basic methods for Comment Entity on service layer
 * @version 1.0 08 February 2016
 * @author Pavel_Hryshyn
 */
public interface ICommentService {
	/**
	 * This method returns list of comments for some newsId that stores in database
	 * @param newsId
	 * 			the param uses for searching list of comments in database
	 * @return list of comments for some newsId from database
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	List<Comment> findByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * This method adds comment to database
	 * @param comment
	 * 			the param uses for adding comment to database
	 * @return comment's id
	 * @throws ServiceException, if arise database access error or other database errors
	 * 				or comment doesn't save in database
	 */
	Long create(Comment comment) throws ServiceException;
	
	/**
	 * This method delete comment with some id that stores in database
	 * @param id
	 * 			the param uses for deleting comment from database
	 * @return true, if comment are deleted, else return false
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	boolean delete(Long id) throws ServiceException;
	
	/**
	 * This method get comments amount for news from database
	 * @param newsId
	 * @return comments amount
	 * @throws ServiceException, if arise database access error or other database errors
	 */
	boolean deleteCommentsByNewsId(Long newsId) throws ServiceException;
}
