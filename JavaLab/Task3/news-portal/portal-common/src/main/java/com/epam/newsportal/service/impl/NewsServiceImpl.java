/*
 * @(#)NewsServiceImpl.java   1.0 2016/02/08
 */
package com.epam.newsportal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsportal.dao.ICommentDao;
import com.epam.newsportal.dao.INewsDao;
import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.INewsService;

/**
 * This class implements methods for News service
 * 
 * @version 1.0 08 February 2016
 * @author Pavel_Hryshyn
 */
@Service("newsService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class NewsServiceImpl implements INewsService {

	@Autowired
	private INewsDao newsDao;
	
	@Autowired
	private ICommentDao commentDao;
	
	@Override
	public List<News> findBetweenPositions(int start, int end)
			throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.findBetweenPositions(start, end);
		} catch (DaoException e) {
			throw new ServiceException("findBetweenPositions() method of NewsService is failed", e);
		}
		return newsList;
	}

	@Override
	public List<News> findNewsWithCriteriaAndPositions(SearchCriteria sc,
			int start, int end) throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.findNewsWithCriteriaAndPositions(sc, start, end);
		} catch (DaoException e) {
			throw new ServiceException("findNewsWithCriteriaAndPositions() method of NewsService is failed", e);
		}
		return newsList;
	}

	@Override
	public News findById(Long newsId) throws ServiceException {
		News news = null;
		try {
			news = newsDao.findById(newsId);
		} catch (DaoException e) {
			throw new ServiceException("findById() method of NewsService is failed", e);
		}
		return news;
	}

	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria sc)
			throws ServiceException {
		long nextNewsId = 0;
		try {
			nextNewsId = newsDao.getNextNewsId(newsId, sc);
		} catch (DaoException e) {
			throw new ServiceException("getNextNewsId() method of NewsService is failed", e);
		}
		return nextNewsId;
	}

	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria sc)
			throws ServiceException {
		Long previousNewsId = 0L;
		try {
			previousNewsId = newsDao.getPreviousNewsId(newsId, sc);
		} catch (DaoException e) {
			throw new ServiceException("getPreviousNewsId() method of NewsService is failed", e);
		}
		return previousNewsId;
	}

	@Override
	public long countAllNews() throws ServiceException {
		long amount = 0;
		try {
			amount = newsDao.countAllNews();
		} catch (DaoException e) {
			throw new ServiceException("countAllNews() method of NewsService is failed", e);
		}
		return amount;
	}

	@Override
	public long countNewsWithCriteria(SearchCriteria searchCriteria)
			throws ServiceException {
		long amount = 0;
		try {
			amount = newsDao.countNewsWithCriteria(searchCriteria);
		} catch (DaoException e) {
			throw new ServiceException("countNewsWithCriteria() method of NewsService is failed", e);
		}
		return amount;
	}

	@Override
	public Long addWithAuthorAndTags(News news, Long authorId, List<Long> tagsId)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateWithAuthorAndTags(News news, Long authorId,
			List<Long> tagsId) throws ServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Long newsId) throws ServiceException {
		// TODO Auto-generated method stub
		return false;
	}

}
