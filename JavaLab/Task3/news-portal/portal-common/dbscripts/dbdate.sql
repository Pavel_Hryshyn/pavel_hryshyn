DECLARE 
testdate TIMESTAMP := SYSTIMESTAMP;
row_count number := 0;

BEGIN 
  LOOP 
    BEGIN 
      insert into news(news_id_pk, news_title, news_short_text, news_full_text, news_creation_date, news_modification_date, news_version) 
      values (news_sequence.nextval, CONCAT('News Title ', to_char(row_count)), CONCAT('News Description ', to_char(row_count)), 
      CONCAT('Test News Full Text ', to_char(row_count)), testdate, testdate, 1); 
      row_count := row_count + 1; 
    END; 
    EXIT WHEN row_count = 10000; 
  END LOOP; 
  COMMIT; 
END;

DECLARE 
testdate TIMESTAMP := SYSTIMESTAMP; 
row_count number := 0; 
news_total number;

BEGIN 
  select count(*) into news_total from NEWS; 
  BEGIN
    for current_news in 1 .. news_total 
      LOOP 
          insert into NEWS_AUTHOR(NEWS_ID_FK, AUTHOR_ID_FK) values ((select NEWS_ID_PK from (select NEWS_ID_PK from NEWS order by dbms_random.random()) where rownum = 1), 
          (select AUTHOR_ID_PK from (select AUTHOR_ID_PK from AUTHOR order by dbms_random.random()) where rownum = 1)); 
      END LOOP; 
     END;
  COMMIT;
END;

DECLARE 
testdate TIMESTAMP := SYSTIMESTAMP; 
row_count number := 0; 
news_total number;

BEGIN
  select count(*) into news_total from NEWS; 
    BEGIN 
    for current_news in 1 .. news_total 
    LOOP 
      LOOP 
      BEGIN 
        insert into NEWS_TAG(NEWS_ID_FK, TAG_ID_FK) values ((select NEWS_ID_PK from (select NEWS_ID_PK from NEWS order by dbms_random.random()) where rownum = 1), 
        (select TAG_ID_PK from (select TAG_ID_PK from TAG order by dbms_random.random()) where rownum = 1)); 
        row_count := row_count + 1; 
      END; 
    EXIT WHEN row_count = 2; 
    END LOOP; 
  row_count := 0; 
  END LOOP; 
  END; 
  COMMIT;
END;