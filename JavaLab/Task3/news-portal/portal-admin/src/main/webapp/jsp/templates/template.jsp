<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html lang="language">
<head>
<meta charset="utf-8">
<title></title>

<link href="<c:url value="/bootstrap/css/style.css"/>" rel="stylesheet">
<link href="<c:url value='/bootstrap/css/ui.dropdownchecklist.standalone.css'></c:url>" rel="stylesheet">
<link href="<c:url value='/bootstrap/css/ui.dropdownchecklist.themeroller.css'></c:url>" rel="stylesheet">
<script src="<c:url value='/bootstrap/js/jquery-1.6.1.min.js'></c:url>"></script>
<script	src="<c:url value='/bootstrap/js/jquery-ui-1.8.13.custom.min.js'/>"></script>
<script src="<c:url value='/bootstrap/js/ui.dropdownchecklist.js'/>"></script>
<script src='<c:url value="/bootstrap/js/switch-enable.js"/>'></script>
<script src='<c:url value="/bootstrap/js/validation.js"/>'></script>

</head>
<body>

	<div class="panel">
		<div class="panel-header">
			<tiles:insertAttribute name="header" />
		</div>

		<div class="panel-body">
			<div class="menu-container">
				<tiles:insertAttribute name="menu" /> 
			</div>
			<div class="content-container">
				<tiles:insertAttribute name="body" />
			</div>				
		</div>

		<div class="panel-footer">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>