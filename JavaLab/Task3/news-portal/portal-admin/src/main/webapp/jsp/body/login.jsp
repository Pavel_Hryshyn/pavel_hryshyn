<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta charset="utf-8">
</head>
<body>
	<spring:message code="Size.login" text="Login must be between 1 and 30 characters" var="badLogin" />
	<spring:message code="Size.password" text="Password must be between 1 and 30 characters" var="badPassword" />

	<c:if test="${errorAttr eq true}">
		<div class="alert alert-danger">
			<spring:message code="body.login.msg.login.password.invalid"
				text="Invalid login or password" />
		</div>
	</c:if>
	<c:if test="${logoutAttr eq true}">
		<div class="alert alert-success">
			<spring:message code="body.login.msg.logout.successful"
				text="Invalid login or password" />
		</div>
	</c:if>

	<c:url value="/login" var="loginUrl" />
	<form class="login-form" action="${loginUrl}" method="post"
		onsubmit="return validate_login_password('${badLogin}', '${badPassword}');">
		<div class="area-edit">
			<label for="login" class="edit-label"><spring:message
					code="body.login.msg.login" text="Login" /></label>
				<input type="text" class="edit-input-short" id="login" name="login"
					placeholder="<spring:message code="body.login.msg.login" text="Login" />" >
		</div>
		<div class="area-edit">
			<label for="password"
				class="edit-label"><spring:message
					code="body.login.msg.password" text="Password" /></label>
				<input type="password" class="edit-input-short" id="password"
					name="password"
					placeholder="<spring:message code="body.login.msg.password" text="Password" />" >
		</div>
		<div class="row-login-btn">
				<input type="submit" class="btn btn-primary btn-lg" value="<spring:message code="button.login" text="Login" />" >				
		</div>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
</body>
</html>