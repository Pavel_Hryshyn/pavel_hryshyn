<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>
	<div class="error-code">
		<spring:message code="body.error403.msg.title" text="Error 403" />
	</div>
	<div class="error-message">
		<spring:message code="body.error403.msg.access.denied" text="Access is denied" />
	</div>
</body>
</html>