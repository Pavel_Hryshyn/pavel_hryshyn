/*
 * @(#)AuthorController.java   1.2 2015/10/12
 */
package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsportal.entity.Author;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.IAuthorService;

/**
 * This class is responsible for processing all authors requests and building
 * appropriate model and passes it to the view for rendering.
 * 
 * @version 1.1 12 October 2015
 * @author Pavel_Hryshyn
 */
@Controller
@RequestMapping("authors")
public class AuthorController {
	
	/* This constant stores the logger instance */
	private static final Logger logger = Logger
			.getLogger(AuthorController.class);

	/* This constant stores the path mapping URIs */
	private static final String PATH_REDIRECT_AUTHORS = "redirect:/authors";

	/* This constant stores the model attribute names */
	private static final String ATR_AUTHORS = "authors";
	private static final String ATR_AUTHOR_ID = "id";
	private static final String ATR_ERROR_AUTHOR_ID = "errorAuthorId";
	private static final String ATR_AUTHOR_FOR_UPDATING = "authorForUpdating";
	private static final String ATR_AUTHOR_FOR_SAVING = "authorForSaving";

	/* This constant stores the view names */
	private static final String VIEW_EDIT_AUTHOR = "editAuthor";
	private static final String VIEW_ERROR = "error";

	@Autowired
	private IAuthorService authorService;

	/**
	 * This method processing GET request to "authors" path mapping URIs, sets
	 * all model attributes and returns "edit authors" view
	 * 
	 * @param model
	 * @return "edit authors" view
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getEditAuthorForm(Model model) {
		List<Author> authors = null;
		try {
			authors = authorService.findAllNonExpiredAuthors();
		} catch (ServiceException e) {
			logger.error("Can't formed author list", e);
			return VIEW_ERROR;
		}
		model.addAttribute(ATR_AUTHORS, authors);
		if (!model.containsAttribute(ATR_AUTHOR_FOR_UPDATING)) {
			model.addAttribute(ATR_AUTHOR_FOR_UPDATING, new Author());
		}
		if (!model.containsAttribute(ATR_AUTHOR_FOR_SAVING)) {
			model.addAttribute(ATR_AUTHOR_FOR_SAVING, new Author());
		}
		return VIEW_EDIT_AUTHOR;
	}

	/**
	 * This method processes POST request to "/save" path mapping URIs, saves
	 * author to database and redirects to "authors" path mapping URIs
	 * 
	 * @param author
	 * @param result
	 *            provides the author validation
	 * @param attr
	 *            flashes the model attributes for a redirect scenario
	 * @param session
	 * @return redirect to "authors" path mapping URIs
	 */
	@RequestMapping(value = { "/save" }, method = RequestMethod.POST)
	public String saveAuthor(
			@Valid @ModelAttribute(ATR_AUTHOR_FOR_SAVING) Author author,
			BindingResult result, RedirectAttributes attr, HttpSession session) {
		String redirectPage = PATH_REDIRECT_AUTHORS;
		long authorId = 0L;
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.authorForSaving",
					result);
			attr.addFlashAttribute(ATR_AUTHOR_FOR_SAVING, author);
			return redirectPage;
		}
		try {
			authorId = authorService.create(author);
			logger.info("Author is saved succesfully. Id = " + authorId);
		} catch (ServiceException e) {
			logger.error("Author is not saved. The reason is: ", e);
			return VIEW_ERROR;
		}
		return redirectPage;
	}

	/**
	 * This method processes POST request to "/update" path mapping URIs,
	 * updates author to database and redirects to "authors" path mapping URIs
	 * 
	 * @param author
	 * @param result
	 *            provides the author validation
	 * @param attr
	 *            flashes the model attributes for a redirect scenario
	 * @param session
	 * @return redirect to "authors" path mapping URIs
	 */
	@RequestMapping(value = { "/update" }, method = RequestMethod.POST)
	public String updateAuthor(
			@Valid @ModelAttribute(ATR_AUTHOR_FOR_UPDATING) Author author,
			BindingResult result, RedirectAttributes attr, HttpSession session) {
		String redirectPage = PATH_REDIRECT_AUTHORS;
		boolean isUpdated = false;
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.authorForUpdating",
					result);
			attr.addFlashAttribute(ATR_AUTHOR_FOR_UPDATING, author);
			attr.addFlashAttribute(ATR_ERROR_AUTHOR_ID, author.getId());
			return redirectPage;
		}
		try {
			isUpdated = authorService.update(author);
			logger.info("Author is updated. Status = " + isUpdated);
		} catch (ServiceException e) {
			logger.error("Author is not updated. The reason is: ", e);
			return VIEW_ERROR;
		}
		return redirectPage;
	}

	/**
	 * This method processes POST request to "/expire/{id}" path mapping URIs,
	 * expires author to database and redirects to "authors" path mapping URIs
	 * 
	 * @param authorId
	 * @return redirect to "authors" path mapping URIs
	 */
	@RequestMapping(value = { "/expire/{id}" }, method = RequestMethod.POST)
	public String expireAuthor(@PathVariable(ATR_AUTHOR_ID) Long authorId) {
		String redirectPage = PATH_REDIRECT_AUTHORS;
		boolean isExpired = false;
		try {
			isExpired = authorService.expire(authorId);
			logger.info("Author is expired. Status = " + isExpired);
		} catch (ServiceException e) {
			logger.error("Author is not expired. The reason is: ", e);
			return VIEW_ERROR;
		}
		return redirectPage;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}
}
