/*
 * @(#)DateFormatValidator.java   1.0 2015/11/06
 */
package com.epam.newsmanagement.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class contains method for date validation
 * 
 * @version 1.0 06 November 2015
 * @author Pavel_Hryshyn
 */
public class DateFormatValidator {

	/* This constant stores date format for validation */
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	private static final String DELIMETER = "/";
	private static final String PATTERN = "(([0-2]{1}[0-9]{1})|([3][0-1]{1}))/(([0][1-9])|([1][0-2]{1}))/([1-2]{1}[0-9]{3})";
	
	/**
	 * This method checks date format
	 * @param dateToValidation
	 * @return true, if date format is correct else return false
	 */
	public boolean checkDateFormat(String dateToValidation){
		boolean isValid = false;
		if (dateToValidation == null) {
			return isValid;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);		
		try {
			sdf.parse(dateToValidation);
			isValid = checkDate(dateToValidation); 
		} catch (ParseException e) {
			return isValid;
		}
		return isValid;
	}
	
	private boolean checkDate(String dateToValidation) throws ParseException {
		boolean isValid = false;
		String[] dateElem = dateToValidation.split(DELIMETER);
		for (String dateStr: dateElem){
			Integer dateInt = Integer.valueOf(dateStr);
			if (dateInt<=0) {
				throw new ParseException("Invalid date", dateInt);
			}			
		}
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(dateToValidation);
		if (matcher.matches()){
			isValid = true;
		}
		return isValid;
	}
}
