/*
 * @(#)ErrorHandlingController.java   1.2 2015/10/12
 */
package com.epam.newsmanagement.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class is responsible for processing all error requests and building
 * appropriate model and passes it to the view for rendering.
 * 
 * @version 1.2 12 October 2015
 * @author Pavel_Hryshyn
 */
@Controller
public class ErrorHandlingController {
	
	/* This constant stores the logger instance */
	private static final Logger logger = Logger
			.getLogger(ErrorHandlingController.class);

	/* This constant stores the path mapping URIs */
	private static final String PATH_REDIRECT_ERROR = "redirect:/error";

	/* This constant stores the view names */
	private static final String VIEW_ERROR = "error";
	private static final String VIEW_ERROR_403 = "error403";
	private static final String VIEW_ERROR_404 = "error404";

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied() {
		return VIEW_ERROR_403;
	}

	@RequestMapping(value = "/404", method = RequestMethod.GET)
	public String pageNotFound() {
		return VIEW_ERROR_404;
	}

	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String getPage(HttpServletRequest req) {
		return VIEW_ERROR;
	}

	@ExceptionHandler(Exception.class)
	public String get(HttpServletRequest req, Exception e) {
		logger.error(e);
		return PATH_REDIRECT_ERROR;
	}
}
