/*
 * @(#)NewsController.java   1.3 2015/10/13
 */
package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.utils.date.DateFormatValidator;
import com.epam.newsmanagement.utils.math.PaginationCalculator;
import com.epam.newsportal.entity.Author;
import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.entity.vo.NewsComplexVO;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.IAuthorService;
import com.epam.newsportal.service.ICommentService;
import com.epam.newsportal.service.INewsService;
import com.epam.newsportal.service.IServiceManager;
import com.epam.newsportal.service.ITagService;

/**
 * This class is responsible for processing all news requests and building
 * appropriate model and passes it to the view for rendering.
 * 
 * @version 1.3 13 October 2015
 * @author Pavel_Hryshyn
 */
@Controller("newsController")
@SessionAttributes({ "searchCriteria" })
public class NewsController {
	
	/* This constant stores the logger instance */
	private static final Logger logger = Logger.getLogger(NewsController.class);

	/* This constant stores the path mapping URIs */
	private static final String PATH_REDIRECT_NEWS_EDIT = "redirect:/news/edit/";
	private static final String PATH_REDIRECT_NEWS_NEW = "redirect:/news/new";
	private static final String PATH_REDIRECT_NEWS_VIEW = "redirect:/news/";
	private static final String PATH_REDIRECT_NEWSLIST = "redirect:/newslist";

	/* This constants store names of model attributes and request parameters */
	private static final String ATR_PAGE = "page";
	private static final String ATR_PAGE_NUMBER = "pageNumber";
	private static final String ATR_PAGE_CURRENT = "currentPage";
	private static final String ATR_DATE_CREATION = "creationDate";
	private static final String ATR_NEWS_ID = "id";
	private static final String ATR_NEWS = "news";
	private static final String ATR_NEWS_IDS_FOR_DELETING = "newsIdForDeleting";
	private static final String ATR_NEWS_NEXT = "nextNews";
	private static final String ATR_NEWS_PREVIOUS = "previousNews";
	private static final String ATR_NEWS_COMPLEX = "newsComplex";
	private static final String ATR_NEWS_COMPLEX_LIST = "newsComplexList";
	private static final String ATR_NEWS_LIST_EMPTY = "emptyNewsList";
	private static final String ATR_COMMENT = "comment";
	private static final String ATR_AUTHORS = "authors";
	private static final String ATR_AUTHOR_ID_FOR_NEWS = "authorIdForNews";
	private static final String ATR_AUTHOR_ID_FROM_FILTER = "authorIdFromFilter";
	private static final String ATR_AUTHOR_ID_SELECTED = "selectedAuthorId";
	private static final String ATR_TAGS = "tags";
	private static final String ATR_TAGS_ID_FOR_NEWS = "tagsIdForNews";
	private static final String ATR_TAGS_ID_SELECTED = "selectedTagsId";
	private static final String ATR_TAGS_ID_FROM_FILTER = "tagsIdFromFilter";
	private static final String ATR_SEARCH_CRITERIA = "searchCriteria";

	/* This constants store values for pagination */
	private static final int INIT_PAGE_NUMBER = 1;
	private static final int INIT_CURRENT_PAGE = 1;
	
	/* This constant stores the view names */
	private static final String VIEW_ERROR = "error";
	private static final String VIEW_ERROR_404 = "error404";
	private static final String VIEW_NEWS = "news";
	private static final String VIEW_EDIT_NEWS = "editNews";
	private static final String VIEW_NEWSLIST = "newslist";
	
	@Value("${jsp_news_per_page}")
	private int NEWS_PER_PAGE;
	
	@Autowired
	private IServiceManager serviceManager;
	
	@Autowired
	private INewsService newsService;
	
	@Autowired
	private IAuthorService authorService;
	
	@Autowired
	private ITagService tagService;
	
	@Autowired
	private ICommentService commentService;

	/**
	 * This method processing GET request to "/" and "/newslist" path mapping
	 * URIs, sets all model attributes and returns "newslist" view
	 * 
	 * @param page
	 *            defines page in news list
	 * @param sc
	 *            defines search criteria for news
	 * @return "newslist" view
	 */
	@RequestMapping(value = { "/", "/newslist**" }, method = RequestMethod.GET)
	public ModelAndView getNewsListPage(
			@RequestParam(value = ATR_PAGE, required = false) String page,
			@ModelAttribute(ATR_SEARCH_CRITERIA) SearchCriteria sc) {
		int pageNumber = INIT_PAGE_NUMBER;
		int currentPage = INIT_CURRENT_PAGE;
		int newsPerPage = NEWS_PER_PAGE;
		long countNews = 0;
		int start = 0;
		int end = 0;
		List<NewsComplexVO> newsComplexList = null;
		List<Author> authors = null;
		List<Tag> tags = null;
		Map<Long, Boolean> selectedTagsId = null;
		ModelAndView model = new ModelAndView();
		if (page != null) {
			currentPage = Integer.parseInt(page);
		}
		try {
			countNews = newsService.countNewsWithCriteria(sc);
			pageNumber = PaginationCalculator.calculateNumberOfPages(countNews,
					newsPerPage);
			start = PaginationCalculator.calculateStartPosition(currentPage,
					newsPerPage);
			end = PaginationCalculator.calculateEndPosition(currentPage,
					newsPerPage);
			
			newsComplexList = serviceManager.findNewsComplexWithCriteriaAndPositions(sc, start, end);
			if (newsComplexList.isEmpty()){
				model.addObject(ATR_NEWS_LIST_EMPTY, true);
			}
			authors = authorService.findAll();
			tags = tagService.findAll();			
		} catch (ServiceException e) {
			logger.error("Can't formed news list", e);
			model.setViewName(VIEW_ERROR);
			return model;
		}
		selectedTagsId = setSelectedTags(sc);
		model.addObject(ATR_TAGS_ID_SELECTED, selectedTagsId);
		model.addObject(ATR_PAGE_NUMBER, pageNumber);
		model.addObject(ATR_PAGE_CURRENT, currentPage);
		model.addObject(ATR_AUTHORS, authors);
		model.addObject(ATR_TAGS, tags);
		model.addObject(ATR_NEWS_COMPLEX_LIST, newsComplexList);
		model.setViewName(VIEW_NEWSLIST);
		return model;
	}

	/**
	 * This method processing POST request to "/filter" path mapping URIs, sets
	 * search criteria to session and redirect to "/newslist"
	 * 
	 * @param authorId
	 * @param tagIds
	 * @param sc
	 *            defines search criteria for news
	 * @return redirect to "/newslist"
	 */
	@RequestMapping(value = { "/filter" }, method = RequestMethod.POST)
	public String setFilterForNewsList(
			@RequestParam(value = ATR_AUTHOR_ID_FROM_FILTER, required = false) Long authorId,
			@RequestParam(value = ATR_TAGS_ID_FROM_FILTER, required = false) List<String> tagIds,
			@ModelAttribute(ATR_SEARCH_CRITERIA) SearchCriteria sc) {
		try {
			buildSearchCriteria(sc, authorId, tagIds);
		} catch (ServiceException e) {
			logger.error("The search criteria building is failed. The reason is: ", e);
			return VIEW_ERROR;
		}
		return PATH_REDIRECT_NEWSLIST;
	}

	/**
	 * This method processing POST request to "/reset" path mapping URIs, reset
	 * search criteria to session and redirect to "/newslist"
	 * 
	 * @param sc
	 *            defines search criteria for news
	 * @return redirect to "/newslist"
	 */
	@RequestMapping(value = { "/reset" }, method = RequestMethod.POST)
	public String resetFilterForNewsList(
			@ModelAttribute(ATR_SEARCH_CRITERIA) SearchCriteria sc) {
		sc.setAuthor(null);
		sc.setTags(null);
		return PATH_REDIRECT_NEWSLIST;
	}

	/**
	 * This method processing GET request to "/news/{id}" path mapping URIs,
	 * sets all model attributes and returns "news" view
	 * 
	 * @param newsId
	 * @param sc
	 *            defines search criteria for news
	 * @param model
	 * @return "news" view
	 */
	@RequestMapping(value = { "/news/{id}" }, method = RequestMethod.GET)
	public String getNews(@PathVariable(ATR_NEWS_ID) Long newsId,
			@ModelAttribute(ATR_SEARCH_CRITERIA) SearchCriteria sc, Model model) {
		NewsComplexVO newsComplex = null;
		Long previousNews = null;
		Long nextNews = null;
		try {
			newsComplex = serviceManager.findByNewsId(newsId);		
			if (newsComplex == null) {
				return VIEW_ERROR_404;
			}	
			previousNews = newsService.getPreviousNewsId(newsId, sc);
			nextNews = newsService.getNextNewsId(newsId, sc);
		} catch (ServiceException e) {
			logger.error("The page of news view can't formed. The reason is: ", e);
			return VIEW_ERROR;
		}
		model.addAttribute(ATR_NEWS_COMPLEX, newsComplex);
		model.addAttribute(ATR_NEWS_NEXT, nextNews);
		model.addAttribute(ATR_NEWS_PREVIOUS, previousNews);
		if (!model.containsAttribute(ATR_COMMENT)) {
			model.addAttribute(ATR_COMMENT, new Comment());
		}
		return VIEW_NEWS;
	}

	/**
	 * This method processing GET request to "news/new" path mapping URIs, sets
	 * all model attributes and returns "edit news" view
	 *
	 * @param model
	 * @return "news" view
	 */
	@RequestMapping(value = { "news/new" }, method = RequestMethod.GET)
	public String getAddNewsForm(Model model) {
		List<Author> authors = null;
		List<Tag> tags = null;
		News news = null;
		try {
			authors = authorService.findAll();
			tags = tagService.findAll();
		} catch (ServiceException e) {
			logger.error("The page of news view can't formed. The reason is: ", e);
			return VIEW_ERROR;
		}
		if (!model.containsAttribute(ATR_NEWS)) {
			news = new News();
			news.setCreationDate(new Date());
			model.addAttribute(ATR_NEWS, news);
		}
		model.addAttribute(ATR_AUTHORS, authors);
		model.addAttribute(ATR_TAGS, tags);
		return VIEW_EDIT_NEWS;
	}

	/**
	 * This method processes POST request to "news/edit/{id}" path mapping URIs,
	 * saved or update news to database and redirects to news view
	 * 
	 * @param news
	 * @param result
	 *            provides the author validation
	 * @param newsId
	 * @param authorId
	 * @param tagIds
	 * @param attr
	 *            flashes the model attributes for a redirect scenario
	 * @param session
	 * @return redirect to news view
	 */
	@RequestMapping(value = { "news/edit/{id}" }, method = RequestMethod.POST)
	public String createNews(
			@Valid @ModelAttribute(ATR_NEWS) News news,
			BindingResult result,
			@PathVariable(ATR_NEWS_ID) Long newsId,
			@RequestParam(value = ATR_DATE_CREATION, required = true) String creationDate,
			@RequestParam(value = ATR_AUTHOR_ID_FOR_NEWS, required = true) Long authorId,
			@RequestParam(value = ATR_TAGS_ID_FOR_NEWS, required = false) List<String> tagIds,
			RedirectAttributes attr, HttpSession session) {
		List<Long> tagIdsLong = null;
		String redirectPage = null;
		Long id = 0L;
		DateFormatValidator validator = new DateFormatValidator();
		if (tagIds != null) {
			tagIdsLong = new ArrayList<Long>();
			for (String tagIdString : tagIds) {
				tagIdsLong.add(Long.valueOf(tagIdString));
			}
		}
		if (result.hasErrors()) {
			redirectPage = PATH_REDIRECT_NEWS_EDIT + newsId;
			Map<Long, Boolean> selectedTags = new HashMap<Long, Boolean>();
			if (tagIdsLong != null) {
				for (Long longTagId : tagIdsLong) {
					selectedTags.put(longTagId, true);
				}
			}
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.news", result);
			if (!validator.checkDateFormat(creationDate)) {
				attr.addFlashAttribute(ATR_DATE_CREATION, true);
			}
			attr.addFlashAttribute(ATR_NEWS, news);
			attr.addFlashAttribute(ATR_AUTHOR_ID_SELECTED, authorId);
			attr.addFlashAttribute(ATR_TAGS_ID_SELECTED, selectedTags);
			if (newsId == 0) {
				redirectPage = PATH_REDIRECT_NEWS_NEW;
			}
			return redirectPage;
		}
		try {
			if (newsId == 0) {
				news.setModificationDate(news.getCreationDate());
				id = newsService.addWithAuthorAndTags(news, authorId, tagIdsLong);
				logger.info("News is saved succesfully. Id = " + id);
			} else {
				news.setModificationDate(new Date());
				newsService.updateWithAuthorAndTags(news, authorId, tagIdsLong);
				id = newsId;
				logger.info("News is updated succesfully. Id = " + id);
			}
		} catch (ServiceException e) {
			logger.error("News is not saved or updated. The reason is: ", e);
			return VIEW_ERROR;
		}
		return PATH_REDIRECT_NEWS_VIEW + id;
	}

	/**
	 * This method processes GET request to "news/edit/{id}" path mapping URIs,
	 * sets all model attributes and returns "edit news" view
	 * 
	 * @param newsId
	 * @param model
	 * @return "edit news" view
	 */
	@RequestMapping(value = { "news/edit/{id}" }, method = RequestMethod.GET)
	public String getEditNewsForm(@PathVariable(ATR_NEWS_ID) Long newsId,
			Model model) {
		List<Author> authors = null;
		List<Tag> tags = null;
		News news = null;
		Author selectedAuthor = null;
		List<Tag> selectedTags = null;
		Map<Long, Boolean> selectedTagIds = new HashMap<Long, Boolean>();
		try {
			authors = authorService.findAll();
			tags = tagService.findAll();
			news = newsService.findById(newsId);
			selectedAuthor = authorService.findByNewsId(newsId);
 			selectedTags = tagService.findByNewsId(newsId);
		} catch (ServiceException e) {
			logger.error("The page of edit news view can't formed. The reason is: ", e);
			return VIEW_ERROR;
		}
		if (!selectedTags.isEmpty()) {
 			for (Tag selectedTag : selectedTags) {
 				selectedTagIds.put(selectedTag.getId(), true);
 			}
 		}
		if (!model.containsAttribute(ATR_NEWS)) {
			model.addAttribute(ATR_NEWS, news);
		}
		model.addAttribute(ATR_AUTHORS, authors);
		model.addAttribute(ATR_AUTHOR_ID_SELECTED, selectedAuthor.getId());
		model.addAttribute(ATR_TAGS, tags);
		model.addAttribute(ATR_TAGS_ID_SELECTED, selectedTagIds);
		return VIEW_EDIT_NEWS;
	}

	@RequestMapping(value = { "/delete" }, method = RequestMethod.POST)
	public String delete(
			@RequestParam(value = ATR_NEWS_IDS_FOR_DELETING, required = false) List<Long> deletedNewsIds) {
		boolean isDeleted = false;
		if (deletedNewsIds != null) {
			for (Long deletedId : deletedNewsIds) {
				try {
					isDeleted = newsService.delete(deletedId);
					logger.info("News is deleted. Id = " + deletedId + "Status = "
							+ isDeleted);
				} catch (ServiceException e) {
					logger.error("News is not deleted. The reason is: ", e);
					return VIEW_ERROR;
				}
			}
		}
		return PATH_REDIRECT_NEWSLIST;
	}

	@ModelAttribute("searchCriteria")
	private SearchCriteria getSearchCriteria() {
		SearchCriteria searchCriteria = new SearchCriteria();
		return searchCriteria;
	}

	private SearchCriteria buildSearchCriteria(SearchCriteria sc,
			Long authorId, List<String> tagIds) throws ServiceException {
		if (authorId > 0) {
			Author author = authorService.findById(authorId);
			sc.setAuthor(author);
		}
		sc.setTags(null);
		if (tagIds != null) {
			List<Tag> tags = new ArrayList<Tag>();
			for (String tagId : tagIds) {
				tags.add(tagService.findById(Long.parseLong(tagId)));
			}
			sc.setTags(tags);
		}
		return sc;
	}
	
	private Map<Long, Boolean> setSelectedTags(SearchCriteria sc){
		Map<Long, Boolean> selectedTags = new HashMap<Long, Boolean>();
		List<Tag> tags = null; 
		if (sc != null && sc.getTags()!= null) {
			tags = sc.getTags();
			for (Tag tag: tags){
				selectedTags.put(tag.getId(), true);
			}
		}
		return selectedTags;
	}

	public IServiceManager getServiceManager() {
		return serviceManager;
	}

	public void setServiceManager(IServiceManager serviceManager) {
		this.serviceManager = serviceManager;
	}

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public void setNEWS_PER_PAGE(int news_per_page) {
		NEWS_PER_PAGE = news_per_page;
	}
}
