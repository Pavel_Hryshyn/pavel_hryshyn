/*
 * @(#)AuthenticationController.java   1.1 2015/10/12
 */
package com.epam.newsmanagement.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * This class is responsible for building model of login page and passes it to
 * the view for rendering.
 * 
 * @version 1.1 12 October 2015
 * @author Pavel_Hryshyn
 */
@Controller
public class AuthenticationController {
	
	/* This constant stores the logger instance */
	private static final Logger logger = Logger
			.getLogger(AuthenticationController.class);

	/* This constant stores the path mapping URIs */
	private static final String PAGE_LOGIN_ERROR = "error";
	private static final String PAGE_LOGIN_LOGOUT = "logout";

	/* This constant stores the model attribute names */
	private static final String ATR_ERROR = "errorAttr";
	private static final String ATR_LOGOUT = "logoutAttr";

	/* This constant stores the view names */
	private static final String VIEW_LOGIN = "login";

	/**
	 * This method sets all model attributes and returns login page view
	 * @param error
	 * 				sets true if login or password are incorrect
	 * @param logout
	 * 				sets true if user session is finished
	 * @return login page view
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = PAGE_LOGIN_ERROR, required = false) String error,
			@RequestParam(value = PAGE_LOGIN_LOGOUT, required = false) String logout) {
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject(ATR_ERROR, true);
			logger.info("Invalid username or password");
		}
		if (logout != null) {
			model.addObject(ATR_LOGOUT, true);
			logger.info("Session is finished");
		}
		model.setViewName(VIEW_LOGIN);
		return model;
	}
}
