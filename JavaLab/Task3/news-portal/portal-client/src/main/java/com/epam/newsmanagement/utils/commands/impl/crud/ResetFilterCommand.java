/*
 * @(#)ResetFilterCommand.java   1.0 2015/09/14
 */
package com.epam.newsmanagement.utils.commands.impl.crud;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.utils.commands.ICommand;
import com.epam.newsportal.exception.ServiceException;

/**
 * This class implements command pattern, resets author and tags filter and gets
 * list of news view
 * 
 * @version 1.0 14 September 2015
 * @author Pavel_Hryshyn
 */
public class ResetFilterCommand implements ICommand {
	
	/* This constants store pathname to jsp pages */
	private static final String PATH_NEWS_LIST = "/news?command=NEWS_LIST";
	
	/* This constants store names of request parameters */
	private static final String SESSION_AUTHOR = "sessionAuthor";
	private static final String SESSION_TAGS = "sessionTags";

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = null;
		request.getSession().setAttribute(SESSION_AUTHOR, null);
		request.getSession().setAttribute(SESSION_TAGS, null);
		page = PATH_NEWS_LIST;
		return page;
	}
}
