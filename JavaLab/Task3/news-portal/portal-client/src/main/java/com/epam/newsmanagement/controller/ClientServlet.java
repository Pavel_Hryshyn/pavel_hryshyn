/*
 * @(#)ClientServlet.java   1.0 2015/09/07
 */
package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.utils.commands.CommandFactory;
import com.epam.newsmanagement.utils.commands.ICommand;
import com.epam.newsmanagement.utils.loaders.BeanLoader;
import com.epam.newsportal.exception.ServiceException;


/**
 * This servlet handles client requests and responses
 * 
 * @version 1.0 07 September 2015
 * @author Pavel_Hryshyn
 */
@WebServlet(urlPatterns = { "/newslist", "/news" })
public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/* This object obtains logger for this class */
	private static final Logger logger = Logger.getLogger(ClientServlet.class);

	/* This constants store path to jsp pages */
	private static final String PATH_PAGE_ERROR_404 = "/jsp/error/error404.jsp";
	
	/* This constants store names of parameters */
	private static final String COMMAND = "command";

	/* This constants store path to error pages */
	private static final String REDIRECT_PAGE_ERROR_404 = "/news?command=ERROR_404";
	private static final String REDIRECT_PAGE_ERROR = "/news?command=ERROR";

	/**
	 * This method creates beans from spring context at same time initializing
	 * servlet
	 * 
	 * @throws ServletException
	 */
	@Override
	public void init() throws ServletException {
		BeanLoader.getInstance().initialize();
		super.init();
	}

	/**
	 * This method closes spring application context, destroying all beans in
	 * its bean factory at same time destroying servlet
	 * 
	 * @throws ServletException
	 */
	@Override
	public void destroy() {
		BeanLoader.getInstance().destroy();
		super.destroy();
	}

	/**
	 * This method handles GET requests
	 * 
	 * @param req
	 *            provides request information for HTTP servlets
	 * @param resp
	 *            provides response information to client
	 * @throws ServletException
	 *             , if the request for the GET could not be handled
	 * @throws IOException
	 *             , if an input or output error is detected when the servlet
	 *             handles the GET request
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp, false);
	}

	/**
	 * This method handles POST requests
	 * 
	 * @param req
	 *            provides request information for HTTP servlets
	 * @param resp
	 *            provides response information to client
	 * @throws ServletException
	 *             , if the request for the POST could not be handled
	 * @throws IOException
	 *             , if an input or output error is detected when the servlet
	 *             handles the POST request
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp, true);
	}
	
	private void processRequest(HttpServletRequest req, HttpServletResponse resp, boolean isPostMethod)
			throws ServletException, IOException {
		String page = null;
		String commandName = req.getParameter(COMMAND);
		CommandFactory client = CommandFactory.getInstance();
		ICommand command = client.getCommand(commandName);
		try {
			page = command.execute(req);
			if (isPostMethod){
				checkPageNotFound(page, isPostMethod);
				resp.sendRedirect(req.getContextPath() + page);
			} else {
				checkPageNotFound(page, isPostMethod);
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
				dispatcher.forward(req, resp);
			}
		} catch (ServiceException e) {
			logger.error("Error of executing command", e);
			page = REDIRECT_PAGE_ERROR;
			resp.sendRedirect(req.getContextPath() + page);
		}
	}	
	
	private String checkPageNotFound(String page, boolean isPostMethod){
		if (page == null){
			logger.debug("Page is not found");
			if (isPostMethod){
				page = REDIRECT_PAGE_ERROR_404;
			} else {
				page = PATH_PAGE_ERROR_404;
			}
		}
		return page;
	}
}
