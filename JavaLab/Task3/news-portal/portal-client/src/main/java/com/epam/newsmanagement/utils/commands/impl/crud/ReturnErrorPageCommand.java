/*
 * @(#)RedirectToErrorPageCommand.java   1.0 2015/10/30
 */
package com.epam.newsmanagement.utils.commands.impl.crud;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.utils.commands.ICommand;
import com.epam.newsportal.exception.ServiceException;

/**
 * This class implements command pattern and return ERROR page
 * 
 * @version 1.0 30 October 2015
 * @author Pavel_Hryshyn
 */
public class ReturnErrorPageCommand implements ICommand {

	/* This constants store pathname to jsp pages */
	private static final String PATH_PAGE_ERROR = "/jsp/error/error.jsp";

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		return PATH_PAGE_ERROR;
	}
}
