/*
 * @(#)FilterCommand.java   1.3 2015/09/15
 */
package com.epam.newsmanagement.utils.commands.impl.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.utils.commands.ICommand;
import com.epam.newsmanagement.utils.jsp.i18n.MessageManager;
import com.epam.newsmanagement.utils.loaders.BeanLoader;
import com.epam.newsmanagement.utils.math.PaginationCalculator;
import com.epam.newsportal.entity.Author;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.entity.vo.NewsComplexVO;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.IAuthorService;
import com.epam.newsportal.service.INewsService;
import com.epam.newsportal.service.IServiceManager;
import com.epam.newsportal.service.ITagService;

/**
 * This class implements command pattern gets list of news using filter
 * 
 * @version 1.3 15 September 2015
 * @author Pavel_Hryshyn
 */
public class NewsListCommand implements ICommand {
	
	/* This constants store pathname to jsp pages */
	private static final String PATH_NEWS_LIST = "/jsp/news/newslist.jsp";

	/* This constants store number of news per page */
	private static final int DEFAULT_NEWS_PER_PAGE = 3;
	private static final String NEWS_PER_PAGE = "jsp.news.per.page";

	/* This constants store names of request parameters */
	private static final String PAGE_NUMBER = "pageNumber";
	private static final String CURRENT_PAGE = "currentPage";
	private static final String PAGE = "page";
	private static final String NEWS_COMPLEX_LIST = "newsComplexList";
	private static final String AUTHORS = "authors";
	private static final String TAGS = "tags";
	private static final String SELECTED_TAGS = "selectedTags";
	private static final String SESSION_AUTHOR = "sessionAuthor";
	private static final String SESSION_TAGS = "sessionTags";
	private static final String NEWSLIST_EMPTY = "listEmpty";

	private IServiceManager serviceManager;
	private IAuthorService authorService;
	private ITagService tagService;
	private INewsService newsService;

	public NewsListCommand() {
		super();
		this.serviceManager = BeanLoader.getInstance().getServiceManager();
		this.authorService = serviceManager.getAuthorService();
		this.tagService = serviceManager.getTagService();
		this.newsService = serviceManager.getNewsService();
	}

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		int pageNumber = 1;
		int currentPage = 1;
		long countNews = 0;
		int start = 0;
		String page = null;
		SearchCriteria sc = null;
		List<News> newsList = null;
		List<Author> authors = null;
		List<Tag> tags = null;
		NewsComplexVO newsComplex = null;
		Map<Long, Boolean> selectedTags = null;
		List<NewsComplexVO> newsComplexList = new ArrayList<NewsComplexVO>();
		int newsPerPage = getNewsPerPage();
		if (request.getParameter(PAGE) != null) {
			currentPage = Integer.parseInt(request.getParameter(PAGE));
		}
		sc = buildSearchCriteriaFromRequest(request);
		selectedTags = setSelectedTags(sc);
		countNews = newsService.countNewsWithCriteria(sc);
		start = PaginationCalculator.calculateStartPosition(currentPage,
				newsPerPage);
		pageNumber = PaginationCalculator.calculateNumberOfPages(countNews,
				newsPerPage);
		newsList = newsService.findNewsWithCriteriaAndPositions(sc, start, newsPerPage);
		if (newsList.isEmpty()) {
			request.setAttribute(NEWSLIST_EMPTY, true);
		} else {
			for (News news : newsList) {
				newsComplex = serviceManager.findByNewsId(news.getId());
				newsComplexList.add(newsComplex);				
			}
		}
		authors = authorService.findAll();
		tags = tagService.findAll();
		request.setAttribute(NEWS_COMPLEX_LIST, newsComplexList);
		request.setAttribute(PAGE_NUMBER, pageNumber);
		request.setAttribute(CURRENT_PAGE, currentPage);
		request.setAttribute(AUTHORS, authors);
		request.setAttribute(TAGS, tags);
		request.setAttribute(SELECTED_TAGS, selectedTags);
		page = PATH_NEWS_LIST;
		return page;
	}

	@SuppressWarnings("unchecked")
	private SearchCriteria buildSearchCriteriaFromRequest(
			HttpServletRequest request) {
		/*
		 * This method builds SearchCriteria object from request
		 */
		SearchCriteria searchCriteria = new SearchCriteria();
		Author authorFromSession = (Author) request.getSession().getAttribute(
				SESSION_AUTHOR);
		List<Tag> tagsFromSession = (List<Tag>) request.getSession()
				.getAttribute(SESSION_TAGS);
		searchCriteria.setAuthor(authorFromSession);
		searchCriteria.setTags(tagsFromSession);
		return searchCriteria;
	}

	private int getNewsPerPage() {
		/*
		 * This method gets number of news per page from .properties file and if
		 * this value doesn't parse or missing in file return
		 * DEFAULT_NEWS_PER_PAGE
		 */
		int newsPerPage = 0;
		try {
			newsPerPage = Integer.parseInt(MessageManager
					.getValue(NEWS_PER_PAGE));
			if (newsPerPage < 0) {
				newsPerPage = DEFAULT_NEWS_PER_PAGE;
			}
		} catch (NumberFormatException | MissingResourceException e) {
			return DEFAULT_NEWS_PER_PAGE;
		}
		return newsPerPage;
	}
	
	private Map<Long, Boolean> setSelectedTags(SearchCriteria sc){
		Map<Long, Boolean> selectedTags = new HashMap<Long, Boolean>();
		List<Tag> tags = null; 
		if (sc != null && sc.getTags()!= null) {
			tags = sc.getTags();
			for (Tag tag: tags){
				selectedTags.put(tag.getId(), true);
			}
		}
		return selectedTags;
	}
}
