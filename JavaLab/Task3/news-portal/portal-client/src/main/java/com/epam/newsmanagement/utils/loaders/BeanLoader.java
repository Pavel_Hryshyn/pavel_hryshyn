/*
 * @(#)BeanLoader.java   1.0 2015/09/10
 */
package com.epam.newsmanagement.utils.loaders;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsportal.service.IServiceManager;

/**
 * This class contains methods for lifecycles managing of spring context
 * 
 * @version 1.0 10 September 2015
 * @author Pavel_Hryshyn
 */
public class BeanLoader {
	
	private final static BeanLoader INSTANCE = new BeanLoader();

	private ClassPathXmlApplicationContext ctx;
	
	private IServiceManager serviceManager;

	private BeanLoader() {

	}

	/**
	 * This methods creates beans from application context
	 */
	public void initialize() {
		ctx = new ClassPathXmlApplicationContext("spring/hibernateBeans.xml");
		serviceManager = (IServiceManager) ctx.getBean("serviceManager");
	}

	/**
	 * This method closes spring application context, destroying all beans in
	 * its bean factory
	 */
	public void destroy() {
		ctx.close();
	}

	/**
	 * This method gets instance of BeanLoader
	 * 
	 * @return instance of BeanLoader
	 */
	public static BeanLoader getInstance() {
		return INSTANCE;
	}

	public IServiceManager getServiceManager() {
		return serviceManager;
	}

	public void setServiceManager(IServiceManager serviceManager) {
		this.serviceManager = serviceManager;
	}
}
