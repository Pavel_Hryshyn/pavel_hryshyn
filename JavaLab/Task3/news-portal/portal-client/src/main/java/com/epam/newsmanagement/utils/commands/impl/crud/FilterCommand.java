/*
 * @(#)FilterCommand.java   1.0 2015/09/14
 */
package com.epam.newsmanagement.utils.commands.impl.crud;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.utils.commands.ICommand;
import com.epam.newsmanagement.utils.loaders.BeanLoader;
import com.epam.newsportal.entity.Author;
import com.epam.newsportal.entity.SearchCriteria;
import com.epam.newsportal.entity.Tag;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.IAuthorService;
import com.epam.newsportal.service.ITagService;
import com.epam.newsportal.service.IServiceManager;

/**
 * This class implements command pattern, sets author and tags filter and gets
 * list of news view
 * 
 * @version 1.0 14 September 2015
 * @author Pavel_Hryshyn
 */
public class FilterCommand implements ICommand {
	
	/* This constants store pathname to jsp pages */
	private static final String PATH_NEWS_LIST = "/news?command=NEWS_LIST";
	
	/* This constants store names of request parameters */
	private static final String SESSION_AUTHOR = "sessionAuthor";
	private static final String SESSION_TAGS = "sessionTags";
	private static final String AUTHOR_ID_FROM_FILTER = "authorIdFromFilter";
	private static final String TAGS_ID_FROM_FILTER = "tagsIdFromFilter";
	
	private IServiceManager serviceManager;
	private IAuthorService authorService;
	private ITagService tagService;

	public FilterCommand() {
		super();
		this.serviceManager = BeanLoader.getInstance().getServiceManager();
		this.authorService = serviceManager.getAuthorService();
		this.tagService = serviceManager.getTagService();
	}

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = null;
		SearchCriteria sc = new SearchCriteria();
		long authorIdfromFilter = Long.parseLong(request
				.getParameter(AUTHOR_ID_FROM_FILTER));
		String[] tagsIdFromFilter = request
				.getParameterValues(TAGS_ID_FROM_FILTER);
		setSearchCriteria(sc, authorIdfromFilter, tagsIdFromFilter);
		request.getSession().setAttribute(SESSION_AUTHOR, sc.getAuthor());
		request.getSession().setAttribute(SESSION_TAGS, sc.getTags());
		page = PATH_NEWS_LIST;
		return page;
	}
	
	private void setSearchCriteria(SearchCriteria sc, Long authorId,
			String[] tagsIdFromFilter) throws ServiceException {
		/*
		 * This methods checks values of authorId and tagsIdFromFilter and sets its
		 * to searchCriteria
		 */
		if (authorId > 0) {
			Author author = authorService.findById(authorId);
			sc.setAuthor(author);
		}
		if (tagsIdFromFilter != null) {
			List<Tag> tags = new ArrayList<Tag>();
			for (String tagIdFromFilter : tagsIdFromFilter) {
				long tagId = Long.parseLong(tagIdFromFilter);
				tags.add(tagService.findById(tagId));
			}
			sc.setTags(tags);
		}
	}
}
