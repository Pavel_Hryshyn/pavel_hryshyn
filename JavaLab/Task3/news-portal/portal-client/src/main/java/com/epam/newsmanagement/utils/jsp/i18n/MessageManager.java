/*
 * @(#)MessageManager.java   1.0 2015/09/07
 */
package com.epam.newsmanagement.utils.jsp.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This class contains methods that get messages that using for application
 * internationalization from file
 * 
 * @version 1.0 07 September 2015
 * @author Pavel_Hryshyn
 */
public class MessageManager {
	
	/* This constants store path in resources folder to file with i18n messages */
	private final static String MESSAGE_I18_PATH = "i18n/messages";

	/* This constant stores value of default locale */
	private final static String EN = "en";

	private static Locale locale = new Locale(EN);

	/* This field is a resource bundle */
	private static ResourceBundle bundle = ResourceBundle.getBundle(
			MESSAGE_I18_PATH, locale);

	/**
	 * This method gets a string for the given key.
	 * 
	 * @param key
	 *            is key for searching string from file
	 * @return string
	 */
	public static String getValue(String key) {
		return bundle.getString(key);
	}

	public static void setBundle(ResourceBundle bundle) {
		MessageManager.bundle = bundle;
	}

	public static void setLocale(Locale locale) {
		MessageManager.locale = locale;
	}
}
