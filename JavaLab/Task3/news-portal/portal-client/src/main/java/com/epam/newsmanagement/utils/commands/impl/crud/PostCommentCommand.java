/*
 * @(#)PostCommentCommand.java   1.0 2015/09/10
 */
package com.epam.newsmanagement.utils.commands.impl.crud;

import java.util.Date;
import java.util.MissingResourceException;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.utils.commands.ICommand;
import com.epam.newsmanagement.utils.jsp.i18n.MessageManager;
import com.epam.newsmanagement.utils.loaders.BeanLoader;
import com.epam.newsportal.entity.Comment;
import com.epam.newsportal.entity.News;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.ICommentService;
import com.epam.newsportal.service.INewsService;
import com.epam.newsportal.service.IServiceManager;

/**
 * This class implements command pattern and create comment for news
 * 
 * @version 1.0 10 September 2015
 * @author Pavel_Hryshyn
 */
public class PostCommentCommand implements ICommand {
	
	/* This constants store pathname to jsp pages */
	private static final String PATH_NEWS = "/news?command=GET_NEWS_BY_ID&newsId=";
	private static final String PATH_EMPTY_COMMENT = "&error=true";

	
	/* This constants store names of request parameters */
	private static final String PARAM_NEWS_ID = "newsId";
	private static final String PARAM_COMMENT_TEXT = "commentText";

	private static final int DEFAULT_MIN_COMMENT_LENGTH = 5;
	private static final String MIN_COMMENT_LENGTH = "jsp.comment.min.length";
	
	private IServiceManager serviceManager;
	private INewsService newsService;
	private ICommentService commentService;

	public PostCommentCommand() {
		super();
		this.serviceManager = BeanLoader.getInstance().getServiceManager();
		this.newsService = serviceManager.getNewsService();
		this.commentService = serviceManager.getCommentService();
	}

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = null;
		boolean isBuild = false;
		Comment comment = new Comment();
		Long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
		isBuild = buildComment(request, comment, newsId);
		page = PATH_NEWS + newsId;
		if (isBuild) {
			commentService.create(comment);
		} else {
			page += PATH_EMPTY_COMMENT;
		}		
		return page;
	}

	private boolean buildComment(HttpServletRequest request, Comment comment,
			Long newsId) throws ServiceException {
		/*
		 * This method builds comment from request
		 */
		boolean isBuild = false;
		News news = null;
		int minLength = getCommentLength();
		String commentText = request.getParameter(PARAM_COMMENT_TEXT);
		if (commentText.length() > minLength) {
			news = newsService.findById(newsId);
			comment.setNews(news);
			comment.setCommentText(commentText);
			comment.setCreationDate(new Date());
			isBuild = true;
		}
		return isBuild;
	}
	
	private int getCommentLength(){
		/*
		 * This method gets min length of comment from .properties file and if
		 * this value doesn't parse or missing in file return
		 * DEFAULT_NEWS_PER_PAGE
		 */
		int length = 0;
		try {
			length = Integer.parseInt(MessageManager.getValue(MIN_COMMENT_LENGTH));
			if (length < 0) {
				length = DEFAULT_MIN_COMMENT_LENGTH;
			}
		} catch (NumberFormatException | MissingResourceException e) {
			return DEFAULT_MIN_COMMENT_LENGTH;
		}
		return length;
	}
}
