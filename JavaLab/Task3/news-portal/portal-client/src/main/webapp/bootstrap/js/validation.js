function validate_comment(badComment) {
 if (document.getElementById("commentText").value.length < 5
   || document.getElementById("commentText").value.length > 100) {
  alert(badComment);
  return false;
 }
 return true;
}

function validate_filter(badFilter) {
	if (document.filter.authorIdFromFilter.value == '-1'
			&& document.filter.tagsIdFromFilter.value <= 0) {
		alert(badFilter);
		return false;
	}
	return true;
}