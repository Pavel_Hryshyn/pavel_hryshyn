<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="language">
<head>
<meta charset="utf-8">
<title><fmt:message key="jsp.title.error" /></title>

<link href="<c:url value="/bootstrap/css/style.css"/>" rel="stylesheet">
</head>
<body>
	<div class="panel">
		<div class="panel-header">
			<jsp:include page="/jsp/bars/header.jsp" />
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-1">
					<form method="GET" action="newslist">
						<input type="hidden" name="command" value="NEWS_LIST" /> 
						<input type="hidden" name="page" value="1"> 
						<input type="submit" class="btn btn-info"
							value="<fmt:message key="jsp.button.back"/>" />
					</form>
				</div>
			</div>
			<div class="error-code">
				<fmt:message key="jsp.msg.page.error.first" />
			</div>
			<div class="error-message">
				<fmt:message key="jsp.msg.page.error.occurred" />
			</div>
		</div>

		<div class="panel-footer">
			<jsp:include page="/jsp/bars/footer.jsp" />
		</div>
	</div>
</body>
</html>