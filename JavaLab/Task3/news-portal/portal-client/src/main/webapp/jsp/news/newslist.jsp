<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n/messages" scope="application" />

<html lang="language">
<head>
<meta charset="utf-8">
<title><fmt:message key="jsp.title.home" /></title>

<link href="<c:url value="/bootstrap/css/style.css"></c:url>"
	rel="stylesheet">
<link
	href="<c:url value='/bootstrap/css/ui.dropdownchecklist.standalone.css'></c:url>"
	rel="stylesheet">
<link
	href="<c:url value='/bootstrap/css/ui.dropdownchecklist.themeroller.css'></c:url>"
	rel="stylesheet">
<script src="<c:url value='/bootstrap/js/jquery-1.6.1.min.js'></c:url>"></script>
<script src="<c:url value='/bootstrap/js/validation.js'></c:url>"></script>
<script
	src="<c:url value='/bootstrap/js/jquery-ui-1.8.13.custom.min.js'/>"></script>
<script src="<c:url value='/bootstrap/js/ui.dropdownchecklist.js'/>"></script>
</head>
<body>
	
	<div class="panel">
		<div class="panel-header">
			<jsp:include page="/jsp/bars/header.jsp" />
			<div class="row">
				<div class="col-offset-9 col">
					<form method="GET" action="newslist">
						<input type="hidden" name="command" value="NEWS_LIST" /> <input
							type="hidden" name="page" value="${currentPage}"> <select
							class="select" id="language" name="language" onchange="submit()">
							<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
							<option value="ru" ${language == 'ru' ? 'selected' : ''}>Russian</option>
						</select>
					</form>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<fmt:message key="Filter.empty" var="badFilter" />
				<form class="filter-form" method="POST" action="newslist" name="filter"
							onsubmit="return validate_filter('${badFilter}');">

					<select class="select select-middle" name="authorIdFromFilter">
						<option value="-1">
							<fmt:message key="jsp.msg.select.author" />
						</option>
						<c:forEach items="${authors}" var="author">
							<c:if test="${sessionAuthor.id eq author.id}">
								<option selected value="${author.id}">
									<c:out value="${author.authorName}" />
								</option>
							</c:if>
							<c:if test="${sessionAuthor.id ne author.id}">
								<option value="${author.id}">
									<c:out value="${author.authorName}" />
								</option>
							</c:if>
						</c:forEach>
					</select> 
					
					<select class="select select-middle" multiple id="multi-dropbox"
						name="tagsIdFromFilter">
						<c:forEach items="${tags}" var="tag">
							<c:choose>
								<c:when test="${not empty selectedTags[tag.id]}">
									<option value="${tag.id}" selected>
										<c:out value="${tag.tagName}" />
									</option>
								</c:when>
								<c:otherwise>
									<option value="${tag.id}">
										<c:out value="${tag.tagName}" />
									</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>

					<input type="hidden" name="command" value="FILTER" /> 
					<input type="submit" class="btn btn-info"
						value="<fmt:message key="jsp.button.filter" />" />

				</form>

				<form method="POST" action="newslist" class="reset-form">
					<input type="hidden" name="command" value="RESET_FILTER" /> 
					<input type="submit" class="btn btn-warning"
						value="<fmt:message key="jsp.button.reset" />" />
				</form>
			</div>

			<c:if test="${listEmpty eq true}">
				<div class="news-block">
					<div class="news-short-text">
						<fmt:message key="jsp.msg.newslist.empty" />
					</div>
				</div>
			</c:if>
			
			<c:forEach items="${newsComplexList}" var="newsComplex">
				<div class="news-block">
					<div class="row">
						<div class="news-title">
							<b><c:out value="${newsComplex.news.title}" /></b>
						</div>
						<div class="news-author">
							<c:forEach items="${newsComplex.news.authors}" var="author">
								<em> 
									<c:out value="by ${author.authorName}, " />
								</em>
							</c:forEach>
						</div>
						<div class="news-date">
							<ins>
								<c:out value="${newsComplex.news.modificationDate}" />
							</ins>
						</div>
					</div>
					<div class="news-short-text">
						<c:out value="${newsComplex.news.shortText}" />
					</div>
					<div class="news-tags">
						<p>
							<c:forEach items="${newsComplex.news.tags}" var="tag">
								<mark>
									<c:out value="${tag.tagName}, " />
								</mark>
							</c:forEach>
						</p>
					</div>
					<div class="news-comments-amount">
						<p>
							<em><c:out value="Comments(${fn:length(newsComplex.comments)})" /></em>
						</p>
					</div>

					<div class="btn-view">
						<form name="getBookingList" method="GET" action="news?id=${newsComplex.news.id}">
							<input type="hidden" name="command" value="GET_NEWS_BY_ID" /> 
							<input type="hidden" name="newsId" value="${newsComplex.news.id}"> 
							<input type="submit" class="btn btn-primary"
								value="<fmt:message key="jsp.button.view" />" />
						</form>
					</div>
				</div>
			</c:forEach>
		</div>

		<div class="pagination">
			<c:forEach var="page" begin="1" end="${pageNumber}" step="1">
				<c:choose>
					<c:when test="${currentPage eq page}">
						<div class="page active">
							<form method="GET" action="newslist?page=${page}">
								<input type="hidden" name="command" value="NEWS_LIST" /> 
								<input type="hidden" name="page" value="${page}"> 
								<input type="submit" class="btn-page active" value="${page}" />
							</form>
						</div>
					</c:when>
					<c:otherwise>
						<div class="page">
							<form method="GET" action="newslist?page=${page}">
								<input type="hidden" name="command" value="NEWS_LIST" /> 
								<input type="hidden" name="page" value="${page}"> 
								<input type="submit" class="btn-page" value="${page}" />
							</form>
						</div>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>

		<div class="panel-footer">
			<jsp:include page="/jsp/bars/footer.jsp" />
		</div>

	</div>
	
	<script type="text/javascript">
		$("#multi-dropbox").dropdownchecklist(
				{
					emptyText : "<fmt:message key="jsp.msg.select.tags" />",
					width : 180
				});
	</script>
</body>
</html>