package by.epam.training.lab.fifth.xmlparsers.logic.parsers.handlers;

import java.util.EnumSet;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.training.lab.fifth.xmlparsers.entities.Device;
import by.epam.training.lab.fifth.xmlparsers.entities.Devices;
import by.epam.training.lab.fifth.xmlparsers.entities.HardwareGroup;
import by.epam.training.lab.fifth.xmlparsers.entities.Port;
import by.epam.training.lab.fifth.xmlparsers.entities.Type;

public class DeviceSAXHandler extends DefaultHandler {
	private Devices devicesList;
	private Device current;
	private Type type;
	private DevicesEnum currentEnum;
	private EnumSet<DevicesEnum> innerTagEnum;
	
	
	public DeviceSAXHandler() {
		devicesList = new Devices();
		type = new Type();
		innerTagEnum = EnumSet.range(DevicesEnum.HARDWARE_NAME, DevicesEnum.CRITICAL	);
	}


	public Devices getDevicesList() {
		return devicesList;
	}


	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if ("device".equalsIgnoreCase(localName)){
			current = new Device();			
			current.setName(attributes.getValue(0));
		} else {
			DevicesEnum temp = DevicesEnum.valueOf(localName.toUpperCase());
				if (innerTagEnum.contains(temp)){
					currentEnum = temp;
				}
		}
	}


	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if ("device".equalsIgnoreCase(localName)){
			current.setType(type);
			devicesList.getDevice().add(current);
		}
	}


	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		String tag = new String(ch, start, length).trim();
		if (currentEnum != null){
			switch (currentEnum) {
			case NAME:
				current.setName(tag);
				break;
			case HARDWARE_NAME:
				current.setHardwareName(tag);
				break;
			case ORIGIN:
				current.setOrigin(tag);
				break;
			case PRICE:
				current.setPrice(Double.parseDouble(tag));
				break;
			case PERIPHERAL:
				type.setPeripheral(Boolean.parseBoolean(tag));
				break;
			case POWER:
				type.setPower(Double.parseDouble(tag));
				break;
			case COOLER:
				type.setCooler(Boolean.parseBoolean(tag));
				break;
			case HARDWARE_GROUP:
				type.setHardwareGroup(HardwareGroup.fromValue(tag));
				break;
			case PORT:
				type.setPort(Port.fromValue(tag));
				break;
			case CRITICAL:
				current.setCritical(Boolean.parseBoolean(tag));;
				break;
			default:
				throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
			}
		}
		currentEnum = null;
	}	
}
