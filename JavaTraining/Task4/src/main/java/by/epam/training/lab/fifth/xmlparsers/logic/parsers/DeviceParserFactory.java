package by.epam.training.lab.fifth.xmlparsers.logic.parsers;

import by.epam.training.lab.fifth.xmlparsers.exceptions.ParserXMLException;

public class DeviceParserFactory {
	private enum TypeParser {
		DOM, SAX, STAX
	}
	
	public AbstractDeviceParser createParser(String typeParser) throws ParserXMLException{
		TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
		switch (type) {
		case DOM:
			return new DeviceDOMParser();
		case SAX:
			return new DeviceSAXParser();	
		case STAX:
			return new DeviceSTAXParser();	
		default:
			throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
		}
	}
}
