package by.epam.training.lab.fifth.xmlparsers.logic.parsers.handlers;

public enum DevicesEnum {
	DEVICES("devices"),
	DEVICE("device"),
	NAME("name"),
	HARDWARE_NAME("Hardware_name"),
	ORIGIN("Origin"),
	PRICE("Price"),
	PERIPHERAL("peripheral"),
	POWER("power"),
	COOLER("cooler"),
	HARDWARE_GROUP("hardware_group"),
	PORT("port"),
	CRITICAL("Critical");
	
	private String value;
	private DevicesEnum(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	
	
	
	
	
}
