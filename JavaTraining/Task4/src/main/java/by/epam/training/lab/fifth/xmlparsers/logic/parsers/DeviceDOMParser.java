package by.epam.training.lab.fifth.xmlparsers.logic.parsers;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.training.lab.fifth.xmlparsers.entities.Device;
import by.epam.training.lab.fifth.xmlparsers.entities.Devices;
import by.epam.training.lab.fifth.xmlparsers.entities.HardwareGroup;
import by.epam.training.lab.fifth.xmlparsers.entities.Port;
import by.epam.training.lab.fifth.xmlparsers.entities.Type;
import by.epam.training.lab.fifth.xmlparsers.exceptions.ParserXMLException;

public class DeviceDOMParser extends AbstractDeviceParser {
	final static Logger logger = Logger.getLogger(DeviceDOMParser.class);
	private DocumentBuilder docBuilder;
	
	public DeviceDOMParser() {
		super();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			System.err.println("Parser configuration error " + e);
		}
	}
	
	public DeviceDOMParser(Devices devices) {
		super(devices);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			System.err.println("Parser configuration error " + e);
		}
	}

	
	@Override
	public void buildDevices(String fileName) throws ParserXMLException {
		Document doc = null;
		try {
			doc = docBuilder.parse(fileName);
			Element root = doc.getDocumentElement();
			NodeList devicesList = root.getElementsByTagName("device");
				for (int i=0; i < devicesList.getLength(); i++){
					Element deviceElement = (Element)devicesList.item(i);
					Device device = buildDeviceElement(deviceElement);
					if (device != null){
					getDevices().getDevice().add(device);
					}
				}
		} catch (IOException e) {
			throw new ParserXMLException("I/O error: ", e);
		} catch (SAXException e) {
			throw new ParserXMLException("Parsing failure: ", e);
		} 

	}
	
	private Device buildDeviceElement(Element deviceElement) {
		Device device = new Device();
		Type type = new Type();
		device.setName(deviceElement.getAttribute("name"));
		device.setHardwareName(getElementTextContent(deviceElement, "Hardware_name"));
		device.setOrigin(getElementTextContent(deviceElement, "Origin"));
		Double price = Double.parseDouble(getElementTextContent(deviceElement, "Price"));
		device.setPrice(price);
		Boolean critical = Boolean.parseBoolean(getElementTextContent(deviceElement, "Critical"));
		device.setCritical(critical);
		
		Boolean peripheral = Boolean.parseBoolean(getElementTextContent(deviceElement, "peripheral"));
		type.setPeripheral(peripheral);
		Double power = Double.parseDouble(getElementTextContent(deviceElement, "power"));
		type.setPower(power);
		Boolean cooler = Boolean.parseBoolean(getElementTextContent(deviceElement, "cooler"));
		type.setPeripheral(cooler);
		String hardware = getElementTextContent(deviceElement, "hardware_group");
		type.setHardwareGroup(HardwareGroup.fromValue(hardware));
		String port = getElementTextContent(deviceElement, "port");
		type.setPort(Port.fromValue(port));;
		
		device.setType(type);
		return device;		
	}
	
	private static String getElementTextContent(Element element, String elementName){
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		String text = node.getTextContent();
		return text;
	}

}
