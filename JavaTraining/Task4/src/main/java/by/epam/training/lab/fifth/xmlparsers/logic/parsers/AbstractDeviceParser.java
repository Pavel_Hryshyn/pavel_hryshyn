package by.epam.training.lab.fifth.xmlparsers.logic.parsers;

import by.epam.training.lab.fifth.xmlparsers.entities.Devices;
import by.epam.training.lab.fifth.xmlparsers.exceptions.ParserXMLException;

public abstract class AbstractDeviceParser {
	private Devices devices;

	public AbstractDeviceParser() {
		devices = new Devices();
	}
	
	public AbstractDeviceParser(Devices devices) {
		this.devices = devices;
	}

	public Devices getDevices() {
		if (devices == null) {
			devices = new Devices();
		}
		return this.devices;	
	}
	
	public void setDevices(Devices devices) {
		this.devices = devices;
	}

	abstract public void buildDevices(String fileName) throws ParserXMLException;
	
}
