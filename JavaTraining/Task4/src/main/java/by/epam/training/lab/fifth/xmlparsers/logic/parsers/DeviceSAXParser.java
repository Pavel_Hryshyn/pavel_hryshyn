package by.epam.training.lab.fifth.xmlparsers.logic.parsers;

import java.io.IOException;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import by.epam.training.lab.fifth.xmlparsers.entities.Devices;
import by.epam.training.lab.fifth.xmlparsers.exceptions.ParserXMLException;
import by.epam.training.lab.fifth.xmlparsers.logic.parsers.handlers.DeviceSAXHandler;

public class DeviceSAXParser extends AbstractDeviceParser {
	private DeviceSAXHandler saxHandler;
	private XMLReader reader;
	
		
	public DeviceSAXParser() throws ParserXMLException {
		super();
		saxHandler = new DeviceSAXHandler();
		try {
			reader = XMLReaderFactory.createXMLReader();
			reader.setContentHandler(saxHandler);
		} catch (SAXException e) {
			throw new ParserXMLException("SAX parser error: ", e);
		}
	}



	public DeviceSAXParser(Devices devices) throws ParserXMLException {
		super(devices);
		saxHandler = new DeviceSAXHandler();
		try {
			reader = XMLReaderFactory.createXMLReader();
			reader.setContentHandler(saxHandler);
		} catch (SAXException e) {
			throw new ParserXMLException("SAX parser error: ", e);
		}
		setDevices(saxHandler.getDevicesList());
	}



	@Override
	public void buildDevices(String fileName) throws ParserXMLException {
		try {
			reader.parse(fileName);
		} catch (SAXException e) {
			throw new ParserXMLException("Parsing failure: ", e);
		} catch (IOException e) {
			throw new ParserXMLException("I/O error", e);
		}
		
	}

	

}
