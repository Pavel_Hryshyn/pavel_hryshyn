package by.epam.training.lab.fifth.xmlparsers.exceptions;

public class ParserXMLException extends Exception {
	private static final long serialVersionUID = 1L;

	public ParserXMLException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParserXMLException(String message) {
		super(message);
	}
	
	
}
