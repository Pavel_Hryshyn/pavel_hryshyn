package by.epam.training.lab.third.threads.runner;

import by.epam.training.lab.third.threads.entities.Car;
import by.epam.training.lab.third.threads.entities.Parking;

public class Runner {
	public static void main(String[] args) {
	Parking parking = new Parking(4);
	
	Car car0 = new Car("Car0", parking);
	Car car1 = new Car("Car1", parking);
	Car car2 = new Car("Car2", parking);
	Car car3 = new Car("Car3", parking);
	Car car4 = new Car("Car4", parking);
	Car car5 = new Car("Car5", parking);
	Car car6 = new Car("Car6", parking);
	Car car7 = new Car("Car7", parking, 10);
	
	
	new Thread(car0).start();
	new Thread(car1).start();
	new Thread(car2).start();
	new Thread(car3).start();
	new Thread(car4).start();
	new Thread(car5).start();
	new Thread(car6).start();
	new Thread(car7).start();
	
	
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		System.err.print("Wake up!");
	}
	
	car0.stopThread();
	car1.stopThread();
	car2.stopThread();
	car3.stopThread();
	car4.stopThread();
	car5.stopThread();
	car6.stopThread();
	car7.stopThread();
	
	}
}
