/*
 * @(#)Parking.java   2.0 2015/04/13
 */
package by.epam.training.lab.third.threads.entities;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.training.lab.third.threads.exception.ParkingException;

/**
 * This class is parking entity and lock or unlock Queue of Parking Places for Car.java.
 * @author Pavel Hryshyn
 * @param BlockingQueue<ParkingPlace> parkingPlacesList, Map<Car, ParkingPlace> usedParkingPlaces;
 */
public class Parking{
	private final static Logger logger = Logger.getRootLogger();
	
	private BlockingQueue<ParkingPlace> parkingPlacesList;
	private Map<Car, ParkingPlace> usedParkingPlaces;

	
	public Parking(int parkingSize) {
		parkingPlacesList = new ArrayBlockingQueue<ParkingPlace>(parkingSize);
		for (int i=0; i<parkingSize; i++){
			parkingPlacesList.add(new ParkingPlace(i));
		}
		usedParkingPlaces = new HashMap<Car, ParkingPlace>();
		
		logger.debug("Parking is created.");
	}
	
	/**
	 * This method locks Queue of Parking Place and gets Parking Place for Car, 
	 * if there is free Parking Place. If there isn't free Parking Place, 
	 * then Car waits some time and leaves
	 * @param Car car
	 * @throws InterruptedException
	 * @return boolean
	 */
	public boolean lockParkingPlace(Car car){
		boolean isAllowedParking = false;
		ParkingPlace parkingPlace = null;
		try {
			parkingPlace = parkingPlacesList.poll(car.getTimeout(), TimeUnit.MILLISECONDS);
					
			if (parkingPlace != null) {
				usedParkingPlaces.put(car, parkingPlace);
//				logger.debug("Car = " + car.getName() + " take parking place = " + parkingPlace.getId());
				isAllowedParking = true;
			} else {
				logger.info("Parking is full and car = " + car.getName() + " wait " + car.getTimeout() 
						+ " seconds and leave");
				isAllowedParking = false;
			}			
		} catch (InterruptedException e) {
		logger.info("Car = " + car.getName() + " isn't allowed to park");
			return false;
		}
		return isAllowedParking;
	}
	
	/**
	 * This method unlocks Queue of Parking Place, when Car leaves,
	 * and returns Parking Place in Queue.
	 * @param Car car
	 * @throws ParkingException
	 * @return boolean
	 */
	public boolean unlockParkingPlace(Car car) throws ParkingException {
		boolean isAllowedLeaving = false;
		ParkingPlace parkingPlace = usedParkingPlaces.get(car);
		
		try {
			if (parkingPlace != null){
//			logger.debug("Car = " + car.getName() + " leave from parking place = " + parkingPlace.getId());
			parkingPlacesList.put(parkingPlace);
			usedParkingPlaces.remove(car);
			isAllowedLeaving = true;
			} else {
				isAllowedLeaving = false;
//				logger.debug("Unfortunally, Car = " + car.getName() + " is broken in the parking.");
			}
						
		}  catch (InterruptedException e) {
			logger.debug("Car = " + car.getName() + " can't leave parking");
			return false;
		}
		return isAllowedLeaving;
	}
	
	public void removeAndAddPairOfCar(Car car1, Car car2) throws InterruptedException, ParkingException{
		Lock lock = new ReentrantLock();
		Condition isFree = lock.newCondition();
		
		try {
			lock.lock();
			ParkingPlace initialPlace = getUsedParkingPlaces().remove(car1);
			ParkingPlace finalPlace = getUsedParkingPlaces().remove(car2);
			getUsedParkingPlaces().put(car1, finalPlace);
			getUsedParkingPlaces().put(car2, initialPlace);
			if (finalPlace != null){
			logger.debug("Car = " + car1.getName() + " MOVED FROM " + initialPlace.getId() 
					+ " TO " + finalPlace.getId() + '\n' + "Car = " + car2.getName()
					+ " MOVED FROM " + finalPlace.getId() + " TO " + initialPlace.getId());
			}
			isFree.signalAll();
		} finally {
			lock.unlock();
		}
		
	}
	
	/**
	 * This method gets Parking Place that is occupied the Car.
	 * @param Car car
	 * @throws ParkingException if there isn't parkingPlace in map
	 * @return ParkingPlace
	 */
	public ParkingPlace getParkingPlace(Car car) throws ParkingException {
		
		ParkingPlace parkingPlace = usedParkingPlaces.get(car);
		
		if (parkingPlace == null) {
			throw new ParkingException("Try to use ParkingPlace without blocking");
		} 
		return parkingPlace;
	}

	/**
	 * This method gets Map of occupied Parking Places, where Car is key and ParkingPlace is value.
	 * @throws ParkingException if Map<Car, ParkingPlace> == null
	 * @return Map<Car, ParkingPlace>
	 */
	public Map<Car, ParkingPlace> getUsedParkingPlaces() throws ParkingException {
		if (usedParkingPlaces == null) {
			throw new ParkingException("Parking is free");
		}
		return usedParkingPlaces;
	}

	
	
	
	

}
