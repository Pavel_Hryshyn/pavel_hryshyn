/*
 * @(#)Car.java   2.0 2015/04/13
 */
package by.epam.training.lab.third.threads.entities;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;

import by.epam.training.lab.third.threads.exception.ParkingException;

/**
 * This class is Car entity and implements Runnable @interface. The Car can park, 
 * leave from parking and exchange parking place with the nearest Car.
 * @author Pavel Hryshyn
 * @param String name, Parking parking,  long timeout
 */
public class Car implements Runnable {
	private final static Logger logger = Logger.getRootLogger();
	private volatile boolean stopThread = false;
	
	private String name;
	private Parking parking;
	private long timeout = 1;
	
	
	public Car(String name, Parking parking) {
		this.name = name;
		this.parking = parking;
	}
	
		
	public Car(String name, Parking parking, long timeout) {
		this.name = name;
		this.parking = parking;
		this.timeout = timeout;
	}

	
	public void stopThread(){
		stopThread = true;
	}
		
	public String getName() {
		return name;
	}
	
	public long getTimeout() {
		return timeout;
	}


	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}


	@Override
	public void run() {
		try {
			while (!stopThread){
				inCity();
				inParking();
			}
		} catch (InterruptedException e) {
			logger.error("Car is broken", e);
		} catch (ParkingException e) {
			logger.error("Car is broken in the parking.", e);
		}		
	}
	
	private void inCity() throws InterruptedException{
		Thread.sleep(1000);
	}
	
	/**
	 * This method parks the Car, leaves from parking
	 * and exchanges parking place with the nearest Car.
	 * @throws ParkingException, InterruptedException
	 */
	public void inParking() throws ParkingException, InterruptedException {
		boolean isLockedParkingPlace = false;
		ParkingPlace parkingPlace = null;
		try {
			isLockedParkingPlace = parking.lockParkingPlace(this);
			
			if (isLockedParkingPlace){
				parkingPlace = parking.getParkingPlace(this);
				logger.debug("Car = " + this.name + " take parking place = " + parkingPlace.getId());
	//			Thread.sleep(100);
				exchangeParkingPlaces(this, parkingPlace);	
			} 
		} finally {
			if (isLockedParkingPlace){
				logger.debug("Car = " + this.name + " leave from parking place = " + parking.getParkingPlace(this).getId());
				parking.unlockParkingPlace(this);				
			}
		}
	}
	
	/**
	 * This method exchanges parking place of the nearest Cars.
	 * @param Car car, ParkingPlace parkingPlace
	 * @throws ParkingException, InterruptedException
	 */
	public void exchangeParkingPlaces(Car car, ParkingPlace parkingPlace)  
									throws InterruptedException, ParkingException{
		Random random = new Random();
		int value = random.nextInt(5000);
		ParkingPlace currentPlace = parkingPlace;
		Car tempCar = null;
		if (value > 0 && value < 2500 && currentPlace != null){
				tempCar = findCarByParkingPlace(car, currentPlace, -1);
				if (tempCar != null) {
					parking.removeAndAddPairOfCar(car, tempCar);					
				}
		} else if (value > 2500 && value < 5000 && currentPlace != null){
				tempCar = findCarByParkingPlace(car, currentPlace, 1);
				if (tempCar != null) {
					parking.removeAndAddPairOfCar(car, tempCar);
		}
			}
	}

	/**
	 * This method find Car that parking near this Car.
	 * @param Car car, ParkingPlace parkingPlace
	 * @throws ParkingException
	 * @return Car
	 */
	public Car findCarByParkingPlace(Car car, ParkingPlace parkingPlace, int id)
								throws ParkingException {
		Map<Car, ParkingPlace> tempMap; 
		tempMap = parking.getUsedParkingPlaces();
		Set<Map.Entry<Car, ParkingPlace>> entrySet = tempMap.entrySet();
		Car tempCar = null;
		ParkingPlace initialPlace = parkingPlace;
		if (initialPlace != null) {
		Iterator<Map.Entry<Car, ParkingPlace>> it = entrySet.iterator();
		while (it.hasNext()) {
			Map.Entry<Car, ParkingPlace> item = it.next();
			if (item != null) {
			if ((initialPlace.getId() + id) == item.getValue().getId()){
				tempCar = item.getKey();
				}
			}
		}
		}
		return tempCar;
	}
}
