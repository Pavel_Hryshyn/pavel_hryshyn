/*
 * @(#)ParkingPlace.java   2.0 2015/04/13
 */
package by.epam.training.lab.third.threads.entities;

/**
 * This class is parking place entity.
 * @author Pavel Hryshyn
 * @param int id
 */
public class ParkingPlace {
	private int id;

	public ParkingPlace(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParkingPlace other = (ParkingPlace) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	
	
}
