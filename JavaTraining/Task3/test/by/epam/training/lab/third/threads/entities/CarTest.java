package by.epam.training.lab.third.threads.entities;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import by.epam.training.lab.third.threads.exception.ParkingException;

public class CarTest extends Assert {
	Map<Car, ParkingPlace> map;
	Parking parking;
	ParkingPlace parkingPlace;
	Car car0;
	Car car1;
	Car car2;
	Car car3;
	Car expectedCar = null;
	
	@Before
	public void createMap() throws ParkingException{
		parking = new Parking(4);
		car1 = new Car("car0", parking);
		car1 = new Car("car1", parking);
		car2 = new Car("car2", parking);
		car3 = new Car("car3", parking);
		parking.getUsedParkingPlaces().put(car0, new ParkingPlace(0));
		parking.getUsedParkingPlaces().put(car1, new ParkingPlace(1));
		parking.getUsedParkingPlaces().put(car2, new ParkingPlace(2));
		parking.getUsedParkingPlaces().put(car3, new ParkingPlace(3));
		parkingPlace = parking.getParkingPlace(car1);
	}
	
	@Test
	public void testFindCarByParkingPlace() throws ParkingException{
		expectedCar = car1.findCarByParkingPlace(car1, parkingPlace, 1);
		System.out.print(expectedCar.getName());
		assertEquals(expectedCar, car2);
	}
	
	@Test
	public void testExchangeParkingPlaces() throws InterruptedException, ParkingException{
		car2.exchangeParkingPlaces(car2, parking.getParkingPlace(car2));
		System.out.println(parking.getParkingPlace(car1).getId());
		System.out.println(parking.getParkingPlace(car2).getId());
		System.out.println(parking.getParkingPlace(car3).getId());
	}
		

}
