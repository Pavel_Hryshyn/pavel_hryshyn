package by.epam.training.lab.third.threads.entities;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import by.epam.training.lab.third.threads.exception.ParkingException;

public class ParkingTest extends Assert {
	Map<Car, ParkingPlace> map;
	Parking parking;
	Car car1;
	Car car2;
	
	@Before
	public void createMap() throws ParkingException{
		parking = new Parking(4);
		map = parking.getUsedParkingPlaces();
		car1 = new Car("car1", parking);
		car2 = new Car("car2", parking);
	}
	
	@Test
	public void testRemoveAndAddPair() throws InterruptedException, ParkingException{
		map.put(car1, new ParkingPlace(1));
		map.put(car2, new ParkingPlace(2));
		
		parking.removeAndAddPairOfCar(car1, car2);
		System.out.println("Car1 placed = " + parking.getParkingPlace(car1).getId());
		System.out.println("Car2 placed = " + parking.getParkingPlace(car2).getId());
		assertEquals(2, parking.getParkingPlace(car1).getId());
		assertEquals(1, parking.getParkingPlace(car2).getId());		
	}
	
	

}
