just get the wrong results.1.1.  The if-then Statement
 If a second statement is later added

1.2.  The if-then-else Statement
 Omitting them can make the code more brittle.1.  The if-then and if-then-else Statements
 The compiler cannot catch this sort of error; you'll
The if-then statement is the most basic of all the control flow statements.to the "then" clause, a common mistake would be forgetting to add the newly required braces.The if-then-else statement provides a secondary path of execution when an "if" clause evaluates to false. It tells your program to execute a certain section of code only if a particular test evaluates to true. In this case, the action is to simply print an error message stating that the bicycle has already stopped. For example, the Bicycle class could allow the brakes to decrease the bicycle's speed only if the bicycle is already in motion. However, once a condition is satisfied, the appropriate statements are executed (grade = 'C';) and the remaining conditions are not evaluated.void applyBrakes() {
    // same as above, but without braces
    if (isMoving)
        currentSpeed--;
} You could use an if-then-else statement in the applyBrakes method to take some action if the brakes are applied when the bicycle is not in motion. In addition, the opening and closing braces are optional, provided that the "then" clause contains only one statement:

Deciding when to omit the braces is a matter of personal taste. One possible implementation of the applyBrakes method could be as follows:

If this test evaluates to false (meaning that the bicycle is not in motion), control jumps to the end of the if-then statement.void applyBrakes() {
    if (isMoving) {
        currentSpeed--;
    } else {
        System.err.println("The bicycle has already stopped!");
    }
}


The output from the program is:
    Grade = C
You may have noticed that the value of testscore can satisfy more than one expression in the compound statement: 76 >= 70 and 76 >= 60.

The following program, IfElseDemo, assigns a grade based on the value of a test score: an A for a score of 90% or above, a B for a score of 80% or above, and so on.void applyBrakes() {
    // the "if" clause: bicycle must be moving
    if (isMoving){
        // the "then" clause: decrease current speed
        currentSpeed--;
    }
}class IfElseDemo {
    public static void main(String[] args) {

        int testscore = 76;
        char grade;

        if (testscore >= 90) {
            grade = 'A';
        } else if (testscore >= 80) {
            grade = 'B';
        } else if (testscore >= 70) {
            grade = 'C';
        } else if (testscore >= 60) {
            grade = 'D';
        } else {
            grade = 'F';
        }
        System.out.println("Grade = " + grade);
    }
}