/**
 * @(#)Word.java   2.0 2015/04/11
 */
package by.epam.training.lab.second.parsingtext.entities;

public class Word extends Lexeme {
	private String id = "word";
	private String word;

			
	public Word() {
		super();
	}

	public Word(String word) {
		super();
		this.word = word;
	}
	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getLexeme() {
		return word;
	}

	@Override
	public void setLexeme(String word) {
		this.word = word;
		
	}

	
	@Override
	public String toString() {
		return word;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Word other = (Word) obj;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}
	
	
	
}
