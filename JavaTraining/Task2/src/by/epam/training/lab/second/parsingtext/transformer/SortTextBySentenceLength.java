package by.epam.training.lab.second.parsingtext.transformer;

import java.util.Collections;
import java.util.List;

import by.epam.training.lab.second.parsingtext.entities.PartOfText;
import by.epam.training.lab.second.parsingtext.entities.Text;

public class SortTextBySentenceLength {
	private Text sortedText;
	
	public Text sort(Text initialText){
		sortedText = new Text(initialText.getParts());
		List<PartOfText> handledList = sortedText.getParts();
		Collections.sort(handledList);
		return sortedText;
	}
}
