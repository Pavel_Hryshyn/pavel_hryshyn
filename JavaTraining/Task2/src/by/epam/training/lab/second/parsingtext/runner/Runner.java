package by.epam.training.lab.second.parsingtext.runner;

import java.util.List;

import by.epam.training.lab.second.parsingtext.entities.Lexeme;
import by.epam.training.lab.second.parsingtext.entities.PartOfText;
import by.epam.training.lab.second.parsingtext.entities.Text;
import by.epam.training.lab.second.parsingtext.logic.io.DataReader;
import by.epam.training.lab.second.parsingtext.logic.io.DataWriter;
import by.epam.training.lab.second.parsingtext.logic.parser.CollectText;
import by.epam.training.lab.second.parsingtext.logic.parser.ParseTextToLexemes;
import by.epam.training.lab.second.parsingtext.transformer.DeleteSomeSizeWords;
import by.epam.training.lab.second.parsingtext.transformer.ReplaceWordsInSentence;
import by.epam.training.lab.second.parsingtext.transformer.SortTextBySentenceLength;

public class Runner {

	public static void main(String[] args) {
		DataReader reader = new DataReader();
		String text = reader.readData("src/resources/InitialText.txt");
		
		ParseTextToLexemes textToLex = new ParseTextToLexemes(text);
		List<Lexeme> lexemes = textToLex.parseTextToLexemes(text);
		ParseTextToLexemes pt = new ParseTextToLexemes(text, new CollectText(text));
		List<PartOfText> textToLexemes;
		List<Lexeme> m1 = pt.parseTextToLexemes(text);
		List<Lexeme> m2 = pt.parseTextToLexemes(text);
		Text textAfterParsing = new Text(pt.handleRequest());
	//	Text textAfterReplacing = new Text(textToLexemes);
		
		
		// Looking at
		DeleteSomeSizeWords delete = new DeleteSomeSizeWords();
		String textAfDeleting = delete.deleteWords(lexemes, 3);
	//	Text replacedWords = new ReplaceWordsInSentence().replaceWords(textAfterReplacing);
		Text sortedTextByLength = new SortTextBySentenceLength().sort(textAfterParsing);
		
		DataWriter txAfParsing = new DataWriter();
		txAfParsing.writeData("textAfterParsing.txt", textAfterParsing.toString());
		DataWriter txAfDeleting = new DataWriter();
		txAfDeleting.writeData("textAfterDeleting.txt", textAfDeleting);
		DataWriter txAfReplasing = new DataWriter();
	//	txAfReplasing.writeData("textAfterReplacingWords.txt", replacedWords.toString());
		DataWriter txAfSorting = new DataWriter();
		txAfSorting.writeData("textAfterSorting.txt", sortedTextByLength.toString());
		
		System.out.print("Runner is finished");

	}

}
