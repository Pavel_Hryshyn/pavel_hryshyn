package by.epam.training.lab.second.parsingtext.logic.parser;

import java.util.List;

import by.epam.training.lab.second.parsingtext.entities.PartOfText;

public interface IParser {
	List<PartOfText> handleRequest();
}
