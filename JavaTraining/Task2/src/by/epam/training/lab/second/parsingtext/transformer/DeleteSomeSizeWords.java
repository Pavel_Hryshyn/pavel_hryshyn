package by.epam.training.lab.second.parsingtext.transformer;
//12
import java.util.List;

import by.epam.training.lab.second.parsingtext.entities.Lexeme;

public class DeleteSomeSizeWords {
	private static String handledText = "";
	
	public String deleteWords(List<Lexeme> lexemes, int length){
		if (length >0) {
			for (int i=0; i<lexemes.get(i).getLexeme().length(); i++) {
				if (lexemes.get(i).getLexeme().length() == length){
				switch (lexemes.get(i).getLexeme().toUpperCase().charAt(0)){
					case 'A': break;
					case 'E': break;
					case 'I': break;
					case 'O': break;
					case 'U': break;
					case 'Y': break;
				default: lexemes.remove(i);
				}
				}
			} handledText = toString(lexemes);
		} else {
			System.err.print("Illegal size of words"); //throws
		}
		return handledText;
	}
	
	private String toString(List<Lexeme> lexemes) {
		StringBuffer buffer = new StringBuffer();
		for (Lexeme lexeme: lexemes){
			buffer.append(lexeme.getLexeme());
		}
		return buffer.toString();
		
	}
	
	@Override
	public String toString() {
		return handledText;
	}
}
