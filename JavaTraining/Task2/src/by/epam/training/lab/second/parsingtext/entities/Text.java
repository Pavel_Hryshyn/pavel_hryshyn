package by.epam.training.lab.second.parsingtext.entities;

import java.util.ArrayList;
import java.util.List;

public class Text implements Comparable<Text>{
	private List<PartOfText> parts = new ArrayList<PartOfText>();

	public Text() {
		super();
	}

	public Text(List<PartOfText> parts) {
		super();
		this.parts = parts;
	}
	
	public void add(PartOfText part) {
		parts.add(part);
		
	}

	public void remove(int index) {
		parts.remove(index);
		
	}

	public PartOfText getPartOfText(int index) {
		return parts.get(index);
	}

	public List<PartOfText> getParts() {
		return parts;
	}

	public void setParts(List<PartOfText> parts) {
		this.parts = parts;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (PartOfText part: parts){
			buffer.append(part.toString());
		}
		
		return buffer.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parts == null) ? 0 : parts.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Text other = (Text) obj;
		if (parts == null) {
			if (other.parts != null)
				return false;
		} else if (!parts.equals(other.parts))
			return false;
		return true;
	}

	@Override
	public int compareTo(Text o) {
		int result = getParts().size()>o.getParts().size() ?1:-1;
		return result;
		}

		
}
