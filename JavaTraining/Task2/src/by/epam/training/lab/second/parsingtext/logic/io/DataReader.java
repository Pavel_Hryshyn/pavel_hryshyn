/**
 * @(#)DataReader.java   2.0 2015/04/11
 */
package by.epam.training.lab.second.parsingtext.logic.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * This class open and read files by BufferedReader. 
 * Save text in StringBuffer object and convert to String object
 * @author Pavel Hryshyn
 */
public class DataReader {
	final static Logger logger = Logger.getLogger(DataReader.class);
	/*
	 * This method create String object from file.
	 * @param String filename
	 * @return String toStringInitText	
	 */
	public String readData(String filename){
		String line = "";
		String toStrInitText = "";
	try{
		BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
		StringBuilder initText = new StringBuilder();
			while ((line = br.readLine()) != null){
			initText.append(line + "\n");
		}
		
		toStrInitText = initText.toString();
		br.close();
	}  catch (FileNotFoundException e){
		logger.error("Error of reading file" + e);
		System.err.print(e);
	}  catch (IOException e){
		logger.error("Error of reading file" + e);
		System.err.print(e);
	} 
		return toStrInitText;
	}
	
	
}
