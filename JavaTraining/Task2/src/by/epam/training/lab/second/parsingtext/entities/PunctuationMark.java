/**
 * @(#)PunctuationMark.java   2.0 2015/04/11
 */
package by.epam.training.lab.second.parsingtext.entities;

public class PunctuationMark extends Lexeme {
	private String id = "punctuation mark";
	private String punctuation;

		
	public PunctuationMark() {
		super();
	}


	public PunctuationMark(String punctuation) {
		super();
		this.punctuation = punctuation;
	}

	
	@Override
	public String getId() {
		return id;
	}


	@Override
	public String getLexeme() {
		// TODO Auto-generated method stub
		return punctuation;
	}


	@Override
	public void setLexeme(String punctuation) {
		this.punctuation = punctuation;
		
	}


	@Override
	public String toString() {
		return punctuation;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((punctuation == null) ? 0 : punctuation.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PunctuationMark other = (PunctuationMark) obj;
		if (punctuation == null) {
			if (other.punctuation != null)
				return false;
		} else if (!punctuation.equals(other.punctuation))
			return false;
		return true;
	}

		
}
