package by.epam.training.lab.second.parsingtext.logic.parser;

import java.util.List;

import by.epam.training.lab.second.parsingtext.entities.BlockOfCode;
import by.epam.training.lab.second.parsingtext.entities.Lexeme;
import by.epam.training.lab.second.parsingtext.entities.PartOfText;
import by.epam.training.lab.second.parsingtext.entities.Sentence;
import by.epam.training.lab.second.parsingtext.entities.Text;

public class CollectText implements IParser {
	private List<Lexeme> textToLexeme;
	private PartOfText sentence = new Sentence();
	private Text textFromSentence = new Text();
	private BlockOfCode blockOfCode = new BlockOfCode();
	
		
	public CollectText(List<Lexeme> textToLexeme) {
		super();
		this.textToLexeme = textToLexeme;
	}
	
	
	public CollectText(String text) {
		super();
		this.textToLexeme = new ParseTextToLexemes(text).parseTextToLexemes(text);
	}




	@Override
	public List<PartOfText> handleRequest() {
		textFromSentence = collectTextFromLexemes();
		return textFromSentence.getParts();
	}
	
	public Text collectTextFromLexemes(){
		String lexPr="";
		String lex;
		Character ch = '\n';
		int startBlock = -1;
		
		for (int index = 0; index < textToLexeme.size(); index++){
			lex = textToLexeme.get(index).getLexeme();
			if (index != 0) {
				lexPr = textToLexeme.get(index-1).getLexeme();
			}
			
			if (( lex.equals("class") || lex.equals("public") || lex.equals("void") 
					|| lex.equals("package") || lex.equals("import") ) 
					&& ( ch.equals(lexPr.charAt(0)) )){
				startBlock = index;
			}
			
			if (startBlock == -1){
			sentence.add(textToLexeme.get(index));
						
			if (((lex.equals(".") || lex.equals("!") || lex.equals("?") || ch.equals(lex.charAt(0)))
					&& lexPr.length() > 1)) {
				textFromSentence.add(sentence);
				sentence = new Sentence();
			} 
		  } else {
			  blockOfCode.add(textToLexeme.get(index));
			  if (lex.equals("}") && !lexPr.equals(" ")) {
					textFromSentence.add(blockOfCode);
					blockOfCode = new BlockOfCode();
					startBlock = -1;
			} 
		  }
		} 
		
		return textFromSentence;
	}

}
