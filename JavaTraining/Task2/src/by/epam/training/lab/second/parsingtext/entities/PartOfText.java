package by.epam.training.lab.second.parsingtext.entities;

import java.util.List;

public interface PartOfText extends Comparable<PartOfText> {
	
	void add(Lexeme lexeme);
	void remove(int index);
	void removeAll();
	Lexeme getElement(int index);
	List<Lexeme> getAllElements();
	String toString();
}
