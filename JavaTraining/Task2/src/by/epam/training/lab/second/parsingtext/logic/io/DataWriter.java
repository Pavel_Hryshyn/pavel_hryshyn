package by.epam.training.lab.second.parsingtext.logic.io;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class DataWriter {
	final static Logger logger = Logger.getLogger(DataWriter.class);
	
	private String filenameWithPath;
	private StringBuilder path = new StringBuilder("src/resources/");
	
	public void writeData(String filename, String text){
		filenameWithPath = createPath(filename);
		File file = new File(filenameWithPath);
		
		try {
			if (!file.exists()){
				file.createNewFile();
			}
			PrintWriter printWriter = new PrintWriter(file.getAbsoluteFile());
			
			try {
				printWriter.print(text);
			} finally {
				printWriter.close();
			}
		} catch (IOException e) {
			logger.error("Error of writing: " + e);
		}
	}
	
	private String createPath(String filename) {
		Pattern pattern  = Pattern.compile("/");
		Matcher matcher = pattern.matcher(filename);
		if (!matcher.find()) {
			path.append(filename);
			filenameWithPath = path.toString();
		} else {
			filenameWithPath = filename;
		}
		return filenameWithPath;
	}
}
