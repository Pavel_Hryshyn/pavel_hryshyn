package by.epam.training.lab.second.parsingtext.entities;

import java.util.ArrayList;
import java.util.List;

public class Sentence implements PartOfText {
	private List<Lexeme> sentence = new ArrayList<Lexeme>();
	
		
	public Sentence() {
		super();
	}

	public Sentence(List<Lexeme> sentence) {
		super();
		this.sentence = sentence;
	}

	@Override
	public void add(Lexeme lexeme) {
		sentence.add(lexeme);
		
	}

	@Override
	public void remove(int index) {
		sentence.remove(index);
		
	}
	
	@Override
	public void removeAll() {
		sentence.clear();
		
	}

	@Override
	public Lexeme getElement(int index) {
		return sentence.get(index);
	}
	
	
	@Override
	public List<Lexeme> getAllElements() {
		List<Lexeme> listOfLexeme = new ArrayList<Lexeme>();
		listOfLexeme.addAll(sentence);
		return listOfLexeme;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (Lexeme lexeme: sentence){
			buffer.append(lexeme.toString());
		}
		
		return buffer.toString();
	}

	@Override
	public int compareTo(PartOfText o) {
		int result = getAllElements().size()>o.getAllElements().size() ?1:-1;
		return result;
	}
	
	 
	
}
