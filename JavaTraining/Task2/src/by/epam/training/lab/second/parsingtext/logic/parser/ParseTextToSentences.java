package by.epam.training.lab.second.parsingtext.logic.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.training.lab.second.parsingtext.entities.ITextComponent;
import by.epam.training.lab.second.parsingtext.entities.Lexeme;
import by.epam.training.lab.second.parsingtext.entities.Sentence;
import by.epam.training.lab.second.parsingtext.entities.Word;

public class ParseTextToSentences implements IParser {
	private String text;
//	private IParser parseSentenceToLexemes;
	private List<Sentence> textToSentences = new ArrayList<Sentence>();
	private final String blockPattern = "((\\n(class|void|public|String)\\p{ASCII}+?\\}"
			+ "\\p{Space}{1,4})\\p{Upper})";
	private final String sentencePattern = "\\p{Upper}).+?(\\.|\\?|\\!\\:)";
	
	@Override
	public String handleRequest() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Sentence> parseTextToSentence(){
		Pattern pattern = Pattern.compile(sentencePattern);
		Matcher matcher = pattern.matcher(text);
		StringBuilder sb = new StringBuilder();
		while (matcher.find()){
			if (matcher.group() != null){
			sb.append(matcher.group());
			
			}
		} System.out.print(sb.toString());
		
		return textToSentences;
	}

}
