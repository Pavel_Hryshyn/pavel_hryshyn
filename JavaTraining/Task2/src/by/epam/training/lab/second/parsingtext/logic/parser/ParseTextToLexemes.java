package by.epam.training.lab.second.parsingtext.logic.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.training.lab.second.parsingtext.entities.Lexeme;
import by.epam.training.lab.second.parsingtext.entities.PartOfText;
import by.epam.training.lab.second.parsingtext.entities.PunctuationMark;
import by.epam.training.lab.second.parsingtext.entities.Sentence;
import by.epam.training.lab.second.parsingtext.entities.Word;

public class ParseTextToLexemes implements IParser {
	private CollectText collectText = null;
	private List<Lexeme> textToLexemes = new ArrayList<Lexeme>();
	private List<PartOfText> partOfText = new ArrayList<PartOfText>();
	private List<Integer> indexes = new ArrayList<Integer>();
	private String text;
	private final String wordPattern = "(\\p{Alnum})+";
	private final String punctPattern = "\\p{Punct}|\\p{Space}";

	
	public ParseTextToLexemes(String text) {
		super();
		this.text = text;
	}
		 	
	public ParseTextToLexemes(String text, CollectText collectText) {
		super();
		this.text = text;
		this.collectText = collectText;
	}




	@Override
	public List<PartOfText> handleRequest() {
	//	textToLexemes = parseTextToLexemes(text);
		partOfText.add(new Sentence(parseTextToLexemes(text)));
		if (collectText != null){
			return collectText.handleRequest();
		} else {
		return partOfText;
		}
		}


	
	public List<Lexeme> parseTextToLexemes(String text) {
		Lexeme txComponent;
		indexes = indexLexemes();
		int startPoint=0;
		int endPoint=0;
		//Integer breakPoint: indexes
		for (Integer breakPoint: indexes){
			endPoint = breakPoint;
			
			txComponent = parseUnitOfTextComponent(text.substring(startPoint, endPoint));
			if (txComponent != null){
			textToLexemes.add(txComponent);
			}
			
			startPoint = endPoint;
			
		} 
		startPoint = 0;
		endPoint = 0;
		indexes = new ArrayList<Integer>();
		return textToLexemes;
	}
	

	private Lexeme parseUnitOfTextComponent(String str){
		Lexeme unit = new Word();
		Pattern wPattern = Pattern.compile(wordPattern);
		Matcher wMatcher = wPattern.matcher(str);
		
    	Pattern pPattern = Pattern.compile(punctPattern);
		Matcher pMatcher = pPattern.matcher(str);
		
		if (wMatcher.find()){
			unit = new Word(wMatcher.group());
		}
		
		if (pMatcher.find()){
			unit = new PunctuationMark(pMatcher.group());
		}
		
		return unit;
		
	}
	
	private List<Integer> indexLexemes(){
		Pattern pattern = Pattern.compile(punctPattern + "|" + wordPattern);
		Matcher matcher = pattern.matcher(text);
	
		while (matcher.find()){
			if (matcher.group() != null){
			indexes.add(matcher.end());
			}
		} 
		return indexes;
	}
}
