package by.epam.training.lab.second.parsingtext.transformer;

import java.util.List;

import by.epam.training.lab.second.parsingtext.entities.Lexeme;
import by.epam.training.lab.second.parsingtext.entities.PartOfText;
import by.epam.training.lab.second.parsingtext.entities.Sentence;
import by.epam.training.lab.second.parsingtext.entities.Text;
import by.epam.training.lab.second.parsingtext.entities.Word;

public class ReplaceWordsInSentence {
	private Text initialText;
	private Text handledText;
	private final String word = "word";
	
	public ReplaceWordsInSentence() {
		super();
	}
	
	public ReplaceWordsInSentence(Text initialText) {
		super();
		this.initialText = initialText;
	}
	
	public Text replaceWords(Text initialText){
		handledText = new Text(initialText.getParts()); 
		List<PartOfText> handledParts = handledText.getParts();
		
		
		int lastWord = 0;
		Lexeme fWord = null;
		Lexeme lWord = null;
		
		
		for (int i = 0; i < handledParts.size(); i++){
			List <Lexeme> handledSentence = handledParts.get(i).getAllElements();
			if (handledSentence.size() > 2) {
			int firstWord = 0;
			lastWord = handledSentence.size()-1;
			
			while (!handledSentence.get(firstWord).getId().equals(word)){
				firstWord++;
			}
			while (!handledSentence.get(lastWord).getId().equals(word)){
				lastWord--;
			}
			fWord = handledSentence.get(firstWord);
			lWord = handledSentence.get(lastWord);
			handledSentence.add(lastWord, fWord);
			handledSentence.add(firstWord, lWord);
			handledSentence.remove(firstWord+1);
			handledSentence.remove(lastWord+1);
			
			}
			handledParts.add(i, new Sentence(handledSentence));
			handledParts.remove(i+1);
			
		} 
		return handledText;
	}

	public Text getInitialText() {
		return initialText;
	}

	public void setInitialText(Text initialText) {
		this.initialText = initialText;
	}

	public Text getHandledText() {
		return handledText;
	}

	
	
}
