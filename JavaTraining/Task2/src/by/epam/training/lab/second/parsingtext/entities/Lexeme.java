package by.epam.training.lab.second.parsingtext.entities;

public abstract class Lexeme implements Comparable<Lexeme> {
	private String lexeme;
	
		
	public Lexeme() {
		super();
	}


	public Lexeme(String lexeme) {
		super();
		lexeme = lexeme;
	}
	
	 public abstract String getId();
	 public abstract String getLexeme();
	 public abstract void setLexeme(String lexeme);

	
	@Override
	public String toString() {
		return lexeme;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lexeme == null) ? 0 : lexeme.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lexeme other = (Lexeme) obj;
		if (lexeme == null) {
			if (other.lexeme != null)
				return false;
		} else if (!lexeme.equals(other.lexeme))
			return false;
		return true;
	}


	@Override
	public int compareTo(Lexeme arg0) {
		return toString().compareTo(arg0.toString());
	}
	
	
	
	
}
