package by.epam.training.lab.second.parsingtext.entities;

import java.util.ArrayList;
import java.util.List;

public class BlockOfCode implements PartOfText {
	private List<Lexeme> blockOfCode = new ArrayList<Lexeme>();
	
		
	public BlockOfCode() {
		super();
	}

	public BlockOfCode(List<Lexeme> blockOfCode) {
		super();
		this.blockOfCode = blockOfCode;
	}

	@Override
	public void add(Lexeme lexeme) {
		blockOfCode.add(lexeme);
		
	}

	@Override
	public void remove(int index) {
		blockOfCode.remove(index);
		
	}
	

	@Override
	public void removeAll() {
		blockOfCode.clear();
		
	}

	@Override
	public Lexeme getElement(int index) {
		return blockOfCode.get(index);
	}
	
	@Override
	public List<Lexeme> getAllElements() {
		List<Lexeme> listOfLexeme = new ArrayList<Lexeme>();
		listOfLexeme.addAll(blockOfCode);
		return listOfLexeme;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (Lexeme lexeme: blockOfCode){
			buffer.append(lexeme.toString());
		}
		
		return buffer.toString();
	
	}
	
	@Override
	public int compareTo(PartOfText o) {
		int result = getAllElements().size()>o.getAllElements().size() ?1:-1;
		return result;
	}
	
}
