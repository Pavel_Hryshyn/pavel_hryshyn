<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>



<div class="container-fluid">
<table class="table table-striped">
	<tr>
		<td><label for="Login"><fmt:message key="jsp.registration.login" />:</label></td>
		<td><c:out value="${user.login}"/></td>
	</tr>
	<tr>
		<td><label for="firstName"><fmt:message key="jsp.registration.first.name" />:</label></td>
		<td><c:out value="${user.firstName}"/></td>
	</tr>
	<tr>
		<td><label for="lastName"><fmt:message key="jsp.registration.last.name" />:</label></td>
		<td><c:out value="${user.lastName}"/></td>
	</tr>
	<tr>
		<td><label for="Email"><fmt:message key="jsp.registration.email" />:</label></td>
		<td><c:out value="${user.email}"/></td>
	</tr>
</table>
<form>
<input class="btn btn-primary" type="button" value="<fmt:message key="jsp.account.update.user.info" />" onClick='location.href="<tg:path key="update_account"/>"'>
</form>
</div>
