<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-sm-4">
			<div class="btn-group" role="group" aria-label="...">
				<div class="btn-group" role="group">
					<form name="languageForm" method="POST" action="controller">
						<input type="hidden" name="command" value="change_language"/>
						<input type="hidden" name="language" value="ru"/>
						<input class="btn btn-default" type="submit" value="<fmt:message key="jsp.button.language.ru"/>" />
					</form>
				</div>
				<div class="btn-group" role="group">
					<form name="languageForm" method="POST" action="controller">
						<input type="hidden" name="command" value="change_language"/>
						<input type="hidden" name="language" value="en"/>
						<input class="btn btn-default" type="submit" value="<fmt:message key="jsp.button.language.en"/>" />
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
			<c:if test="${not empty user}">
				<i class="glyphicon glyphicon-user"></i> 
				<c:out value="${user.firstName}"/>
				<c:out value="${user.lastName}"/>
			</c:if>
		</div>
	</div>
</div>


