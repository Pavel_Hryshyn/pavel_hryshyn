<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<link rel="stylesheet" href="../css/style.css">
</head>
<c:if test="${empty user}">
	<jsp:include page="/jsp/menu/guestmenu.jsp"></jsp:include>
</c:if>
<c:if test="${user.admin}">
	<jsp:include page="/jsp/menu/adminmenu.jsp"></jsp:include>
</c:if>
<c:if test="${not empty user and not user.admin}">
	<jsp:include page="/jsp/menu/usermenu.jsp"></jsp:include>
</c:if>
</html>