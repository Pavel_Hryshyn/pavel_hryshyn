<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tg" uri="tags"%>

<html lang="$language">
<head>
<title>Created Invoice</title>
</head>
<body>
	<jsp:include page="/jsp/header/header.jsp" />
	<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>

	<form class="form-horizontal" name="AddInvoiceForm" method="POST"
		action="controller">
		<input type="hidden" name="command" value="save_invoice" />
		<div class="form-group">
			<label for="FullName" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.user.full.name" /></label>
			<div class="col-md-4 col-lg-4">
				<label for="FullName" class="control-label"><c:out
						value="${invoice.booking.user.lastName} ${invoice.booking.user.firstName}" /></label>
			</div>
		</div>
		<c:if test="${invoice.id ne 0}">
			<div class="form-group">
				<label for="invoiceId" class="col-md-2 col-lg-2 control-label"><fmt:message
						key="jsp.invoice.id" /></label>
				<div class="col-md-4 col-lg-4">
					<input type="text" class="form-control" id="invoiceId"
						name="invoiceId" value="${invoice.id}" readonly="readonly">
				</div>
			</div>
		</c:if>
		<div class="form-group">
			<label for="totalAmount" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.invoice.total.amount" /></label>
			<div class="col-md-4 col-lg-4">
				<input type="text" class="form-control" id="totalAmount"
					name="totalAmount" value="${invoice.totalAmount}"
					readonly="readonly">
			</div>
		</div>
		<div class="form-group">
			<label for="hotelRoomId" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.room.type" /></label>
			<div class="col-md-4 col-lg-4">
				<select class="form-control" id="hotelRoomId" name="hotelRoomId">
					<c:if test="${not empty invoice.hotelRoom.id}">
						<option selected value="${invoice.hotelRoom.id}">${invoice.hotelRoom.id}</option>
					</c:if>
					<c:forEach items="${freeRooms}" var="room">
						<option value="${room.id}">${room.id}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="invoiceComment" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.invoice.comment" /></label>
			<div class="col-md-4 col-lg-4">
				<textarea class="form-control" rows="5" name="invoiceComment"><c:out
						value="${invoice.invoiceComment}" /></textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="bookingId" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.invoice.id" /></label>
			<div class="col-md-4 col-lg-4">
				<input type="text" class="form-control" id="bookingId"
					name="bookingId" value="${invoice.booking.id}" readonly="readonly">
			</div>
		</div>
		<div class="form-group">
			<label for="RoomType" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.room.type" /></label>
			<div class="col-md-4 col-lg-4">
				<label for="RoomType" class="control-label"><c:out
						value="${invoice.booking.roomType}" /></label>
			</div>
		</div>
		<div class="form-group">
			<label for="startDate" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.start.date" /></label>
			<div class="col-md-4 col-lg-4">
				<label for="startDate" class="control-label"><fmt:formatDate
						value="${invoice.booking.startDate}" pattern="yyyy-MM-dd" /></label>
			</div>
		</div>
		<div class="form-group">
			<label for="endDate" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.end.date" /></label>
			<div class="col-md-4 col-lg-4">
				<label for="endDate" class="control-label"><fmt:formatDate
						value="${invoice.booking.endDate}" pattern="yyyy-MM-dd" /></label>
			</div>
		</div>
		<div class="form-group">
			<label for="bookingComment" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.comment" /></label>
			<div class="col-md-4 col-lg-4">
				<label for="bookingComment" class="control-label"><c:out
						value="${invoice.booking.bookingComment}" /></label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10 col-md-10 col-lg-10">
				<input class="btn btn-primary" type="submit"
					value="<fmt:message key="jsp.button.save"/>" />
			</div>
		</div>

	</form>

	<div class="panel panel-warning">
		<div class="panel-footer"><jsp:include
				page="/jsp/header/footer.jsp" /></div>
	</div>
</body>
</html>