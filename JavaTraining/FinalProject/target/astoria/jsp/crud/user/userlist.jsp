<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>

<html lang="$language">
<head>
	<title><fmt:message key="jsp.title.user.list" /></title>
</head>
<body>
	<jsp:include page="/jsp/header/header.jsp"/>
	<jsp:include page="/jsp/menu/menu.jsp"/>
	<c:if test="${not empty userListEmpty}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${userListEmpty}"/>
		</div>
	</c:if>	

<div class="container-fluid" >
	<div class="row">
	<table  class="table table-striped">
		<thead>
			<tr>
				<th><fmt:message key="jsp.user.id"/></th>
				<th><fmt:message key="jsp.user.login"/></th>
				<th><fmt:message key="jsp.user.first.name"/></th>
				<th><fmt:message key="jsp.user.last.name"/></th>
				<th><fmt:message key="jsp.user.email"/></th>
				<th colspan=2>Action</th>	
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${userList}" var="user">
				<tr>
					<td><c:out value="${user.id}"/></td>
					<td><c:out value="${user.login}"/></td>
					<td><c:out value="${user.firstName}"/></td>
					<td><c:out value="${user.lastName}"/></td>
					<td><c:out value="${user.email}"/></td>
					<td>
					<form name="getBookingList" method="POST" action="controller">
						<input type="hidden" name="command" value="booking_list_by_user" />
						<input type="hidden" name="userId" value="${user.id}">
						<input class="btn btn-default" type="submit" value="<fmt:message key="jsp.account.button.get.booking.list"/>"/>
					</form>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form name="getUserList" method="POST" action="controller">
		<input type="hidden" name="command" value="user_list" />
		<input class="btn btn-primary" type="submit" value="<fmt:message key="jsp.user.button.get.user.list"/>"/>
	</form>
	</div>
</div>
<div class="panel panel-default">
  <div class="panel-footer"><jsp:include page="/jsp/header/footer.jsp"/></div>
</div>
</body>
</html>