<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>

<html lang="$language">
<head>
	<title><fmt:message key="jsp.title.booking.list" /></title>
</head>
<body>
	<jsp:include page="/jsp/header/header.jsp"></jsp:include>
	<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>
	<c:if test="${not empty bookingUpdated}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${bookingUpdated}"/>
		</div>
	</c:if>
	<c:if test="${not empty bookingDeleted}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${bookingDeleted}"/>
		</div>
	</c:if>	
	<c:if test="${not empty bookingListEmpty}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${bookingListEmpty}"/>
		</div>
	</c:if>
	<c:if test="${not empty incorrectFormatUserId}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${incorrectFormatUserId}"/>
		</div>
	</c:if>		

<div class="container-fluid" >
	<div class="row">
	<table class="table table-striped">
		<thead>
			<tr>
				<th><fmt:message key="jsp.booking.id"/></th>
				<th><fmt:message key="jsp.booking.user.last.name"/></th>
				<th><fmt:message key="jsp.booking.number.of.guest"/></th>
				<th><fmt:message key="jsp.booking.room.type"/></th>
				<th><fmt:message key="jsp.booking.start.date"/></th>
				<th><fmt:message key="jsp.booking.end.date"/></th>
				<th><fmt:message key="jsp.booking.comment"/></th>
				<th><fmt:message key="jsp.booking.handled"/></th>
				<th colspan=2>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${bookingList}" var="booking">
				<tr>
					<td><c:out value="${booking.id}"/></td>
					<td><c:out value="${booking.user.lastName}"/></td>
					<td><c:out value="${booking.numberOfGuest}"/></td>
					<td><c:out value="${booking.roomType}"/></td>
					<td><fmt:formatDate pattern="yyyy-MM-dd" value="${booking.startDate}" /></td>
					<td><fmt:formatDate pattern="yyyy-MM-dd" value="${booking.endDate}" /></td>
					<td><c:out value="${booking.bookingComment}"/></td>
					<td><c:out value="${booking.handledBooking}"/></td>
					<td>
					<c:if test="${not booking.handledBooking}">
						<form name="createBookingForm" method="POST" action="controller">
							<input type="hidden" name="command" value="change_invoice"/>
							<input type="hidden" name="invoiceId" value="${invoice.id}">
							<input type="hidden" name="bookingId" value="${booking.id}">
							<input class="btn btn-info" type="submit" value="<fmt:message key="jsp.button.create.invoice"/>"/>
						</form>
					</c:if>
					<c:if test="${booking.handledBooking}">
						<form name="getInvoiceForm" method="POST" action="controller">
							<input type="hidden" name="command" value="GET_INVOICE_BY_BOOKING"/>
							<input type="hidden" name="bookingId" value="${booking.id}">
							<input class="btn btn-default" type="submit" value="<fmt:message key="jsp.button.get.invoice"/>"/>
						</form>
					</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
</div>	
<div class="btn-group" role="group" aria-label="...">
	<div class="btn-group" role="group">
		<form name="getBookingList" method="POST" action="controller">
			<input type="hidden" name="command" value="booking_list" />
			<input class="btn btn-primary" type="submit" value="<fmt:message key="jsp.account.button.get.booking.list"/>"/>
		</form>
	</div>
	<div class="btn-group" role="group">
		<form name="getUnhandledBookingList" method="POST" action="controller">
			<input type="hidden" name="command" value="BOOKING_LIST_UNHANDLED" />
			<input class="btn btn-primary" type="submit" value="<fmt:message key="jsp.button.get.booking.list.unhandled"/>"/>
		</form>
	</div>
</div>

</body>
</html>