<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tg" uri="tags"%>

<html>
<head>
<title><fmt:message key="jsp.title.login" /></title>
</head>
<body>
	<jsp:include page="/jsp/header/header.jsp" />
	<jsp:include page="/jsp/menu/menu.jsp" />

	<div class="row">
		<c:if test="${not empty errorLoginOrPass}">
			<div
				class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-sm-6 col-md-6 col-lg-6 alert alert-danger alert-dismissible"
				role="alert">
				<c:out value="${errorLoginOrPass}" />
			</div>
		</c:if>
	</div>


	<form class="form-horizontal" name="loginForm" method="POST"
		action="controller">
		<input type="hidden" name="command" value="login" />

		<div class="form-group">
			<label for="Login" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.login.login" /></label>
			<div class="col-md-6 col-lg-6">
				<input type="text" class="form-control" id="Login"
					placeholder="<fmt:message key="jsp.login.login" />" name="login"
					value="${lastLogin}">
			</div>
		</div>
		<div class="form-group">
			<label for="Password" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.login.password" /></label>
			<div class="col-md-6 col-lg-6">
				<input type="password" class="form-control" id="Password"
					placeholder="<fmt:message key="jsp.login.password" />" name="password" value="">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10 col-md-10 col-lg-10">
				<input class="btn btn-primary" type="submit"
					value="<fmt:message key="jsp.login.button.sign"/>" />
			</div>
		</div>

	</form>

	<div class="panel panel-warning">
		<div class="panel-footer"><jsp:include
				page="/jsp/header/footer.jsp" /></div>
	</div>
</body>
</html>
