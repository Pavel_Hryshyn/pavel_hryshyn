<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>


<html lang="${language}">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Create account</title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet">
 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<title><fmt:message key="jsp.title.registration"/></title></head>
<body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>

<jsp:include page="/jsp/header/header.jsp"/>
<jsp:include page="/jsp/menu/menu.jsp"/>

<form class="form-horizontal" name="registrationForm" method="POST" action="controller">
  <input type="hidden" name="command" value="registrate" />  
  	<div class="form-group">
    	<label for="Login" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.login" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="Login" placeholder="<fmt:message key="jsp.registration.login" />" name="login" value="${login}">
 		</div>		
 			<c:if test="${not empty userExist or not empty incorrectLogin}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${incorrectLogin}"/>
					<c:out value="${userExist}"/>
				</div>
			</c:if>		
  	</div>
 	<div class="form-group">
    	<label for="Password" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.password" /></label>
    	<div class="col-md-4 col-lg-4">
    		<input type="password" class="form-control" id="Password" placeholder="Password" name="password" value="">
  		</div>
  		<c:if test="${not empty nonConfirmPass or not empty incorrectPass}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${nonConfirmPass}"/>
					<c:out value="${incorrectPass}"/>
				</div>
		</c:if>	
  	</div>
	<div class="form-group">
    	<label for="confirmPassword" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.confirm.password" /></label>
    	<div class="col-md-4 col-lg-4">
    		<input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password" name="confirmPassword" value="">
  		</div>
  	</div>
	<div class="form-group">
    	<label for="firstName" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.first.name" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="firstName" placeholder="<fmt:message key="jsp.registration.first.name" />" name="firstName" value="${firstName}">
 		</div>		
 			<c:if test="${not empty incorrectFirstName}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${incorrectFirstName}"/>
				</div>
			</c:if>		
  	</div>
  	<div class="form-group">
    	<label for="lastName" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.last.name" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="lastName" placeholder="<fmt:message key="jsp.registration.last.name" />" name="lastName" value="${lastName}">
 		</div>		
 			<c:if test="${not empty incorrectLastName}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${incorrectLastName}"/>
				</div>
			</c:if>		
  	</div>
  	<div class="form-group">
    	<label for="Email" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.email" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="email" placeholder="<fmt:message key="jsp.registration.email" />" name="email" value="${email}">
 		</div>		
 			<c:if test="${not empty incorrectEmail}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${incorrectEmail}"/>
				</div>
			</c:if>		
  	</div>
  	  <div class="form-group">
    	<div class="col-sm-offset-2 col-sm-10 col-md-10 col-lg-10">
      		<input class="btn btn-primary" type="submit" value="<fmt:message key="jsp.registration.button.registrate"/>" />
    	</div>
  	</div>
</form>

<div class="panel panel-warning">
	<div class="panel-footer"><jsp:include page="/jsp/header/footer.jsp"/></div>
</div>
</body>
</html>