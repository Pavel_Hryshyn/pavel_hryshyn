<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tg" uri="tags"%>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n/message" scope="application" />

<html lang="language">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!-- Bootstrap -->
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<title><fmt:message key="jsp.title.home" /></title>

</head>
<body>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>


	<jsp:include page="../header/header.jsp" />
	<jsp:include page="../menu/menu.jsp" />

	<div class="container">
		<c:if test="${not empty bookingCreated}">
			<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
				<c:out value="${bookingCreated}" />
			</div>
		</c:if>
		<c:if test="${not empty userCreated}">
			<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
				<c:out value="${userCreated}" />
			</div>
		</c:if>
		<c:if test="${not empty userUpdated}">
			<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
				<c:out value="${userUpdated}" />
			</div>
		</c:if>
	</div>
	<div class="container">
		<div class="row">
			<div class="carousel slide" id="Carousel" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#Carousel" data-slide-to="0"></li>
					<li data-target="#Carousel" data-slide-to="1"></li>
					<li class="active" data-target="#Carousel" data-slide-to="2"></li>
					<li data-target="#Carousel" data-slide-to="3"></li>
					<li data-target="#Carousel" data-slide-to="4"></li>
				</ol>
				<div class="carousel-inner">
					<div class="item">
						<div class="container">
							<div class="row">
								<div
									class="col-md-offset-2 col-md-8 col-md-offset-2 col-lg-offset-2 col-lg-8 col-lg-offset-2">
									<img alt="Standart"
										src="${pageContext.servletContext.contextPath}/images/room/standart.jpg">
								</div>
								<div class="carousel-caption">
									<h1><fmt:message key="jsp.carousel.title.standart" /></h1>
									<h3><fmt:message key="jsp.carousel.description.standart" /></h3>
									<p><fmt:message key="jsp.carousel.price.standart" /></p>
								</div>
							</div>
						</div>
					</div>
					<div class="item active">
						<div class="container">
							<div class="row">
								<div
									class="col-md-offset-2 col-md-8 col-md-offset-2 col-lg-offset-2 col-lg-8 col-lg-offset-2">
									<img alt="Bedroom"
										src="${pageContext.servletContext.contextPath}/images/room/bedroom.jpg">
									<div class="container">
										<div class="carousel-caption">
											<h1><fmt:message key="jsp.carousel.title.bedroom" /></h1>
											<h3><fmt:message key="jsp.carousel.description.bedroom" /></h3>
											<p><fmt:message key="jsp.carousel.price.bedroom" /></p>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="item">
						<div class="container">
							<div class="row">
								<div
									class="col-md-offset-2 col-md-8 col-md-offset-2 col-lg-offset-2 col-lg-8 col-lg-offset-2">
									<img alt="Suite"
										src="${pageContext.servletContext.contextPath}/images/room/suite.jpg">
									<div class="container">
										<div class="carousel-caption">
											<h1><fmt:message key="jsp.carousel.title.suite" /></h1>
											<h3><fmt:message key="jsp.carousel.description.suite" /></h3>
											<p><fmt:message key="jsp.carousel.price.suite" /></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="container">
							<div class="row">
								<div
									class="col-md-offset-2 col-md-8 col-md-offset-2 col-lg-offset-2 col-lg-8 col-lg-offset-2">
									<img alt="Business room"
										src="${pageContext.servletContext.contextPath}/images/room/business_room.jpg">
									<div class="container">
										<div class="carousel-caption">
											<h1><fmt:message key="jsp.carousel.title.business.room" /></h1>
											<h3><fmt:message key="jsp.carousel.description.business.room" /></h3>
											<p><fmt:message key="jsp.carousel.price.business.room" /></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="container">
							<div class="row">
								<div
									class="col-md-offset-2 col-md-8 col-md-offset-2 col-lg-offset-2 col-lg-8 col-lg-offset-2">
									<img alt="President suite"
										src="${pageContext.servletContext.contextPath}/images/room/president_suite.jpg">
									<div class="container">
										<div class="carousel-caption">
											<h1><fmt:message key="jsp.carousel.title.president.suite" /></h1>
											<h3><fmt:message key="jsp.carousel.description.president.suite" /></h3>
											<p><fmt:message key="jsp.carousel.price.president.suite" /></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<a class="left carousel-control" role="button" href="#Carousel"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" role="button" href="#Carousel"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
	<div class="panel panel-warning">
		<div class="panel-footer"><jsp:include
				page="/jsp/header/footer.jsp" /></div>
	</div>
</body>
</html>
