package by.epam.training.finalproject.hotel.database.dbconnection;

import java.util.ResourceBundle;

public class TestDBResourcesManager {
	private final static String DB_RESOURCE_PATH = "testdatabase";
	
	private final static DBResourceManager instance = new DBResourceManager();
	
	private static ResourceBundle bundle = ResourceBundle.getBundle(DB_RESOURCE_PATH);
	
	public static DBResourceManager getInstance() {
		return instance;
	}

	public String getValue(String key){
		return bundle.getString(key);
	}
}
