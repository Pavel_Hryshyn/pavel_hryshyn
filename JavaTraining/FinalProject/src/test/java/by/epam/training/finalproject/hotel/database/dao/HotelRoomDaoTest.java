/**
 * 
 */
package by.epam.training.finalproject.hotel.database.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;

/**
 * @author Pavel Hryshyn
 *
 */
public class HotelRoomDaoTest {
	private static HotelRoom room1;
	private static HotelRoom room2;
	private static HotelRoom room3;
	private static HotelRoom room4;
	private static HotelRoom room5;
	
	private HotelRoomDao roomDao = new HotelRoomDao();
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		room1 = new HotelRoom();
		room2 = new HotelRoom();
		room3 = new HotelRoom();
		room4 = new HotelRoom();
		room5 = new HotelRoom();
		room1.setId(101);
		room1.setCapacity(2);
		room1.setPrice(30.0);
		room1.setRoomType(HotelRoomType.STANDART);
		
		room2.setId(102);
		room2.setCapacity(2);
		room2.setPrice(50.0);
		room2.setRoomType(HotelRoomType.BEDROOM);
		
		room3.setId(103);
		room3.setCapacity(2);
		room3.setPrice(60.0);
		room3.setRoomType(HotelRoomType.SUITE);
		
		room4.setId(104);
		room4.setCapacity(2);
		room4.setPrice(75.0);
		room4.setRoomType(HotelRoomType.BUSINESS_ROOM);
		
		room5.setId(105);
		room5.setCapacity(2);
		room5.setPrice(550.0);
		room5.setRoomType(HotelRoomType.PRESIDENT_SUITES);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	//delete Ignore before test
	@Ignore
	@Test
	public void testFindById() throws DaoTechException{
		assertEquals(room1, roomDao.findById(101));
		assertEquals(room2, roomDao.findById(102));
		assertEquals(room3, roomDao.findById(103));
		assertEquals(room4, roomDao.findById(104));
		assertEquals(room5, roomDao.findById(105));
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindAll() throws DaoTechException{
		List<HotelRoom> rooms = roomDao.findAll();
		
		assertEquals(room1, rooms.get(0));
		assertEquals(room2, rooms.get(1));
		assertEquals(room3, rooms.get(2));
		assertEquals(room4, rooms.get(3));
		assertEquals(room5, rooms.get(4));
	}
	
	@Test(expected = DaoTechException.class)
	public void testDelete() throws DaoTechException{
		roomDao.delete(101);
	}
	
	@Test(expected = DaoTechException.class)
	public void testAdd() throws DaoTechException{
		roomDao.add(room1);
	}
	
	@Test(expected = DaoTechException.class)
	public void testUpdate() throws DaoTechException{
		roomDao.update(room1);
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindAllRoomsByRoomType() throws DaoTechException{
		List<HotelRoom> roomsStandart = roomDao.findAllRoomsByRoomType(HotelRoomType.STANDART);
		List<HotelRoom> roomsSuite = roomDao.findAllRoomsByRoomType(HotelRoomType.SUITE);

		assertEquals(1, roomsStandart.size());
		assertEquals(room1, roomsStandart.get(0));
		
		assertEquals(1, roomsSuite.size());
		assertEquals(room3, roomsSuite.get(0));
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testRoomPriceByRoomType() throws DaoTechException{
		double priceRoomStandart = roomDao.findRoomPriceByRoomType(HotelRoomType.STANDART);
		double priceRoomBedroom = roomDao.findRoomPriceByRoomType(HotelRoomType.BEDROOM);
		double priceRoomSuite = roomDao.findRoomPriceByRoomType(HotelRoomType.SUITE);
		double priceRoomBusiness = roomDao.findRoomPriceByRoomType(HotelRoomType.BUSINESS_ROOM);
		double priceRoomPresident = roomDao.findRoomPriceByRoomType(HotelRoomType.PRESIDENT_SUITES);
		double delta = 0;
		
		assertEquals("Price shoud be equals",room1.getPrice(), priceRoomStandart, delta);
		assertEquals("Price shoud be equals",room2.getPrice(), priceRoomBedroom, delta);
		assertEquals("Price shoud be equals",room3.getPrice(), priceRoomSuite, delta);
		assertEquals("Price shoud be equals",room4.getPrice(), priceRoomBusiness, delta);
		assertEquals("Price shoud be equals",room5.getPrice(), priceRoomPresident, delta);	
	}

}
