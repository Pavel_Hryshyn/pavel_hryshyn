/**
 * 
 */
package by.epam.training.finalproject.hotel.database.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.User;

/**
 * @author Pavel Hryshyn
 *
 */
public class UserDaoTest {
	private static User user1;
	private static User user2;
	private static User user3;
	private static User user4;
	private static UserDao userDao = new UserDao();
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		user1 = new User();
		user2 = new User();
		user3 = new User();
		user1.setId(1);
		user1.setLogin("user1");
		user1.setPassword("12345");
		user1.setFirstName("FirstName");
		user1.setLastName("LastName");
		user1.setEmail("email1@email.com");
		user2.setId(2);
		user2.setLogin("user2");
		user2.setPassword("qwerty");
		user2.setFirstName("FirstName");
		user2.setLastName("LastName");
		user2.setEmail("email2@email.com");		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
//		userDao.delete(user3.getId());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		
	}
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindUserByLogin() throws DaoTechException{
		User actUser1 = userDao.findUserByLogin(user1.getLogin());
		User actUser2 = userDao.findUserByLogin(user2.getLogin());
		
		assertEquals("This users should be some", user1, actUser1);
		assertEquals("This users should be some", user2, actUser2);
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testAdd() throws DaoTechException {
		user3.setLogin("user3");
		user3.setPassword("12345");
		user3.setFirstName("FirstNameTh");
		user3.setLastName("LastNameTh");
		user3.setEmail("email3@email.com");
				
		userDao.add(user3);
				
		User actUser3 = userDao.findUserByLogin(user3.getLogin());
		
		assertEquals("This users should be some", user3, actUser3);
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testUpdate() throws DaoTechException {
		user3 = userDao.findUserByLogin(user3.getLogin());
		user3.setPassword("54321");
		user3.setFirstName("FirstNameThUpdate");
		user3.setLastName("LastNameThUpdate");
		user3.setEmail("email3update@email.com");
				
		userDao.update(user3);
				
		User actUser3 = userDao.findUserByLogin(user3.getLogin());
		
		assertEquals("This users should be some", user3, actUser3);
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindUserByLoginPassword() throws DaoTechException{
		User actualUser = userDao.findUserByLoginPassword(user1.getLogin(), user1.getPassword());
		System.out.println(actualUser);
		assertEquals("This users should be some", user1, actualUser);
		
	}

	//delete Ignore before test
	@Ignore
	@Test
	public void testFindById() throws DaoTechException{
		User tempUser1 = userDao.findUserByLogin(user1.getLogin());
		User actUser1 = userDao.findById(tempUser1.getId());
		
		assertEquals("This users should be some", user1, actUser1);
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindAll() throws DaoTechException{
		List<User> users = userDao.findAll();
		
		assertEquals("List size should be equals", 3, users.size());
		assertEquals("This users should be some", user1, users.get(0));
		assertEquals("This users should be some", user2, users.get(1));
		assertEquals("This users should be some", user3, users.get(2));
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testDelete() throws DaoTechException{
		user4 = new User();
		user4.setLogin("user4");
		user4.setPassword("12345");
		user4.setFirstName("FirstNameFor");
		user4.setLastName("LastNameFor");
		user4.setEmail("email4@email.com");
				
		userDao.add(user4);
		User actUser = userDao.findUserByLogin(user4.getLogin());
		assertEquals("This users should be some", user4, actUser);
		
		userDao.delete(actUser.getId());
		
		assertNull("User should be null", userDao.findUserByLogin(user4.getLogin()));
	}

}
