/**
 * 
 */
package by.epam.training.finalproject.hotel.database.dao;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
	
/**
 * @author Pavel Hryshyn
 *
 */
public class BookingDaoTest {
	private final static String PATTERN_FORMAT_DATE = "pattern.format.date";
	
	private static UserDao userDao;
	private static BookingDao bookingDao;
	private static Booking booking1;
	private static Booking booking2;
	private static Booking booking3;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		userDao = new UserDao();
		bookingDao = new BookingDao();
		booking1 = new Booking();
		booking2 = new Booking();
		booking3 = new Booking();
		booking1.setId(1);
		booking1.setNumberOfGuest(1);
		Date startDate1 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-02");
		Date endDate1 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-04");
		booking1.setStartDate(startDate1);
		booking1.setEndDate(endDate1);
		booking1.setHandledBooking(true);
		booking1.setRoomType(HotelRoomType.STANDART);
		booking1.setUser(userDao.findById(1));
		
		booking2.setId(2);
		booking2.setNumberOfGuest(2);
		Date startDate2 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-05");
		Date endDate2 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-09");
		booking2.setStartDate(startDate2);
		booking2.setEndDate(endDate2);
		booking2.setHandledBooking(false);
		booking2.setRoomType(HotelRoomType.SUITE);
		booking2.setUser(userDao.findById(2));
		
		booking3.setNumberOfGuest(2);
		Date startDate3 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-10");
		Date endDate3 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-12");
		booking3.setStartDate(startDate3);
		booking3.setEndDate(endDate3);
		booking3.setRoomType(HotelRoomType.SUITE);
		booking3.setUser(userDao.findById(2));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
			
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/*
	@Test
	public void testFindAll() throws DaoTechException {
		List<Booking> bookingList = bookingDao.findAll();
		
		assertEquals("List size should be equals", 2, bookingList.size());
		assertEquals("This booking should be some", booking1, bookingList.get(0));
		assertEquals("This booking should be some", booking2, bookingList.get(1));
	}
	
	@Test
	public void testFindAllUnhandledBooking() throws DaoTechException {
		List<Booking> bookingList = bookingDao.findAllUnhandledBooking();
		
		assertEquals("List size should be equals", 1, bookingList.size());
		assertEquals("This booking should be some", booking2, bookingList.get(0));
	}
	
	@Test
	public void testFindById() throws DaoTechException {
		Booking actBooking1 = bookingDao.findById(1);
		Booking actBooking2 = bookingDao.findById(2);
		
		assertEquals("This booking should be some", booking1, actBooking1);
		assertEquals("This booking should be some", booking2, actBooking2);
	}
	
	@Test
	public void testFindByUserId() throws DaoTechException {
		List<Booking> bookingList1 = bookingDao.findBookingByUserId(1);
		List<Booking> bookingList2 = bookingDao.findBookingByUserId(2);
		
		assertEquals("List size should be equals", 1, bookingList1.size());
		assertEquals("List size should be equals", 1, bookingList2.size());
		
		assertEquals("This booking should be some", booking1, bookingList1.get(0));
		assertEquals("This booking should be some", booking2, bookingList2.get(0));
	}

	@Test
	public void testAddUpdateDelete() throws DaoTechException {
		List<Booking> bookingListBeforeAdd = bookingDao.findBookingByUserId(2);		
		assertEquals("List size should be equals", 1, bookingListBeforeAdd.size());
		
		bookingDao.add(booking3);
		List<Booking> bookingListAfterAdd = bookingDao.findBookingByUserId(2);
		booking3.setId(bookingListAfterAdd.get(1).getId());
		assertEquals("List size should be equals", 2, bookingListAfterAdd.size());
		assertEquals("This booking should be some", booking3, bookingListAfterAdd.get(1));		
	
		Booking actBookingBeforeUpdate = bookingDao.findById(booking3.getId());
		assertEquals("This booking should be some", booking3, actBookingBeforeUpdate);
		
		booking3.setNumberOfGuest(1);
		booking3.setBookingComment("Comment");
		booking3.setRoomType(HotelRoomType.PRESIDENT_SUITES);
		bookingDao.update(booking3);
		Booking actBookingAfterUpdate = bookingDao.findById(booking3.getId());
		assertEquals("This booking should be some", booking3, actBookingAfterUpdate);
		
		bookingDao.delete(booking3.getId());
		List<Booking> bookingListAfterDelete = bookingDao.findBookingByUserId(2);
		assertEquals("List size should be equals", 1, bookingListAfterDelete.size());
	}
*/	
}
