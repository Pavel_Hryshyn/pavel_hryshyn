/*
 * @(#)Booking.java   1.0 2015/05/13
 */
package by.epam.training.finalproject.hotel.model.entities;

import java.io.Serializable;
import java.util.Date;

import by.epam.training.finalproject.hotel.model.entities.interfaces.IEntity;

/**
 * This class is entity that defines the parameters of booking the hotel room
 * @version 1.0 13 May 2015
 * @author Pavel Hryshyn
 */
public class Booking implements Serializable, IEntity {
	private static final long serialVersionUID = 2981834649959516247L;

	private int id;
	private int numberOfGuest;
	private Date startDate;
	private Date endDate;
	private String bookingComment;
	private boolean handledBooking = false;
	private HotelRoomType roomType;
	private User user;
	
	public Booking() {
		super();
	}

	@Override
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumberOfGuest() {
		return numberOfGuest;
	}

	public void setNumberOfGuest(int numberOfGuest) {
		this.numberOfGuest = numberOfGuest;
	}

	public HotelRoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(HotelRoomType roomType) {
		this.roomType = roomType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBookingComment() {
		return bookingComment;
	}

	public void setBookingComment(String bookingComment) {
		this.bookingComment = bookingComment;
	}

	public boolean isHandledBooking() {
		return handledBooking;
	}

	public void setHandledBooking(boolean handledBooking) {
		this.handledBooking = handledBooking;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bookingComment == null) ? 0 : bookingComment.hashCode());
		result = prime * result + id;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + (handledBooking ? 1231 : 1237);
		result = prime * result + numberOfGuest;
		result = prime * result
				+ ((roomType == null) ? 0 : roomType.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Booking other = (Booking) obj;
		if (bookingComment == null) {
			if (other.bookingComment != null)
				return false;
		} else if (!bookingComment.equals(other.bookingComment))
			return false;
		if (id != other.id)
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (handledBooking != other.handledBooking)
			return false;
		if (numberOfGuest != other.numberOfGuest)
			return false;
		if (roomType != other.roomType)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Booking: No=" + id + ", numberOfGuest="
				+ numberOfGuest + ", startDate=" + startDate + ", endDate="
				+ endDate + ", bookingComment=" + bookingComment
				+ ", handledBooking=" + handledBooking + ", roomType="
				+ roomType + ", user=" + user;
	}

	
	
	
}
