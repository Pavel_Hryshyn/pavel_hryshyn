/*
 * @(#)InvoiceDao.java   1.2 2015/05/20
 */
package by.epam.training.finalproject.hotel.database.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.database.dao.interfaces.IInvoiceDao;
import by.epam.training.finalproject.hotel.database.dao.util.ConvertToSQLDate;
import by.epam.training.finalproject.hotel.database.dao.util.DaoCheckersForLogging;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
import by.epam.training.finalproject.hotel.model.entities.Invoice;
import by.epam.training.finalproject.hotel.model.entities.User;

/**
 * This class implements DAO pattern and contains methods that bind the HotelRoom entity 
 * with database
 * @version 1.2 20 May 2015
 * @author Pavel Hryshyn
 */
public class InvoiceDao extends AbstractDao implements IInvoiceDao {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(InvoiceDao.class);
	
	/** This constants stores names of database column */
	private final static String INVOICE_ID = "id";
	private final static String TOTAL_AMOUNT = "total_amount";
	private final static String INVOICE_COMMENT = "invoice_comment";
	private final static String BOOKING_ID = "booking.id";
	private final static String NUMBER_OF_GUEST = "guest_number";
	private final static String START_DATE = "start_date";
	private final static String END_DATE = "end_date";
	private final static String IS_HANDLED = "is_handled";
	private final static String BOOKING_COMMENT = "booking_comment";
	private final static String ROOM_TYPE = "room_type_type";
	private final static String USER_ID = "users.id";
	private final static String LOGIN = "login";
	private final static String FIRST_NAME = "first_name";
	private final static String LAST_NAME = "last_name";
	private final static String EMAIL = "email";
	private final static String ROOM_ID = "hotel_rooms.id";
	private final static String CAPACITY = "capacity";
	private final static String PRICE = "price";
	private final static String DESCRIPTION = "description";
		
	/** This constants stores SQL queries for database */
	private final static String SQL_FIND_ALL_INVOICES = "sql.invoice.find.all";
	private final static String SQL_FIND_BY_ID = "sql.invoice.find.by.id";
	private final static String SQL_FIND_BY_USER_ID = "sql.invoice.find.by.user.id";
	private final static String SQL_FIND_NONFREE_ROOMS_BETWEEN_DATES = "sql.invoice.find.rooms.between.dates";
	private final static String SQL_FIND_NONFREE_ROOMS_BETWEEN_DATES_BY_TYPE = "sql.invoice.find.rooms.between.dates.by.type";
	private final static String SQL_DELETE = "sql.invoice.delete";
	private final static String SQL_ADD = "sql.invoice.add";
	private final static String SQL_UPDATE = "sql.invoice.update";
	private final static String SQL_FIND_BY_BOOKING_ID = "sql.invoice.find.by.booking.id";
	
	/** This constants stores exception messages */
	private static final String EXC_SQL = "dao.exc.sql";
	 
	/** This constants stores logger messages */
	private static final String LOGGER_MSG_INVOICE_LIST_EMPTY = "logger.message.invoice.list.empty";
	private static final String LOGGER_MSG_INVOICE_FIND_ALL = "logger.message.invoice.find.all";
	private static final String LOGGER_MSG_INVOICE_FIND_ALL_BY_USER = "logger.message.booking.find.all.by.user";
	private static final String LOGGER_MSG_INVOICE_FIND_BY_ID = "logger.message.invoice.find.by.id";
	private static final String LOGGER_MSG_INVOICE_FIND_BY_ID_NOT_FOUND = "logger.message.invoice.find.by.id.not.found";
	private static final String LOGGER_MSG_INVOICE_FIND_BY_BOOKING_ID = "logger.message.invoice.find.by.booking.id";
	private static final String LOGGER_MSG_INVOICE_FIND_BY_BOOKING_ID_NOT_FOUND = "logger.message.invoice.find.by.booking.id.not.found";
	private static final String LOGGER_MSG_INVOICE_DELETE = "logger.message.invoice.delete";
	private static final String LOGGER_MSG_INVOICE_NO_DELETE = "logger.message.invoice.delete.not";
	private static final String LOGGER_MSG_INVOICE_DELETE_NOT_FOUND = "logger.message.invoice.delete.not.found";
	private static final String LOGGER_MSG_INVOICE_ADD = "logger.message.invoice.add";
	private static final String LOGGER_MSG_INVOICE_NO_ADD = "logger.message.invoice.add.not";
	private static final String LOGGER_MSG_INVOICE_UPDATE = "logger.message.invoice.update";
	private static final String LOGGER_MSG_INVOICE_NO_UPDATE = "logger.message.invoice.update.not";
	private static final String LOGGER_MSG_ROOM_LIST_EMPTY = "logger.message.invoice.room.list.empty";
	private static final String LOGGER_MSG_ROOM_FIND_ALL = "logger.message.invoice.room.list.find.all";
	
	/**
	 * This method returns all invoices that stores in database
	 * @return list of all invoices from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<Invoice> findAll() throws DaoTechException {
		connection = getConnection();
		List<Invoice> invoices = new ArrayList<Invoice>();
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_ALL_INVOICES));
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				invoices.add(buildInvoice(resultSet));
			}
			DaoCheckersForLogging.isEmptyListChecker(invoices, logger, LOGGER_MSG_INVOICE_LIST_EMPTY, LOGGER_MSG_INVOICE_FIND_ALL);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return invoices;
	}
	
	/**
	 * This method returns all invoices of some user that stores in database
	 * @return list of all invoices of some from database
	 * @param id
	 * 			the param is user id and uses for searching user's invoice list from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<Invoice> findInvoiceByUserId(Integer id) throws DaoTechException {
		connection = getConnection();
		List<Invoice> invoices = new ArrayList<Invoice>();
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_USER_ID));
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				invoices.add(buildInvoice(resultSet));
			}
			DaoCheckersForLogging.isEmptyListChecker(invoices, logger, LOGGER_MSG_INVOICE_LIST_EMPTY, LOGGER_MSG_INVOICE_FIND_ALL_BY_USER);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return invoices;
	}

	/**
	 * This method returns invoice with some id that stores in database
	 * @param id
	 * 			the param uses for searching invoice in database
	 * @return invoice with some id from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public Invoice findById(Integer id) throws DaoTechException {
		connection = getConnection();
		Invoice invoice = null;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_ID));
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()){
				invoice = buildInvoice(resultSet);
				logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_FIND_BY_ID) + id);
			} else {
				logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_FIND_BY_ID_NOT_FOUND) + id);
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return invoice;
	}
	
	/**
	 * This method checks that invoice with some id is exist and deletes it from database
	 * @param id
	 * 			the param uses for deleting invoice from database
	 * @return true if invoice is deleted from database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean delete(Integer id) throws DaoTechException {
		Invoice invoice = findById(id);
		boolean isDeleted = false;
		int row = 0;
		connection = getConnection();
		try {
			if (invoice != null) {
				PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_DELETE));
				statement.setInt(1, id);
				row = statement.executeUpdate();
				isDeleted = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_INVOICE_DELETE, LOGGER_MSG_INVOICE_NO_DELETE);
			} else {
				logger.info(id + MessageManager.getValue(LOGGER_MSG_INVOICE_DELETE_NOT_FOUND));
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isDeleted;
	}

	/**
	 * This method adds invoice to database
	 * @param invoice
	 * 			the param uses for adding invoice to database
	 * @return true if invoice is added to database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean add(Invoice invoice) throws DaoTechException {
		connection = getConnection();
		boolean isAdded = false;
		int row = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_ADD));
			statement.setDouble(1, invoice.getTotalAmount());
			statement.setString(2, invoice.getInvoiceComment());
			statement.setInt(3, invoice.getBooking().getId());
			statement.setInt(4, invoice.getHotelRoom().getId());
			row = statement.executeUpdate();
			isAdded = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_INVOICE_ADD, LOGGER_MSG_INVOICE_NO_ADD);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isAdded;
	}

	/**
	 * This method updates invoice to database
	 * @param invoice
	 * 			the param uses for updating invoice to database
	 * @return true if invoice is updated to database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean update(Invoice invoice) throws DaoTechException {
		connection = getConnection();
		boolean isUpdated = false;
		int row = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_UPDATE));
			statement.setString(1, invoice.getInvoiceComment());
			statement.setInt(2, invoice.getHotelRoom().getId());
			statement.setInt(3, invoice.getId());
			row = statement.executeUpdate();
			isUpdated = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_INVOICE_UPDATE, LOGGER_MSG_INVOICE_NO_UPDATE);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isUpdated;
	}
	
	/**
	 * This method returns invoice with some booking id that stores in database
	 * @param id
	 * 			the param uses for searching invoice in database
	 * @return invoice with some booking id from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public Invoice findInvoiceByBookingId(Integer id) throws DaoTechException {
		connection = getConnection();
		Invoice invoice = null;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_BOOKING_ID));
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()){
				invoice = buildInvoice(resultSet);
				logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_FIND_BY_BOOKING_ID) + id);
			} else {
				logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_FIND_BY_BOOKING_ID_NOT_FOUND) + id);
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return invoice;
	}
	
	/**
	 * This method returns list of all non-free rooms between dates that store in database
	 * @return list of all non-free rooms between dates from database
	 * @param startDate
	 * 			the param defines start date and uses for searching list of non-free rooms from database
	 * @param endDate
	 * 			the param defines end date and uses for searching list of non-free rooms from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<HotelRoom> findNonFreeRoomBetweenDates(Date startDate, Date endDate) throws DaoTechException{
		connection = getConnection();
		List<HotelRoom> rooms = new ArrayList<HotelRoom>();
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_NONFREE_ROOMS_BETWEEN_DATES));
			statement.setDate(1, ConvertToSQLDate.convert(startDate));
			statement.setDate(2, ConvertToSQLDate.convert(endDate));
			statement.setDate(3, ConvertToSQLDate.convert(startDate));
			statement.setDate(4, ConvertToSQLDate.convert(endDate));
			statement.setDate(5, ConvertToSQLDate.convert(startDate));
			statement.setDate(6, ConvertToSQLDate.convert(endDate));
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				rooms.add(buildRoom(resultSet));
			}
			DaoCheckersForLogging.isEmptyListChecker(rooms, logger, LOGGER_MSG_ROOM_LIST_EMPTY, LOGGER_MSG_ROOM_FIND_ALL);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return rooms;
	}
	
	/**
	 * This method returns list of all non-free rooms between dates and with some type that store in database
	 * @return list of all non-free rooms between dates and with some type from database
	 * @param startDate
	 * 			the param defines start date and uses for searching list of non-free rooms from database
	 * @param endDate
	 * 			the param defines end date and uses for searching list of non-free rooms from database
	 * @param type
	 * 			the param defines type of room and uses for searching list of non-free rooms from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	public List<HotelRoom> findNonFreeRoomBetweenDatesByTypes(Date startDate, Date endDate, HotelRoomType type) throws DaoTechException {
		connection = getConnection();
		List<HotelRoom> rooms = new ArrayList<HotelRoom>();
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_NONFREE_ROOMS_BETWEEN_DATES_BY_TYPE));
			statement.setString(1, type.toString());
			statement.setDate(2, ConvertToSQLDate.convert(startDate));
			statement.setDate(3, ConvertToSQLDate.convert(endDate));
			statement.setDate(4, ConvertToSQLDate.convert(startDate));
			statement.setDate(5, ConvertToSQLDate.convert(endDate));
			statement.setDate(6, ConvertToSQLDate.convert(startDate));
			statement.setDate(7, ConvertToSQLDate.convert(endDate));
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				rooms.add(buildRoom(resultSet));
			}
			DaoCheckersForLogging.isEmptyListChecker(rooms, logger, LOGGER_MSG_ROOM_LIST_EMPTY, LOGGER_MSG_ROOM_FIND_ALL);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return rooms;
	}
	
	/**
	 * This method builds invoice from ResultSet
	 * @param resultSet
	 * 			the param uses for returning result of query from database
	 * @return invoice
	 * @throws SQLException, if arise database access error or other database errors
	 */
	private Invoice buildInvoice(ResultSet resultSet) throws SQLException, DaoTechException{
		Invoice invoice = new Invoice();
		User user = buildUser(resultSet);
		Booking booking = buildBooking(resultSet, user);
		HotelRoom room = buildRoom(resultSet);
		
		invoice.setId(resultSet.getInt(INVOICE_ID));
		invoice.setTotalAmount(resultSet.getDouble(TOTAL_AMOUNT));
		invoice.setInvoiceComment(resultSet.getString(INVOICE_COMMENT));
		invoice.setBooking(booking);
		invoice.setHotelRoom(room);
		return invoice;
	}
	
	/**
	 * This method builds user from ResultSet
	 * @param resultSet
	 * 			the param uses for returning result of query from database
	 * @return user
	 * @throws SQLException, if arise database access error or other database errors
	 */
	private User buildUser(ResultSet resultSet) throws SQLException{
		User user = new User();
		user.setId(resultSet.getInt(USER_ID));
		user.setLogin(resultSet.getString(LOGIN));
		user.setFirstName(resultSet.getString(FIRST_NAME));
		user.setLastName(resultSet.getString(LAST_NAME));
		user.setEmail(resultSet.getString(EMAIL));
		return user;
	}
	
	/**
	 * This method builds booking from ResultSet
	 * @param resultSet
	 * 			the param uses for returning result of query from database
	 * @return booking
	 * @throws SQLException, if arise database access error or other database errors
	 */
	private Booking buildBooking(ResultSet resultSet, User user) throws SQLException{
		Booking booking = new Booking();
		booking.setId(resultSet.getInt(BOOKING_ID));
		booking.setNumberOfGuest(resultSet.getInt(NUMBER_OF_GUEST));
		booking.setStartDate(resultSet.getDate(START_DATE));
		booking.setEndDate(resultSet.getDate(END_DATE));
		booking.setHandledBooking(resultSet.getBoolean(IS_HANDLED));
		booking.setBookingComment(resultSet.getString(BOOKING_COMMENT));
		booking.setRoomType(HotelRoomType.valueOf(resultSet.getString(ROOM_TYPE)));
		booking.setUser(user);
		return booking;
	}
	
	/**
	 * This method builds room from ResultSet
	 * @param resultSet
	 * 			the param uses for returning result of query from database
	 * @return room
	 * @throws SQLException, if arise database access error or other database errors
	 */
	private HotelRoom buildRoom(ResultSet resultSet) throws SQLException{
		HotelRoom room = new HotelRoom();
		room.setId(resultSet.getInt(ROOM_ID));
		room.setCapacity(resultSet.getInt(CAPACITY));
		room.setPrice(resultSet.getDouble(PRICE));
		room.setDescription(resultSet.getString(DESCRIPTION));
		room.setRoomType(HotelRoomType.valueOf(resultSet.getString(ROOM_TYPE)));
		return room;
	}
}
