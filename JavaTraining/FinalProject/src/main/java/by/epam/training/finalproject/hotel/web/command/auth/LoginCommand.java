/*
 * @(#)LoginCommand.java   1.0 2015/06/01
 */
package by.epam.training.finalproject.hotel.web.command.auth;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.UserDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and creates user session 
 * @version 1.0 01 June 2015
 * @author Pavel Hryshyn
 */
public class LoginCommand extends Command {
	/** This object obtains logger for this class*/
	private static Logger logger = Logger.getLogger(LoginCommand.class);
	
	/** This constants store names of request parameters */
	private static final String PARAM_USER = "user";
	private static final String PARAM_LOGIN = "login";
	private static final String PARAM_LAST_LOGIN = "lastLogin";
	private static final String PARAM_PASSWORD = "password";
	private static final String PARAM_ERROR_LOGIN = "errorLoginOrPass";
	
	/** This constants store path to jsp pages */
	private static final String PATH_PAGE_MAIN = "path.page.main";
	private static final String PATH_PAGE_LOGIN = "path.page.login";
	private static final String PATH_PAGE_ERROR = "path.page.error";
	
	/** This constants store messages that output to jsp page */
	private static final String MSG_ERROR_LOGIN = "jsp.message.error.login";
	
	/** This constants store logger messages */
	private static final String LOGGER_MSG_COMMAND_SESSION_CREATED = "logger.message.command.session.created";
	private static final String LOGGER_MSG_INCORRECT_LOGIN = "logger.message.command.user.incorrect.login.or.password";
	
	
	/** This field is UserDao object */
	private UserDao userDao = new UserDao();
	
	/**
	 * This method creates user session
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		User user = null;
		String login = request.getParameter(PARAM_LOGIN);
		String password = request.getParameter(PARAM_PASSWORD);
		
		try {
			if (login.length() >0 && password.length() >0) {
				user = userDao.findUserByLoginPassword(login, password);
			}	
			if (user != null){
				request.getSession().setAttribute(PARAM_USER, user);
				page = PageConfigurationManager.getValue(PATH_PAGE_MAIN);
				logger.info(MessageManager.getValue(LOGGER_MSG_COMMAND_SESSION_CREATED) + user.getLogin());
			} else {
				request.setAttribute(PARAM_LAST_LOGIN, login);
				request.setAttribute(PARAM_ERROR_LOGIN, MessageManager.getValue(MSG_ERROR_LOGIN));
				logger.debug(MessageManager.getValue(LOGGER_MSG_INCORRECT_LOGIN));
				page = PageConfigurationManager.getValue(PATH_PAGE_LOGIN);
			}
			} catch (DaoTechException e) {
				logger.error(e);
				page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
				return page;
			} 
		return page;
	}
}
