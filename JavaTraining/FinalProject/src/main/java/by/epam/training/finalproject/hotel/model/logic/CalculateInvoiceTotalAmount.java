/*
 * @(#)CalculateInvoiceTotalAmount.java   1.0 2015/06/15
 */
package by.epam.training.finalproject.hotel.model.logic;

import java.util.Date;

/**
 * This class contains methods to calculate total amount of invoice 
 * @version 1.0 15 June 2015
 * @author Pavel Hryshyn
 */
public class CalculateInvoiceTotalAmount {
	/** This constant store amount of milliseconds in day */
	private final static long DAY_IN_MILLISECONDS = 86400000;
	
	/**
	 * This method calculates the total amount of invoice 
	 * @param price
	 * 			the param is the room amount per night
	 * @param startDate
	 * 			the param is the check-in date
	 * @param endDate
	 * 			the param is the check-out date
	 * @return the total amount of invoice
	 */
	public static double calculate(double price, Date startDate, Date endDate){
		double totalAmount = 0;
		long diffDays = (endDate.getTime() - startDate.getTime())/DAY_IN_MILLISECONDS;
		totalAmount = price*diffDays;
		return totalAmount;
	}
}

	