/*
 * @(#)ContextListener.java   1.0 2015/07/05
 */
package by.epam.training.finalproject.hotel.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * This class is listener for event creating and deleting of servlet context 
 * @version 1.0 05 July 2015
 * @author Pavel Hryshyn
 */
public class ContextListener implements ServletContextListener {	
	/** This constants store reference root path of context */
	private static final String ROOT_PATH = "rootPath";

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// TODO Auto-generated method stub

	}

	/**
	 * This method gets path to servlet context 
	 * @param event
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		System.setProperty(ROOT_PATH, context.getRealPath("/"));
	}
}
