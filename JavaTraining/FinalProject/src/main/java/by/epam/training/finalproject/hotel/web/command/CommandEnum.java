/*
 * @(#)CommandEnum.java   1.0 2015/06/02
 */
package by.epam.training.finalproject.hotel.web.command;

import by.epam.training.finalproject.hotel.web.command.auth.LoginCommand;
import by.epam.training.finalproject.hotel.web.command.auth.LogoutCommand;
import by.epam.training.finalproject.hotel.web.command.auth.RegistrationCommand;
import by.epam.training.finalproject.hotel.web.command.crud.booking.BookingListByUserIdCommand;
import by.epam.training.finalproject.hotel.web.command.crud.booking.BookingListCommand;
import by.epam.training.finalproject.hotel.web.command.crud.booking.DeleteBookingCommand;
import by.epam.training.finalproject.hotel.web.command.crud.booking.SaveBookingCommand;
import by.epam.training.finalproject.hotel.web.command.crud.booking.UnhandledBookingListCommand;
import by.epam.training.finalproject.hotel.web.command.crud.booking.UpdateBookingCommand;
import by.epam.training.finalproject.hotel.web.command.crud.invoice.DeleteInvoiceCommand;
import by.epam.training.finalproject.hotel.web.command.crud.invoice.GetInvoiceByBookingIdCommand;
import by.epam.training.finalproject.hotel.web.command.crud.invoice.GetInvoiceByIdCommand;
import by.epam.training.finalproject.hotel.web.command.crud.invoice.InvoiceListCommand;
import by.epam.training.finalproject.hotel.web.command.crud.invoice.SaveInvoiceCommand;
import by.epam.training.finalproject.hotel.web.command.crud.user.UpdateUserCommand;
import by.epam.training.finalproject.hotel.web.command.crud.user.UserListCommand;
import by.epam.training.finalproject.hotel.web.command.language.ChangeLanguageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.AccountPageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.BookingsPageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.CreateInvoicePageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.DeniedAccessPageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.HomePageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.InvoicesPageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.LoginPageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.NewBookingPageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.RegistrationPageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.UpdateAccountPageCommand;
import by.epam.training.finalproject.hotel.web.command.navigation.UsersPageCommand;
import by.epam.training.finalproject.hotel.web.util.UserRole;

/**
 * This enum stores commands and defines access rules to it
 * @version 1.0 02 June 2015
 * @author Pavel Hryshyn
 */
public enum CommandEnum {
	LOGIN(UserRole.GUEST) {{this.command = new LoginCommand();}},
	LOGOUT(UserRole.USER) {{this.command = new LogoutCommand();}},
	REGISTRATE(UserRole.GUEST){{this.command = new RegistrationCommand();}},
	UPDATE_USER(UserRole.USER){{this.command = new UpdateUserCommand();}},
	USER_LIST(UserRole.ADMIN){{this.command = new UserListCommand();}},
	CHANGE_LANGUAGE(UserRole.ALL){{this.command = new ChangeLanguageCommand();}},
	BOOKING_LIST(UserRole.ADMIN){{this.command = new BookingListCommand();}},
	BOOKING_LIST_UNHANDLED(UserRole.ADMIN){{this.command = new UnhandledBookingListCommand();}},
	BOOKING_LIST_BY_USER(UserRole.USER){{this.command = new BookingListByUserIdCommand();}},
	UPDATE_BOOKING(UserRole.USER){{this.command = new UpdateBookingCommand();}},
	DELETE_BOOKING(UserRole.USER){{this.command = new DeleteBookingCommand();}},
	SAVE_BOOKING(UserRole.USER){{this.command = new SaveBookingCommand();}},
	INVOICE_LIST(UserRole.ADMIN){{this.command = new InvoiceListCommand();}},
	GET_INVOICE(UserRole.ADMIN){{this.command = new GetInvoiceByIdCommand();}},
	GET_INVOICE_BY_BOOKING(UserRole.USER){{this.command = new GetInvoiceByBookingIdCommand();}},
	SAVE_INVOICE(UserRole.ADMIN){{this.command = new SaveInvoiceCommand();}},
	DELETE_INVOICE(UserRole.ADMIN){{this.command = new DeleteInvoiceCommand();}},
	
	/** Navigation commands */
	HOME(UserRole.ALL){{this.command = new HomePageCommand();}},
	BOOKINGS(UserRole.USER){{this.command = new BookingsPageCommand();}},
	INVOICES(UserRole.ADMIN){{this.command = new InvoicesPageCommand();}},
	LOGIN_PAGE(UserRole.GUEST){{this.command = new LoginPageCommand();}},
	REGISTRATION(UserRole.GUEST){{this.command = new RegistrationPageCommand();}},
	ACCOUNT(UserRole.USER){{this.command = new AccountPageCommand();}},
	NEW_BOOKING(UserRole.USER){{this.command = new NewBookingPageCommand();}},
	USERS(UserRole.ADMIN){{this.command = new UsersPageCommand();}},
	UPDATE_ACCOUNT(UserRole.USER){{this.command = new UpdateAccountPageCommand();}},
	CHANGE_INVOICE(UserRole.ADMIN){{this.command = new CreateInvoicePageCommand();}},
	DENIED_ACCESS(UserRole.ALL){{this.command = new DeniedAccessPageCommand();}};
	
	/** This field defines command */
	protected Command command;
	
	/** This field defines access rules for commands */
	private UserRole userRole;

	private CommandEnum(UserRole userRole) {
		this.userRole = userRole;
	}
	
	private CommandEnum() {
	}

	public UserRole getUserRole() {
		return userRole;
	} 
	
	public Command getCommand() {
		return command;
	}	
}
