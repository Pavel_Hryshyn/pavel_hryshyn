/*
 * @(#)SaveBookingCommand.java   1.5 2015/06/18
 */
package by.epam.training.finalproject.hotel.web.command.crud.booking;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.CommandCheckers;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and save booking to database
 * 
 * @version 1.5 18 June 2015
 * @author Pavel Hryshyn
 */
public class SaveBookingCommand extends Command {
	/** This object obtains logger for this class */
	private final static Logger logger = Logger
			.getLogger(SaveBookingCommand.class);

	/** This constants store names of request parameters */
	private final static String USER = "user";
	private final static String PARAM_BOOKING = "booking";
	private final static String PARAM_BOOKING_ID = "bookingId";
	private final static String PARAM_NUMBER_OF_GUEST = "numberOfGuest";
	private final static String PARAM_INCORRECT_NUMBER_OF_GUEST = "incorrectNumberOfGuest";
	private final static String PARAM_START_DATE = "startDate";
	private final static String PARAM_END_DATE = "endDate";
	private final static String PARAM_ERROR_START_DATE_BEFORE = "errorStartDateBefore";
	private final static String PARAM_ERROR_END_DATE_BEFORE = "errorEndDateBefore";
	private final static String PARAM_INCORRECT_START_DATE = "incorrectStartDate";
	private final static String PARAM_INCORRECT_END_DATE = "incorrectEndDate";
	private final static String PARAM_BOOKING_COMMENT = "bookingComment";
	private final static String PARAM_ROOM_TYPE = "roomType";
	private final static String PARAM_INCORRECT_ROOM_TYPE = "incorrectRoomType";
	private final static String PARAM_BOOKING_CREATED = "bookingCreated";
	private final static String PARAM_BOOKING_UPDATED = "bookingUpdated";

	/** This constants store messages that output to jsp page */
	private final static String MSG_INCORRECT_NUMBER_OF_GUEST = "jsp.message.incorrect.guest.number";
	private final static String MSG_INCORRECT_DATE = "jsp.message.incorrect.date";
	private final static String MSG_ERROR_START_DATE_BEFORE = "jsp.message.error.start.date.before";
	private final static String MSG_ERROR_END_DATE_BEFORE = "jsp.message.error.end.date.before";
	private final static String MSG_INCORRECT_ROOM_TYPE = "jsp.message.incorrect.room.type";
	private final static String MSG_BOOKING_CREATED = "jsp.message.booking.created";
	private final static String MSG_BOOKING_UPDATED = "jsp.message.booking.updated";

	/** This constants store logger messages */
	private final static String PATH_PAGE_MAIN = "path.page.main";
	private final static String PATH_PAGE_ERROR = "path.page.error";
	private final static String PATH_PAGE_ADD_BOOKING = "path.page.booking.add";
	private final static String PATH_PAGE_BOOKING_LIST = "path.page.booking.list";

	/** This constants store format date */
	private final static String PATTERN_FORMAT_DATE = "pattern.format.date";

	/** This constants store logger messages */
	private final static String LOGGER_MSG_BOOKING_ADD = "logger.message.command.booking.add";
	private final static String LOGGER_MSG_BOOKING_UPDATE = "logger.message.command.booking.update";

	private BookingDao bookingDao = new BookingDao();
	private Booking booking;
	private Date startDate;
	private Date endDate;

	/**
	 * This method save booking to database
	 * 
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		booking = new Booking();

		try {
			if (request.getParameter(PARAM_BOOKING_ID) != null) {
				int bookingId = Integer.parseInt(request
						.getParameter(PARAM_BOOKING_ID));
				booking.setId(bookingId);
			}
			if (build(request)) {
				page = saveBooking(request, booking);
			} else {
				page = PageConfigurationManager.getValue(PATH_PAGE_ADD_BOOKING);
				setInputDate(request);
				setBookingId(request);
			}
		} catch (ParseException e) {
			logger.debug(e);
			request.setAttribute(PARAM_INCORRECT_START_DATE,
					MessageManager.getValue(MSG_INCORRECT_DATE));
			request.setAttribute(PARAM_INCORRECT_END_DATE,
					MessageManager.getValue(MSG_INCORRECT_DATE));
			logger.debug(MessageManager.getValue(MSG_INCORRECT_DATE));
			setInputDate(request);
			setBookingId(request);
			page = PageConfigurationManager.getValue(PATH_PAGE_ADD_BOOKING);
			return page;
		} catch (NumberFormatException e) {
			logger.debug(e);
			request.setAttribute(PARAM_INCORRECT_NUMBER_OF_GUEST,
					MessageManager.getValue(MSG_INCORRECT_NUMBER_OF_GUEST));
			setInputDate(request);
			setBookingId(request);
			page = PageConfigurationManager.getValue(PATH_PAGE_ADD_BOOKING);
			return page;
		} catch (IllegalArgumentException e) {
			logger.debug(e);
			request.setAttribute(PARAM_INCORRECT_ROOM_TYPE,
					MessageManager.getValue(MSG_INCORRECT_ROOM_TYPE));
			setInputDate(request);
			setBookingId(request);
			page = PageConfigurationManager.getValue(PATH_PAGE_ADD_BOOKING);
			return page;
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		return page;
	}

	/**
	 * This method checks if building of booking finished successful
	 * 
	 * @param request
	 * @return true if building of booking finished successful, else return
	 *         false
	 */
	public boolean build(HttpServletRequest request) throws ParseException {
		boolean isBuilt = true;
		isBuilt &= buildNumberOfGuest(request);
		isBuilt &= buildStartDate(request);
		isBuilt &= buildEndDate(request);
		isBuilt &= buildBookingComment(request);
		isBuilt &= buildRoomType(request);
		isBuilt &= buildUser(request);
		return isBuilt;
	}

	/**
	 * This method builds guest number in booking
	 * 
	 * @param request
	 * @return true if guest number is built successful, else return false
	 */
	private boolean buildNumberOfGuest(HttpServletRequest request) {
		boolean isBuilt = false;
		int numberOfGuest = Integer.parseInt(request
				.getParameter(PARAM_NUMBER_OF_GUEST));
		if (numberOfGuest > 0) {
			booking.setNumberOfGuest(numberOfGuest);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_NUMBER_OF_GUEST,
					MessageManager.getValue(MSG_INCORRECT_NUMBER_OF_GUEST));
			logger.debug(MessageManager.getValue(MSG_INCORRECT_NUMBER_OF_GUEST));
		}
		return isBuilt;
	}

	/**
	 * This method builds booking's start date
	 * 
	 * @param request
	 * @return true if start date is built successful, else return false
	 * @throws ParseException
	 */
	private boolean buildStartDate(HttpServletRequest request)
			throws ParseException {
		boolean isBuilt = false;
		Date currentDate = new Date(System.currentTimeMillis());
		startDate = new SimpleDateFormat(
				MessageManager.getValue(PATTERN_FORMAT_DATE)).parse(request
				.getParameter(PARAM_START_DATE));
		if (CommandCheckers.checkSequenceDate(currentDate, startDate)) {
			booking.setStartDate(startDate);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_ERROR_START_DATE_BEFORE,
					MessageManager.getValue(MSG_ERROR_START_DATE_BEFORE));
			logger.debug(MessageManager.getValue(MSG_ERROR_START_DATE_BEFORE));
		}
		return isBuilt;
	}

	/**
	 * This method builds booking's end date
	 * 
	 * @param request
	 * @return true if end date is built successful, else return false
	 * @throws ParseException
	 */
	private boolean buildEndDate(HttpServletRequest request)
			throws ParseException {
		boolean isBuilt = false;
		endDate = new SimpleDateFormat(
				MessageManager.getValue(PATTERN_FORMAT_DATE)).parse(request
				.getParameter(PARAM_END_DATE));
		if (CommandCheckers.checkSequenceDate(startDate, endDate)) {
			booking.setEndDate(endDate);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_ERROR_END_DATE_BEFORE,
					MessageManager.getValue(MSG_ERROR_END_DATE_BEFORE));
			logger.debug(MessageManager.getValue(MSG_ERROR_END_DATE_BEFORE));
		}
		return isBuilt;
	}

	/**
	 * This method builds booking comment
	 * 
	 * @param request
	 * @return true if booking comment is built successful, else return false
	 */
	private boolean buildBookingComment(HttpServletRequest request) {
		boolean isBuilt = false;
		String bookingComment = request.getParameter(PARAM_BOOKING_COMMENT);
		booking.setBookingComment(bookingComment);
		isBuilt = true;
		return isBuilt;
	}

	/**
	 * This method builds room type in booking
	 * 
	 * @param request
	 * @return true if room type is built successful, else return false
	 */
	private boolean buildRoomType(HttpServletRequest request) {
		boolean isBuilt = false;
		HotelRoomType roomType = HotelRoomType.valueOf(request
				.getParameter(PARAM_ROOM_TYPE));
		booking.setRoomType(roomType);
		isBuilt = true;
		return isBuilt;
	}

	/**
	 * This method builds user from session
	 * 
	 * @param request
	 * @return true if user is built successful, else return false
	 */
	private boolean buildUser(HttpServletRequest request) {
		boolean isBuilt = false;
		User user = (User) request.getSession().getAttribute(USER);
		if (user != null) {
			booking.setUser(user);
			isBuilt = true;
		}
		return isBuilt;
	}

	/**
	 * This method set input data to jsp page
	 * 
	 * @param request
	 */
	private void setInputDate(HttpServletRequest request) {
		request.setAttribute(PARAM_BOOKING, booking);
	}

	/**
	 * This method set bookingId to jsp page
	 * 
	 * @param request
	 */
	private void setBookingId(HttpServletRequest request) {
		if (booking.getId() != 0) {
			request.setAttribute(PARAM_BOOKING_ID, booking.getId());
		}
	}

	/**
	 * This method adds booking to database
	 * 
	 * @param request
	 * @return page where request is forwarded
	 * @throws DaoTechException
	 */
	private String addBooking(HttpServletRequest request)
			throws DaoTechException {
		String page = null;
		bookingDao.add(booking);
		request.setAttribute(PARAM_BOOKING_CREATED,
				MessageManager.getValue(MSG_BOOKING_CREATED));
		page = PageConfigurationManager.getValue(PATH_PAGE_MAIN);
		return page;
	}

	/**
	 * This method updates booking to database
	 * 
	 * @param request
	 * @return page where request is forwarded
	 * @throws DaoTechException
	 */
	private String updateBooking(HttpServletRequest request)
			throws DaoTechException {
		String page = null;
		bookingDao.update(booking);
		request.setAttribute(PARAM_BOOKING_UPDATED,
				MessageManager.getValue(MSG_BOOKING_UPDATED));
		page = PageConfigurationManager.getValue(PATH_PAGE_BOOKING_LIST);
		return page;
	}

	/**
	 * This method saves booking to database
	 * 
	 * @param request
	 * @return page where request is forwarded
	 * @throws DaoTechException
	 */
	private String saveBooking(HttpServletRequest request, Booking booking)
			throws DaoTechException {
		String page = null;
		if (booking.getId() == 0) {
			page = addBooking(request);
			logger.info(MessageManager.getValue(LOGGER_MSG_BOOKING_ADD)
					+ booking.getId());
		} else {
			page = updateBooking(request);
			logger.info(MessageManager.getValue(LOGGER_MSG_BOOKING_UPDATE)
					+ booking.getId());
		}
		return page;
	}
}
