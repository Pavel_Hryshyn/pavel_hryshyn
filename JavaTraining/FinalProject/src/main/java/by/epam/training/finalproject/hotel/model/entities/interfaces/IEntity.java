/*
 * @(#)IEntity.java   1.0 2015/05/15
 */
package by.epam.training.finalproject.hotel.model.entities.interfaces;

/**
 * This interface defines common method for all entities
 * @version 1.0 12 May 2015
 * @author Pavel Hryshyn
 */
public interface IEntity {
	int getId();
}
