/*
 * @(#)DeleteInvoiceCommand.java   1.0 2015/06/20
 */
package by.epam.training.finalproject.hotel.web.command.crud.invoice;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.InvoiceDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.Invoice;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and delete invoice from database
 * 
 * @version 1.2 10 June 2015
 * @author Pavel Hryshyn
 */
public class DeleteInvoiceCommand extends Command {
	/** This object obtains logger for this class */
	Logger logger = Logger.getLogger(DeleteInvoiceCommand.class);

	/** This constants store names of request parameters */
	private final static String PARAM_INVOICE_ID = "invoiceId";
	private final static String PARAM_INVOICE_LIST = "invoiceList";
	private final static String PARAM_INVOICE_DELETED = "invoiceDeleted";

	/** This constants store messages that output to jsp page */
	private final static String MSG_INVOICE_DELETED_SUCCESS = "jsp.message.invoice.delete.success";
	private final static String MSG_INVOICE_DELETED_FAILURE = "jsp.message.invoice.delete.failure";

	/** This constants store path to jsp pages */
	private final static String PATH_PAGE_ERROR = "path.page.error";
	private final static String PATH_PAGE_INVOICE_LIST = "path.page.invoice.list";

	/** This constants store logger messages */
	private final static String LOGGER_MSG_INVOICE_DELETE = "logger.message.command.invoice.delete";
	private final static String LOGGER_MSG_INVOICE_DELETE_FAILURE = "logger.message.command.invoice.delete.failure";

	private InvoiceDao invoiceDao = new InvoiceDao();

	/**
	 * This method deletes invoice from database
	 * 
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			int invoiceId = Integer.parseInt(request
					.getParameter(PARAM_INVOICE_ID));
			if (delete(request, invoiceId)) {
				request.setAttribute(PARAM_INVOICE_DELETED,
						MessageManager.getValue(MSG_INVOICE_DELETED_SUCCESS));
				logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_DELETE));
			} else {
				request.setAttribute(PARAM_INVOICE_DELETED,
						MessageManager.getValue(MSG_INVOICE_DELETED_FAILURE));
				logger.info(MessageManager
						.getValue(LOGGER_MSG_INVOICE_DELETE_FAILURE));
			}
			request.setAttribute(PARAM_INVOICE_LIST, invoiceDao.findAll());
			page = PageConfigurationManager.getValue(PATH_PAGE_INVOICE_LIST);
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		return page;
	}

	/**
	 * This method deletes invoice from database
	 * 
	 * @param request
	 * @param invoiceId
	 * @throws DaoTechException
	 * @return true if invoice is deleted, else return false
	 */
	private boolean delete(HttpServletRequest request, int invoiceId)
			throws DaoTechException {
		boolean isDeleted = false;
		Invoice invoice = invoiceDao.findById(invoiceId);
		isDeleted = invoiceDao.delete(invoiceId);
		if (isDeleted) {
			BookingDao bookingDao = new BookingDao();
			Booking booking = invoice.getBooking();
			booking.setHandledBooking(false);
			bookingDao.update(booking);
		}
		return isDeleted;
	}
}
