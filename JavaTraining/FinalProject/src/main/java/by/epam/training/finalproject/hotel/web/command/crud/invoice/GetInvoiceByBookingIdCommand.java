/*
 * @(#)GetInvoiceByBookingIdCommand.java   1.2 2015/06/18
 */
package by.epam.training.finalproject.hotel.web.command.crud.invoice;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.InvoiceDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Invoice;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and gets invoice with some booking id
 * from database
 * 
 * @version 1.2 18 June 2015
 * @author Pavel Hryshyn
 */
public class GetInvoiceByBookingIdCommand extends Command {
	/** This object obtains logger for this class */
	private final static Logger logger = Logger
			.getLogger(GetInvoiceByBookingIdCommand.class);

	/** This constants store names of request parameters */
	private final static String PARAM_BOOKING_ID = "bookingId";
	private final static String PARAM_INVOICE = "invoice";
	private final static String PARAM_INVOICE_EMPTY = "invoiceEmpty";
	private final static String PARAM_INVOICE_CONFIRM = "invoiceConfirm";

	/** This constants store path to jsp pages */
	private final static String PATH_PAGE_ERROR = "path.page.error";
	private final static String PATH_PAGE_GET_INVOICE = "path.page.invoice.get";

	/** This constants store messages that output to jsp page */
	private final static String MSG_INVOICE_EMPTY = "jsp.message.invoice.empty";
	private final static String MSG_INVOICE_CONFIRM = "jsp.message.invoice.confirm";

	/** This constants store logger messages */
	private final static String LOGGER_MSG_INVOICE_GET = "logger.message.command.invoice.by.booking.get";
	private final static String LOGGER_MSG_INVOICE_NOT_FOUND = "logger.message.command.invoice.by.booking.not.found";

	private InvoiceDao invoiceDao = new InvoiceDao();

	/**
	 * This method gets invoice with some booking id from database
	 * 
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			int bookingId = Integer.parseInt(request
					.getParameter(PARAM_BOOKING_ID));
			Invoice invoice = invoiceDao.findInvoiceByBookingId(bookingId);
			if (invoice != null) {
				request.setAttribute(PARAM_INVOICE, invoice);
				request.setAttribute(PARAM_INVOICE_CONFIRM,
						MessageManager.getValue(MSG_INVOICE_CONFIRM));
				logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_GET));
			} else {
				request.setAttribute(PARAM_INVOICE_EMPTY,
						MessageManager.getValue(MSG_INVOICE_EMPTY));
				logger.debug(MessageManager
						.getValue(LOGGER_MSG_INVOICE_NOT_FOUND));
			}
			page = PageConfigurationManager.getValue(PATH_PAGE_GET_INVOICE);
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		return page;
	}
}
