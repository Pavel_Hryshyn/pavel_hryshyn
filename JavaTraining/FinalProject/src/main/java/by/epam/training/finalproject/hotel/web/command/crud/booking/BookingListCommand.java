/*
 * @(#)BookingListCommand.java   1.2 2015/06/10
 */
package by.epam.training.finalproject.hotel.web.command.crud.booking;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and get list of all booking from database
 * @version 1.2 10 June 2015
 * @author Pavel Hryshyn
 */
public class BookingListCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(BookingListCommand.class);
	
	/** This constants store names of request parameters */
	private final static String PARAM_BOOKING_LIST = "bookingList";
	private final static String PARAM_BOOKING_LIST_EMPTY = "bookingListEmpty";
	
	/** This constants store path to jsp pages */
	private final static String PATH_BOOKING_LIST = "path.page.booking.list";
	private final static String PATH_PAGE_ERROR = "path.page.error";
	
	/** This constants store messages that output to jsp page */
	private final static String MSG_BOOKING_LIST_EMPTY = "jsp.message.booking.list.empty";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_BOOKING_LIST_GET = "logger.message.command.booking.list.get";
	private final static String LOGGER_MSG_BOOKING_LIST_EMPTY = "logger.message.command.booking.list.empty";
	
	private BookingDao bookingDao = new BookingDao();	
	
	/**
	 * This method gets list of all booking from database
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			List<Booking> bookingList = bookingDao.findAll();
			if (bookingList != null){
				request.setAttribute(PARAM_BOOKING_LIST, bookingList);
				logger.info(MessageManager.getValue(LOGGER_MSG_BOOKING_LIST_GET));
			} else {
				request.setAttribute(PARAM_BOOKING_LIST_EMPTY, MessageManager.getValue(MSG_BOOKING_LIST_EMPTY));
				logger.info(MessageManager.getValue(LOGGER_MSG_BOOKING_LIST_EMPTY));
			}		
			page = PageConfigurationManager.getValue(PATH_BOOKING_LIST);
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		return page;
	}
}
