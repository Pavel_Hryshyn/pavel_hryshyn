/*
 * @(#)IBookingDao.java   1.5 2015/06/15
 */
package by.epam.training.finalproject.hotel.database.dao.interfaces;

import java.util.List;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.Booking;

/**
 * This interface defines methods that bind the Booking entity with database
 * @version 1.5 15 June 2015
 * @author Pavel Hryshyn
 */
public interface IBookingDao extends ICommonDao<Integer, Booking> {
	List<Booking> findBookingByUserId(Integer id) throws DaoTechException;
	List<Booking> findAllUnhandledBooking() throws DaoTechException;
}
