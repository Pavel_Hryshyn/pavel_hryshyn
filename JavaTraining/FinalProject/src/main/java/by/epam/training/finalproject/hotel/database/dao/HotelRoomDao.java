/*
 * @(#)HotelRoomDao.java   1.2 2015/05/15
 */
package by.epam.training.finalproject.hotel.database.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.database.dao.interfaces.IHotelRoomDao;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;

/**
 * This class implements DAO pattern and contains methods that bind the HotelRoom entity 
 * with database
 * @version 1.2 15 May 2015
 * @author Pavel Hryshyn
 */
public class HotelRoomDao extends AbstractDao implements IHotelRoomDao {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(HotelRoomDao.class);
	
	/** This constants stores names of database column */
	private final static String ROOM_ID = "id";
	private final static String CAPACITY = "capacity";
	private final static String PRICE = "price";
	private final static String DESCRIPTION = "description";
	private final static String ROOM_TYPE = "room_type_type";
	
	/** This constants stores SQL queries for database */
	private final String SQL_FIND_ALL_ROOMS = "sql.rooms.find.all";
	private final String SQL_FIND_BY_ID = "sql.rooms.find.by.id";
	private final String SQL_FIND_BY_TYPE = "sql.rooms.find.by.type";
	private final String SQL_FIND_PRICE_BY_TYPE = "sql.rooms.find.price.by.type";
	
	/** This constants stores exception messages */
	private static final String EXC_SQL = "dao.exc.sql";
	private static final String EXC_UNSUPPORTED_OPERATION = "dao.exc.unsupported.operation";
	
	/** This constants stores logger messages */
	private static final String LOGGER_MSG_ROOM_FIND_ALL = "logger.message.room.find.all";
	private static final String LOGGER_MSG_ROOM_FIND_ALL_BY_TYPE = "logger.message.room.find.all.by.type";
	private static final String LOGGER_MSG_ROOM_FIND_BY_ID = "logger.message.room.find.by.id";
	private static final String LOGGER_MSG_ROOM_FIND_BY_ID_NOT_FOUND = "logger.message.room.find.by.id.not.found";
	private static final String LOGGER_MSG_ROOM_FIND_PRICE = "logger.message.room.find.price";
	private static final String LOGGER_MSG_ROOM_PRICE_NOT_FOUND = "logger.message.room.find.price.not.found";
	
	/**
	 * This method returns list of all rooms in hotel that store in database
	 * @return list of all hotel room from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<HotelRoom> findAll() throws DaoTechException {
		connection = getConnection();
		List<HotelRoom> rooms = new ArrayList<HotelRoom>();
		try {
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(MessageManager.getValue(SQL_FIND_ALL_ROOMS));
			while (resultSet.next()){
				rooms.add(buildRoom(resultSet));
			}
			logger.info(MessageManager.getValue(LOGGER_MSG_ROOM_FIND_ALL));
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return rooms;
	}
	
	/**
	 * This method returns room with some id that stores in database
	 * @param id
	 * 			the param uses for searching room in database
	 * @return room with some id from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public HotelRoom findById(Integer id) throws DaoTechException {
		connection = getConnection();
		HotelRoom room = null;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_ID));
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()){
				room = buildRoom(resultSet);
				logger.info(MessageManager.getValue(LOGGER_MSG_ROOM_FIND_BY_ID) + id);
			} else {
				logger.info(MessageManager.getValue(LOGGER_MSG_ROOM_FIND_BY_ID_NOT_FOUND) + id);
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return room;
	}

	/**
	 * This method is unsupported for HotelRoom entity
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean delete(Integer id) throws DaoTechException {
		try {
			throw new UnsupportedOperationException();
		} catch (UnsupportedOperationException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_UNSUPPORTED_OPERATION), e);
		}
		
	}

	/**
	 * This method is unsupported for HotelRoom entity
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean add(HotelRoom entity) throws DaoTechException {
		try {
			throw new UnsupportedOperationException();
		} catch (UnsupportedOperationException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_UNSUPPORTED_OPERATION), e);
		}
	}

	/**
	 * This method is unsupported for HotelRoom entity
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean update(HotelRoom entity) throws DaoTechException {
		try {
			throw new UnsupportedOperationException();
		} catch (UnsupportedOperationException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_UNSUPPORTED_OPERATION), e);
		}
	}

	/**
	 * This method returns list of all rooms with some type that store in database
	 * @param roomType
	 * 			the param uses for searching rooms in database
	 * @return list of all hotel room with some type from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<HotelRoom> findAllRoomsByRoomType(HotelRoomType roomType) throws DaoTechException {
		connection = getConnection();
		List<HotelRoom> rooms = new ArrayList<HotelRoom>();
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_TYPE));
			statement.setString(1, roomType.toString());
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				rooms.add(buildRoom(resultSet));
			}
			logger.info(MessageManager.getValue(LOGGER_MSG_ROOM_FIND_ALL_BY_TYPE));
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return rooms;
	}
	
	/**
	 * This method returns nightly rate of room with some type that store in database
	 * @param roomType
	 * 			the param uses for searching room in database
	 * @return nightly rate of room with some type from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public Double findRoomPriceByRoomType(HotelRoomType roomType) throws DaoTechException{
		connection = getConnection();
		Double price = null;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_PRICE_BY_TYPE));
			statement.setString(1, roomType.toString());
			resultSet = statement.executeQuery();
			if (resultSet.next()){
				price = resultSet.getDouble("price");
				logger.info(MessageManager.getValue(LOGGER_MSG_ROOM_FIND_PRICE) + roomType);
			} else {
				logger.info(MessageManager.getValue(LOGGER_MSG_ROOM_PRICE_NOT_FOUND) + roomType);
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return price;
	}
	
	/**
	 * This method builds HotelRoom from ResultSet
	 * @param resultSet
	 * 			the param uses for returning result of query from database
	 * @return room that build from query
	 * @throws SQLException, if arise database access error or other database errors
	 */
	private HotelRoom buildRoom(ResultSet resultSet) throws SQLException{
		HotelRoom room = new HotelRoom();
		room.setId(resultSet.getInt(ROOM_ID));
		room.setCapacity(resultSet.getInt(CAPACITY));
		room.setPrice(resultSet.getDouble(PRICE));
		room.setDescription(resultSet.getString(DESCRIPTION));
		room.setRoomType(HotelRoomType.valueOf(resultSet.getString(ROOM_TYPE)));
		return room;
	}
}
