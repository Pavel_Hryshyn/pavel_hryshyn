/*
 * @(#)ICommonDao.java   1.0 2015/05/15
 */
package by.epam.training.finalproject.hotel.database.dao.interfaces;

import java.util.List;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.interfaces.IEntity;

/**
 * This interface defines common methods that bind the entities with database
 * @version 1.0 15 May 2015
 * @author Pavel Hryshyn
 */
public interface ICommonDao <K extends Number, T extends IEntity> {
	List<T> findAll() throws DaoTechException;
	T findById(K id) throws DaoTechException;
	boolean delete(K id) throws DaoTechException;
	boolean add(T entity) throws DaoTechException;
	boolean update(T entity) throws DaoTechException;
}
