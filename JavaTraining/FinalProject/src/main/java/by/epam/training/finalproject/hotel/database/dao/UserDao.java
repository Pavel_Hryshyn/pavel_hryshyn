/*
 * @(#)UserDao.java   1.3 2015/05/21
 */
package by.epam.training.finalproject.hotel.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.database.dao.interfaces.IUserDao;
import by.epam.training.finalproject.hotel.database.dao.util.DaoCheckersForLogging;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.User;

/**
 * This class implements DAO pattern and contains methods that bind the User entity 
 * with database
 * @version 1.3 21 May 2015
 * @author Pavel Hryshyn
 */
public class UserDao extends AbstractDao implements IUserDao {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(UserDao.class);
	
	/** This constants stores names of database column */
	private final static String USER_ID = "id";
	private final static String LOGIN = "login";
	private final static String FIRST_NAME = "first_name";
	private final static String LAST_NAME = "last_name";
	private final static String EMAIL = "email";
	private final static String ADMIN = "admin";
	
	/** This constants stores SQL queries for database */
	private static final String SQL_FIND_ALL_USERS = "sql.user.find.all.users";
	private static final String SQL_FIND_BY_LOGIN_PASSWORD = "sql.user.find.by.login.password";
	private static final String SQL_FIND_BY_LOGIN = "sql.user.find.by.login";
	private static final String SQL_FIND_BY_ID = "sql.user.find.by.id";
	private static final String SQL_ADD = "sql.user.add";
	private static final String SQL_DELETE = "sql.user.delete";
	private static final String SQL_UPDATE = "sql.user.update";
	private static final String SQL_UPDATE_PASSWORD = "sql.user.update.password";
	
	/** This constants stores exception messages */
	private static final String EXC_SQL = "dao.exc.sql";
	
	/** This constants stores logger messages */
	private static final String LOGGER_MSG_USER_LIST_EMPTY = "logger.message.user.list.empty";
	private static final String LOGGER_MSG_USER_FIND_ALL = "logger.message.user.find.all";
	private static final String LOGGER_MSG_USER_FIND_BY_LOGIN = "logger.message.user.find.by.login";
	private static final String LOGGER_MSG_USER_FIND_BY_LOGIN_NOT_FOUND = "logger.message.user.find.by.login.not.found";
	private static final String LOGGER_MSG_USER_FIND_BY_LOGIN_PASSWORD = "logger.message.user.find.by.login.password";
	private static final String LOGGER_MSG_USER_FIND_BY_LOGIN_PASSWORD_NOT_FOUND = "logger.message.user.find.by.login.password.not.found";
	private static final String LOGGER_MSG_USER_FIND_BY_ID = "logger.message.user.find.by.id";
	private static final String LOGGER_MSG_USER_FIND_BY_ID_NOT_FOUND = "logger.message.user.find.by.id.not.found";
	private static final String LOGGER_MSG_USER_DELETE = "logger.message.user.delete";
	private static final String LOGGER_MSG_USER_NOT_DELETE = "logger.message.user.delete.not";
	private static final String LOGGER_MSG_USER_DELETE_NOT_FOUND = "logger.message.user.delete.not.found";
	private static final String LOGGER_MSG_USER_ADD = "logger.message.user.add";
	private static final String LOGGER_MSG_USER_NO_ADD = "logger.message.user.add.not";
	private static final String LOGGER_MSG_USER_UPDATE = "logger.message.user.update";
	private static final String LOGGER_MSG_USER_NO_UPDATE = "logger.message.user.update.not";
	private static final String LOGGER_MSG_PASSWORD_UPDATE = "logger.message.user.password.update";
	private static final String LOGGER_MSG_PASSWORD_NO_UPDATE = "logger.message.user.password.update.not";
	
	/**
	 * This method returns all users that stores in database
	 * @return list of all users from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<User> findAll() throws DaoTechException {
		connection = getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		List<User> userList = new ArrayList<User>();
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(MessageManager.getValue(SQL_FIND_ALL_USERS));
			while (resultSet.next()){
				userList.add(buildUser(resultSet));
			}
			DaoCheckersForLogging.isEmptyListChecker(userList, logger, LOGGER_MSG_USER_LIST_EMPTY, LOGGER_MSG_USER_FIND_ALL);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return userList;
	}
	
	/**
	 * This method returns user with some login that stores in database
	 * @param login
	 * 			the param uses for searching user in database
	 * @return user with some login from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public User findUserByLogin(String login) throws DaoTechException {
		connection = getConnection();
		User user = null;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_LOGIN));
			statement.setString(1, login);
			resultSet = statement.executeQuery();
			if (resultSet.next()){
				user = buildUser(resultSet);
				logger.info(MessageManager.getValue(LOGGER_MSG_USER_FIND_BY_LOGIN) + login);
			} else {
				logger.info(MessageManager.getValue(LOGGER_MSG_USER_FIND_BY_LOGIN_NOT_FOUND) + login);
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}	
		return user;
	}
	
	/**
	 * This method returns user with some login and password that stores in database
	 * @param login
	 * 			the param uses for searching user in database
	 * @param password
	 * 			the param uses for user validation when creating session
	 * @return user with login and password from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public User findUserByLoginPassword(String login, String password) throws DaoTechException {
		connection = getConnection();
		User user = null;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_LOGIN_PASSWORD));
			statement.setString(1, login);
			statement.setString(2, password);
			resultSet = statement.executeQuery();
			if (resultSet.next()){
				user = buildUser(resultSet);
				logger.info(MessageManager.getValue(LOGGER_MSG_USER_FIND_BY_LOGIN_PASSWORD) + login);
			} else {
				logger.info(MessageManager.getValue(LOGGER_MSG_USER_FIND_BY_LOGIN_PASSWORD_NOT_FOUND) + login);
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return user;
	}

	/**
	 * This method returns user with some id that stores in database
	 * @param id
	 * 			the param uses for searching user in database
	 * @return user with some id from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public User findById(Integer id) throws DaoTechException {
		connection = getConnection();
		User user = null;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_ID));
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()){
				user = buildUser(resultSet);
				logger.info(MessageManager.getValue(LOGGER_MSG_USER_FIND_BY_ID) + id);
			} else {
				logger.info(MessageManager.getValue(LOGGER_MSG_USER_FIND_BY_ID_NOT_FOUND) + id);
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}	
		return user;
	}

	/**
	 * This method checks that user with some id is exist and deletes it from database
	 * @param id
	 * 			the param uses for deleting user from database
	 * @return true if user is deleted from database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean delete(Integer id) throws DaoTechException {
		User user = findById(id);
		int row = 0;
		connection = getConnection();
		boolean isDeleted = false;
		try {
			if (user != null) {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_DELETE));
			statement.setInt(1, id);
			row = statement.executeUpdate();
			isDeleted = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_USER_DELETE, LOGGER_MSG_USER_NOT_DELETE);
			} else {
				logger.info(id + MessageManager.getValue(LOGGER_MSG_USER_DELETE_NOT_FOUND));
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isDeleted;
	}

	/**
	 * This method adds user to database
	 * @param user
	 * 			the param uses for adding user to database
	 * @return true if user is added to database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean add(User user) throws DaoTechException {
		connection = getConnection();
		boolean isAdded = false;
		int row = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_ADD));
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getFirstName());
			statement.setString(4, user.getLastName());
			statement.setString(5, user.getEmail());
			row = statement.executeUpdate();
			isAdded = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_USER_ADD, LOGGER_MSG_USER_NO_ADD);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isAdded;
	}

	/**
	 * This method updates user to database
	 * @param user
	 * 			the param uses for updating user to database
	 * @return true if user is updated to database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean update(User user) throws DaoTechException {
		connection = getConnection();
		boolean isUpdated = false;
		int row = 0;
		try {
			if (user.getPassword() != null){
				updatePass(user.getId(), user.getPassword(), connection);
			}
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_UPDATE));
			statement.setString(1, user.getFirstName());
			statement.setString(2, user.getLastName());
			statement.setString(3, user.getEmail());
			statement.setInt(4, user.getId());
			row = statement.executeUpdate();
			isUpdated = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_USER_UPDATE, LOGGER_MSG_USER_NO_UPDATE);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isUpdated;
	}
	
	/**
	 * This method builds user from ResultSet
	 * @param resultSet
	 * 			the param uses for returning result of query from database
	 * @return user
	 * @throws SQLException, if arise database access error or other database errors
	 */
	private User buildUser(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setId(resultSet.getInt(USER_ID));
		user.setLogin(resultSet.getString(LOGIN));
		user.setFirstName(resultSet.getString(FIRST_NAME));
		user.setLastName(resultSet.getString(LAST_NAME));
		user.setEmail(resultSet.getString(EMAIL));
		user.setAdmin(resultSet.getBoolean(ADMIN));
		return user;
	}
	
	/**
	 * This method updates user password
	 * @param id
	 * 			the param uses for user identifying
	 * @param password
	 * 			the param is new password
	 * @param connection
	 * 			the param is connection to database
	 * @throws SQLException, if arise database access error or other database errors
	 */
	private void updatePass(int id, String password, Connection connection) throws SQLException{
		int row = 0;
		PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_UPDATE_PASSWORD));
		statement.setString(1, password);
		statement.setInt(2, id);
		row = statement.executeUpdate();
		DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_PASSWORD_UPDATE, LOGGER_MSG_PASSWORD_NO_UPDATE);
		}
}
