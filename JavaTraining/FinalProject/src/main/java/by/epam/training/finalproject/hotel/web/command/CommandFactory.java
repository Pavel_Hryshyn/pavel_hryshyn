/*
 * @(#)CommandFactory.java   1.0 2015/06/02
 */
package by.epam.training.finalproject.hotel.web.command;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.web.util.AccessToCommandChecker;

/**
 * This class is factory for command
 * @version 1.0 02 June 2015
 * @author Pavel Hryshyn
 */
public class CommandFactory {
	/** This object obtains logger for this class*/
	private static Logger logger = Logger.getLogger(CommandFactory.class);
	
	/** This constants store names of parameters */
	private static final String PARAM_DENIED_ACCESS = "DENIED_ACCESS";
	private static final String COMMAND = "command";
	private static final String WRONG_ACTION = "wrongAction";
	private static final String WRONG_ACTION_MESSAGE = "jsp.message.wrong.action";
	
	/** This constants store logger messages */
	private static final String LOGGER_MSG_COMMAND_NOT_FOUND = "logger.message.command.not.found";
	
	/**
	 * This method creates command
	 * @param request
	 * @return command
	 */
	public Command defineCommand(HttpServletRequest request){
		Command command = null;
		String action = request.getParameter(COMMAND);
		try {
			CommandEnum commandEnum = CommandEnum.valueOf(action.toUpperCase());
			if (AccessToCommandChecker.check(commandEnum, request)){
				command = commandEnum.getCommand();
			} else {
				commandEnum = CommandEnum.valueOf(PARAM_DENIED_ACCESS);
				command = commandEnum.getCommand();
			}
		} catch (IllegalArgumentException e) {
			logger.error(MessageManager.getValue(LOGGER_MSG_COMMAND_NOT_FOUND) + e);
			request.setAttribute(WRONG_ACTION, action + MessageManager.getValue(WRONG_ACTION_MESSAGE));
		}
		return command;
	}
}
