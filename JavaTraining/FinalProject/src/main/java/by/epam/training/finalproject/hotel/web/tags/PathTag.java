/*
 * @(#)PathTag.java   1.5 2015/06/15
 */
package by.epam.training.finalproject.hotel.web.tags;

import java.io.IOException;
import java.util.MissingResourceException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class creates tag for navigation to web application
 * @version 1.5 15 June 2015
 * @author Pavel Hryshyn
 */
public class PathTag extends TagSupport {
	private static final long serialVersionUID = -7368642083696256107L;

	/** This object obtains logger for this class*/
	private static Logger logger = Logger.getLogger(PathTag.class);
		
	/** This constants store key to navigation pattern */
	public static final String NAVIGATION_PATTERN = "navigation.pattern";
    
	/**
     * This is name of property in resource file  
     */
    private String key;
    private String toPage;
    
    /**
	 * This method creates tag for navigation to web application
	 * @throws JspException
	 * @return SKIP_BODY
	 */
    @Override
	public int doStartTag() throws JspException {
		JspWriter writer = pageContext.getOut();
		StringBuffer navigation = null;
		try {
			try {
				navigation = new StringBuffer(PageConfigurationManager.getValue(NAVIGATION_PATTERN));
				toPage = navigation.append(key).toString();
				writer.print(toPage);
			} catch(MissingResourceException e){
				logger.error(e);
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return SKIP_BODY;
	}
    
    public void setKey(String key) {
		this.key = key;
	} 
}
