/*
 * @(#)AbstractDao.java   1.2 2015/05/15
 */
package by.epam.training.finalproject.hotel.database.dao;

import java.sql.Connection;
import java.sql.ResultSet;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.database.dbconnection.DBConnectionPool;
import by.epam.training.finalproject.hotel.database.dbconnection.exception.DBConnectionException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;

/**
 * This is a superclass for all DAO classes and holds methods of 
 * get and close connections
 * @version 1.2 15 May 2015
 * @author Pavel Hryshyn
 */
public class AbstractDao {	
	/** This constants store exception messages */
	private final static String EXC_CONNECT_GOT = "dao.exc.connect.got";
	private final static String EXC_CONNECT_CLOSE = "dao.exc.connect.close";
	
	/** This object stores connection from connection pool */
	protected Connection connection;
	
	/** This objest stores results of queries for database */
	protected ResultSet resultSet;
	
	/**
	 * This method gets the instance of connection from connection pool
	 * @return the instance of connection
	 * @throws DaoTechException, if the instance of connection doesn't get
	 */
	public Connection getConnection() throws DaoTechException {
		try {
			connection = DBConnectionPool.getInstance().takeConnection();
			return connection;
		} catch (DBConnectionException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_CONNECT_GOT), e);
		}	
	}
	
	/**
	 * This method return the instance of connection to connection pool
	 * @throws DaoTechException, if the instance of connection doesn't return to pool
	 */
	public void close() throws DaoTechException {
		if (connection != null) {
		try {
			DBConnectionPool.getInstance().closeConnection(connection);
		} catch (DBConnectionException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_CONNECT_CLOSE), e);
		}	
		}
	}
	
	
}
