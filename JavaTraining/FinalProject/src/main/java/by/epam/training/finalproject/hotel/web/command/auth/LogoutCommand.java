/*
 * @(#)LogoutCommand.java   1.0 2015/06/01
 */
package by.epam.training.finalproject.hotel.web.command.auth;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and closes user session 
 * @version 1.0 01 June 2015
 * @author Pavel Hryshyn
 */
public class LogoutCommand extends Command {
	/** This object obtains logger for this class*/
	private static Logger logger = Logger.getLogger(LogoutCommand.class);
	
	/** This constants store names of request parameters */
	private static final String PARAM_USER = "user";
	
	/** This constants store path to jsp pages */
	private final static String PATH_PAGE_INDEX = "path.page.index";
	
	/** This constants store logger messages */
	private static final String LOGGER_MSG_COMMAND_SESSION_FINISHED = "logger.message.command.session.finished";

	
	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(PARAM_USER);
		String page = PageConfigurationManager.getValue(PATH_PAGE_INDEX);
		request.getSession().invalidate();
		logger.info(MessageManager.getValue(LOGGER_MSG_COMMAND_SESSION_FINISHED) + user.getLogin());
		return page;
	}
	
	
}
