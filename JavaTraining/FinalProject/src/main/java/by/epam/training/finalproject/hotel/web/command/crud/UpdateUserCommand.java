package by.epam.training.finalproject.hotel.web.command.crud;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.UserDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

public class UpdateUserCommand extends Command {
	private final static Logger logger = Logger.getLogger(UpdateUserCommand.class);
	
	private static final String USER = "user";
    private static final String PARAM_NONCONFIRMED_PASSWORD = "nonConfirmPass";
    private static final String PARAM_CURRENT_PASSWORD = "currentPass";
	private static final String PARAM_INCORRECT_PASSWORD = "incorrectPass";
	private static final String PARAM_INCORRECT_CURRENT_PASSWORD = "incorrectCurrentPass";
	private static final String PARAM_INCORRECT_FIRST_NAME = "incorrectFirstName";
	private static final String PARAM_INCORRECT_LAST_NAME = "incorrectLastName";
	private static final String PARAM_INCORRECT_EMAIL = "incorrectEmail";
	private static final String PARAM_USER_UPDATED = "userUpdated";

	private static final String PARAM_USER_EXIST = "userExist";
	private static final String PATH_PAGE_MAIN = "path.page.main";
	private static final String PATH_PAGE_UPDATE_USER = "path.page.update.user";
	private static final String PATH_PAGE_ERROR = "path.page.error";
	
	private static final String MSG_USER_EXIST = "jsp.message.user.exist";
	private static final String MSG_INCORRECT_LOGIN = "jsp.message.incorrect.login";
	private static final String MSG_NONCONFIRMED_PASSWORD = "jsp.message.nonconfirmed.pass";
	private static final String MSG_INCORRECT_PASSWORD = "jsp.message.incorrect.pass";
	private static final String MSG_INCORRECT_CURRENT_PASSWORD = "jsp.message.incorrect.current.pass";
	private static final String MSG_INCORRECT_FIRST_NAME = "jsp.message.incorrect.first.name";
	private static final String MSG_INCORRECT_LAST_NAME = "jsp.message.incorrect.last.name";
	private static final String MSG_INCORRECT_EMAIL = "jsp.message.incorrect.email";
	private static final String MSG_USER_UPDATED = "jsp.message.user.updated";
	
	
	private final String PATTERN_LOGIN = "pattern.login";
	private final String PATTERN_PASSWORD = "pattern.password";
	private final String PATTERN_NAME = "pattern.name";
	private final String PATTERN_EMAIL = "pattern.email";
	private final String LOGIN = "login";
	private final String PASSWORD = "password";
	private final String CONFIRM_PASSWORD = "confirmPassword";
	private final String FIRST_NAME = "firstName";
	private final String LAST_NAME = "lastName";
	private final String EMAIL = "email";
	
	
	UserDao userDao = new UserDao();
	User user;
	
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		user = (User) request.getSession().getAttribute(USER);
		try {
			if (build(request)){
				userDao.update(user);
				request.setAttribute(PARAM_USER_UPDATED, MessageManager.getValue(MSG_USER_UPDATED));
				page = PageConfigurationManager.getValue(PATH_PAGE_MAIN);
			} else {
				page = PageConfigurationManager.getValue(PATH_PAGE_UPDATE_USER);
			}
			} catch (DaoTechException e) {
				logger.debug(e);
				page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
				return page;
			} 	
				return page;
	}
	
	public boolean build(HttpServletRequest request) throws DaoTechException {
		boolean isBuilt = true;
		isBuilt &= buildPassword(request);
		isBuilt &= buildFirstName(request);
		isBuilt &= buildLastName(request);
		isBuilt &= buildEmail(request);
		return isBuilt;
	}

	
	private boolean buildPassword(HttpServletRequest request) throws DaoTechException{
		boolean isBuilt = false;
		String currentPassword = request.getParameter(PARAM_CURRENT_PASSWORD);
		String login = request.getParameter(LOGIN);
		if (currentPassword.length() != 0){
			User confirmUser = userDao.findUserByLoginPassword(login, currentPassword);
			if (confirmUser != null) {
				isBuilt = confirmPassword(request);
			} else {
				request.setAttribute(PARAM_INCORRECT_CURRENT_PASSWORD, MessageManager.getValue(MSG_INCORRECT_CURRENT_PASSWORD));
			}
		} else {
			isBuilt = true;
		}
		return isBuilt;
	}
	
	private boolean buildFirstName(HttpServletRequest request){
		boolean isBuilt = false;
		String firstName = request.getParameter(FIRST_NAME);
		if (firstName != null && firstName.matches(MessageManager.getValue(PATTERN_NAME))) {
			user.setFirstName(firstName);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_FIRST_NAME, MessageManager.getValue(MSG_INCORRECT_FIRST_NAME));
		}
		return isBuilt;
	}
	
	private boolean buildLastName(HttpServletRequest request){
		boolean isBuilt = false;
		String lastName = request.getParameter(LAST_NAME);
		if (lastName != null && lastName.matches(MessageManager.getValue(PATTERN_NAME))) {
			user.setLastName(lastName);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_LAST_NAME, MessageManager.getValue(MSG_INCORRECT_LAST_NAME));
		}
		return isBuilt;
	}
	
	private boolean buildEmail(HttpServletRequest request){
		boolean isBuilt = false;
		String email = request.getParameter(EMAIL);
		if (email != null && email.matches(MessageManager.getValue(PATTERN_EMAIL))) {
			user.setEmail(email);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_EMAIL, MessageManager.getValue(MSG_INCORRECT_EMAIL));
		}
		return isBuilt;
	}
	
	private boolean confirmPassword(HttpServletRequest request){
		boolean isConfirmed = false;
		String password = request.getParameter(PASSWORD);
		if (password != null && password.matches(MessageManager.getValue(PATTERN_PASSWORD))) {
			if (password.equals(request.getParameter(CONFIRM_PASSWORD))){
				user.setPassword(password);
				isConfirmed = true;
			} else {
				request.setAttribute(PARAM_NONCONFIRMED_PASSWORD, MessageManager.getValue(MSG_NONCONFIRMED_PASSWORD));
			}			
		} else {
			request.setAttribute(PARAM_INCORRECT_PASSWORD, MessageManager.getValue(MSG_INCORRECT_PASSWORD));
		}
		return isConfirmed;
	}

}
