/*
 * @(#)IHotelRoomDao.java   1.4 2015/06/12
 */
package by.epam.training.finalproject.hotel.database.dao.interfaces;

import java.util.List;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;

/**
 * This interface defines methods that bind the HotelRoom entity with database
 * @version 1.4 12 June 2015
 * @author Pavel Hryshyn
 */
public interface IHotelRoomDao extends ICommonDao<Integer, HotelRoom> {
	List<HotelRoom> findAllRoomsByRoomType(HotelRoomType roomType) throws DaoTechException;
	Double findRoomPriceByRoomType(HotelRoomType roomType) throws DaoTechException;
}
