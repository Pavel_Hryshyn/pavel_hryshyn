/*
 * @(#)FindFreeRooms.java   1.0 2015/06/15
 */
package by.epam.training.finalproject.hotel.model.logic;

import java.util.ArrayList;
import java.util.List;

import by.epam.training.finalproject.hotel.model.entities.HotelRoom;

/**
 * This class contain methods for find free room
 * @version 1.0 15 June 2015
 * @author Pavel Hryshyn
 */
public class FindFreeRooms {
	/**
	 * This method returns all free rooms between dates that stores in database
	 * @param allRooms
	 * 			the param is the list of all rooms
	 * @param nonFreeRooms
	 * 			the param is the list of non-free rooms
	 * @return list of free rooms between dates from database
	 */
	public static List<HotelRoom> find(List<HotelRoom> allRooms, List<HotelRoom> nonFreeRooms){
		List<HotelRoom> freeRooms = new ArrayList<HotelRoom>();
		freeRooms.addAll(allRooms);
		freeRooms.removeAll(nonFreeRooms);
		return freeRooms;
	}
}
