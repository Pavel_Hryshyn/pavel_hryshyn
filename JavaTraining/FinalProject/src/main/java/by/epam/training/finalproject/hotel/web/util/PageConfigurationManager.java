/*
 * @(#)PageConfigurationManager.java   1.0 2015/05/25
 */
package by.epam.training.finalproject.hotel.web.util;

import java.util.ResourceBundle;

/**
 * This class uses for getting path to jsp pages from file
 * @version 1.0 25 May 2015
 * @author Pavel Hryshyn
 */
public class PageConfigurationManager {
	/** This constants store path in resources folder to file with paths to jsp pages */
	private final static String PAGES_CONFIG = "pages_config";
		
	/** This field is a resource bundle  */
	private final static ResourceBundle bundle = ResourceBundle.getBundle(PAGES_CONFIG);
		
	/**
	 * This method gets a string for the given key from resource bundle.
	 */	
	public static String getValue(String key){
		return bundle.getString(key);
	}
}
