/*
 * @(#)ChangeLanguageCommand.java   1.6 2015/06/25
 */
package by.epam.training.finalproject.hotel.web.command.language;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.ChangeLocale;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and change language in application
 * @version 1.6 25 June 2015
 * @author Pavel Hryshyn
 */
public class ChangeLanguageCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(ChangeLanguageCommand.class);
	
	/** This constants store names of request parameters */
	private static final String PARAM_LANGUAGE = "language";
	
	/** This constants store path to jsp pages */
	private static final String PATH_PAGE_MAIN = "path.page.main";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_CHANGE_LANGUAGE = "logger.message.command.change.language";
	
	/**
	 * This method changes application language
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String language = request.getParameter(PARAM_LANGUAGE);	
		request.getSession().setAttribute(PARAM_LANGUAGE, language);
		ChangeLocale.changeLocale(language);
		logger.info(MessageManager.getValue(LOGGER_MSG_CHANGE_LANGUAGE) + language);
		page = PageConfigurationManager.getValue(PATH_PAGE_MAIN);
		return page;
	}
}
