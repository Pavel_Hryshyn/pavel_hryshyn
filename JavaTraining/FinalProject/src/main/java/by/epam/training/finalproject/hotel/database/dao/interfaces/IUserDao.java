/*
 * @(#)IUserDao.java   1.2 2015/05/20
 */
package by.epam.training.finalproject.hotel.database.dao.interfaces;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.User;

/**
 * This interface defines methods that bind the User entity with database
 * @version 1.2 20 May 2015
 * @author Pavel Hryshyn
 */
public interface IUserDao extends ICommonDao<Integer, User> {
	User findUserByLoginPassword(String login, String password) throws DaoTechException;
	User findUserByLogin(String login) throws DaoTechException;
}
