/*
 * @(#)CreateInvoicePageCommand.java   1.0 2015/06/16
 */
package by.epam.training.finalproject.hotel.web.command.navigation;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.HotelRoomDao;
import by.epam.training.finalproject.hotel.database.dao.InvoiceDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
import by.epam.training.finalproject.hotel.model.entities.Invoice;
import by.epam.training.finalproject.hotel.model.logic.CalculateInvoiceTotalAmount;
import by.epam.training.finalproject.hotel.model.logic.FindFreeRooms;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and forwards to creating invoice page
 * @version 1.0 16 June 2015
 * @author Pavel Hryshyn
 */
public class CreateInvoicePageCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(CreateInvoicePageCommand.class);
	
	/** This constants store names of request parameters */
	private static final String PARAM_BOOKING_ID = "bookingId";
	private static final String PARAM_INVOICE = "invoice";
	private static final String PARAM_INVOICE_ID = "invoiceId";
	private static final String PARAM_FREE_ROOMS = "freeRooms";
	
	/** This constants store path to jsp pages */
	private static final String PATH_PAGE_ADD_INVOICE = "path.page.invoice.add";
	private final static String PATH_PAGE_ERROR = "path.page.error";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_CREATE_INVOICE_PAGE = "logger.message.command.page.invoice.create";
	private final static String LOGGER_MSG_UPDATE_INVOICE_PAGE = "logger.message.command.page.invoice.update";
	
	private BookingDao bookingDao = new BookingDao();
	private HotelRoomDao roomDao = new HotelRoomDao();
	private InvoiceDao invoiceDao = new InvoiceDao();
	private Booking booking;
	private Invoice invoice;
	private List<HotelRoom> freeRooms; 

	/**
	 * This method forwards to save invoice page and sets parameters for creating or updating invoice
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			String stringInvoiceId = request.getParameter(PARAM_INVOICE_ID);
			if (stringInvoiceId.length() > 0){
				int invoiceId = Integer.parseInt(stringInvoiceId);
				invoice = prepareForUpdatingInvoice(request, invoiceId);
			} else {
				int bookingId = Integer.parseInt(request.getParameter(PARAM_BOOKING_ID));
				invoice = prepareForCreatingInvoice(request, bookingId);
			}
			freeRooms = findFreeRoomsByTypes(invoice);
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		request.setAttribute(PARAM_FREE_ROOMS, freeRooms);
		request.setAttribute(PARAM_INVOICE, invoice);
		page = PageConfigurationManager.getValue(PATH_PAGE_ADD_INVOICE);
		return page;
	}
	
	/**
	 * This method sets parameters for creating invoice
	 * @param request
	 * @param bookingId
	 * @throws DaoTechException
	 * @return page where request is forwarded
	 */
	private Invoice prepareForCreatingInvoice(HttpServletRequest request, int bookingId) throws DaoTechException{
		invoice = new Invoice();
		booking = bookingDao.findById(bookingId);
		Double price = roomDao.findRoomPriceByRoomType(booking.getRoomType());
		Date startDate = booking.getStartDate();
		Date endDate = booking.getEndDate();
		double totalAmount = CalculateInvoiceTotalAmount.calculate(price, startDate, endDate);
		invoice.setTotalAmount(totalAmount);
		invoice.setBooking(booking);
		logger.info(MessageManager.getValue(LOGGER_MSG_CREATE_INVOICE_PAGE));
		return invoice;
	}
	
	/**
	 * This method sets parameters for updating invoice
	 * @param request
	 * @param invoiceId
	 * @throws DaoTechException
	 * @return page where request is forwarded
	 */
	private Invoice prepareForUpdatingInvoice(HttpServletRequest request, int invoiceId) throws DaoTechException{
		invoice = invoiceDao.findById(invoiceId);
		logger.info(MessageManager.getValue(LOGGER_MSG_UPDATE_INVOICE_PAGE));
		return invoice;
	}
	
	/**
	 * This method gets list of free rooms using start date, end date and room type from invoice
	 * @param invoice
	 * @throws DaoTechException
	 * @return list of free room
	 */
	private List<HotelRoom> findFreeRoomsByTypes(Invoice invoice) throws DaoTechException{
		List<HotelRoom> freeRoomList = null;
		Date startDate = invoice.getBooking().getStartDate();
		Date endDate = invoice.getBooking().getEndDate();
		HotelRoomType roomType = invoice.getBooking().getRoomType();
		List<HotelRoom> roomsByType = roomDao.findAllRoomsByRoomType(invoice.getBooking().getRoomType());
		List<HotelRoom> nonFreeRooms = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDate, endDate, roomType);
		freeRoomList = FindFreeRooms.find(roomsByType, nonFreeRooms);
		return freeRoomList;
	}
}
