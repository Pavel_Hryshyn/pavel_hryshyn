/*
 * @(#)DeleteBookingCommand.java   1.0 2015/06/10
 */
package by.epam.training.finalproject.hotel.web.command.crud.booking;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and delete booking from database
 * @version 1.0 10 June 2015
 * @author Pavel Hryshyn
 */
public class DeleteBookingCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(DeleteBookingCommand.class);
	
	/** This constants store names of request parameters */
	private final static String PARAM_BOOKING_ID = "bookingId";
	private final static String PARAM_BOOKING_LIST = "bookingList";
	private final static String PARAM_BOOKING_DELETED = "bookingDeleted";
	private final static String PARAM_USER = "user";
	
	/** This constants store messages that output to jsp page */
	private final static String MSG_BOOKING_DELETED_SUCCESS = "jsp.message.booking.delete.success";
	private final static String MSG_BOOKING_DELETED_FAILURE = "jsp.message.booking.delete.failure";
	
	/** This constants store path to jsp pages */	
	private final static String PATH_PAGE_ERROR = "path.page.error";
	private final static String PATH_PAGE_ERROR_403 = "path.page.error403";
	private final static String PATH_PAGE_BOOKING_LIST = "path.page.booking.list";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_NO_ACCESS = "logger.message.command.booking.delete.no.access";
	private final static String LOGGER_MSG_BOOKING_DELETE = "logger.message.command.booking.delete";
	
	private BookingDao bookingDao = new BookingDao();
	private User user;
	
	/**
	 * This method deletes booking from database
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		user = (User) request.getSession().getAttribute(PARAM_USER);
		int bookingId = Integer.parseInt(request.getParameter(PARAM_BOOKING_ID));
		try {
			if (checkUserAccessForDeletingBooking(user, bookingId)) {
				boolean isDeleted = bookingDao.delete(bookingId);
				checkDeletingMessage(request, isDeleted);
				request.setAttribute(PARAM_BOOKING_LIST, bookingDao.findBookingByUserId(user.getId()));
				page = PageConfigurationManager.getValue(PATH_PAGE_BOOKING_LIST);
				logger.info(MessageManager.getValue(LOGGER_MSG_BOOKING_DELETE) + bookingId);
			} else {
				page =  PageConfigurationManager.getValue(PATH_PAGE_ERROR_403);
				logger.info(MessageManager.getValue(LOGGER_MSG_NO_ACCESS) + bookingId);
			}
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		return page;
	}
	
	/**
	 * This method checks if booking is deleted and display results message to jsp
	 * @param request
	 * @param isDeleted
	 */
	private void checkDeletingMessage(HttpServletRequest request, boolean isDeleted){
		if (isDeleted){
			request.setAttribute(PARAM_BOOKING_DELETED, MessageManager.getValue(MSG_BOOKING_DELETED_SUCCESS));
		} else {
			request.setAttribute(PARAM_BOOKING_DELETED, MessageManager.getValue(MSG_BOOKING_DELETED_FAILURE));	
		}
	}
	
	/**
	 * This method checks user access for deleting booking
	 * @param user
	 * 			the param is user whose booking
	 * @param bookingId
	 * 			the param is booking id
	 * @return true if booking belongs to user 
	 */
	private boolean checkUserAccessForDeletingBooking(User user, int bookingId) throws DaoTechException{
		boolean isChecked = false;
		Booking booking = bookingDao.findById(bookingId);
		int verifiableUserId = booking.getUser().getId();
		if (verifiableUserId == user.getId()){
			isChecked = true;
		}
		return isChecked;
	}
}
