package by.epam.training.finalproject.hotel.web.command.crud;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.el.ELException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

public class AddBookingCommand extends Command {
	private final static Logger logger = Logger.getLogger(AddBookingCommand.class);
	
	private final static String USER = "user";
	private final static String PARAM_NUMBER_OF_GUEST = "numberOfGuest";
	private final static String PARAM_INCORRECT_NUMBER_OF_GUEST = "incorrectNumberOfGuest";
	private final static String PARAM_START_DATE = "startDate";
	private final static String PARAM_END_DATE = "endDate";
	private final static String PARAM_ERROR_START_DATE_BEFORE = "errorStartDateBefore";
	private final static String PARAM_ERROR_END_DATE_BEFORE = "errorEndDateBefore";
	private final static String PARAM_INCORRECT_START_DATE = "incorrectStartDate";
	private final static String PARAM_INCORRECT_END_DATE = "incorrectEndDate";
	private final static String PARAM_BOOKING_COMMENT = "bookingComment";
	private final static String PARAM_ROOM_TYPE ="roomType";
	private final static String PARAM_INCORRECT_ROOM_TYPE = "incorrectRoomType";
	private final static String PARAM_BOOKING_CREATED = "bookingCreated";
	
	private final static String MSG_INCORRECT_NUMBER_OF_GUEST = "jsp.message.incorrect.guest.number";
	private final static String MSG_INCORRECT_DATE = "jsp.message.incorrect.date";
	private final static String MSG_ERROR_START_DATE_BEFORE = "jsp.message.error.start.date.before";
	private final static String MSG_ERROR_END_DATE_BEFORE = "jsp.message.error.end.date.before";
	private final static String MSG_INCORRECT_ROOM_TYPE = "jsp.message.incorrect.room.type";
	private final static String MSG_BOOKING_CREATED = "jsp.message.booking.created";

	private final static String PATH_PAGE_MAIN = "path.page.main";
	private final static String PATH_PAGE_ERROR = "path.page.error";
	private final static String PATH_PAGE_ADD_BOOKING = "path.page.booking.add";
	
	private final static String PATTERN_FORMAT_DATE = "pattern.format.date";
	
	BookingDao bookingDao = new BookingDao();
	Booking booking;
	Date startDate;
	Date endDate;
	
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		booking = new Booking();
		try {
			if (build(request)){
				bookingDao.add(booking);
				request.setAttribute(PARAM_BOOKING_CREATED, MessageManager.getValue(MSG_BOOKING_CREATED));
				page = PageConfigurationManager.getValue(PATH_PAGE_MAIN);
			} else {
				page = PageConfigurationManager.getValue(PATH_PAGE_ADD_BOOKING);
				setInputDate(request);
			}
		} catch(ParseException e) {
			logger.debug(e);
			request.setAttribute(PARAM_INCORRECT_START_DATE, MessageManager.getValue(MSG_INCORRECT_DATE));
			request.setAttribute(PARAM_INCORRECT_END_DATE, MessageManager.getValue(MSG_INCORRECT_DATE));
			setInputDate(request);
			page = PageConfigurationManager.getValue(PATH_PAGE_ADD_BOOKING);
			return page;
		} catch(NumberFormatException e) {
			logger.debug(e);
			request.setAttribute(PARAM_INCORRECT_NUMBER_OF_GUEST, MessageManager.getValue(MSG_INCORRECT_NUMBER_OF_GUEST));
			setInputDate(request);
			page = PageConfigurationManager.getValue(PATH_PAGE_ADD_BOOKING);
			return page;
		} catch(DaoTechException e) {
			logger.debug(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}		
		return page;
	}
	
	public boolean build(HttpServletRequest request) throws ParseException {
		boolean isBuilt = true;
		isBuilt &= buildNumberOfGuest(request);
		isBuilt &= buildStartDate(request);
		isBuilt &= buildEndDate(request);
		isBuilt &= buildBookingComment(request);
		isBuilt &= buildRoomType(request);
		isBuilt &= buildUser(request);
		return isBuilt;
	}
	
	private boolean buildNumberOfGuest(HttpServletRequest request){
		boolean isBuilt = false;
		int numberOfGuest = Integer.parseInt(request.getParameter(PARAM_NUMBER_OF_GUEST));
		if (numberOfGuest > 0) {
			booking.setNumberOfGuest(numberOfGuest);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_NUMBER_OF_GUEST, MessageManager.getValue(MSG_INCORRECT_NUMBER_OF_GUEST));
		}
		System.out.println("NumberOfGuest = " + isBuilt);
		return isBuilt;
	}
	
	private boolean buildStartDate(HttpServletRequest request) throws ParseException{
		boolean isBuilt = false;
		Date currentDate = new Date(System.currentTimeMillis());
		startDate = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
							.parse(request.getParameter(PARAM_START_DATE));
		if (startDate.after(currentDate)){
			if (startDate != null) {
				booking.setStartDate(startDate);
				isBuilt = true;
			} else {
				request.setAttribute(PARAM_INCORRECT_START_DATE, MessageManager.getValue(MSG_INCORRECT_DATE));
			}
		} else {
			request.setAttribute(PARAM_ERROR_START_DATE_BEFORE, MessageManager.getValue(MSG_ERROR_START_DATE_BEFORE));
		}
		System.out.println("startDate = " + isBuilt);
		return isBuilt;
	}	
	
	private boolean buildEndDate(HttpServletRequest request) throws ParseException {
		boolean isBuilt = false;
			endDate = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
								.parse(request.getParameter(PARAM_END_DATE));
		if (endDate != null) {
			if (endDate.after(startDate)){
			booking.setEndDate(endDate);
			isBuilt = true;
			} else {
				request.setAttribute(PARAM_ERROR_END_DATE_BEFORE, MessageManager.getValue(MSG_ERROR_END_DATE_BEFORE));
			}
		} else {
			request.setAttribute(PARAM_INCORRECT_END_DATE, MessageManager.getValue(MSG_INCORRECT_DATE));
		}
		System.out.println("endDate = " + isBuilt);
		return isBuilt;
	}	

	private boolean buildBookingComment(HttpServletRequest request){
		boolean isBuilt = false;
		String bookingComment = request.getParameter(PARAM_BOOKING_COMMENT);
		booking.setBookingComment(bookingComment);
		isBuilt = true;
		System.out.println("Comment = " + isBuilt);
		return isBuilt;
	}
	
	private boolean buildRoomType(HttpServletRequest request) {
		boolean isBuilt = false;
		HotelRoomType roomType = HotelRoomType.valueOf(request.getParameter(PARAM_ROOM_TYPE));
		if (roomType != null){
			booking.setRoomType(roomType);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_ROOM_TYPE, MessageManager.getValue(MSG_INCORRECT_ROOM_TYPE));
		}
		System.out.println("roomType = " + isBuilt);
		return isBuilt;		
	}
	
	private boolean buildUser(HttpServletRequest request) {
		boolean isBuilt = false;
		User user = (User) request.getSession().getAttribute(USER);
		if (user != null) {
			booking.setUser(user);
			isBuilt = true;
		}
		System.out.println("user = " + isBuilt);
		return isBuilt;	
	}
	
	
	private void setInputDate(HttpServletRequest request) {
		request.setAttribute(PARAM_NUMBER_OF_GUEST, request.getParameter(PARAM_NUMBER_OF_GUEST));
		request.setAttribute(PARAM_BOOKING_COMMENT, request.getParameter(PARAM_BOOKING_COMMENT));

	}
}
