package by.epam.training.finalproject.hotel.web.command.navigation;

import javax.servlet.http.HttpServletRequest;

import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

public class ToPageCommand extends Command {
	private final static String PARAM_TO_PAGE = "page";
	private final static String ERROR_PAGE_404 = "path.page.error404";
	
	@Override
	public String execute(HttpServletRequest request) {
		String page = request.getParameter(PARAM_TO_PAGE);
		if (page == null){
			page = PageConfigurationManager.getValue(ERROR_PAGE_404); 
		}
		return page;
	}

}
