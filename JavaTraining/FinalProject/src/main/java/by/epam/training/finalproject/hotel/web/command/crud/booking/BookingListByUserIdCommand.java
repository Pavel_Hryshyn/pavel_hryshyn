/*
 * @(#)BookingListByUserIdCommand.java   1.2 2015/06/15
 */
package by.epam.training.finalproject.hotel.web.command.crud.booking;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and get list of all booking for some user
 * @version 1.2 15 June 2015
 * @author Pavel Hryshyn
 */
public class BookingListByUserIdCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(BookingListByUserIdCommand.class);
	
	/** This constants store names of request parameters */
	private final static String PARAM_USER_ID = "userId";
	private final static String PARAM_BOOKING_LIST = "bookingList";
	private final static String PARAM_BOOKING_LIST_EMPTY = "bookingListEmpty";
	private final static String PARAM_USER_ID_INCORRECT_FORMAT = "incorrectFormatUserId";
	
	/** This constants store path to jsp pages */
	private final static String PATH_BOOKING_LIST = "path.page.booking.list";
	private final static String PATH_PAGE_ERROR = "path.page.error";
	
	/** This constants store messages that output to jsp page */
	private final static String MSG_BOOKING_LIST_EMPTY = "jsp.message.booking.list.empty";
	private final static String MSG_USER_ID_INCORRECT_FORMAT = "jsp.message.booking.list.user.id.incorrect.format";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_COMMAND_BOOKING_LIST_GET = "logger.message.command.booking.list.user.get";
	private final static String LOGGER_MSG_COMMAND_BOOKING_LIST_EMPTY = "logger.message.command.booking.list.user.empty";
	
	private BookingDao bookingDao = new BookingDao();	
	
	/**
	 * This method gets list of all booking for some user
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			int userId = Integer.parseInt(request.getParameter(PARAM_USER_ID));
			List<Booking> bookingList = bookingDao.findBookingByUserId(userId);
			if (bookingList != null){
				request.setAttribute(PARAM_BOOKING_LIST, bookingList);
				logger.info(MessageManager.getValue(LOGGER_MSG_COMMAND_BOOKING_LIST_GET) + userId);
			} else {
				request.setAttribute(PARAM_BOOKING_LIST_EMPTY, MessageManager.getValue(MSG_BOOKING_LIST_EMPTY));
				logger.info(MessageManager.getValue(LOGGER_MSG_COMMAND_BOOKING_LIST_EMPTY) + userId);
			}		
			page = PageConfigurationManager.getValue(PATH_BOOKING_LIST);
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		} catch (NumberFormatException e) {
			logger.error(e);
			request.setAttribute(PARAM_USER_ID_INCORRECT_FORMAT, MessageManager.getValue(MSG_USER_ID_INCORRECT_FORMAT));
			page = PageConfigurationManager.getValue(PATH_BOOKING_LIST);
			return page;
		}
		return page;
	}

}
