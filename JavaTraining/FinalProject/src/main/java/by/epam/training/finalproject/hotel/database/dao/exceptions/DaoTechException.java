/*
 * @(#)DaoTechException.java   1.0 2015/05/15
 */
package by.epam.training.finalproject.hotel.database.dao.exceptions;

/**
 * This class is wrapper for all DAO level exceptions  
 * @version 1.0 15 May 2015
 * @author Pavel Hryshyn
 */
public class DaoTechException extends Exception {
	private static final long serialVersionUID = 5552905985555822015L;

	public DaoTechException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
