/*
 * @(#)UserListCommand.java   1.2 2015/06/15
 */
package by.epam.training.finalproject.hotel.web.command.crud.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.UserDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and get list of all users from database
 * @version 1.2 15 June 2015
 * @author Pavel Hryshyn
 */
public class UserListCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(UserListCommand.class);
	
	/** This constants store names of request parameters */
	private final static String PARAM_USER_LIST = "userList";
	private final static String PARAM_USER_LIST_EMPTY = "userListEmpty";
	
	/** This constants store path to jsp pages */
	private final static String PATH_USER_LIST = "path.page.user.list";
	private final static String PATH_PAGE_ERROR = "path.page.error";
	
	/** This constants store messages that output to jsp page */
	private final static String MSG_USER_LIST_EMPTY = "jsp.message.user.list.empty";

	/** This constants store logger messages */
	private final static String LOGGER_MSG_USER_LIST_GET = "logger.message.command.user.list.get";
	private final static String LOGGER_MSG_USER_LIST_EMPTY = "logger.message.command.user.list.empty";
	
	private UserDao userDao = new UserDao();
	
	/**
	 * This method gets list of all users from database
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			List<User> userList = userDao.findAll();
			if (userList != null){
				request.setAttribute(PARAM_USER_LIST, userList);
				logger.info(MessageManager.getValue(LOGGER_MSG_USER_LIST_GET));
			} else {
				request.setAttribute(PARAM_USER_LIST_EMPTY, MessageManager.getValue(MSG_USER_LIST_EMPTY));
				logger.info(MessageManager.getValue(LOGGER_MSG_USER_LIST_EMPTY));
			}
			page = PageConfigurationManager.getValue(PATH_USER_LIST);
		} catch (DaoTechException e) {
			logger.debug(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		return page;
	}
}
