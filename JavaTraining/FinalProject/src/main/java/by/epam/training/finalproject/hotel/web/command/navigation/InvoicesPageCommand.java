/*
 * @(#)InvoicesPageCommand.java   1.0 2015/06/15
 */
package by.epam.training.finalproject.hotel.web.command.navigation;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and forwards to invoice list page
 * @version 1.0 15 June 2015
 * @author Pavel Hryshyn
 */
public class InvoicesPageCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(InvoicesPageCommand.class);
	
	/** This constants store path to jsp pages */
	private final static String PATH_INVOICES_PAGE = "path.page.invoice.list";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_INVOICE_LIST_PAGE = "logger.message.command.page.invoice.list";

	/**
	 * This method forwards to invoice list page if account page not found then return error 404 page
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = PageConfigurationManager.getValue(PATH_INVOICES_PAGE);
		logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_LIST_PAGE));
		return page;
	}
}
