/*
 * @(#)HotelRoomType.java   1.0 2015/05/13
 */
package by.epam.training.finalproject.hotel.model.entities;

/**
 * This enum stores types of hotel room
 * @version 1.0 13 May 2015
 * @author Pavel Hryshyn
 */
public enum HotelRoomType {
	STANDART, BEDROOM, SUITE, BUSINESS_ROOM, PRESIDENT_SUITES
}
