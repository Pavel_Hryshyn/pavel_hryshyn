SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema hoteldb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hoteldb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `hoteldb` ;

-- -----------------------------------------------------
-- Table `hoteldb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hoteldb`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(64) NOT NULL,
  `admin` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `iduser_UNIQUE` (`id` ASC),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hoteldb`.`room_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hoteldb`.`room_type` (
  `type` ENUM('STANDART', 'BEDROOM', 'SUITE', 'BUSINESS_ROOM', 'PRESIDENT_SUITES') NOT NULL,
  UNIQUE INDEX `type_UNIQUE` (`type` ASC),
  PRIMARY KEY (`type`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hoteldb`.`hotel_rooms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hoteldb`.`hotel_rooms` (
  `id` INT NOT NULL,
  `capacity` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `description` VARCHAR(255) NULL,
  `room_type_type` ENUM('STANDART', 'BEDROOM', 'SUITE', 'BUSINESS_ROOM', 'PRESIDENT_SUITES') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `room_number_UNIQUE` (`id` ASC),
  INDEX `fk_hotel_rooms_room_type1_idx` (`room_type_type` ASC),
  CONSTRAINT `fk_hotel_rooms_room_type1`
    FOREIGN KEY (`room_type_type`)
    REFERENCES `hoteldb`.`room_type` (`type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hoteldb`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hoteldb`.`booking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `guest_number` INT NOT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `is_handled` TINYINT(1) NOT NULL DEFAULT 0,
  `booking_comment` VARCHAR(45) NULL,
  `users_id` INT NOT NULL,
  `room_type_type` ENUM('STANDART', 'BEDROOM', 'SUITE', 'BUSINESS_ROOM', 'PRESIDENT_SUITES') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_booking_users1_idx` (`users_id` ASC),
  INDEX `fk_booking_room_type1_idx` (`room_type_type` ASC),
  CONSTRAINT `fk_booking_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `hoteldb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_room_type1`
    FOREIGN KEY (`room_type_type`)
    REFERENCES `hoteldb`.`room_type` (`type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hoteldb`.`invoices`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hoteldb`.`invoices` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `total_amount` DOUBLE NOT NULL,
  `invoice_comment` VARCHAR(45) NULL,
  `booking_id` INT NOT NULL,
  `hotel_rooms_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_invoice_booking1_idx` (`booking_id` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_invoices_hotel_rooms1_idx` (`hotel_rooms_id` ASC),
  CONSTRAINT `fk_invoice_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `hoteldb`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoices_hotel_rooms1`
    FOREIGN KEY (`hotel_rooms_id`)
    REFERENCES `hoteldb`.`hotel_rooms` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;