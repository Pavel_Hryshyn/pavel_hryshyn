<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head><title>Index</title></head>
<body>
	<jsp:forward page="/controller">
    	<jsp:param name="command" value="TO_PAGE"/>
        <jsp:param name="toPage" value="/jsp/main/main.jsp"/>
    </jsp:forward>
</body>
</body></html>

