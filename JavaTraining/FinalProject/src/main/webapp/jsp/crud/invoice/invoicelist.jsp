<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tg" uri="tags"%>


<html lang="$language">
<head>
<title>Created Invoice</title>
</head>
<body>
	<jsp:include page="/jsp/header/header.jsp"></jsp:include>
	<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>
	<c:if test="${not empty invoiceUpdated}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${invoiceUpdated}" />
		</div>
	</c:if>
	<c:if test="${not empty invoiceDeleted}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${invoiceDeleted}" />
		</div>
	</c:if>
	<c:if test="${not empty invoiceListEmpty}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${invoiceListEmpty}" />
		</div>
	</c:if>


	<div class="container-fluid">
		<div class="row">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><fmt:message key="jsp.invoice.id" /></th>
						<th><fmt:message key="jsp.invoice.user.last.name" /></th>
						<th><fmt:message key="jsp.invoice.number.of.guest" /></th>
						<th><fmt:message key="jsp.invoice.room.type" /></th>
						<th><fmt:message key="jsp.invoice.room.id" /></th>
						<th><fmt:message key="jsp.invoice.start.date" /></th>
						<th><fmt:message key="jsp.invoice.end.date" /></th>
						<th><fmt:message key="jsp.invoice.booking.comment" /></th>
						<th><fmt:message key="jsp.invoice.comment" /></th>
						<th colspan=2>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${invoiceList}" var="invoice">
						<tr>
							<td><c:out value="${invoice.id}" /></td>
							<td><c:out value="${invoice.booking.user.lastName}" /></td>
							<td><c:out value="${invoice.booking.numberOfGuest}" /></td>
							<td><c:out value="${invoice.hotelRoom.roomType}" /></td>
							<td><c:out value="${invoice.hotelRoom.id}" /></td>
							<td><fmt:formatDate pattern="yyyy-MM-dd"
									value="${invoice.booking.startDate}" /></td>
							<td><fmt:formatDate pattern="yyyy-MM-dd"
									value="${invoice.booking.endDate}" /></td>
							<td><c:out value="${invoice.booking.bookingComment}" /></td>
							<td><c:out value="${invoice.booking.handledBooking}" /></td>
							<td>
								<form name="getInvoiceForm" method="POST" action="controller">
									<input type="hidden" name="command" value="get_invoice" /> <input
										type="hidden" name="invoiceId" value="${invoice.id}" /> <input
										class="btn btn-default" type="submit"
										value="<fmt:message key="jsp.button.get.invoice"/>" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<form name="createBookingForm" method="POST" action="controller">
				<input type="hidden" name="command" value="INVOICE_LIST" /> <input
					class="btn btn-primary" type="submit"
					value="<fmt:message key="jsp.button.get.invoice.list"/>" />
			</form>

		</div>
	</div>


	<div class="panel panel-warning">
		<div class="panel-footer"><jsp:include
				page="/jsp/header/footer.jsp"></jsp:include></div>
	</div>
</body>
</html>