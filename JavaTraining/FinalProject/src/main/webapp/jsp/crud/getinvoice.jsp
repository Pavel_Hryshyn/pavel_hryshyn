<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>

<html lang="$language">
<head>
	<title>Invoice</title>
</head>
<body>
<jsp:include page="/jsp/header/header.jsp"/>
<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>

<div class="container" >
	<c:if test="${not empty invoiceEmpty}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${invoiceEmpty}"/>
		</div>
	</c:if>
	<c:if test="${not empty invoiceConfirm}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${invoiceConfirm}"/>
		</div>
	</c:if>	

	
<table class="table table-striped">
	<thead>
		<tr class="info">
			<th colspan="2"><h4><fmt:message key="jsp.invoice.booking.information"/></h4></th>
		</tr>
	</thead>
	<tr>
		<td><fmt:message key="jsp.booking.id"/></td>
		<td><c:out value="${invoice.booking.id}"/></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.booking.number.of.guest"/></td>
		<td><c:out value="${invoice.booking.numberOfGuest}"/></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.booking.room.type"/></td>
		<td><c:out value="${invoice.booking.roomType}"/></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.booking.start.date"/></td>
		<td><fmt:formatDate pattern="yyyy-MM-dd" value="${invoice.booking.startDate}" /></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.booking.end.date"/></td>
		<td><fmt:formatDate pattern="yyyy-MM-dd" value="${invoice.booking.endDate}" /></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.booking.comment"/></td>
		<td><c:out value="${booking.bookingComment}"/></td>
	</tr>
	
	<tr class="info">
		<td colspan="2"><h4><fmt:message key="jsp.invoice.invoice.information"/></h4></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.invoice.id"/></td>
		<td><c:out value="${invoice.id}"/></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.invoice.total.amount"/></td>
		<td><c:out value="${invoice.totalAmount}"/></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.invoice.comment"/></td>
		<td><c:out value="${invoice.invoiceComment}"/></td>
	</tr>
	<tr class="info">
		<td colspan="2"><h4><fmt:message key="jsp.invoice.room.information"/></h4></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.invoice.room.id"/></td>
		<td><c:out value="${invoice.hotelRoom.id}"/></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.invoice.room.type"/></td>
		<td><c:out value="${invoice.hotelRoom.roomType}"/></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.invoice.room.price"/></td>
		<td><c:out value="${invoice.hotelRoom.price}"/></td>
	</tr>
	<tr>
		<td><fmt:message key="jsp.invoice.room.description"/></td>
		<td><c:out value="${invoice.hotelRoom.description}"/></td>
	</tr>
</table>


<c:if test="${user.admin}">
	<div class="btn-group" role="group" aria-label="...">
		<div class="btn-group" role="group">
			<form name="getInvoiceForm" method="POST" action="controller">
				<input type="hidden" name="command" value="change_invoice"/>
				<input type="hidden" name="invoiceId" value="${invoice.id}">
				<input class="btn btn-primary" type="submit" value="<fmt:message key="jsp.button.update"/>"/>
			</form>
		</div>
		<div class="btn-group" role="group">
			<form method="post" action="controller" onSubmit='return confirm("<fmt:message key="jsp.message.booking.delete.comfirm"/>");'>
				<input type="hidden" name="command" value="DELETE_INVOICE"/>
				<input type="hidden" name="invoiceId" value="${invoice.id}">
				<input class="btn btn-danger" type=submit value="<fmt:message key="jsp.button.delete"/>" >
			</form>
		</div>
	</div>
</c:if>
</div>
</body>