<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>

<html lang="$language">
<head>
	<title><fmt:message key="jsp.title.user.update"/></title>
</head>
<body>
<jsp:include page="/jsp/header/header.jsp"></jsp:include>
<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>

<div class="container fluid">
	<div class="row">
<form class="form-horizontal" name="updateUserForm" method="POST" action="controller">
  <input type="hidden" name="command" value="update_user" />
  <div class="form-group">
    	<label for="Login" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.login" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="Login" name="login" value="${user.login}" readonly>
 		</div>			
  	</div>
  	<div class="form-group">
    	<label for="currentPassword" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.update.user.current.password" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="password" class="form-control" id="currentPassword" name="currentPass" value="">
 		</div>	
 		<c:if test="${not empty incorrectCurrentPass}">
			<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
				<c:out value="${incorrectCurrentPass}"/>
			</div>
		</c:if>				
  	</div>
  	<div class="form-group">
    	<label for="Password" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.password" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="password" class="form-control" id="Password" name="password" value="">
 		</div>	
 		<c:if test="${not empty nonConfirmPass or not empty incorrectPass}">
			<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
				<c:out value="${incorrectPass}"/>
				<c:out value="${nonConfirmPass}"/>
			</div>
		</c:if>				
  	</div>
 	<div class="form-group">
    	<label for="confirmPassword" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.confirm.password" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="password" class="form-control" id="confirmPassword" name="confirmPassword" value="">
 		</div>			
  	</div>
 	<div class="form-group">
    	<label for="FirstName" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.first.name" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="FirstName" name="firstName" value="${user.firstName}">
 		</div>	
 		<c:if test="${not empty incorrectFirstName}">
			<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
				<c:out value="${incorrectFirstName}"/>
			</div>
		</c:if>				
  	</div>
  	<div class="form-group">
    	<label for="LastName" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.last.name" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="LastName" name="lastName" value="${user.lastName}">
 		</div>	
 		<c:if test="${not empty incorrectLastName}">
			<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
				<c:out value="${incorrectLastName}"/>
			</div>
		</c:if>				
  	</div>
  	<div class="form-group">
    	<label for="Email" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.registration.email" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="Email" name="email" value="${user.email}">
 		</div>	
 		<c:if test="${not empty incorrectEmail}">
			<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
				<c:out value="${incorrectEmail}"/>
			</div>
		</c:if>				
  	</div>

<input class="btn btn-primary" type="submit" value="<fmt:message key="jsp.button.save"/>"/>
</form>
</div>
</div>
<div class="panel panel-default">
  <div class="panel-footer"><jsp:include page="/jsp/header/footer.jsp"></jsp:include></div>
</div>

</body>
</html>
