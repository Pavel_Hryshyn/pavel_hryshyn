<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html lang="language">
<head>
<title></title>
</head>
<body>
	<jsp:include page="/jsp/header/header.jsp"></jsp:include>
	<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<c:if test="${language ne 'ru'}">
					<img
						src="${pageContext.servletContext.contextPath}/images/error/error404en.png"
						class="img-responsive" alt="error404">
				</c:if>
				<c:if test="${language eq 'ru'}">
					<img
						src="${pageContext.servletContext.contextPath}/images/error/error404ru.png"
						class="img-responsive" alt="error404">
				</c:if>
			</div>
		</div>
	</div>


	<div class="panel panel-warning">
		<div class="panel-footer"><jsp:include
				page="/jsp/header/footer.jsp" /></div>
	</div>
</body>
</html>