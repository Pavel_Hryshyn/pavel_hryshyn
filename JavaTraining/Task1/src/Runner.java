import java.io.ObjectInputStream.GetField;
import java.nio.file.WatchEvent.Kind;
import java.util.Collections;
import java.util.List;

import by.epam.training.lab.first.oop.entities.Apartment;
import by.epam.training.lab.first.oop.entities.Appliance;
import by.epam.training.lab.first.oop.entities.ApplianceFactory;
import by.epam.training.lab.first.oop.entities.kitchen.KitchenAppliance;
import by.epam.training.lab.first.oop.entities.kitchen.Refrigerator;
import by.epam.training.lab.first.oop.logic.calc.PowerCalculation;
import by.epam.training.lab.first.oop.logic.io.Print;
import by.epam.training.lab.first.oop.logic.parser.ApartmentBuilder;
import by.epam.training.lab.first.oop.logic.parser.ApartmentXMLBuilder;
import by.epam.training.lab.first.oop.logic.search.SearchByParameters;


public class Runner {

	public static void main(String[] args) {
		double expPower = 250.0;
		double minPower = 200.0;
		double maxPower = 1000.0;
		double minIdPR = 200.0;
		double maxIdPR = 1000.0;
		double idPower = 200.0;
		String id = "Iron";
		String idPR = "laptop";
		boolean plugin = true;
		
		
		
		ApartmentBuilder apbuilder = new ApartmentXMLBuilder();
		Apartment appartment = apbuilder.buildApartment("src/resources/Apartment.xml");
		
				
		//Print apList before sorting
		List<Appliance> apList = appartment.getAppliances();
		
		//Calculation total power
		PowerCalculation.calculateTotalPower(apList);
		System.out.println();
		System.out.println("Initial list of appliances:");
		Print.printList(apList);
				
		
		//Print apList after sorting
		List<Appliance> sortedApList = apList;
		Collections.sort(sortedApList);
		System.out.println();
		System.out.println("Sorted list of appliances:");
		Print.printList(sortedApList);
		
		//Search appliance by parameters
		List<Appliance> apListByPower = SearchByParameters.searchByPower(apList, expPower);
		System.out.println();
		System.out.println("Appliances with power: "+expPower + " Watt");
		Print.printList(apListByPower);
		
		System.out.println();
		System.out.println("Appliances with Power Range from " + minPower + " to " + maxPower + " Watt");
		Print.printList(SearchByParameters.searchByPowerRange(apList, minPower, maxPower));
		
		System.out.println();
		System.out.println("Appliances with ID=" + id + " and Power=" + idPower);
		Print.printList(SearchByParameters.searchByIdAndPower(apList, "iron", idPower));
		
		System.out.println();
		System.out.println("Appliances with ID=" + idPR + " and Power Range" + minIdPR + " - " + maxIdPR);
		Print.printList(SearchByParameters.searchByIdAndPowerRange(apList, idPR, minIdPR, maxIdPR));
		
		System.out.println();
		System.out.println("Appliances with plugged in flag = " + plugin);
		Print.printList(SearchByParameters.searchByPluginApp(apList, plugin));
		
		System.out.println();
		System.out.println("Appliances with plugged in flag = " + !plugin);
		Print.printList(SearchByParameters.searchByPluginApp(apList, !plugin));
	}

}
