/**
 * @(#)Washing.java   1.3 2015/03/30
 */


package by.epam.training.lab.first.oop.entities.cleanup.behavior;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IWashingBehavior;

/**
 * This class implements IWashingBehavior.class and realized for 
 * appliance that can wash clothes
 * @author Pavel Hryshyn
 * @param String flag
 */
public class Washing implements IWashingBehavior {
	private String flag = "is able to wash";
	
	
	@Override
	public void washClothes() {
		System.out.println("This appliance " + flag);

	}
	
	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}

}
