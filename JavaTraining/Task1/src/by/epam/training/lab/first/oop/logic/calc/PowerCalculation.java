/**
 * @(#)PowerCalculation.java   1.3 2015/04/01
 */

package by.epam.training.lab.first.oop.logic.calc;

import java.util.List;

import by.epam.training.lab.first.oop.entities.Appliance;

/**
 * This class calculate total power all appliances that have isConnectedPlug() = true
 * @author Pavel Hryshyn
 * @param static double totalPower
 */
public class PowerCalculation {
	private static double totalPower = 0;
	
	public static void calculateTotalPower(List<Appliance> appList){
		double power = 0;
		boolean plugIn = false;
		
		for (Appliance appliance: appList){
			plugIn = appliance.isConnectedPlug();
			if (plugIn){
				power = appliance.getPower();
			} else {
				power = 0;
			}
			totalPower += power;
		}
		
		System.out.println("Total power turned on appliances is: " + totalPower);
		
	}

	public static double getTotalPower() {
		return totalPower;
	}
	
	

}
