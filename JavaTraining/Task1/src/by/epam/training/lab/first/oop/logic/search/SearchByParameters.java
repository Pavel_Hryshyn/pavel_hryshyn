/**
 * @(#)SearchByParameters.java   1.3 2015/04/01
 */
package by.epam.training.lab.first.oop.logic.search;

import java.util.ArrayList;
import java.util.List;

import by.epam.training.lab.first.oop.entities.Appliance;

/**
 * This class search appliance in dependence on expected parameters
 * @author Pavel Hryshyn
 */
public class SearchByParameters {
	
	public static List<Appliance> searchByPower(List<Appliance> appliances, Double expectedPower){
		List<Appliance> listByPower = new ArrayList<Appliance>();
		for (Appliance appliance: appliances){
			if (expectedPower.equals(appliance.getPower())){
				listByPower.add(appliance);
			}
		}
		return listByPower;
	}
	
	public static List<Appliance> searchByPowerRange(List<Appliance> appliances, Double minPower, Double maxPower){
		List<Appliance> listByPowerRange = new ArrayList<Appliance>();
		for (Appliance appliance: appliances){
			if (appliance.getPower()>= minPower && appliance.getPower()<=maxPower){
				listByPowerRange.add(appliance);
			}
		}
		return listByPowerRange;
	}
	
	public static List<Appliance> searchByIdAndPower(List<Appliance> appliances, String expID, Double expPower){
		List<Appliance> listByIdAndPower = new ArrayList<Appliance>();
		for (Appliance appliance: appliances){
			if (expID.equalsIgnoreCase(appliance.getId()) && expPower.equals(appliance.getPower())){
				listByIdAndPower.add(appliance);
			}
		}
		return listByIdAndPower;
	}
	
	public static List<Appliance> searchByIdAndPowerRange(List<Appliance> appliances, String expID, Double minPower, Double maxPower){
		List<Appliance> listByIdAndPowerRange = new ArrayList<Appliance>();
		for (Appliance appliance: appliances){
			if (expID.equalsIgnoreCase(appliance.getId()) && appliance.getPower()>= minPower && appliance.getPower()<=maxPower){
				listByIdAndPowerRange.add(appliance);
			}
		}
		return listByIdAndPowerRange;
	}
	
	public static List<Appliance> searchByPluginApp(List<Appliance> appliances, Boolean expPlugin){
		List<Appliance> listByPlugin = new ArrayList<Appliance>();
		for (Appliance appliance: appliances){
			if (expPlugin.equals(appliance.isConnectedPlug())){
				listByPlugin.add(appliance);
			}
		}
		return listByPlugin;
	}

}
