/**
 * @(#)Print.java   1.3 2015/04/01
 */

package by.epam.training.lab.first.oop.logic.io;

import java.util.List;

/**
 * This class print every element of list in new string and its position in list
 * @author Pavel Hryshyn
 */
public class Print {
	
	public static <T> void printList(List<T> list){
		String space = "  ";
		if (!list.isEmpty()){
		for (int i=0; i<list.size(); i++){
			if (i>=10){
				space = " ";
			}
			System.out.println("No" + i + space + list.get(i));
		}
		} else {
			System.out.println("This list is EMPTY");
		}
	}

}
