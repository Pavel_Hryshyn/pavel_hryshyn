/**
 * @(#)EntertainmentAppliance.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.entertainment;

import by.epam.training.lab.first.oop.entities.Appliance;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.IGamingBehavior;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.IPlayingDiscBehavior;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.ISoftwareBehavior;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.IWatchingMovieBehavior;

/**
 * This class extends Appliance and is super class for all entertainment appliances in apartment.
 * @author Pavel Hryshyn
 * @param IGamingBehavior gamingBehavior, IPlayingDiscBehavior playingDiscBehavior,
 * @param ISoftwareBehavior softwareBehavior, IWatchingMovieBehavior watchingMovieBehavior
 */
public class EntertainmentAppliance extends Appliance {
	private IGamingBehavior gamingBehavior;
	private IPlayingDiscBehavior playingDiscBehavior;
	private ISoftwareBehavior softwareBehavior;
	private IWatchingMovieBehavior watchingMovieBehavior;
	
	public EntertainmentAppliance() {
		super();
	}
	
	
	public void performPlayGame(){
		gamingBehavior.playGame();
	}
	
	public void performPlayDisc(){
		playingDiscBehavior.playDisc();
	}
	
	public void performType(){
		softwareBehavior.type();
	}
	
	public void performWatchMovie(){
		watchingMovieBehavior.watchMovie();
	}


	public IGamingBehavior getGamingBehavior() {
		return gamingBehavior;
	}


	public void setGamingBehavior(IGamingBehavior gamingBehavior) {
		this.gamingBehavior = gamingBehavior;
	}


	public IPlayingDiscBehavior getPlayingDiscBehavior() {
		return playingDiscBehavior;
	}


	public void setPlayingDiscBehavior(IPlayingDiscBehavior playingDiscBehavior) {
		this.playingDiscBehavior = playingDiscBehavior;
	}


	public ISoftwareBehavior getSoftwareBehavior() {
		return softwareBehavior;
	}


	public void setSoftwareBehavior(ISoftwareBehavior softwareBehavior) {
		this.softwareBehavior = softwareBehavior;
	}


	public IWatchingMovieBehavior getWatchingMovieBehavior() {
		return watchingMovieBehavior;
	}


	public void setWatchingMovieBehavior(
			IWatchingMovieBehavior watchingMovieBehavior) {
		this.watchingMovieBehavior = watchingMovieBehavior;
	}


	@Override
	public String toString() {
		return super.toString() + ", " + gamingBehavior
				+ ", " + playingDiscBehavior
				+ ", " + softwareBehavior
				+ ", " + watchingMovieBehavior + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((gamingBehavior == null) ? 0 : gamingBehavior.hashCode());
		result = prime
				* result
				+ ((playingDiscBehavior == null) ? 0 : playingDiscBehavior
						.hashCode());
		result = prime
				* result
				+ ((softwareBehavior == null) ? 0 : softwareBehavior.hashCode());
		result = prime
				* result
				+ ((watchingMovieBehavior == null) ? 0 : watchingMovieBehavior
						.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntertainmentAppliance other = (EntertainmentAppliance) obj;
		if (gamingBehavior == null) {
			if (other.gamingBehavior != null)
				return false;
		} else if (!gamingBehavior.equals(other.gamingBehavior))
			return false;
		if (playingDiscBehavior == null) {
			if (other.playingDiscBehavior != null)
				return false;
		} else if (!playingDiscBehavior.equals(other.playingDiscBehavior))
			return false;
		if (softwareBehavior == null) {
			if (other.softwareBehavior != null)
				return false;
		} else if (!softwareBehavior.equals(other.softwareBehavior))
			return false;
		if (watchingMovieBehavior == null) {
			if (other.watchingMovieBehavior != null)
				return false;
		} else if (!watchingMovieBehavior.equals(other.watchingMovieBehavior))
			return false;
		return true;
	}
	
	
	
	

	

}
