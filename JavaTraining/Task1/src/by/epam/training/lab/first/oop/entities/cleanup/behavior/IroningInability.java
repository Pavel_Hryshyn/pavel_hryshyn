/**
 * @(#)IroningInability.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.cleanup.behavior;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IIroningBehavior;

/**
 * This class implements IIroningBehavior.class and realized for 
 * appliance that can't iron
 * @author Pavel Hryshyn
 * @param String flag
 */
public class IroningInability implements IIroningBehavior {
	private String flag = "";
	
	@Override
	public void ironClothes() {
		// TODO Auto-generated method stub

	}
	
	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}
	

}
