/**
 * @(#)CleaningInability.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.cleanup.behavior;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.ICleaningBehavior;

/**
 * This class implements ICleaningBehavior.class and realized for 
 * appliance that can't clean in apartment
 * @author Pavel Hryshyn
 * @param String flag
 */
public class CleaningInability implements ICleaningBehavior {
	private String flag = "";
	
	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}
	
	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}
	
}
