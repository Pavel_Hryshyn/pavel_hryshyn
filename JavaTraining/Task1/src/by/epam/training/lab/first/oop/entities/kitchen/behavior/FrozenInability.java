/**
 * @(#)FrozenInability.java   1.3 2015/03/30
 */
package by.epam.training.lab.first.oop.entities.kitchen.behavior;

import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IFrozenBehavior;

/**
 * This class implements IFrozenBehavior.java and realized for 
 * appliance that can't freeze
 * @author Pavel Hryshyn
 * @param String flag
 */
public class FrozenInability implements IFrozenBehavior {
	String flag = "is not able to freeze";
	
	@Override
	public void freeze() {
		// TODO Auto-generated method stub
		
	}
	
	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FrozenInability other = (FrozenInability) obj;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		return true;
	}

	
}
