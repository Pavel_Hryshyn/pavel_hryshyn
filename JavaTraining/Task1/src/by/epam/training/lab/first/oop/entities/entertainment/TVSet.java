/**
 * @(#)TVSet.java   1.3 2015/04/01
 */

package by.epam.training.lab.first.oop.entities.entertainment;

import by.epam.training.lab.first.oop.entities.entertainment.behavior.GameInability;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.PlayingDiscInability;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.SupportingSoftwareInability;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.WatchingMovie;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.IGamingBehavior;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.IPlayingDiscBehavior;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.ISoftwareBehavior;
import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.IWatchingMovieBehavior;

/**
 * This class extends EntertainmentAppliance.java.
 * @author Pavel Hryshyn
 * @param IGamingBehavior gamingBehavior, IPlayingDiscBehavior playingDiscBehavior,
 * @param ISoftwareBehavior softwareBehavior, IWatchingMovieBehavior watchingMovieBehavior
 */
public class TVSet extends EntertainmentAppliance {
	private IGamingBehavior gamingBehavior = new GameInability();
	private IPlayingDiscBehavior playingDiscBehavior = new PlayingDiscInability();
	private ISoftwareBehavior softwareBehavior = new SupportingSoftwareInability();
	private IWatchingMovieBehavior watchingMovieBehavior = new WatchingMovie();
	
	
	public TVSet() {
		super();
		super.setGamingBehavior(gamingBehavior);
		super.setPlayingDiscBehavior(playingDiscBehavior);
		super.setSoftwareBehavior(softwareBehavior);
		super.setWatchingMovieBehavior(watchingMovieBehavior);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((gamingBehavior == null) ? 0 : gamingBehavior.hashCode());
		result = prime
				* result
				+ ((playingDiscBehavior == null) ? 0 : playingDiscBehavior
						.hashCode());
		result = prime
				* result
				+ ((softwareBehavior == null) ? 0 : softwareBehavior.hashCode());
		result = prime
				* result
				+ ((watchingMovieBehavior == null) ? 0 : watchingMovieBehavior
						.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TVSet other = (TVSet) obj;
		if (gamingBehavior == null) {
			if (other.gamingBehavior != null)
				return false;
		} else if (!gamingBehavior.equals(other.gamingBehavior))
			return false;
		if (playingDiscBehavior == null) {
			if (other.playingDiscBehavior != null)
				return false;
		} else if (!playingDiscBehavior.equals(other.playingDiscBehavior))
			return false;
		if (softwareBehavior == null) {
			if (other.softwareBehavior != null)
				return false;
		} else if (!softwareBehavior.equals(other.softwareBehavior))
			return false;
		if (watchingMovieBehavior == null) {
			if (other.watchingMovieBehavior != null)
				return false;
		} else if (!watchingMovieBehavior.equals(other.watchingMovieBehavior))
			return false;
		return true;
	}
	
	
	

}
