/**
 * @(#)DryingInability.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.cleanup.behavior;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IDryingBehavior;

/**
 * This class implements IDryingBehavior.class and realized for 
 * appliance that can't dry
 * @author Pavel Hryshyn
 * @param String flag
 */
public class DryingInability implements IDryingBehavior {
	private String flag = "";
	
	
	@Override
	public void dry() {
		// TODO Auto-generated method stub

	}

	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}
}
