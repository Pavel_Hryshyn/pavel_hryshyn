/**
 * @(#)Drying.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.cleanup.behavior;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IDryingBehavior;

/**
 * This class implements IDryingBehavior.class and realized for 
 * appliance that can dry
 * @author Pavel Hryshyn
 * @param String flag
 */
public class Drying implements IDryingBehavior {
	private String flag = "is able to dry";
	
	
	@Override
	public void dry() {
		System.out.println("This appliance " + flag);

	}

	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}
	
	
	

}
