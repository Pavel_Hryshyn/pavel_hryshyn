/**
 * @(#)ApplianceEnum.java   1.3 2015/03/31
 */

package by.epam.training.lab.first.oop.entities;

/**
 * This enum stores all appliances that can be create by ApplianceFactory.
 * @author Pavel Hryshyn
 */
public enum ApplianceEnum {
	REFRIGERATOR, MICROWAVE_OVEN, ELECTRIC_KETTLE, DISHWASHER, TOASTER,
	VACUUM_CLEANER, IRON, WASHING_MACHINE, HAIR_DRYER,
	TV_SET, PC, GAME_CONSOLE, DVD_PLAYER, LAPTOP
}
