/**
 * @(#)WashingInability.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.cleanup.behavior;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IWashingBehavior;

/**
 * This class implements IWashingBehavior.class and realized for 
 * appliance that can't wash clothes
 * @author Pavel Hryshyn
 * @param String flag
 */
public class WashingInability implements IWashingBehavior {
	private String flag = "";
	
	@Override
	public void washClothes() {
		// TODO Auto-generated method stub

	}
	
	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}

}
