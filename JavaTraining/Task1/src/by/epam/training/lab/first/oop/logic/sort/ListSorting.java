/**
 * @(#)ListSorting.java   1.3 2015/04/01
 */

package by.epam.training.lab.first.oop.logic.sort;

import java.util.Collections;
import java.util.List;

import by.epam.training.lab.first.oop.entities.Appliance;

/**
 * This class sorting list of appliances in dependence on power
 * @author Pavel Hryshyn
 */
public class ListSorting {
	
	public static List<Appliance> sortListByPower(List<Appliance> appliances){
		Collections.sort(appliances);
		return appliances;
	}
}
