/**
 * @(#)WashingMashine.java   1.0 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.cleanup;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.CleaningInability;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.DryingInability;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.IroningInability;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.Washing;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.ICleaningBehavior;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IDryingBehavior;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IIroningBehavior;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IWashingBehavior;


/**
 * This class extends is CleaningAppliance.class.
 * @author Pavel Hryshyn
 * @param ICleaningBehavior cleaningBehavior, IDryingBehavior dryingBehavior,
 * @param IIroningBehavior ironingBehavior, IWashingBehavior washingBehavior
 */
public class WashingMashine extends CleaningAppliance{
	private ICleaningBehavior cleaningBehavior = new CleaningInability();
	private IDryingBehavior dryingBehavior = new DryingInability();
	private IIroningBehavior ironingBehavior = new IroningInability();
	private IWashingBehavior washingBehavior = new Washing();
	
	public WashingMashine() {
		super();
		super.setCleaningBehavior(cleaningBehavior);
		super.setDryingBehavior(dryingBehavior);
		super.setIroningBehavior(ironingBehavior);
		super.setWashingBehavior(washingBehavior);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((cleaningBehavior == null) ? 0 : cleaningBehavior.hashCode());
		result = prime * result
				+ ((dryingBehavior == null) ? 0 : dryingBehavior.hashCode());
		result = prime * result
				+ ((ironingBehavior == null) ? 0 : ironingBehavior.hashCode());
		result = prime * result
				+ ((washingBehavior == null) ? 0 : washingBehavior.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WashingMashine other = (WashingMashine) obj;
		if (cleaningBehavior == null) {
			if (other.cleaningBehavior != null)
				return false;
		} else if (!cleaningBehavior.equals(other.cleaningBehavior))
			return false;
		if (dryingBehavior == null) {
			if (other.dryingBehavior != null)
				return false;
		} else if (!dryingBehavior.equals(other.dryingBehavior))
			return false;
		if (ironingBehavior == null) {
			if (other.ironingBehavior != null)
				return false;
		} else if (!ironingBehavior.equals(other.ironingBehavior))
			return false;
		if (washingBehavior == null) {
			if (other.washingBehavior != null)
				return false;
		} else if (!washingBehavior.equals(other.washingBehavior))
			return false;
		return true;
	}
	
	

}
