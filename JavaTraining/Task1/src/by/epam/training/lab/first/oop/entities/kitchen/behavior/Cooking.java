/**
 * @(#)Cooking.java   1.3 2015/03/30
 */
package by.epam.training.lab.first.oop.entities.kitchen.behavior;

import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.ICookingBehavior;

/**
 * This class implements ICookingBehavior.java and realized for 
 * appliance that can prepare meals
 * @author Pavel Hryshyn
 * @param String flag
 */
public class Cooking implements ICookingBehavior {
	private String flag = "is able to cook";
	
	@Override
	public void prepareMeals() {
		System.out.println("This appliance " + flag);

	}
	
	
	public String getFlag() {
		return flag;
	}


	@Override
	public String toString() {
		return flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cooking other = (Cooking) obj;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		return true;
	}
	
	
	

}
