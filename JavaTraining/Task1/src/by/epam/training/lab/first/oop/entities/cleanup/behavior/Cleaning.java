/**
 * @(#)Cleaning.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.cleanup.behavior;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.ICleaningBehavior;

/**
 * This class implements ICleaningBehavior.class and realize cleaning in apartment
 * @author Pavel Hryshyn
 * @param String flag
 */
public class Cleaning implements ICleaningBehavior{
	private String flag = "is able to clean";

		
	@Override
	public void clean() {
		System.out.println("This appliance " + flag);
		
	}
	
	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}
	
}
