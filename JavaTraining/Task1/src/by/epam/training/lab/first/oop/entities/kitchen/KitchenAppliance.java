/**
 * @(#)KitchenAppliance.java   1.2 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.kitchen;

import by.epam.training.lab.first.oop.entities.Appliance;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.ICookingBehavior;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IFrozenBehavior;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IHeatingBehavior;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IWashingBehavior;

/**
 * This class extends Appliance.java and is super class for all kitchen appliances.
 * @author Pavel Hryshyn
 * @param ICookingBehavior cookingBehavior, IFrozenBehavior frozenBehavior,
 * @param IHeatingBehavior heatingBehavior, IWashingBehavior washingBehavior
 */
public class KitchenAppliance extends Appliance {
	private ICookingBehavior cookingBehavior;
	private IFrozenBehavior frozenBehavior;
	private IHeatingBehavior heatingBehavior;
	private IWashingBehavior washingBehavior;
		
	public KitchenAppliance() {
		super();
	}

		
	public void performCook(){
		cookingBehavior.prepareMeals();
	}
	
	public void performFreeze(){
		frozenBehavior.freeze();
	}
	
	public void performHeat(){
		heatingBehavior.heat();
	}
	
	public void performWash(){
		washingBehavior.washDishes();
	}

	public ICookingBehavior getCookingBehavior() {
		return cookingBehavior;
	}

	public void setCookingBehavior(ICookingBehavior cookingBehavior) {
		this.cookingBehavior = cookingBehavior;
	}

	public IFrozenBehavior getFrozenBehavior() {
		return frozenBehavior;
	}

	public void setFrozenBehavior(IFrozenBehavior frozenBehavior) {
		this.frozenBehavior = frozenBehavior;
	}

	public IHeatingBehavior getHeatingBehavior() {
		return heatingBehavior;
	}

	public void setHeatingBehavior(IHeatingBehavior heatingBehavior) {
		this.heatingBehavior = heatingBehavior;
	}

	public IWashingBehavior getWashingBehavior() {
		return washingBehavior;
	}

	public void setWashingBehavior(IWashingBehavior washingBehavior) {
		this.washingBehavior = washingBehavior;
	}
	
	

	@Override
	public String toString() {
		return super.toString() + ", " + cookingBehavior
				+ ",  " + frozenBehavior + ",  "
				+ heatingBehavior + ",  " + washingBehavior + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cookingBehavior == null) ? 0 : cookingBehavior.hashCode());
		result = prime * result
				+ ((frozenBehavior == null) ? 0 : frozenBehavior.hashCode());
		result = prime * result
				+ ((heatingBehavior == null) ? 0 : heatingBehavior.hashCode());
		result = prime * result
				+ ((washingBehavior == null) ? 0 : washingBehavior.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		KitchenAppliance other = (KitchenAppliance) obj;
		if (cookingBehavior == null) {
			if (other.cookingBehavior != null)
				return false;
		} else if (!cookingBehavior.equals(other.cookingBehavior))
			return false;
		if (frozenBehavior == null) {
			if (other.frozenBehavior != null)
				return false;
		} else if (!frozenBehavior.equals(other.frozenBehavior))
			return false;
		if (heatingBehavior == null) {
			if (other.heatingBehavior != null)
				return false;
		} else if (!heatingBehavior.equals(other.heatingBehavior))
			return false;
		if (washingBehavior == null) {
			if (other.washingBehavior != null)
				return false;
		} else if (!washingBehavior.equals(other.washingBehavior))
			return false;
		return true;
	}
	
	
	
}
