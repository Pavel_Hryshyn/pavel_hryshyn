/**
 * @(#)Apartment.java   1.0 2015/03/30
 */

package by.epam.training.lab.first.oop.entities;


import java.util.List;

/**
 * This class stores list with all electric appliances that there are in apartment.
 * @author Pavel Hryshyn
 * @param List<Appliance> appliances
 */
public class Apartment {
	private List<Appliance> appliances;
	
		
	public Apartment() {
		super();
	}
	
	public Apartment(List<Appliance> appliances) {
		super();
		this.appliances = appliances;
	}

		
	public List<Appliance> getAppliances() {
		return appliances;
	}
	
	public void setAppliances(List<Appliance> appliances) {
		this.appliances = appliances;
	}

	@Override
	public String toString() {
		return "Apartment [appliances: " + appliances + "]";
	}
	
	

}
