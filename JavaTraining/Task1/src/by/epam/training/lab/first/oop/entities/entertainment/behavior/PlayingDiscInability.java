/**
 * @(#)Cleaning.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.entertainment.behavior;

import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.IPlayingDiscBehavior;

/**
 * This class implements IPlayingDiscBehavior.java and realized for 
 * appliance that can't play disc
 * @author Pavel Hryshyn
 * @param String flag
 */
public class PlayingDiscInability implements IPlayingDiscBehavior {
	private String flag = "";

	@Override
	public void playDisc() {
		

	}
	
	public String getFlag() {
		return flag;
	}


	@Override
	public String toString() {
		return flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayingDiscInability other = (PlayingDiscInability) obj;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		return true;
	}
	
	


}
