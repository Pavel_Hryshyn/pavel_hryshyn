/**
 * @(#)ApartmentBuilder.java   1.3 2015/04/01
 */

package by.epam.training.lab.first.oop.logic.parser;

import by.epam.training.lab.first.oop.entities.Apartment;

/**
 * This class is superclass for all classes that creating Apartment from file
 * @author Pavel Hryshyn
 * @param Apartment apartment
 */
public abstract class ApartmentBuilder {
	private Apartment apartment = new Apartment();

	public Apartment getApartment() {
		return apartment;
	}
	
	/*
	 * This method build Apartment entity from file with with fileName
	 */
	public abstract Apartment buildApartment(String fileName);
	

}
