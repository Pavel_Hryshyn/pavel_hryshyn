/**
 * @(#)ICleaningBehavior.java   1.3 2015/03/30
 */
package by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces;

public interface ICleaningBehavior {
	void clean();
}
