/**
 * @(#)Cleaning.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.entertainment.behavior;

import by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces.ISoftwareBehavior;

/**
 * This class implements ISoftwareBehavior.java and realized for 
 * appliance that can type text
 * @author Pavel Hryshyn
 * @param String flag
 */
public class SupportingSoftware implements ISoftwareBehavior {
	private String flag = "is able to type and support many applications";
	
	@Override
	public void type() {
		System.out.println("This appliance " + flag);

	}
	
	public String getFlag() {
		return flag;
	}


	@Override
	public String toString() {
		return flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SupportingSoftware other = (SupportingSoftware) obj;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		return true;
	}
	
	

}
