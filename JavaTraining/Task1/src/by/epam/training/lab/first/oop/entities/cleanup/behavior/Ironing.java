/**
 * @(#)Ironing.java   1.3 2015/03/30
 */


package by.epam.training.lab.first.oop.entities.cleanup.behavior;

import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IIroningBehavior;

/**
 * This class implements IIroningBehavior.class and realized for 
 * appliance that can iron
 * @author Pavel Hryshyn
 * @param String flag
 */
public class Ironing implements IIroningBehavior {
	private String flag = "is able to iron";
	
	@Override
	public void ironClothes() {
		System.out.println("This appliance " + flag);

	}
	
	public String getFlag() {
		return flag;
	}

	@Override
	public String toString() {
		return flag;
	}
	

}
