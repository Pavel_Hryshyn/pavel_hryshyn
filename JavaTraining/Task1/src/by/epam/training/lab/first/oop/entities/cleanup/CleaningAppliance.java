/**
 * @(#)CleaningAppliance.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.cleanup;

import by.epam.training.lab.first.oop.entities.Appliance;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.ICleaningBehavior;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IDryingBehavior;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IIroningBehavior;
import by.epam.training.lab.first.oop.entities.cleanup.behavior.interfaces.IWashingBehavior;

/**
 * This class extends Appliance and is super class for appliances for cleaning apartment.
 * @author Pavel Hryshyn
 * @param ICleaningBehavior cleaningBehavior, IDryingBehavior dryingBehavior,
 * @param IIroningBehavior ironingBehavior, IWashingBehavior washingBehavior
 */
public class CleaningAppliance extends Appliance {
	private ICleaningBehavior cleaningBehavior;
	private IDryingBehavior dryingBehavior;
	private IIroningBehavior ironingBehavior;
	private IWashingBehavior washingBehavior;
	

	public CleaningAppliance() {
		super();
	}
	
	
	public void performClean(){
		cleaningBehavior.clean();;
	}
	
	public void performDry(){
		dryingBehavior.dry();
	}
	
	public void performIron(){
		ironingBehavior.ironClothes();
	}
	
	public void performWash(){
		washingBehavior.washClothes();
	}


	public ICleaningBehavior getCleaningBehavior() {
		return cleaningBehavior;
	}


	public void setCleaningBehavior(ICleaningBehavior cleaningBehavior) {
		this.cleaningBehavior = cleaningBehavior;
	}


	public IDryingBehavior getDryingBehavior() {
		return dryingBehavior;
	}


	public void setDryingBehavior(IDryingBehavior dryingBehavior) {
		this.dryingBehavior = dryingBehavior;
	}


	public IIroningBehavior getIroningBehavior() {
		return ironingBehavior;
	}


	public void setIroningBehavior(IIroningBehavior ironingBehavior) {
		this.ironingBehavior = ironingBehavior;
	}


	public IWashingBehavior getWashingBehavior() {
		return washingBehavior;
	}


	public void setWashingBehavior(IWashingBehavior washingBehavior) {
		this.washingBehavior = washingBehavior;
	}


	@Override
	public String toString() {
		return super.toString() + ", " + cleaningBehavior
				+ ", " + dryingBehavior + ", "
				+ ironingBehavior + ", " + washingBehavior + "]";
	}
	
	

	

}
