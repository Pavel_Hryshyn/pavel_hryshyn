package by.epam.training.lab.first.oop.entities.kitchen;



public class KitchenApplianceFactory {
	private static KitchenApplianceEnum kAppliance = KitchenApplianceEnum.REFRIGERATOR;
	public static KitchenAppliance getKitchenApplianceFromFactory(String id){
		kAppliance = KitchenApplianceEnum.valueOf(id.toUpperCase());
	
		//�������� 0 � ������
		switch (kAppliance) {
		case REFRIGERATOR:return new Refrigerator(100);
		case MICROWAVE_OVEN: return new MicrowaveOven(700);
		
		default:throw new EnumConstantNotPresentException(KitchenApplianceEnum.class, kAppliance.name());
		}
	}

}
