/**
 * @(#)Frozen.java   1.3 2015/03/30
 */
package by.epam.training.lab.first.oop.entities.kitchen.behavior;

import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IFrozenBehavior;

/**
 * This class implements IFrozenBehavior.java and realized for 
 * appliance that can freeze
 * @author Pavel Hryshyn
 * @param String flag
 */
public class Frozen implements IFrozenBehavior {
	String flag = "is able to freeze";
	
	@Override
	public void freeze() {
		System.out.println("This appliance " + flag);

	}
	
	public String getFlag() {
		return flag;
	}
	
	@Override
	public String toString() {
		return flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frozen other = (Frozen) obj;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		return true;
	}
	
	

}
