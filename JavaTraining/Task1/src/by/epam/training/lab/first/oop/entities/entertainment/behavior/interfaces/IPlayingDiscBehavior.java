/**
 * @(#)IPlayingDiscBehavior.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities.entertainment.behavior.interfaces;

public interface IPlayingDiscBehavior {
	void playDisc();
}
