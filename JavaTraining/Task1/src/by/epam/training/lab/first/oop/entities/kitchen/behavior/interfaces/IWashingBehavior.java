/**
 * @(#)IWashingBehavior.java   1.3 2015/03/30
 */
package by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces;

public interface IWashingBehavior {
	void washDishes();
}
