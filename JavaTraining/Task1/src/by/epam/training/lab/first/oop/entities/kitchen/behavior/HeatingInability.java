/**
 * @(#)HeatingInability.java   1.3 2015/03/30
 */
package by.epam.training.lab.first.oop.entities.kitchen.behavior;

import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IHeatingBehavior;

/**
 * This class implements IHeatingBehavior.java and realized for 
 * appliance that can't heat something
 * @author Pavel Hryshyn
 * @param String flag
 */
public class HeatingInability implements IHeatingBehavior {
	String flag = "is not able to heat";
	
	@Override
	public void heat() {
		System.out.println(" ");

	}
	
	public String getFlag() {
		return flag;
	}
	
	@Override
	public String toString() {
		return flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HeatingInability other = (HeatingInability) obj;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		return true;
	}
	
	

}
