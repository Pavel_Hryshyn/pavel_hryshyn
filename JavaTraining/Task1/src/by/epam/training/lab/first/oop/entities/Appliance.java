/**
 * @(#)Appliance.java   1.3 2015/03/30
 */

package by.epam.training.lab.first.oop.entities;

/**
 * This class is super class for all appliances.
 * @author Pavel Hryshyn
 * @param id, manufacturer,model, connectedPlug, power
 */
public class Appliance implements Comparable<Appliance>{
	private String id;
	private String manufacturer;
	private String model;
	private boolean connectedPlug = false;
	private Double power;
		
	public Appliance() {
		super();
	}
		
	public Appliance(String id, double power) {
		super();
		this.id = id;
		this.power = power;
	}

	public Appliance(String id, String manufacturer, String model,
			boolean connectedPlug, double power) {
		super();
		this.id = id;
		this.manufacturer = manufacturer;
		this.model = model;
		this.connectedPlug = connectedPlug;
		this.power = power;
	}

	
	/*
	 * This method sets ConnectedPlug to "true"
	 */
	public void turnOn(){
		setConnectedPlug(true);
		System.out.println("The appliance is turned on. Its power is " + power + " Watt");
	}
		
	/*
	 * This method sets ConnectedPlug to "false"
	 */
	public void turnOff(){
		setConnectedPlug(false);
		System.out.println("The appliance is turned off.");
	}
	
	
	/*
	 * getters and setters
	 */	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public boolean isConnectedPlug() {
		return connectedPlug;
	}

	public void setConnectedPlug(boolean connectedPlug) {
		this.connectedPlug = connectedPlug;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	
	@Override
	public String toString() {
		return  id + "=[manufacturer=" + manufacturer
				+ ", model=" + model + ", power=" + power
				+  ", connectedPlug=" + connectedPlug;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((manufacturer == null) ? 0 : manufacturer.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		long temp;
		temp = Double.doubleToLongBits(power);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appliance other = (Appliance) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (Double.doubleToLongBits(power) != Double
				.doubleToLongBits(other.power))
			return false;
		return true;
	}

	
	@Override
	public int compareTo(Appliance o) {
		int result = power.compareTo(o.power);
		return result;
	}
	
	
	
}
