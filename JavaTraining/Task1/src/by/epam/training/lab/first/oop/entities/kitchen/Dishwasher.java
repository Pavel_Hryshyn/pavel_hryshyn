/**
 * @(#)Dishwasher.java   1.2 2015/03/30
 */


package by.epam.training.lab.first.oop.entities.kitchen;

import by.epam.training.lab.first.oop.entities.kitchen.behavior.CookingInability;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.FrozenInability;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.HeatingInability;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.Washing;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.ICookingBehavior;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IFrozenBehavior;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IHeatingBehavior;
import by.epam.training.lab.first.oop.entities.kitchen.behavior.interfaces.IWashingBehavior;

/**
 * This class extends KitchenAppliance.java.
 * @author Pavel Hryshyn
 * @param ICookingBehavior cookingBehavior, IFrozenBehavior frozenBehavior,
 * @param IHeatingBehavior heatingBehavior, IWashingBehavior washingBehavior
 */
public class Dishwasher extends KitchenAppliance {
	ICookingBehavior cookingBehavior = new CookingInability();
	IFrozenBehavior frozenBehavior = new FrozenInability();
	IHeatingBehavior heatingBehavior = new HeatingInability();
	IWashingBehavior washingBehavior = new Washing();
	
	public Dishwasher() {
		super();
		super.setCookingBehavior(cookingBehavior);
		super.setFrozenBehavior(frozenBehavior);
		super.setHeatingBehavior(heatingBehavior);
		super.setWashingBehavior(washingBehavior);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cookingBehavior == null) ? 0 : cookingBehavior.hashCode());
		result = prime * result
				+ ((frozenBehavior == null) ? 0 : frozenBehavior.hashCode());
		result = prime * result
				+ ((heatingBehavior == null) ? 0 : heatingBehavior.hashCode());
		result = prime * result
				+ ((washingBehavior == null) ? 0 : washingBehavior.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dishwasher other = (Dishwasher) obj;
		if (cookingBehavior == null) {
			if (other.cookingBehavior != null)
				return false;
		} else if (!cookingBehavior.equals(other.cookingBehavior))
			return false;
		if (frozenBehavior == null) {
			if (other.frozenBehavior != null)
				return false;
		} else if (!frozenBehavior.equals(other.frozenBehavior))
			return false;
		if (heatingBehavior == null) {
			if (other.heatingBehavior != null)
				return false;
		} else if (!heatingBehavior.equals(other.heatingBehavior))
			return false;
		if (washingBehavior == null) {
			if (other.washingBehavior != null)
				return false;
		} else if (!washingBehavior.equals(other.washingBehavior))
			return false;
		return true;
	}
	
	

}
