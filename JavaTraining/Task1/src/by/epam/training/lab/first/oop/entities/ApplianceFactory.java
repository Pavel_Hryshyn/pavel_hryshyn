/**
 * @(#)ApplianceFactory.java   1.3 2015/03/31
 */

package by.epam.training.lab.first.oop.entities;

import by.epam.training.lab.first.oop.entities.cleanup.HairDryer;
import by.epam.training.lab.first.oop.entities.cleanup.Iron;
import by.epam.training.lab.first.oop.entities.cleanup.VacuumCleaner;
import by.epam.training.lab.first.oop.entities.cleanup.WashingMashine;
import by.epam.training.lab.first.oop.entities.entertainment.DVDPlayer;
import by.epam.training.lab.first.oop.entities.entertainment.GameConsole;
import by.epam.training.lab.first.oop.entities.entertainment.Laptop;
import by.epam.training.lab.first.oop.entities.entertainment.PC;
import by.epam.training.lab.first.oop.entities.entertainment.TVSet;
import by.epam.training.lab.first.oop.entities.kitchen.Dishwasher;
import by.epam.training.lab.first.oop.entities.kitchen.ElectricKettle;
import by.epam.training.lab.first.oop.entities.kitchen.MicrowaveOven;
import by.epam.training.lab.first.oop.entities.kitchen.Refrigerator;
import by.epam.training.lab.first.oop.entities.kitchen.Toaster;


/**
 * This class is factory for creating appliances.
 * @author Pavel Hryshyn
 */
public class ApplianceFactory {
	private static ApplianceEnum appEnum = ApplianceEnum.REFRIGERATOR;
	
	/*
	 * This static method creates and @return appliance from ApplianceEnum 
	 * if id matches with transferred parameter
	 * @param String id
	 * @throws EnumConstantNotPresentException if there isn't appliance in ApplianceEnum with such id
	 */
	public static Appliance getApplianceFromFactory(String id){
		appEnum = ApplianceEnum.valueOf(id.toUpperCase());
			
		switch (appEnum) {
		case REFRIGERATOR:return new Refrigerator();
		case MICROWAVE_OVEN: return new MicrowaveOven();
		case ELECTRIC_KETTLE: return new ElectricKettle();
 		case DISHWASHER: return new Dishwasher();
 		case TOASTER: return new Toaster();
 		case VACUUM_CLEANER: return new VacuumCleaner();
 		case IRON: return new Iron();
 		case WASHING_MACHINE: return new WashingMashine();
 		case HAIR_DRYER: return new HairDryer();
 		case TV_SET: return new TVSet();
 		case PC: return new PC();
 		case GAME_CONSOLE: return new GameConsole();
 		case DVD_PLAYER: return new DVDPlayer();
 		case LAPTOP: return new Laptop();
		
		default:throw new EnumConstantNotPresentException(ApplianceEnum.class, appEnum.name());
		}
	}
	
}
