/**
 * @(#)ApartmentXMLBuilder.java   1.3 2015/04/01
 */

package by.epam.training.lab.first.oop.logic.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.training.lab.first.oop.entities.Apartment;
import by.epam.training.lab.first.oop.entities.Appliance;
import by.epam.training.lab.first.oop.entities.ApplianceFactory;

/**
 * This class extends ApartmentBuilder.java and create Apartment from XML file
 * @author Pavel Hryshyn
 * @param Apartment apartment
 */
public class ApartmentXMLBuilder extends ApartmentBuilder {
	final static Logger logger = Logger.getLogger(ApartmentXMLBuilder.class);
	private List<Appliance> xmlAppliances;
	private DocumentBuilder docBuilder;
	
	public ApartmentXMLBuilder() {
		super();
		this.xmlAppliances = new ArrayList<Appliance>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			System.err.println("Parser configuration error " + e);
		}
	}

	/*
	 * This method override method AppartmentBuilder.java and build 
	 * Apartment entity from XML file with fileName
	 */
	@Override
	public Apartment buildApartment(String fileName) {
		Apartment xmlApartment = null;
		Document doc = null;
		
		try {
			doc = docBuilder.parse(fileName);
			Element root = doc.getDocumentElement();
			
			//get list of inner <apartment> elements
			NodeList appliacesXmlList = root.getElementsByTagName("appliance");
			for (int i=0; i<appliacesXmlList.getLength();i++){
				Element applianceXmlElem = (Element)appliacesXmlList.item(i);
				Appliance appliance = buildAppliance(applianceXmlElem);
				if (appliance !=null){
				xmlAppliances.add(appliance);
				}
			}
			
		} catch (IOException e) {
			System.err.println("File error or I/O error: " + e);
			logger.error("File error or I/O error: ", e);
		} catch (SAXException e) {
			System.err.println("Parsing failure: " + e);
			logger.error("Parsing failure: ", e);
		} catch (IllegalArgumentException e) {
			System.err.println("Incorrect appliance id: " + e);
			logger.error("Incorrect appliance id: ", e);
		} 
		xmlApartment = super.getApartment();
		xmlApartment.setAppliances(xmlAppliances);
		return xmlApartment;
	}

	
	public Appliance buildAppliance(Element applianceXmlElem) {
		String id = applianceXmlElem.getAttribute("id");
		Appliance appliance = ApplianceFactory.getApplianceFromFactory(id);
		appliance.setId(id);
		appliance.setModel(applianceXmlElem.getElementsByTagName("model").item(0).getTextContent());
		appliance.setManufacturer(applianceXmlElem.getElementsByTagName("manufacturer").item(0).getTextContent());
		
		Double pow = Double.parseDouble(applianceXmlElem.getElementsByTagName("power").item(0).getTextContent());
		appliance.setPower(pow);
		Boolean conPlug = Boolean.parseBoolean(applianceXmlElem.getElementsByTagName("connected_plug").item(0).getTextContent());
		appliance.setConnectedPlug(conPlug);
		
		return appliance;
	}

	public List<Appliance> getXmlAppliances() {
		return xmlAppliances;
	}
	
	

}
